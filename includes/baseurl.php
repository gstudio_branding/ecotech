<?php 
class BaseUrl{
	
	private $basepath = '';
	private $relpath = '';

	public function __construct($basepath='', $path=''){
		if(strpos($path, '\\')){ //if the path is on a windows machine
			$path = str_replace('\\', '/', $path);
		}

		@$this->$basepath = $basepath;

		if($basepath==''){
			echo 'Basepath not set';
		}
		else{
			if($basepath==$path){
				$this->relpath = './';
			}
			else{
				$bp_exp = explode('/', $basepath);
				$p_exp = explode('/', $path);

				if(end($bp_exp)==''){ unset($bp_exp[sizeof($bp_exp)-1]); } //if the last item is a slash remove it
				
				foreach($bp_exp as $key=>$folder){ //remove the like parts of the path
					if(isset($p_exp[$key]) && $folder == $p_exp[$key]){
						unset($bp_exp[$key]);
						unset($p_exp[$key]);
					}
				}
				
				//remap the indexes of the arrays
				$bp_exp = array_values($bp_exp);
				$p_exp = array_values($p_exp);

				if(sizeof($p_exp)==0){ $this->relpath = './'.$this->getBasePath($bp_exp); } //if the path doesnt have a size the directory is root
				else{ $this->relpath = $this->getUpPath($p_exp).$this->getBasePath($bp_exp); } //otherwise get the relative path
			}
		}
	}

	private function getBasePath($baseArray){
		$temp_rel = '';
		foreach($baseArray as $folder){
			$temp_rel .= $folder.'/';
		}

		return $temp_rel;
	}

	private function getUpPath($pathArray){
		$temp_rel = '';
		foreach($pathArray as $folder){
			$temp_rel .= '../';
		}

		return $temp_rel;
	}

	public function getRelPath(){
		return $this->relpath;
	}
}