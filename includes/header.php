<?php
include_once('cms/includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

$parentNavId = -1;
$cat_id = -1;

if(isset($_GET['cat_slug'])){
	$cat_id = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_slug="'.$_GET['cat_slug'].'"', 'page_category_sort ASC');
	if($dl->totalrows>0){ $cat_id = $cat_id[0]['page_category_id']; }
	else{ $cat_id = -1; }
}
else if(isset($_GET['cat_id'])){
	$cat_id = $_GET['cat_id'];
}
else{
	$cat_id = 325;
}

if($cat_id>=0){
	$isService = false;
	$mainHeading = $subHeading = '';

	$crumb = getBreadCrumb($dl, $cat_id);

	$mainHeading = $crumb[(sizeof($crumb)-1)]['title'];
	if(sizeof($crumb)>1){
		if(strtolower($crumb[(sizeof($crumb)-1)]['title'])=='services'){
			$isService = true;
		}
		$subHeading = $crumb[(sizeof($crumb)-2)]['title'];
	}

	if($isService){
		$parentNavId = $crumb[(sizeof($crumb)-2)]['slug'];
	}
	else{
		$parentNavId = $crumb[(sizeof($crumb)-1)]['slug'];
	}

	$parentNavId = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_slug="'.$parentNavId.'"', 'page_category_sort ASC');
	if($dl->totalrows){ $parentNavId = $parentNavId[0]['page_category_id']; }
	else{ $parentNavId = -1; }

}

$navHtml = '';
$navTop = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_category_id=0', 'page_category_sort ASC');
if($dl->totalrows>0){
	$navHtml .= '<ul id="nav">';
	foreach ($navTop as $nt) {
		$nav = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_category_id='.$nt['page_category_id'], 'page_category_sort ASC');
		if($dl->totalrows>0){
			$tempNavHtml = '';
			$firstNavLink = '#';
			foreach ($nav as $n) {
				$subItems = '';
				if($firstNavLink=='#'){ $firstNavLink = $basepath.'content/'.$n['page_category_slug']; }
				$subItems = getRecursiveCategories($dl, $n['page_category_id'], $basepath, $cat_id);

				$tempNavHtml .= '<li><a href="'.$basepath.'content/'.$n['page_category_slug'].'" '.( ($n['page_category_id']==$cat_id)?'class="active"':'' ).' >'.$n['page_category_heading'].'</a>'.( ($subItems!='')?'<a href="#" class="submenu-toggle"></a>':'' );
				$tempNavHtml .= $subItems;
				$tempNavHtml .= '</li>';
			}

			$navHtml .= '<li><a href="'.$firstNavLink.'">'.$nt['page_category_heading'].'</a>'.( ($tempNavHtml!='')?'<a href="#" class="submenu-toggle"></a>':'' );
			$navHtml .= '<ul>';
			$navHtml .= $tempNavHtml; //add inner contents 
			$navHtml .= '</ul>';
			$navHtml .= '</li>';
		}
		else{
			$navHtml .= '<li><a href="'.$basepath.'content/'.$nt['page_category_slug'].'">'.$nt['page_category_heading'].'</a></li>';
		}
	}
	// $navHtml .= '<li><img src="'.$basepath.'elements/icon-burger.png" /></li>';
	$navHtml .= '<li></li>';
	$navHtml .= '</ul>';
}
?>
<div id="screen-type" style="position:absolute; top:0; left:0; background-color:#ffffff; z-index:10000; padding:2px; display:none;">
  <div class="visible-xs screen-type" screen="xs">XS</div>
  <div class="visible-sm screen-type" screen="sm">SM</div>
  <div class="visible-md screen-type" screen="md">MD</div>
  <div class="visible-lg screen-type" screen="lg">LG</div>
</div>

<div id="overlay"></div>
<div id="menu-overlay">
	<?php echo $navHtml; ?>
</div>

<div class="burger-toggle"></div>

<div id="header">
	<a href="<?php echo $basepath; ?>" id="logo-ecotech"><img src="<?php echo $basepath; ?>elements/logo-ecotech.png" /></a>
	<?php echo $navHtml; ?>
</div>
