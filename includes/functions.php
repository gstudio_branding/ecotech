<?php
function getColData($dl, $data, $basepath){
    if( isset($data->{'type'}) ){
        switch ($data->{'type'}) {
            case 'text': 
                return str_replace('[basepath]', $basepath, stripslashes($data->{'data'}));
            break;

            case 'image': 
                $imageAlt = '';
                $imageData = $dl->select('cms_media_files', 'media_files_id="'.$data->{'data'}->{'id'}.'"');
                if($dl->totalrows>0){ $imageAlt = $imageData[0]['media_files_original_name']; }
                $img = '<div class="spacer10"></div><img src="'.$basepath.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$data->{'data'}->{'id'}.'&width='.$data->{'data'}->{'width'};
                if($data->{'data'}->{'aspect_r'}!=0){ $img.='&aspect_r='.$data->{'data'}->{'aspect_r'}; }
                 $img.='" alt="'.$imageAlt.'" class="img-responsive centered" /><div class="spacer10"></div>';
                 return $img;
            break;
            
            default: break;
        }
    }
}

function getBreadCrumb($dl, $cat_id){
	$cat = $dl->select('mod_page_category', 'page_category_id="'.$cat_id.'"');
	if($dl->totalrows>0){
		$cat = $cat[0];
		$dataArr = array();

		array_push($dataArr, array('slug'=>$cat['page_category_slug'], 'title'=>$cat['page_category_heading']));
		
		if($cat['page_category_category_id']!=0){
			$temp = getBreadCrumb($dl, $cat['page_category_category_id']);

			if(is_array($temp) && sizeof($temp)>0){
				foreach ($temp as $t) {
					array_push($dataArr, array('slug'=>$t['slug'], 'title'=>$t['title']));
				}
			}
		}
		return $dataArr;
		
	}
}

function getRecursiveCategories($dl, $cat_id, $basepath, $active_cat=''){
	// if($cat_id!=0){
	// 	//$cats = $dl->select('mod_page_category', 'page_category_category_id="'.$cat_id.'" AND page_category_archived="0" AND (page_category_heading NOT LIKE "page" AND page_category_heading NOT LIKE "in the news")', 'page_category_heading ASC');

	// 	$cats = $dl->select('mod_page_category', 'page_category_category_id="'.$cat_id.'" AND page_category_archived="0" AND (page_category_heading NOT LIKE "page" AND page_category_heading NOT LIKE "in the news")', 'page_category_sort ASC');
	// }
	// else{
	// 	$cats = $dl->select('mod_page_category', 'page_category_category_id="'.$cat_id.'" AND page_category_archived="0" AND (page_category_heading NOT LIKE "page" AND page_category_heading NOT LIKE "in the news")', 'page_category_sort ASC');
	// }

	$cats = $dl->select('mod_page_category', 'page_category_category_id="'.$cat_id.'" AND page_category_archived="0"', 'page_category_sort ASC');
	
	$data = '';

	if($dl->totalrows>0){
		// $data .= '<ul class="category-list">';
		$data .= '<ul class="mini-category-list">';
		foreach ($cats as $c){
			$temp = getRecursiveCategories($dl, $c['page_category_id'], $basepath);

			$data .= '<li>';

			$parentCat = '';
			if($cat_id!=0){
				$tempCat = $dl->select('mod_page_category', 'page_category_id="'.$cat_id.'" AND page_category_archived="0"');
				if($dl->totalrows>0){
					$parentCat = $tempCat[0]['page_category_slug'].'/';
				}
			}

			// if($temp==''){ $data .= '<a href="'.$basepath.$c['page_category_slug'].'">'; }
			$data .= '<a href="'.$basepath.'content/'.$c['page_category_slug'].'"'.( ($c['page_category_id']==$active_cat)?' class="active"':'' ).'>';
			$data .= $c['page_category_heading'];
			$data .= '</a>';
			if($temp!=''){ $data .= $temp; }
			//if($temp==''){ $data .= '</a>'; }
			

			$data .= '</li>';
		}
		$data .= '</ul>';
		return $data;
	}
	else{
	 	return '';
	}
}