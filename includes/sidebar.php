<div id="services-submenu-wrap">
	<ul id="services-submenu" class="<?php if($showSidebar){ echo 'active'; } ?>">
		<li><a href="#" class="closeToggle"><img src="<?php echo $basepath; ?>elements/icon-services.png"/><img src="<?php echo $basepath; ?>elements/curves/services.png" class="curved-text left" <?php if($showSidebar){ echo 'style="display:none;"'; } ?> /></a></li>
		<li><h2><strong>Services</strong></h2></li>
		<?php
		$serviceId = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_slug="services"', 'page_category_sort ASC');
		if($dl->totalrows>0){ 
			$serviceId = $serviceId[0]['page_category_id']; 

			$services = $dl->select('mod_page_category', 'page_category_archived=0 AND page_category_category_id="'.$serviceId.'"', 'page_category_sort ASC');
			foreach ($services as $s) {
				// echo '<li><a href="'.$basepath.'content/'.$s['page_category_slug'].'">'.$s['page_category_heading'].'</a></li>';
				echo '
					<li class="services-link">
						<a href="#">'.$s['page_category_heading'].'<img src="'.$basepath.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$s['media_files_id'].'" /></a>
						<div class="service-info">
							<img src="'.$basepath.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$s['media_files_id'].'" />
							<h2>'.$s['page_category_heading'].'</h2>
							'.$s['page_category_short_description'].'
							<a href="'.$basepath.'content/'.$s['page_category_slug'].'" class="readmore">find out more</a>
						</div>
					</li>';
			}
		}
		else{ $serviceId = 0; }
		?>
	</ul>
</div>

<div id="category-submenu" class="col-xs-12 col-sm-3 col-md-2">
	<?php echo getRecursiveCategories($dl, $parentNavId, $basepath, $cat_id); ?>
</div>

<div class="flyout-container">
	<div class="flyout-info" data-tab="contact">
		<div class="flyout-icon"><img src="<?php echo $basepath; ?>elements/icon-contact.png"/><img src="<?php echo $basepath; ?>elements/curves/contact.png" class="curved-text" /></div>
		<img src="<?php echo $basepath; ?>elements/flyout-number.png" />
	</div>
</div>

<div class="flyout-container">
	<div class="flyout-info" data-tab="partners">
		<div class="flyout-icon"><img src="<?php echo $basepath; ?>elements/icon-partners.png"/><img src="<?php echo $basepath; ?>elements/curves/our-partners.png" class="curved-text" /></div>
		<h2><strong>Partners</strong></h2>
		<div class="col-block">
			<!-- <h3>INFRASTRUCTURE</h3> -->
			<img src="<?php echo $basepath; ?>pieces/partners/SEACOM_OfficialPartnerlogo-300x138.png" />

		</div>
		<div class="col-block">
			<!-- <h3>SOFTWARE</h3> -->
			<img src="<?php echo $basepath; ?>pieces/partners/microsoft-small-business-specialist.png" />
			<img src="<?php echo $basepath; ?>pieces/partners/microsoftpartner.png" /><br>
			<img src="<?php echo $basepath; ?>pieces/partners/hp_logo_preferred_partner_w.png" />
			<img src="<?php echo $basepath; ?>pieces/partners/dell_partnerdirect_preferred_rgb3.png" />

		</div>
		<div class="col-block">
			<!-- <h3>CONNECTIVITY</h3> -->
			<img src="<?php echo $basepath; ?>pieces/partners/Neotel-Channel-partner-logo.png" />
			<img src="<?php echo $basepath; ?>pieces/partners/cisco_registered_Partner.png" />

		</div>
	</div>
</div>

<div class="flyout-container">
	<div class="flyout-info" data-tab="awards">
		<div class="flyout-icon"><img src="<?php echo $basepath; ?>elements/icon-awards.png"/><img src="<?php echo $basepath; ?>elements/curves/awards.png" class="curved-text" /></div>
		<h2><strong>Awards</strong></h2>
		<div class="col-block">
			<img src="<?php echo $basepath; ?>/pieces/awards/Gold_Partner_Insignia.png" />
			<img src="<?php echo $basepath; ?>/pieces/awards/MS.png" />
		</div>
	</div>
</div>