<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $basepath; ?>scripts/bootstrap.min.js"></script>

<script src="<?php echo $basepath; ?>scripts/main.js?v=1.5"></script>
<script src="<?php echo $basepath; ?>scripts/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php echo $basepath; ?>scripts/owl.carousel.min.js"></script>
<script src="<?php echo $basepath; ?>formHandler/jquery.validate.js"></script>