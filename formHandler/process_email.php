<?php	

session_start();

$error = false;
$data = array();
$email_text = '';

foreach($_POST as $key=>$value)
{
	$data[$key] = $value;
	
	if($key != 'submit' && $key != 'submit_x' && $key != 'submit_y' && $key != 'subject' && $key != 'from' && $key != 'to' && $key != 'success-url' && $key != 'from-page' && $key != 'required-fields')
	{	
		$mailData[$key] = $value;
		if(!is_array($value))
			$email_text .= ucwords(str_replace(array("_", "-")," ",$key)).": \n".$value." \n\n";
		else
		{
			$email_text .= ucwords(str_replace(array("_", "-")," ",$key)).": \n";
			foreach($value as $array_item)
			{
				$email_text .= $array_item."\n";
			}
			$email_text .= " \n";
		}
	}
}

include_once('./emailer/mail.template.class.php');
$template = new MailTemplate('./emailer/templates/skeleton.html');
$template->setTemplateContent( date('d F', time()), 'http://burma.co.za/formHandler/emailer/', '', 'The following information was sent from www.burma.co.za:', $mailData );

include_once('../cms/includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

$requiredFields = substr($data['required-fields'],0,-1);
$requiredFields = explode(';',$requiredFields);
foreach($requiredFields as $field)
{
	
	if(empty($data[$field]))
		$error = true;	
}

if(!$error)
{		


	$to = (!isset($_POST['to']))?$_POST['email']:$_POST['to'];	

	$postData = '';
	foreach ($_POST as $key => $val) {
		$postData .= '['.$key.'=>'.$val.']';
	}


	$dl->insert('email_storage', array(
		'storage_email' => $_POST['email'],
		'storage_name' => $_POST['name'],
		'storage_surname' => $_POST['surname'],
		'storage_mobile' => $_POST['tel'],
		'storage_postdata' => $postData,
		'storage_timestamp' => time()
	));
	//echo $dl->getError();
	
	
	$subject = $data['subject'];
	
	// $headers = 'From: ' . $_POST['from'] . "\r\n";
	$headers = 'From: ' . 'info@burma.co.za' . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$to = 'nick@gstudio.co.za, nicksimp08@gmail.com';
	//mail($to, $subject, $template->getTemplateHtml(), $headers);
}
header("Location: ".$data['success-url']);
?>