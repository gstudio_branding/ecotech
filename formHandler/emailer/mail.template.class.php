<?php 
class MailTemplate { 

	// Template VARS:
	// [MAIL_ROOTPATH]
	// [MAIL_DATE]
	// [MAIL_GREETING]
	// [MAIL_MESSAGE]
	// <MAIL_POST_VARS>foo</MAIL_POST_VARS>
	// [MAIL_POST_VARS]


	private $template_html,
			$template_file;

	public 	$mail_rootpath,
			$mail_date,
			$mail_greeting,
			$mail_message;

	public function __construct($template_file="./templates/skeleton.html") {
		$this->template_file = $template_file;
		$this->template_html = $this->getTemplate();
    } 

    private function getTemplate(){
    	return file_get_contents($this->template_file);
    }

    public function getTemplateHtml(){
    	return $this->template_html;
    }

    public function setTemplateContent($date='', $root='', $greeting='Hi there,', $message='You have recevied a message from your website, the messge follows:', $post_vars=array()){
		
		preg_match_all('/<MAIL_POST_VARS>(.*?)<\/MAIL_POST_VARS>/s', $this->template_html, $matches);
		$temp_row = ($matches[1][0]);

		$this->template_html = str_replace($matches[0][0], '[MAIL_POST_VARS]', $this->template_html);

    	$this->template_html = str_replace('[MAIL_ROOTPATH]', $root, $this->template_html);
    	$this->template_html = str_replace('[MAIL_DATE]', $date, $this->template_html);
    	$this->template_html = str_replace('[MAIL_GREETING]', $greeting, $this->template_html);
    	$this->template_html = str_replace('[MAIL_MESSAGE]', $message, $this->template_html);

    	$temp_content = '<table style="background-color:#eef0f3;" width="650" align="center" border="0" cellpadding="5" cellspacing="5">';
    	if(is_array($post_vars) && sizeof($post_vars)>0){
    		foreach ($post_vars as $key => $val) {

	    		$temp_row_content = $temp_row;
    			$temp_row_content = str_replace('[MAIL_KEY]', str_replace('_', ' ', ucwords($key)), $temp_row_content);
    			$temp_row_content = str_replace('[MAIL_VAL]', $val, $temp_row_content).'
    			';

				$temp_content .= $temp_row_content;
				
	    	}
    	}
    	$temp_content .= '</table>';

    	$this->template_html = str_replace('[MAIL_POST_VARS]', $temp_content, $this->template_html);
    }

} 

?>