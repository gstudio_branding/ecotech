/*!
 * jQuery.Validate
 * http://www.gstudio.co.za
 * @author Derek Serfontein
 * @version 1.0
 */

;(function ( $ ) {
	$.fn.validate = function( options ) {
		
		var settings = $.extend({
			fromEmail			: '',
			toEmail				: '',
			successUrl			: '',
			subject				: '',
			fromPage			: '',
			inputErrorClass		: '',
			errorMessageClass	: '',
			manageValues		: true
		}, options );
				
		if(settings.manageValues)
		{
			this.find('input[type=text], textarea').each(function(index, element) {
				$(this).attr('data-value',$(this).val());
				
				$(this).focus(function(e){
					if($(this).val()==$(this).data('value'))
						$(this).val('');
				}).blur(function(e){
					if($(this).val()=='')
						$(this).val($(this).data('value'));
				});
				
			});
		}
		
		this.submit(function(e) {
			
			error = false;
			
			form = $(this);
			
			form.find('input[type="submit"], input[type="image"]').animate({'opacity':'0.4'},1000,function(){ 
				$(this).attr('disabled', '');
			});
			
			form.find('.validation-error').removeClass('validation-error');
			form.find('.validation-error-msg').remove();
			
			form.find('[data-validate]').each(function(index, element)
			{
				if(settings.manageValues)
					value = ($(this).val()==$(this).data('value'))?'':$(this).val();
				else
					value = $(this).val();
				
				if ($(this).data('validate').indexOf("required") >= 0)
				{	
					if(value == '')
					{
						error = true;
						$(this).addClass('validation-error');
						$(this).after( "<span class='validation-error-msg'>* Required</span>" );			
					}
				}
				
				if($(this).data('validate').indexOf("email") >= 0 && value != '')
				{
					var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
					if(!pattern.test($(this).val()))
					{
						error = true;
						$(this).addClass('validation-error');
						$(this).after( "<span class='validation-error-msg'>* Invalid email</span>" );		
					}
				}
				else if($(this).data('validate').indexOf("url") >= 0 && value != '')
				{
					var pattern = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
					if(!pattern.test($(this).val()))
					{
						error = true;
						$(this).addClass('validation-error');
						$(this).after( "<span class='validation-error-msg'>* Invalid address</span>" );
					}
				}
				else if($(this).data('validate').indexOf("date") >= 0 && value != '')
				{
					var pattern = new RegExp(/^\d{1,2}\/\d{1,2}\/\d{4}$/);
					if(!pattern.test($(this).val()))
					{
						error = true;
						$(this).addClass('validation-error');
						$(this).after( "<span class='validation-error-msg'>* Invalid date</span>" );
					}
				}
				else if($(this).data('validate').indexOf("number") >= 0 && value != '')
				{
					var formats = "(999)999-9999|(999) 999 9999|999-999-9999|999 999 9999|999 9999 999|9999999999";
					var pattern = RegExp("^(" +
					   formats
						 .replace(/([\(\)])/g, "\\$1")
						 .replace(/9/g,"\\d") +
					   ")$");
									
					//var pattern = new RegExp(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/);
					if(!pattern.test($(this).val()))
					{
						error = true;
						$(this).addClass('validation-error');
						$(this).after( "<span class='validation-error-msg'>* Invalid number</span>" );
					}
				}
			});
			
			if(error)
			{
				
				if(settings.inputErrorClass != '')
					form.find('.validation-error').addClass(settings.inputErrorClass);
					
				if(settings.errorMessageClass != '')
					form.find('.validation-error-msg').addClass(settings.errorMessageClass);
				
				form.find('input[type="submit"], input[type="image"]').stop().animate({'opacity':'1'},1000,function(){ 
					$(this).removeAttr('disabled');
				});
				return false;
			}
			else
			{
				form.find('input[type="submit"], input[type="image"]').stop().animate({'opacity':'1'},1000,function(){ 
					$(this).removeAttr('disabled');
				});
				
				form.prepend('<input type="hidden" name="required-fields" value="" />');
				form.find('[data-validate^="required"]').each(function(index, element) {
					form.find('[name^="required-fields"]').val(form.find('[name^="required-fields"]').val() + $(this).attr('name') + ";");
                });
				
				if(settings.fromEmail != '')
					form.prepend('<input type="hidden" name="from" value="'+settings.fromEmail+'" />');
				if(settings.toEmail != '')
					form.prepend('<input type="hidden" name="to" value="'+settings.toEmail+'" />');
				if(settings.successUrl != '')
					form.prepend('<input type="hidden" name="success-url" value="'+settings.successUrl+'" />');
				if(settings.subject != '')
					form.prepend('<input type="hidden" name="subject" value="'+settings.subject+'" />');
				if(settings.fromPage != '')
					form.prepend('<input type="hidden" name="from-page" value="'+settings.fromPage+'" />');
				
				return true;
			}
			
        });
		
	};
}( jQuery ));