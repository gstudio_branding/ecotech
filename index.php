<?php
session_start();
$showSidebar = false;

if(!isset($_SESSION['landed'])){
	$showSidebar = true;
	$_SESSION['landed'] = true;
}

include_once('./includes/config.php');
include_once('./includes/baseurl.php'); 
$bp = new BaseUrl($includepath, dirname(__FILE__));
$includepath = $bp->getRelPath();

include('includes/functions.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecotech</title>
    
    <?php include('includes/css.php'); ?>
    <style type="text/css"></style>
    <script type="text/javascript"> var basepath = '<?php echo $basepath; ?>'; </script>
  </head>

  <body>
  	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-82399841-1', 'auto');
	  ga('send', 'pageview');

	</script>

  	<div id="pageWrap">
  		<?php include('includes/header.php'); ?>
	  	<div class="container-fluid">
	  		<div class="row">
  				<?php 
  				if($cat_id>=0 && $cat_id==325){
  					$showSidebar = true;
  				}
  				include('includes/sidebar.php'); ?>
			  	<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-8 col-md-offset-1 col-md-6" id="main-wrapper">
		  			<div class="spacer50 hidden-xs"></div>

				<?php 
				if($cat_id>=0){
					$page = $dl->select('mod_page', 'page_category_id='.$cat_id);
					if($dl->totalrows>0){
						$page = $page[0];
						if($isService){
							$serviceIcon = $dl->select('mod_page_category', 'page_category_id='.$page['page_category_id']);
							if($dl->totalrows>0 && $serviceIcon[0]['media_files_id']!=0 && $serviceIcon[0]['media_files_id']!=''){
								echo '<img src="'.$basepath.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$serviceIcon[0]['media_files_id'].'" class="page-icon" />';
							}
						}
						if($cat_id!=325){
							?>
							<div class="main-headings">
								<h1><?php echo $mainHeading; ?></h1>
								<h2><?php echo $subHeading; ?></h2>
							</div>
							<div class="spacer10"></div>
							<?php	
						}
						?>
						<div class="">
							<?php
							$prefix = 'page';
							$e = $page;
					        $json = json_decode($e[$prefix.'_json']);

					        $rows = '';
					        if($json!=''){ $rows = $json->{'content'}; }
					        $rowCount = 0;

					        if(sizeof($rows)>0 && $rows!=''){
					            foreach ($rows as $row => $value) {

					                $columns = $rows->{$row};
					                if(sizeof($columns)>0){
					                    $colCount = 0;
					                    
					                    if( isset($columns->{'col1'}) ){ $colCount = 1; }
					                    if( isset($columns->{'col2'}) ){ $colCount = 2; }
					                    if( isset($columns->{'col3'}) ){ $colCount = 3; }
					                    if( isset($columns->{'col4'}) ){ $colCount = 4; }

					                    switch ($colCount) {
					                        case 1 : 
					                            echo '<div class="row"><div class="col-xs-12">'.getColData($dl, $columns->{'col1'}, $basepath).'</div></div>';
					                        break;
					                        case 2 : 
					                            echo '
					                            <div class="row">
					                                <div class="col-xs-12 col-md-6">'.getColData($dl, $columns->{'col1'}, $basepath).'</div>
					                                <div class="col-xs-12 col-md-6">'.getColData($dl, $columns->{'col2'}, $basepath).'</div>
					                            </div>';
					                        break; 
					                        case 3 : 
					                            echo '
					                            <div class="row">
					                                <div class="col-xs-12 col-sm-4">'.getColData($dl, $columns->{'col1'}, $basepath).'</div>
					                                <div class="col-xs-12 col-sm-4">'.getColData($dl, $columns->{'col2'}, $basepath).'</div>
					                                <div class="col-xs-12 col-sm-4">'.getColData($dl, $columns->{'col3'}, $basepath).'</div>
					                            </div>';
					                        break;
					                        case 4:
					                            echo '
					                            <div class="row">
					                                <div class="col-xs-12 col-md-3">'.getColData($dl, $columns->{'col1'}, $basepath).'</div>
					                                <div class="col-xs-12 col-md-3">'.getColData($dl, $columns->{'col2'}, $basepath).'</div>
					                                <div class="col-xs-12 col-md-3">'.getColData($dl, $columns->{'col3'}, $basepath).'</div>
					                                <div class="col-xs-12 col-md-3">'.getColData($dl, $columns->{'col4'}, $basepath).'</div>
					                            </div>';
					                        break;
					                        
					                        default: break;
					                    }
					                    $rowCount ++;                   
					                }
					            }
					        }
							?>
							<div class="spacer10"></div>

							<div class="visible-xs">
								
								<div class="mobile-number">
									<img src="<?php echo $basepath; ?>elements/icon-contact.png"/>
									<img src="<?php echo $basepath; ?>elements/flyout-number.png" />
								</div>

								<div class="">
									<div class="spacer20"></div>
									<div class="diplay-inline">
										<img src="<?php echo $basepath; ?>elements/icon-partners.png" />
										<h2><strong>OUR PARTNERS</strong></h2>
									</div>
									<div class="spacer10"></div>
									<div class="footer-slider">
										<img src="<?php echo $basepath; ?>pieces/partners/SEACOM_OfficialPartnerlogo-300x138.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/microsoft-small-business-specialist.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/microsoftpartner.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/hp_logo_preferred_partner_w.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/dell_partnerdirect_preferred_rgb3.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/Neotel-Channel-partner-logo.png" />
										<img src="<?php echo $basepath; ?>pieces/partners/cisco_registered_Partner.png" />
									</div>
									<div class="spacer20"></div>
								</div>

								<hr>

								<div class="">
									<div class="spacer20"></div>
									<div class="diplay-inline">
										<img src="<?php echo $basepath; ?>elements/icon-awards.png" />
										<h2><strong>OUR AWARDS</strong></h2>
									</div>
									<div class="spacer10"></div>
									<div class="footer-slider">
										<img src="<?php echo $basepath; ?>/pieces/awards/Gold_Partner_Insignia.png" />
										<img src="<?php echo $basepath; ?>/pieces/awards/MS.png" />
									</div>
									<div class="spacer20"></div>
								</div>

							</div>
						</div>
						<?php
					}
					else{
						echo 'no content';
					}
					
				}
				?>
				</div>
	  		</div><!-- row -->
	  	</div><!-- container-fluid -->
		<div class="clearfix"></div>
	</div><!-- pageWrap -->
	<div class="clearfix"></div>
	<?php include('includes/footer.php'); ?>
	<?php include('includes/scripts.php'); ?>

  </body>
</html>