<?php 
include_once('includes/settings.php');

session_start();
unset($_SESSION['pa']); //kill the users session
unset($_SESSION); //kill the users session

include_once('includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = true;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $client; ?> | CMS</title>
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/login.css" rel="stylesheet">
	<link href="css/jquery-ui-1.9.0.custom.css" rel="stylesheet">
	
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	
	<div class="login container-fluid">
		<div class="header row">
			<div class="col-xs-4">
				<img src="<?php echo $logo; ?>"  height="99"  />
			</div><!-- col-xs-4 -->
			<div class="col-xs-5 col-xs-offset-9">
				<img src="elements/powered_by.png"  height="99"  />
			</div><!-- col-xs-5 -->
		</div><!-- row -->
		
		
		<div class="row login-bg">
			<div class="col-xs-offset-6 col-xs-6">
				<div class="row login-box">
					<div class="col-xs-offset-1 col-xs-16">
						<form role="form" class="loginbox">
							<h3>Login</h3>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="username" name="uname">
							</div>
							<div class="form-group">
								<input type="password" class="form-control reset_pw" placeholder="password" name="pword">
							</div>
							<button type="submit" class="btn btn-default subimt-btn">Submit</button>
						</form>
					</div><!-- col-xs-14 -->
				</div><!-- row -->
			</div><!-- col-xs-6 -->
		</div><!-- row -->
		
	</div>
	
	
	<div class="modal fade" id="system-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default modal-close-btn" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary modal-save-btn">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="loader">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width: 64px; left: 50%; margin-left: -32px;">
				<div class="modal-body">
					<img src="elements/loader/loader.gif">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	
	<script src="scripts/system/jquery.min.js"></script>
	<script src="scripts/system/bootstrap.min.js"></script>
	<script src="scripts/system/system.js"></script>
	<script src="scripts/system/jquery-ui.min.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){	
		$('.loginbox').submit(function(){
			$.ajax({
				url : 'includes/pandora/openPandora.php',
				data : $('.loginbox').serialize(),
				dataType : 'json',
				type : 'post',
				beforeSend : function(){  },
				success : function(data){ 
					if(data.status==1){
						changeSystemModal('<span class="text-success">Login Successful</span>', data.msg, '', '', '', '', '');
						$('#system-modal').modal('show');
						window.location = 'index.php';
					}else{
						changeSystemModal('<span class="text-danger">Login Unsuccessful</span>', data.msg, '', 'Close', '', '', '');
						$('#system-modal').modal('show');
						$('input.reset_pw').val(''); //clear password field 
					}
				},
				error : function(){},
				complete : function(){  }			
			});
			
			return false;
		});
	});
	</script>
</body>
</html>