<?php 
include_once('includes/settings.php');

session_start();
if(!isset($_SESSION) || !isset($_SESSION['pa'])){
	header('Location: login.php');
}

include_once('includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $client; ?> | CMS</title>
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/jquery-ui-1.9.0.custom.css" rel="stylesheet">
	<link href="css/jquery.jcrop.min.css" rel="stylesheet">
	
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	
	<div class="main container-fluid">
		<div class="header row">
			<div class="col-xs-4">
				<img src="<?php echo $logo; ?>" height="99" />
			</div><!-- col-xs-4 -->
			<div class="col-xs-5 col-xs-offset-9">
				<img src="elements/powered_by.png"  height="99" />
			</div><!-- col-xs-5 -->
		</div><!-- row -->
		
		
		<div class="row">
			<div class="col-xs-18 nav-container">
			
				<?php 
				$permissions = array();

				if(is_array($modules) && sizeof($modules)>0){
					foreach($modules as $m){
						//check if the user has permissions
						$permission = $dl->select('access_level_'.$m['prefix'], $m['prefix'].'_acc_admin_id="'.$_SESSION['pa']['id'].'"');
						
						if($dl->totalrows>0 && $permission[0][$m['prefix'].'_acc_level']==1){
							array_push($permissions, array($m['prefix']=>1));
							?><div class="nav-item"><a class="btn btn-default" data-location="nav-item-content" href="<?php echo $m['path']; ?>"><?php echo $m['mod']; ?></a></div><?php
						}
					}
				}
				?>
				<a href="login.php" class="nav-item pull-right"><div class="btn btn-default"><span class="glyphicon glyphicon-log-out"></span></div></a>
			</div><!-- col-xs-18 -->
		</div><!-- row -->
		
		<div class="row">
			<div class="col-xs-18">
				<hr style="margin-top:0; margin-bottom:0; border:1px solid #fff;">
			</div><!-- col-xs-18 -->
		</div><!-- row -->
		
		<div class="row">
			<div class="col-xs-18 nav-item-content">
			</div><!-- col-xs-18 -->
		</div><!-- row -->
		
	</div><!-- container fluid -->
	
	
	
	<div class="modal fade" id="system-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default modal-close-btn" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary modal-save-btn">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal fade" id="system-modal-2">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default modal-close-btn" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary modal-save-btn">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="modal fade" id="loader">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" style="width: 64px; left: 50%; margin-left: -32px;">
				<div class="modal-body">
					<img src="elements/loader/loader.gif">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	
	
	<script src="scripts/system/jquery.min.js"></script>
	<script src="scripts/system/bootstrap.min.js"></script>
	<script src="scripts/system/system.js"></script>
	<script src="scripts/system/jquery-ui.min.js"></script>
	<script src="scripts/tinymce/tinymce.min.js"></script>
	<script src="scripts/tinymce/jquery.tinymce.min.js"></script>
	<script src="scripts/jcrop/jquery.jcrop.min.js"></script>
</body>
</html>