<script type="text/javascript" src="scripts/system/module.options.js?t=<?php echo time(); ?>"></script>
<script type="text/javascript">
var cmsOptionsLen = Object.keys(cmsOptions).length,
	cmsOptionsString = '<select class="content-type form-control"><option>select type...';

if(cmsOptionsLen>0){
	for (key in cmsOptions){
    	cmsOptionsString += '<option value="'+key+'">'+key+'</option>';
    }
}
cmsOptionsString += '</option></select>';

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function rebindContentManagement(){

	$('#system-modal-2').find('.modal-save-btn').unbind();
	$('#system-modal-2').find('.modal-save-btn').click(function(){

		passedInformation = {};
		$('.content-type option:selected').val();

		switch($('.content-type option:selected').val()){

			case 'Text' : 
				console.log( $('.text-area-form').serialize() );

				// %5Bbasepath%5D

				// src%3D%22modules%2Fmedia%2Fscripts%2Fimage%2Fimage.handler.php%3Fmedia_files_id
				// src%3D%22%5Bbasepath%5D%2Fmodules%2Fmedia%2Fscripts%2Fimage%2Fimage.handler.php%3Fmedia_files_id

				var html = tinymce.get('tinymce-modal').getContent(),
					content = $('.text-area-form').serialize();

				content = replaceAll(content, 'src%3D%22modules%2Fmedia%2Fscripts%2Fimage%2Fimage.handler.php%3Fmedia_files_id', 'src%3D%22%5Bbasepath%5D%2Fcms%2Fmodules%2Fmedia%2Fscripts%2Fimage%2Fimage.handler.php%3Fmedia_files_id');

				content = content.replace("text-area-form=", "");
				content = strEncode(content);


				if($('#tinymce-modal_ifr').length>0){ $(".tinymce").tinymce().remove(); }
				
				passedInformation = {
					type : 'text' ,
					data : content,
					html : html
				};

				console.log(passedInformation);
			break;

			default : 
				console.log($('.content-type option:selected').val());
			break;
		}

		$('#'+$(this).parents('.modal').eq(0).attr('id')).modal('hide');
	});
}

$(document).ready(function(){

	$('#content-type-container').html(cmsOptionsString);

	$('.content-type').unbind();
	$('.content-type').change(function(){
		var key = $(this).val(),
			cmsOption = cmsOptions[key],
			container = $('#content-type-content'),
			templateString = cmsOption['template'];

		templateString = templateString.replace('[*title*]', '');
		templateString = templateString.replace('[*name*]', '');
		templateString = templateString.replace('[*value*]', '');
		templateString = templateString.replace('[*class*]', '');
		templateString = templateString.replace('[*checked*]', '');
			
		if($('#tinymce-modal_ifr').length>0){ $("#tinymce-modal").tinymce().remove(); }

		container.html(templateString);
		if('functions' in cmsOption){ cmsOption.functions(); }	
		rebindContentManagement();
	});

	

});
</script>

<div id="content-type-container"></div>
<div id="content-type-content"></div>
<!-- <div id="content-type-json" class="hidden"></div> -->