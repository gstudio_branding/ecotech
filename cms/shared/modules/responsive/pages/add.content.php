<?php 

$folder = '';
$prefix = '';
$filename = '';
$options = '';
$optionsArray = array();

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

$folder = 'responsive';

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		$optionsArray[strtolower($t)] = true;
	}
}

$mode = 'add';

if(isset($_POST['mode']) && $_POST['mode']!=''){
	include_once('../../../../includes/pandora/pandora.php');
	include_once('../../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	include_once('../../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('mod_'.$prefix, $prefix.'_id="'.$_POST[$prefix.'_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}
if((!isset($dl))){
	include_once('../../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
}

$instructions = $dl->select('mod_instruction', '', 'instruction_heading ASC');


$categories = $dl->select('mod_category', '', 'category_heading ASC');
function getCategories($dl, $entry, $instructionsArr){
	$ret = '';
	$categories = $dl->select('mod_product_category_link AS pcl LEFT JOIN mod_category AS c ON pcl.category_id=c.category_id', 'product_id="'.$entry['product_id'].'"', 'category_heading ASC');

	if($dl->totalrows>0){
		foreach ($categories as $c) {
			$ret .= getCategoryHTML($c, $instructionsArr);
		}
	}

	return $ret;
}

function getRecursiveCategories($dl, $cat_id, $indent, $selected, $prefix){
	$cats = $dl->select('mod_'.$prefix.'_category', $prefix.'_category_category_id="'.$cat_id.'" AND '.$prefix.'_category_archived="0"', $prefix.'_category_sort ASC');
	$data = '';
	$indent .= '&nbsp;&nbsp;&nbsp;&nbsp;';

	if($dl->totalrows>0){
		foreach ($cats as $c){
			$selectOption = '';
			if($selected == $c[$prefix.'_category_id']){
				$selectOption = 'selected="selected"';
			}
			$data .= '<option value="'.$c[$prefix.'_category_id'].'" '.$selectOption.'>';
			if($cat_id!=0){ 
				
				$data .= $indent; 
			}
			$data .= $c[$prefix.'_category_heading'].'</option>';			
			$data .= getRecursiveCategories($dl, $c[$prefix.'_category_id'], $indent, $selected, $prefix);
		}
		return $data;
	}
	else{
	 	return '';
	}
}

function getColData($data){
	if( isset($data->{'type'})){
		switch ($data->{'type'}) {
			case 'text': 
				// return stripslashes($data->{'data'});
				return str_replace('[basepath]', '../', stripslashes($data->{'data'}));
			break;

			case 'image': 
				$img = '<div class="col-xs-18 img-container"><img src="modules/media/scripts/image/image.handler.php?media_files_id='.$data->{'data'}->{'id'}.'&width='.$data->{'data'}->{'width'};
				if($data->{'data'}->{'aspect_r'}!=0){ $img.='&aspect_r='.$data->{'data'}->{'aspect_r'}; }
				 $img.='" class="img-responsive" /></div>';
				 return $img;
			break;
			
			default: break;
		}
	}
	else{
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
	}
}
?>

<style type="text/css">
.responsive-content-container{
	border: 1px solid #000;
	margin: 5px;
	/*text-align: center;*/
	min-height: 100px;


	display: block;
    padding: 6px 12px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.row-seperator{
	border-color:#999;
}
.add-content{
	width: 100%;
	display: block;
	margin: 10px 0;
	clear: both;
}
</style>

<form role="form" class="add-entry">
	<div class="row">
		<?php if(isset($optionsArray['date'])){ ?>
		<div class="col-xs-4">
			Date
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_date" class="form-control input-sm date" value="<?php if($mode=='edit' && $entry[$prefix.'_date']!=0 && $entry[$prefix.'_date']!=''){ echo date('d F Y', $entry[$prefix.'_date']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['category']) ) { ?>
		<div class="col-xs-4">
			Category
		</div>

		<div class="col-xs-14">
			<div class="form-group">
				<select name="<?php echo $prefix; ?>_category_id" class="form-control">
					<?php 

					echo getRecursiveCategories($dl, 0, '', ( ($mode=='edit')?$entry[$prefix.'_category_id']:'' ), $prefix);

					// $cats = $dl->select('mod_'.$prefix.'_category', $prefix.'_category_archived=0');
					// if($dl->totalrows>0){
					// 	foreach ($cats as $c) {
					// 		if($mode=='edit' && $c[$prefix.'_category_id']==$entry[$prefix.'_category_id']){ 
					// 			echo '<option value="'.$c[$prefix.'_category_id'].'" selected="selected">'.$c[$prefix.'_category_heading'].'</option>';
					// 		}
					// 		else{
					// 			echo '<option value="'.$c[$prefix.'_category_id'].'">'.$c[$prefix.'_category_heading'].'</option>';
					// 		}
					// 	}
					// }
					// else{
					// 	echo '<option value="0">Please first add a category</option>';
					// }
					?>
				</select>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->


        <?php } ?>


        <?php if( isset($optionsArray['category_link']) ) { ?>
		<div class="col-xs-4">
			Categories
		</div>
		<div class="col-xs-14">
			<div class="form-group categories" style="margin-bottom:0;">

				<?php
				if($mode=='edit'){  echo getCategories($dl, $entry, $categories); }
				else{  echo getCategoryHTML('', $categories); }
				?>

			</div><!-- form-group -->
			<a href="#" class="add_categories">add category</a>
			<div class="spacer20"></div>&nbsp;
		</div><!-- col-xs-14 -->
        <?php } ?>



		<?php if(isset($optionsArray['heading'])){ ?>
        <div class="col-xs-4">
			Heading
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_heading" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_heading']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


        <?php if( isset($optionsArray['author']) ) { ?>
        <div class="col-xs-4">
			Author
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_author" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_author']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>



		<?php if(isset($optionsArray['slug'])){ ?>
        <div class="col-xs-4">
			Slug
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_slug" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_slug']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['price']) ) { ?>
        <div class="col-xs-4">
			Price
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_price" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_price']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['quantity']) ) { ?>
        <div class="col-xs-4">
			Quantity
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_quantity" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_quantity']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['code']) ) { ?>
        <div class="col-xs-4">
			Code
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_code" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_code']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['short_description']) ) { ?>
        <div class="col-xs-4">
			Short Intro
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<textarea name="<?php echo $prefix; ?>_short_description" class="form-control input-sm tinymce-inline"><?php if($mode=='edit'){ echo $entry[$prefix.'_short_description']; } ?></textarea>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['description']) && 1==2 ) { ?>
		<div class="col-xs-4">
			Content
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<textarea name="<?php echo $prefix; ?>_description" class="form-control input-sm tinymce-inline"><?php if($mode=='edit'){ echo $entry[$prefix.'_description']; } ?></textarea>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['special']) ) { ?>
        <div class="col-xs-4">
			<!-- Featured -->
			In the News
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_special" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_special']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>

		<?php if( isset($optionsArray['special']) && 1==2) { ?>
        <div class="col-xs-4">
			In the News
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_in_the_news" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_in_the_news']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>

		<?php if( isset($optionsArray['special']) ) { ?>
        <div class="col-xs-4">
			<!-- Latest Blog -->
			From the Archives
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_latest_blog" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_latest_blog']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>



		<?php if( isset($optionsArray['special']) ) { ?>
        <div class="col-xs-4">
			<!-- Latest Blog -->
			Latest Article 1
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_latest_article_1" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_latest_article_1']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['special']) ) { ?>
        <div class="col-xs-4">
			<!-- Latest Blog -->
			Latest Article 2
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_latest_article_2" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_latest_article_2']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['special']) ) { ?>
        <div class="col-xs-4">
			<!-- Latest Blog -->
			Latest Article 3
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_latest_article_3" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_latest_article_3']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>




		<?php if( isset($optionsArray['special'])  && 1==2) { ?>
        <div class="col-xs-4">
			Latest Article
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_latest_article" class="" value="1" <?php if($mode=='edit' && $entry[$prefix.'_latest_article']==1){ echo 'checked="checked"'; } ?>>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['link_video']) ) { ?>
        <div class="col-xs-4">
			Youtube/Video Link 
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_link_video" class="" <?php if($mode=='edit' && $entry[$prefix.'_link_video']=="1"){ echo 'checked="checked"'; } ?> value="1">
			</div><!-- form-group -->

		</div><!-- col-xs-14 -->
		<?php } ?>

        
        <?php if( isset($optionsArray['link']) ) { ?>
        <div class="col-xs-4">
			Link
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_link" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_link']); } ?>">
			</div><!-- form-group -->

		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( isset($optionsArray['image']) ) { ?>
		<div class="col-xs-4">
			image
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-image" style="margin-top:10px;">select image</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-image" style="margin-top:10px;">remove image</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 image-container">
			<?php 
			if($mode=='edit' && $entry['media_files_id']!="" && $entry['media_files_id']!="0"){ 
				?>
				<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="3x2">crop image</div><input type="hidden" name="media_file_id" value="<?php echo $entry['media_files_id']; ?>"><img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $entry['media_files_id']; ?>&width=200&aspect_r=3x2" class="img-responsive" /></div><?php
			}
			?>
		</div><!-- col-xs-18 -->
		<?php } ?>

			
		<?php if( isset($optionsArray['gallery']) ) { ?>
		<div class="col-xs-4">
			Gallery
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-images" style="margin-top:10px;">select images</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-images" style="margin-top:10px;">remove images</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 gallery-images-container">
			<?php 
			if($mode=='edit'){ 
				$media_files = $dl->select('mod_'.$prefix.'_image_link', $prefix.'_id="'.$_POST[$prefix.'_id'].'"', $prefix.'_media_link_sort'.' ASC');
				if($dl->totalrows>0){
					foreach ($media_files as $m){
						?><div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="3x2">crop image</div><input type="hidden" name="media_files_id[]" value="<?php echo $m['media_files_id']; ?>"><img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $m['media_files_id']; ?>&width=200&aspect_r=3x2" class="img-responsive" /></div><?php
					}
				}
			?>
			
			<?php } ?>
		</div><!-- col-xs-18 -->
		<?php } ?>



		


		<?php if( isset($optionsArray['documents']) ) { ?>
		<div class="col-xs-4">
			Documents
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-documents" style="margin-top:10px;">select documents</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-documents" style="margin-top:10px;">remove documents</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 documents-container">
			<?php 
			if($mode=='edit'){ 
				$media_files = $dl->select('mod_'.$prefix.'_document_link', $prefix.'_id="'.$_POST[$prefix.'_id'].'"', $prefix.'_media_link_sort'.' ASC');
				if($dl->totalrows>0){
					foreach ($media_files as $m){
						?><div class="col-xs-3 img-container"><input type="hidden" name="document_media_files_id[]" value="<?php echo $m['media_files_id']; ?>"><img src="elements/icons/icon-pdf.png" class="img-responsive" /></div><?php
					}
				}
			?>
			
			<?php } ?>
		</div><!-- col-xs-18 -->
		<?php } ?>


		<?php if( isset($optionsArray['offset']) ) { ?>
        <div class="col-xs-4">
			Offset
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_offset" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_offset']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>
		



		<?php if( isset($optionsArray['video_link']) ) { ?>
		<div class="col-xs-4">
			Videos
		</div>
		<div class="col-xs-14">
			<div class="form-group video-container" style="margin-bottom:0;">

				<?php
				if($mode=='edit'){  echo getVideos($dl, $entry, $videos); }
				?>

			</div><!-- form-group -->
			<a href="#" class="add_video">add video</a>
			<div class="spacer20"></div>&nbsp;
		</div><!-- col-xs-14 -->

        <?php } ?>
		


		<?php if( isset($optionsArray['instructions_link']) ) { ?>
		<div class="col-xs-4">
			Instructions
		</div>
		<div class="col-xs-14">
			<div class="form-group bake_instruction" style="margin-bottom:0;">

				<?php
				echo 'Bake';
				if($mode=='edit'){  echo getInstructions($dl, $entry, 'bake', $instructions); }
				?>

			</div><!-- form-group -->
			<a href="#" class="add_bake_instruction">add instruction</a>
			<div class="spacer20"></div>&nbsp;
		</div><!-- col-xs-14 -->

		<div class="col-xs-14 col-xs-offset-4">
			<div class="form-group fry_instruction" style="margin-bottom:0;">

				<?php
				echo 'Fry';
				if($mode=='edit'){  echo getInstructions($dl, $entry, 'fry', $instructions); }
				?>

			</div><!-- form-group -->
			<a href="#" class="add_fry_instruction">add instruction</a>
			<div class="spacer20"></div>&nbsp;
		</div><!-- col-xs-14 -->
        <?php } ?>

	</div>

	<?php if($mode=='edit'){ ?>
	<input type="hidden" name="<?php echo $prefix; ?>_id" class="form-control input-sm" value="<?php echo $entry[$prefix.'_id']; ?>">
	<?php } ?>

	<!-- <pre> -->
	<?php 
	$jsonLayoutContent = '';
	$colTools = '<a href="#" class="remove-layout-content pull-right clearfix"><i class="glyphicon glyphicon-remove"></i></a><a href="#" class="add-layout-content pull-right clearfix"><i class="glyphicon glyphicon-pencil"></i>&nbsp;</a><hr>';
	if($mode == 'edit'){
		$json = json_decode($entry[$prefix.'_json']);

		$rows = '';
		if($json!=''){ $rows = $json->{'content'}; }
		$rowCount = 0;

		if(sizeof($rows)>0 && $rows!=''){
			foreach ($rows as $row => $value) {
				$columns = $rows->{$row};
				if(sizeof($columns)>0){
					$colCount = 0;
					
					if( isset($columns->{'col1'}) ){ $colCount = 1; }
					if( isset($columns->{'col2'}) ){ $colCount = 2; }
					if( isset($columns->{'col3'}) ){ $colCount = 3; }
					if( isset($columns->{'col4'}) ){ $colCount = 4; }

					switch ($colCount) {
						case 1 : 
							$jsonLayoutContent .= '
							<div class="row row-layout-container">
								<div class="col-xs-18"></div>
								<div class="col-xs-18">
									<a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a>
								</div>
								<div class="col-xs-18">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col1">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col1'}))?$columns->{'col1'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-18"><hr class="row-seperator"></div>
							</div>';
						break;
						case 2 : 
							$jsonLayoutContent .=' 
							<div class="row row-layout-container">
								<div class="col-xs-18"></div>
								<div class="col-xs-18">
									<a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a>
								</div>
								<div class="col-xs-9">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col1">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col1'}))?$columns->{'col1'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-9">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col2">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col2'}))?$columns->{'col2'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-18"><hr class="row-seperator"></div>
							</div>';
						break; 
						case 3 : 
							$jsonLayoutContent .= '
							<div class="row row-layout-container">
								<div class="col-xs-18"></div>
								<div class="col-xs-18">
									<a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a>
								</div>
								<div class="col-xs-6">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col1">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col1'}))?$columns->{'col1'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col2">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col2'}))?$columns->{'col2'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col3">
										'.$colTools.'
										<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col3'}))?$columns->{'col3'}:'' ) ).'</div>
									</div>
								</div>
								<div class="col-xs-18"><hr class="row-seperator"></div>
							</div>';
						break;
						case 4:
							$jsonLayoutContent .= '
							<div class="row row-layout-container">
								<div class="col-xs-18"></div>
								<div class="col-xs-18">
									<a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a>
								</div>
								<div class="col-xs-9" style="padding:0;">
									<div class="col-xs-9">
										<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col1">
											'.$colTools.'
											<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col1'}))?$columns->{'col1'}:'' ) ).'</div>
										</div>
									</div>
									<div class="col-xs-9">
										<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col2">
											'.$colTools.'
											<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col2'}))?$columns->{'col2'}:'' ) ).'</div>
										</div>
									</div>
								</div>

								<div class="col-xs-9" style="padding:0;">
									<div class="col-xs-9">
										<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col3">
											'.$colTools.'
											<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col3'}))?$columns->{'col3'}:'' ) ).'</div>
										</div>
									</div>
									<div class="col-xs-9">
										<div class="responsive-content-container" data-row-id="row-'.$rowCount.'" data-col-id="col4">
											'.$colTools.'
											<div class="responsive-inner-container row">'.getColData( ( (isset($columns->{'col4'}))?$columns->{'col4'}:'' ) ).'</div>
										</div>
									</div>
								</div>
								<div class="col-xs-18"><hr class="row-seperator"></div>
							</div>';
						break;
						
						default: break;
					}

					$rowCount ++;					

					// foreach ($columns as $col => $value) {
					// 	print_r($col);
					// }
				}
			}
		}
	} 
	?>
	<!-- </pre> -->
	

	
	<div class="row">
		<?php if( isset($optionsArray['json']) ) { ?>	
		<div class="col-xs-18"><hr class="row-seperator"></div>
		<div class="col-xs-18 layout-sortable"><?php echo $jsonLayoutContent; ?></div>
		<div class="col-xs-18"><a href="#" class="add-content">add row <i class="glyphicon glyphicon-plus"></i></a></div>
		<?php } ?>
		<div class="col-xs-18"><button type="submit" class="btn btn-default">Submit</button></div>
	</div>
	

</form>

<script type="text/javascript">
var passedInformation,
	content = {},
	colTools = '<?php echo $colTools; ?>',
	sortableSet = false;

<?php 
if($mode=='edit'){
	//$temp = json_encode($entry[$prefix.'_json']);
	//$temp = substr($temp, 1, (strlen($temp)-2));
	$temp = ($entry[$prefix.'_json']);
?>	
	//temp = '<?php echo $temp; ?>';
	//if(temp!=''){
		content = <?php echo $temp; ?>;//JSON.parse('<?php echo $temp; ?>');
		content = content.content;
	//}
<?php
}
?>


function refreshContentBindings(){
	if(sortableSet){ 
		$('.layout-sortable').sortable('destroy'); 
	}
	else{ sortableSet = true; }

	$('.layout-sortable').unbind();
	$('.layout-sortable').sortable({
		placeholder: "ui-state-highlight",
		handle: "a.move",
		helper: "clone",
		update : function(){
			updateRowInfomation();
			// $.ajax({
			// 	url : 'shared/modules/process/add.<?php echo $filename; ?>.php?a=3&prefix=<?php echo $prefix; ?>',
			// 	beforeSend : function(){
			// 		showLoader();
			// 	},
			// 	type : 'post',
			// 	data : $( ".layout-sortable" ).sortable( "serialize"),
			// 	complete : function(){
			// 		hideLoader();
			// 	}
			// });
		}
	});

	$('.add-layout-content').click(function(){
		if($('#tinymce-modal_ifr').length>0){ $(".tinymce").tinymce().remove(); }
		passedInformation = {};
		var $this = $(this),
			container = $this.parents('.responsive-content-container').eq(0);
			rowId = container.attr('data-row-id'),
			colId = container.attr('data-col-id');

		changeSystemModal2('choose content', 'loading...', 'shared/modules/<?php echo $folder; ?>/pages/manage.col.<?php echo $filename; ?>.php', 'Cancel', 'Update', 
			function(){

				switch(content[rowId][colId].type){
					case 'text' :				
						passedInformation = {
							html : container.find('.responsive-inner-container').html()
						}
						$('#system-modal-2').find('.content-type option[value="Text"]').attr('selected', 'selected');
						$('.content-type').trigger('change');
					break;

					case 'image' :		
						passedInformation = {
							data : content[rowId][colId]
						}

						$('#system-modal-2').find('.content-type option[value="Image"]').attr('selected', 'selected');
						$('.content-type').trigger('change');
					break;
				}
			}, 
			function(){
				if($('#tinymce-modal_ifr').length>0){ $(".tinymce").tinymce().remove(); }

				if(Object.keys(passedInformation).length>0){
					confirmMainNavigate = true;

					// if(content[rowId][colId]==undefined){
					// 	content[rowId][colId] = { empty : '' };
					// }

					if('type' in passedInformation && 'data' in passedInformation){
						if('html' in passedInformation){
							container.find('.responsive-inner-container').html(passedInformation.html);

						}					

						if('empty' in content[rowId][colId]){
							 delete content[rowId][colId]["empty"];
						}
						
						if(passedInformation.data == 'empty'){
							content[rowId][colId] = {
								empty : ''
							};	
						}
						else{
							content[rowId][colId] = {
								type : passedInformation.type,
								data : passedInformation.data
							}
						}
					}
				}
				refreshBindings();
			}
		);
		$('#system-modal-2').modal('show');
	});

	refreshBindings();
}

function updateRowInfomation(){
	var eq = 0,
		temp = content;//,
		
	content = {};

	// console.log(temp);
	// console.log(content);
	
	$( ".layout-sortable" ).find('.row-layout-container').each(function(){
		$(this).find('[data-row-id]').each(function(){
			var rowId = $(this).attr('data-row-id'),
				colId = $(this).attr('data-col-id');

			$(this).attr('data-row-id', 'row-'+eq);
			if(content['row-'+eq]==undefined){ content['row-'+eq] = {}; }
			content['row-'+eq][colId] = temp[rowId][colId];
			

		});

		eq++;
	});
	// console.log(content);
}

function refreshBindings(){	
	removeLayoutContentBinding();
}

function removeLayoutContentBinding(){
	$('.remove-layout-content').unbind();
	$('.remove-layout-content').click(function(){
		confirmMainNavigate = true;
		var $this = $(this),
			container = $this.parents('.responsive-content-container').eq(0);
			rowId = container.attr('data-row-id'),
			colId = container.attr('data-col-id');


		$('.modal-save-btn').unbind();
		$('.modal-save-btn').click(function(){
				
			delete content[rowId];

			var rowCount = 0
				temp = {};

			if(Object.keys(content).length>0){
				for (var key in content) {
					if(key.indexOf('row-')>=0){
						temp['row-'+rowCount] = content[key];
						rowCount++;
					}
				}
			}
			content = temp;
			container = container.parents('.row').eq(0);
			container.prev('div.col-xs-18').remove();
			container.remove();

			$('#'+$(this).parents('.modal').eq(0).attr('id')).modal('hide');
		});		

		changeSystemModal('remove row', 'This will remove the entire row and its contents', '', 'Cancel', 'Remove', function(){  }, function(){

		});
		$('#system-modal').modal('show');
		return false;
	});

}

$(document).ready(function(){
	refreshContentBindings();

	$('input, select, textarea').change(function(){
		confirmMainNavigate = true;
	});


	$('.add-content').click(function(){
		changeSystemModal('choose layout', 'loading content', 'shared/modules/<?php echo $folder; ?>/pages/layout.php', 'Cancel', 'Choose layout', function(){  }, function(){ 
				var data = JSON.parse($('.ret-data').html()),
					layout = '';
				
				if(Object.keys(data).length>0){
					if(data.saved){

						var rowKey = 'row-',
							editMessage = '<p class="text-center">click the edit button to manage content</p>';

						if(content=='""' || content=='' || content==undefined){ rowKey += '0'; content = {}; }
						else{ rowKey += Object.keys(content).length; }

						switch(data.layout){
							case 'one-col' : 
								layout += '<div class="row row-layout-container"><div class="col-xs-18"></div><div class="col-xs-18"><a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a></div><div class="col-xs-18"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col1">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div>';
								layout += '<div class="col-xs-18"><hr class="row-seperator"></div>';
								layout += '</div>';

								content[rowKey] = {
									col1 : { empty : '' }
								};
							break;

							case 'two-col' : 
								layout += '<div class="row row-layout-container"><div class="col-xs-18"></div><div class="col-xs-18"><a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a></div><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col1">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col2">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div>';
								layout += '<div class="col-xs-18"><hr class="row-seperator"></div>';
								layout += '</div>';

								content[rowKey] = {
									col1 : { empty : '' },
									col2 : { empty : '' }
								};
							break;

							case 'three-col' : 
								layout += '<div class="row row-layout-container"><div class="col-xs-18"></div><div class="col-xs-18"><a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a></div><div class="col-xs-6"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col1">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div><div class="col-xs-6"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col2">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div><div class="col-xs-6"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col3">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div>';
								layout += '<div class="col-xs-18"><hr class="row-seperator"></div>';
								layout += '</div>';

								content[rowKey] = {
									col1 : { empty : '' },
									col2 : { empty : '' },
									col3 : { empty : '' }
								};
							break;

							case 'four-col' : 
								layout += '<div class="row row-layout-container"><div class="col-xs-18"></div><div class="col-xs-18"><a href="#" class="tools pull-right move ui-sortable-handle"><span class="glyphicon glyphicon-move"></span></a></div><div class="col-xs-9" style="padding:0;"><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col1">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col2">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div></div><div class="col-xs-9" style="padding:0;"><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col3">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div><div class="col-xs-9"><div class="responsive-content-container" data-row-id="'+rowKey+'" data-col-id="col4">'+colTools+'<div class="responsive-inner-container row">'+editMessage+'</div></div></div></div>';
								layout += '<div class="col-xs-18"><hr class="row-seperator"></div>';
								layout += '</div>';

								content[rowKey] = {
									col1 : { empty : '' },
									col2 : { empty : '' },
									col3 : { empty : '' },
									col4 : { empty : '' }
								};
							break;
						}
					}

					//$('.add-content').before(layout);
					confirmMainNavigate = true;
					$('.layout-sortable').append(layout);
					refreshContentBindings();
				}
		 	} 
		 );
		$('#system-modal').modal('show');
	});

	$('.add-entry').submit(function(){

		var inputs = {};

		$(this).find('input,textarea,select').each(function(){
			if( $(this).attr('type') == 'checkbox' ){ 
				if($(this).is(":checked")){
					inputs[$(this).attr("name")] = $(this).val();
				}
			}
			else{

				if( $(this).attr("name").indexOf('[]')>=0 ){
					var del = '',
						key = $(this).attr("name").replace('[]', '');

					if(inputs[key]!=undefined){ del = ','; }
					else{ inputs[key] = ''; }
					inputs[key] = inputs[key]+del+$(this).val();
				}
				else{
					inputs[$(this).attr("name")] = $(this).val();
				}
			}
		    delete inputs["undefined"];
		});

		// var sendData = {
		// 		rows : {
		// 			content
		// 		},
		// 		inputs,
		// 		prefix : '<?php echo $prefix; ?>'
		// 	},

		//safari fix
		var sendData = {
			rows : { content : {} },
			inputs : '',
			prefix : '<?php echo $prefix; ?>'
		};

		sendData.rows.content = content;
		sendData.inputs = inputs;

		var msg = 'Your content has been added';


		<?php if($mode=='edit'){ ?>
		var msg = 'Your content has been updated';
		<?php } ?>

		$.ajax({
			<?php 
			if(isset($_GET['test'])){
			?>
			url : 'shared/modules/<?php echo $folder; ?>/process/add.<?php echo $filename; ?>.test.php?a=<?php if($mode=="add"){ echo 1; }else{ echo 2; } ?>',
			<?php	
			}
			else{
			?>
			url : 'shared/modules/<?php echo $folder; ?>/process/add.<?php echo $filename; ?>.php?a=<?php if($mode=="add"){ echo 1; }else{ echo 2; } ?>',
			<?php
			}
			?>
			data : sendData,
			dataType : 'json',
			type : 'post',
			beforeSend : function(){
				showLoader();
			},
			success : function(data){
				changeSystemModal('<span class="text-success">Success!</span>', msg, '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });

				<?php if($mode=='add'){ ?>
				$('input,textarea').val('');
				$('input:checkbox').attr('checked',false);
				$('.image-container, .gallery-images-container, .documents-container').html('');
				<?php } ?>
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
				<?php if($mode=='edit'){ ?>
				$('.go-back').trigger('click');
				confirmMainNavigate = false;
				<?php } ?>
			}
		});

		return false;
	});

	removeLayoutContentBinding();

	

	var today = new Date();
	var month = '';
	
	//set the date to today
	switch(today.getMonth()){
		case 0 : month ='January'; break;
		case 1 : month ='February'; break;
		case 2 : month ='March'; break;
		case 3 : month ='April'; break;
		case 4 : month ='May'; break;
		case 5 : month ='June'; break;
		case 6 : month ='July'; break;
		case 7 : month ='August'; break;
		case 8 : month ='September'; break;
		case 9 : month ='October'; break;
		case 10 : month ='November'; break;
		case 11 : month ='December'; break;
	}
	var date = today.getDate() + ' ' +month+ ' ' +today.getFullYear();
	$('.date').val(date);
	$('.date').datepicker({
		dateFormat: "dd MM yy"
	});

	$('[name*="_heading"]').change(function(){		
		if($('[name*="_slug"]').val()==''){
			var find = ' ';
			var re = new RegExp(find, 'g');
			var slug = (($('[name*="_heading"]').val().toLowerCase()).replace(/[^\w\s]/gi, '')).replace(re, '-');
			$('[name*="_slug"]').val(slug);
		}
	});


	$('.select-image').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.image-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><!-- <div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="1x1">crop image</div> --><input type="hidden" name="media_file_id" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=1x1" class="img-responsive" /></div>';
				}
				
				$('.image-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.select-images').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.gallery-images-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.multi.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="3x2">crop image</div><input type="hidden" name="media_files_id[]" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=3x2" class="img-responsive" /></div>';
				}
				
				$('.gallery-images-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	

	$('.remove-image').click(function(){
		$(this).parents('div').next('.image-container').html('');
	});

	$('.select-documents').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.documents-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose documents', 'loading documents', 'shared/system/window.add.multi.document.php?'+data, 'Cancel', 'Choose Documents', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><input type="hidden" name="document_media_files_id[]" value="'+imgs[i]+'"><img src="elements/icons/icon-pdf.png" class="img-responsive" /></div>';
				}
				
				$('.documents-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});
	
	$('.remove-documents').click(function(){
		$('.documents-container').html('');
	});



	$('.tinymce-inline').tinymce({
		//toolbar: 'link',
		toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink | table",
        plugins: 'link,paste,table',
		menubar: "false",
		height : 200,
		paste_text_sticky : true,
	    paste_use_dialog : false,
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : true,
		paste_remove_styles : true,
		paste_retain_style_properties : "",
		style_formats : [
			{title : 'Headers', items: [
				{title : 'Header 1', block : 'h1'},
				{title : 'Header 2', block : 'h2'},
				{title : 'Header 3', block : 'h3'},
				{title : 'Header 4', block : 'h4'},
				{title : 'Header 5', block : 'h5'},
				{title : 'Header 6', block : 'h6'}
			]},
			{title : 'Inline', items: [
				{title : 'Bold'         , icon : "bold"         , inline : 'strong'},
				{title : 'Italic'       , icon : "italic"       , inline : 'em'},
				{title : 'Underline'    , icon : "underline"    , inline : 'span', styles : {'text-decoration' : 'underline'}},
				{title : 'Strikethrough', icon : "strikethrough", inline : 'span', styles : {'text-decoration' : 'line-through'}},
				{title : 'Superscript'  , icon : "superscript"  , inline : 'sup'},
				{title : 'Subscript'    , icon : "subscript"    , inline : 'sub'},
				{title : 'Code'         , icon : "code"         , inline : 'code'}
			]},
			{title : 'Blocks', items: [
				{title : 'Paragraph' , block : 'p'},
				{title : 'Blockquote', block : 'blockquote'},
				{title : 'Div'       , block : 'div'},
				{title : 'Pre'       , block : 'pre'}
			]},
			{title : 'Alignment', items: [
				{title : 'Left'   , icon : "alignleft"   , block : 'div', styles : {'text-align' : 'left'}},
				{title : 'Center' , icon : "aligncenter" , block : 'div', styles : {'text-align' : 'center'}},
				{title : 'Right'  , icon : "alignright"  , block : 'div', styles : {'text-align' : 'right'}},
				{title : 'Justify', icon : "alignjustify", block : 'div', styles : {'text-align' : 'justify'}}
			]},
			{title : 'Styles', items: [
				{title: 'Call to action', inline: 'span', classes: 'call-to-action'}
			]}
		]
	});


});
</script>	