<?php 
$folder = '';
$prefix = '';
$filename = '';
$options = '';

//options for the cms
$optionsArray['date'] = false;
$optionsArray['category'] = false;
$optionsArray['category_category'] = false;
$optionsArray['heading'] = false;
$optionsArray['short_description'] = false;
$optionsArray['description'] = false;
$optionsArray['link'] = false;
$optionsArray['link_video'] = false;
$optionsArray['image'] = false;
$optionsArray['gallery'] = false;

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		if(isset($optionsArray[$t])){
			$optionsArray[$t] = true;
		}
	}
}
else{
	foreach($optionsArray as $key=>$val){
		$optionsArray[$key] = true;
	}
}

?>
<div class="row">
	<!-- Group 1 -->
	
	<div class="col-xs-18 manage-area">
		<?php if($optionsArray['category'] || $optionsArray['category_category']) { ?>
		<div class="col-xs-8"><strong>Category</strong></div>
		<div class="col-xs-7"><strong>Heading</strong></div>
		<?php } 
		else{ ?>
		<div class="col-xs-15"><strong>Heading</strong></div>
		<?php } ?>

		<div class="col-xs-3"><strong>Tools</strong></div>
		<div class="col-xs-18 managed-content"></div>
	</div><!-- col-xs-18 -->
	
	
	<div class="col-xs-18 edit-area" style="display:none;">
		<div class="col-xs-18" style="border-bottom: 2px solid #fff; margin-bottom:20px;"><a href="#" class="go-back pull-right btn btn-warning btn-bt-margin">Go Back</a></div>
		<div class="col-xs-18 ajax-area"></div>
	</div><!-- col-xs-18 -->
	<!-- Group 1 End -->		
	
</div><!-- row -->

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	var sortableSet = false;
	function goBack(){
		$('.go-back').click(function(){
			$('.edit-area').slideUp(300, function(){ $('.manage-area').slideDown(300); $('.edit-area .ajax-area').html(''); });
			getStory();
		});
		
		return false;
	}
	
	function refreshBindings(){
		$('.tools.edit').unbind();
		$('.tools.edit').bind('click', function(){ editEntry($(this)); });
		
		$('.tools.archive').unbind();
		$('.tools.archive').bind('click', function(){ archiveEntry($(this)); return false; });

		$('.tools.remove').removeClass('hidden').show();
		$('.tools.remove').unbind();
		$('.tools.remove').bind('click', function(){ 
			var $this = $(this);
			changeSystemModal('<span class="text-danger">Confirm Delete</span>', 'Are you sure you want to remove this entry, subcontents will also be removed. This cannot be undone', '', 'Cancel', 'Delete');
			$('#system-modal').modal('show');
			$('.modal-save-btn').unbind();
			$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); removeEntry($this); }); 

			return false;
		});
		
		$('.mod-perm').unbind();
		$('.mod-perm').bind('click', function(){ updatePermission($(this)); return false; });

		$('a.move').removeClass('hidden');

		if(sortableSet){ $('.managed-content').sortable('destroy'); }
		else{ sortableSet = true; }

		$('.managed-content').unbind();
		$('.managed-content').sortable({
			placeholder: "ui-state-highlight",
			handle: "a.move",
			helper: "clone",
			update : function(){
				$.ajax({
					url : 'shared/modules/process/add.<?php echo $filename; ?>.php?a=3&prefix=<?php echo $prefix; ?>',
					beforeSend : function(){
						showLoader();
					},
					type : 'post',
					data : $( ".managed-content" ).sortable( "serialize"),
					complete : function(){
						hideLoader();
					}
				});
			}
		});
	}
	
	function editEntry($this){
		$('.manage-area').slideUp(300, function(){ $('.edit-area').slideDown(300); });
		
		
		$.ajax({
			url : 'shared/modules/responsive/pages/add.<?php echo $filename; ?>.php?<?php if(isset($_GET['test'])){ echo 'test=1&'; } ?>folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename; ?>&options=<?php echo $options; ?>',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'mode=edit&<?php echo $prefix; ?>_id='+$this.parents('div.row').eq(0).attr('<?php echo $prefix; ?>_id'),
			success : function(data){
				$('.edit-area .ajax-area').html(data);
			},
			complete : function(){
				hideLoader();
			}
		});
		
		return false;
	}
	
	function archiveEntry($this){
		$.ajax({
			url : 'shared/modules/process/remove.<?php echo $filename; ?>.php?a=1&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename; ?>&options=<?php echo $options; ?>',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : '<?php echo $prefix; ?>_id='+$this.parents('div.row').eq(0).attr('<?php echo $prefix; ?>_id'),
			success : function(data){
				getStory();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}

	function removeEntry($this){
		$.ajax({
			url : 'shared/modules/process/remove.<?php echo $filename; ?>.php?a=2&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename; ?>&options=<?php echo $options; ?>',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : '<?php echo $prefix; ?>_id='+$this.parents('div.row').eq(0).attr('<?php echo $prefix; ?>_id'),
			success : function(data){
				getStory();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function updatePermission($this){
		$.ajax({
			url : 'shared/modules/process/add.<?php echo $filename; ?>.php?a=3&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename; ?>&options=<?php echo $options; ?>',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'a<?php echo $prefix; ?>_id='+$this.parents('div.row').eq(0).attr('<?php echo $prefix; ?>_id')+'&mod_prefix='+$this.attr('mod-prefix')+'&mod_state='+$this.attr('mod-state'),
			success : function(data){
				getStory();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function getStory(){
		$.ajax({
			url : 'shared/modules/process/get.<?php echo $filename; ?>.php?a=2&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename; ?>&options=<?php echo $options; ?>',
			beforeSend : function(){
				showLoader();
			},
			dataType : 'json',
			success : function(data){
				if(data.status==1){
					$('.managed-content').html(data.data);
				}
				refreshBindings();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});
	}
	
	goBack();
	getStory();
});
</script>