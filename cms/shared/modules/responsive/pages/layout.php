<style type="text/css">
.layout-container{
	margin: 0 -10px;
}
	
.layout-container .row:hover, .layout-container .row.active{
	background-color: #e7e8e9;
	cursor: pointer;
}

.layout-preview{
	padding: 20px 0;
	border: 1px solid #000;
	background-color: #fff;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	margin: 5px;
	text-align: center;

	display: block;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>

<form>
	<div class="container-fluid layout-container">
		<div class="row" data-layout="one-col">
			<div class="col-xs-18"><div class="layout-preview">content</div></div>
		</div>

		<div class="row" data-layout="two-col">
			<div class="col-xs-9"><div class="layout-preview">content</div></div>
			<div class="col-xs-9"><div class="layout-preview">content</div></div>
		</div>

		<div class="row" data-layout="three-col">
			<div class="col-xs-6"><div class="layout-preview">content</div></div>
			<div class="col-xs-6"><div class="layout-preview">content</div></div>
			<div class="col-xs-6"><div class="layout-preview">content</div></div>
		</div>

		<div class="row" data-layout="four-col">
			<div class="col-xs-9" style="padding:0;">
				<div class="col-xs-9"><div class="layout-preview">content</div></div>
				<div class="col-xs-9"><div class="layout-preview">content</div></div>
			</div>
			<div class="col-xs-9" style="padding:0;">
				<div class="col-xs-9"><div class="layout-preview">content</div></div>
				<div class="col-xs-9"><div class="layout-preview">content</div></div>
			</div>
		</div>

		</div><!-- row -->
	</div><!-- container-fluid -->

	<input type="hidden" name="layout-type" value="one-col" />
	<div class="ret-data hidden">{}</div>
</form>

<script type="text/javascript">
$(document).ready(function(){
	$('.layout-container .row').click(function(){
		$('.layout-container .row.active').removeClass('active');
		$(this).addClass('active');
	});

	$('.modal-save-btn').unbind();
	$('.modal-save-btn').click(function(){
		$('.ret-data').html('{"saved":true,"layout":"'+$('.layout-container .row.active').attr('data-layout')+'"}');
		$('#'+$(this).parents('.modal').eq(0).attr('id')).modal('hide');
	});
	
	// console.log($(this).parents('.modal').eq(0).attr('id'));

	// $($(this).parents('.modal').eq(0).attr('id')).find('.modal-save-btn').click(function(){
	// 	$($(this).parents('.modal').eq(0).attr('id')).find('.ret-data').html('{"saved":true,"layout":"'+$('.layout-container .row.active').attr('data-layout')+'"}');
	// 	$(this).parents('.modal').eq(0).attr('id').modal('hide');
	// });
});
</script>