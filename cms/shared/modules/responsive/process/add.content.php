<?php
// function fixJson($json){
// 	$json = json_encode($json);
// 	return $json;
// }

function fixJson($json){
	if( isset($json['content']) && is_array($json['content']) && sizeof($json['content'])>0 ){
		foreach ($json['content'] as $rowKey => $row) {
			if( isset($row) && is_array($row) && sizeof($row)>0 ){
				foreach ($row as $colKey => $col) {
					if( isset($col['data']) && $col['data']!='' ){
						//echo 'found: ['.$rowKey.']['.$colKey.']['.$json[$rowKey][$colKey].']';
						if( !is_array($col['data']) && get_magic_quotes_gpc() ){
							$json['content'][$rowKey][$colKey]['data'] = stripslashes(stripslashes($col['data']));
						}
						else{
							$json['content'][$rowKey][$colKey]['data'] = ($col['data']);
						}
					}
				}
			}
		}
	}

	$json = json_encode($json);
	return $json;
}


$folder = '';
$prefix = '';
$filename = '';
$options = '';

//options for the cms
$optionsArray['date'] = false;
$optionsArray['category'] = false;
$optionsArray['heading'] = false;
$optionsArray['price'] = false;
$optionsArray['short_description'] = false;
$optionsArray['description'] = false;
$optionsArray['link'] = false;
$optionsArray['link_video'] = false;
$optionsArray['image'] = false;
$optionsArray['gallery'] = false;

if(isset($_POST['folder'])){ $folder=$_POST['folder']; }
if(isset($_POST['prefix'])){ $prefix=$_POST['prefix']; }
if(isset($_POST['filename'])){ $filename=$_POST['filename']; }
if(isset($_POST['options'])){ $options=$_POST['options']; }

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		if(isset($optionsArray[$t])){
			$optionsArray[$t] = true;
		}
	}
}
else{
	foreach($optionsArray as $key=>$val){
		$optionsArray[$key] = true;
	}
}

if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	include_once('../../../../includes/pandora/pandora.php');
	include_once('../../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key
	
	switch($_GET['a']){
		case '1' : //add entry
			$json = (isset($_POST['rows']))?$_POST['rows']:'';

			if($prefix == 'blog'){
				if(isset($_POST['inputs'][$prefix.'_special'])){
					$dl->update('mod_'.$prefix, array($prefix.'_special'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_in_the_news'])){
					$dl->update('mod_'.$prefix, array($prefix.'_in_the_news'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_latest_blog'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_blog'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_latest_article'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article'=>0));
				}		

				if(isset($_POST['inputs'][$prefix.'_latest_article_1'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_1'=>0));
				}	
				if(isset($_POST['inputs'][$prefix.'_latest_article_2'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_2'=>0));
				}	
				if(isset($_POST['inputs'][$prefix.'_latest_article_3'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_3'=>0));
				}		

			}

			$dl->insert('mod_'.$prefix.'', array(
				$prefix.'_timestamp'			=> @time(),
				'media_files_id'				=> @($_POST['inputs']['media_file_id']),
				$prefix.'_date'					=> @strtotime($_POST['inputs'][$prefix.'_date']),
				$prefix.'_heading'				=> @stripslashes($_POST['inputs'][$prefix.'_heading']),
				$prefix.'_author'				=> @stripslashes($_POST['inputs'][$prefix.'_author']),
				$prefix.'_slug'					=> @stripslashes($_POST['inputs'][$prefix.'_slug']),
				$prefix.'_price'				=> @stripslashes($_POST['inputs'][$prefix.'_price']),
				$prefix.'_quantity'				=> @stripslashes($_POST['inputs'][$prefix.'_quantity']),
				$prefix.'_code'					=> @stripslashes($_POST['inputs'][$prefix.'_code']),
				$prefix.'_offset'				=> @stripslashes($_POST['inputs'][$prefix.'_offset']),
				$prefix.'_category_id'			=> @stripslashes($_POST['inputs'][$prefix.'_category_id']),
				$prefix.'_short_description'	=> @stripslashes($_POST['inputs'][$prefix.'_short_description']),
				$prefix.'_description'			=> @stripslashes($_POST['inputs'][$prefix.'_description']),
				$prefix.'_special'				=> @((isset($_POST['inputs'][$prefix.'_special']))?1:0),
				$prefix.'_link'					=> @stripslashes($_POST['inputs'][$prefix.'_link']),
				$prefix.'_link_video'			=> @($_POST['inputs'][$prefix.'_link_video']),
				@$prefix.'_json' 				=> @fixJson($json)
			));

			$entry_id = $dl->insert_id;
		

			// if(isset($_POST['inputs']['media_files_id']) && is_array($_POST['inputs']['media_files_id']) && sizeof($_POST['inputs']['media_files_id'])>0){
			// 	foreach($_POST['inputs']['media_files_id'] as $key=>$val){
			// 		$dl->insert('mod_'.$prefix.'_image_link', array(
			// 			$prefix.'_id'				=>$entry_id,
			// 			'media_files_id'			=>$val,
			// 			$prefix.'_media_link_sort'	=>$key
			// 		));
			// 	}
			// }

			$dl->delete('mod_'.$prefix.'_image_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['inputs']['media_files_id']) ){
				$exp = explode(',', $_POST['inputs']['media_files_id']);
				if(  is_array($exp) && sizeof($exp)>0 ){
					foreach($exp as $key=>$val){
						$dl->insert('mod_'.$prefix.'_image_link', array(
							$prefix.'_id'				=>$entry_id,
							'media_files_id'			=>$val,
							$prefix.'_media_link_sort'	=>$key
						));
					}
				}
			}

			$dl->delete('mod_'.$prefix.'_document_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['inputs']['document_media_files_id']) ){
				$exp = explode(',', $_POST['inputs']['document_media_files_id']);
				if(  is_array($exp) && sizeof($exp)>0 ){
					foreach($exp as $key=>$val){
						$dl->insert('mod_'.$prefix.'_document_link', array(
							$prefix.'_id'				=>$entry_id,
							'media_files_id'			=>$val,
							$prefix.'_media_link_sort'	=>$key
						));
					}
				}
			}

			if(isset($_POST['inputs']['categories_id']) && is_array($_POST['inputs']['categories_id']) && sizeof($_POST['inputs']['categories_id'])>0){
				$dl->delete('mod_product_category_link', 'product_id="'.$entry_id.'"');
				foreach($_POST['inputs']['categories_id'] as $key=>$val){
					$dl->insert('mod_product_category_link', array(
						'product_id'				=>$entry_id,
						'category_id'				=>$val
					));
				}
			}

			if(isset($_POST['inputs']['blog_in_the_news'])){
				$dl->update('mod_blog', array(
					'blog_in_the_news'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['inputs']['blog_latest_blog'])){
				$dl->update('mod_blog', array(
					'blog_latest_blog'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['inputs']['blog_latest_article'])){
				$dl->update('mod_blog', array(
					'blog_latest_article'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}


			if(isset($_POST['inputs']['blog_latest_article_1'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			if(isset($_POST['inputs']['blog_latest_article_2'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			if(isset($_POST['inputs']['blog_latest_article_3'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			
			echo json_encode(array('success'=>1));

		break;
		
		case '2' : //edit entry
			$json = (isset($_POST['rows']))?$_POST['rows']:'';

			if($prefix == 'blog'){
				if(isset($_POST['inputs'][$prefix.'_special'])){
					$dl->update('mod_'.$prefix, array($prefix.'_special'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_in_the_news'])){
					$dl->update('mod_'.$prefix, array($prefix.'_in_the_news'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_latest_blog'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_blog'=>0));
				}	
				
				if(isset($_POST['inputs'][$prefix.'_latest_article'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article'=>0));
				}	

				if(isset($_POST['inputs'][$prefix.'_latest_article_1'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_1'=>0));
				}	
				if(isset($_POST['inputs'][$prefix.'_latest_article_2'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_2'=>0));
				}	
				if(isset($_POST['inputs'][$prefix.'_latest_article_3'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_3'=>0));
				}		
			}
			
			$dl->update('mod_'.$prefix.'', array(
				$prefix.'_timestamp'			=> @time(),
				'media_files_id'				=> @($_POST['inputs']['media_file_id']),
				$prefix.'_date'					=> @strtotime($_POST['inputs'][$prefix.'_date']),
				$prefix.'_heading'				=> @stripslashes($_POST['inputs'][$prefix.'_heading']),
				$prefix.'_author'				=> @stripslashes($_POST['inputs'][$prefix.'_author']),
				$prefix.'_slug'					=> @stripslashes($_POST['inputs'][$prefix.'_slug']),
				$prefix.'_price'				=> @stripslashes($_POST['inputs'][$prefix.'_price']),
				$prefix.'_quantity'				=> @stripslashes($_POST['inputs'][$prefix.'_quantity']),
				$prefix.'_code'					=> @stripslashes($_POST['inputs'][$prefix.'_code']),
				$prefix.'_offset'				=> @stripslashes($_POST['inputs'][$prefix.'_offset']),
				$prefix.'_category_id'			=> @stripslashes($_POST['inputs'][$prefix.'_category_id']),
				$prefix.'_short_description'	=> @stripslashes($_POST['inputs'][$prefix.'_short_description']),
				$prefix.'_description'			=> @stripslashes($_POST['inputs'][$prefix.'_description']),
				$prefix.'_special'				=> @((isset($_POST['inputs'][$prefix.'_special']))?1:0),
				$prefix.'_link'					=> @stripslashes($_POST['inputs'][$prefix.'_link']),
				$prefix.'_link_video'			=> @($_POST['inputs'][$prefix.'_link_video']),
				@$prefix.'_json' 				=> @fixJson($json)
			), $prefix.'_id="'.$_POST['inputs'][$prefix.'_id'].'"');

			$entry_id = $_POST['inputs'][$prefix.'_id'];


			// $dl->delete('mod_'.$prefix.'_image_link', $prefix.'_id="'.$entry_id.'"');
			// if(isset($_POST['inputs']['media_files_id']) && is_array($_POST['inputs']['media_files_id']) && sizeof($_POST['inputs']['media_files_id'])>0){
			// 	foreach($_POST['inputs']['media_files_id'] as $key=>$val){
			// 		$dl->insert('mod_'.$prefix.'_image_link', array(
			// 			$prefix.'_id'				=>$entry_id,
			// 			'media_files_id'			=>$val,
			// 			$prefix.'_media_link_sort'	=>$key
			// 		));
			// 	}
			// }

			$dl->delete('mod_'.$prefix.'_image_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['inputs']['media_files_id']) ){
				$exp = explode(',', $_POST['inputs']['media_files_id']);
				if(  is_array($exp) && sizeof($exp)>0 ){
					foreach($exp as $key=>$val){
						$dl->insert('mod_'.$prefix.'_image_link', array(
							$prefix.'_id'				=>$entry_id,
							'media_files_id'			=>$val,
							$prefix.'_media_link_sort'	=>$key
						));
					}
				}
			}

			if(isset($_POST['inputs']['categories_id']) && is_array($_POST['inputs']['categories_id']) && sizeof($_POST['inputs']['categories_id'])>0){
				$dl->delete('mod_product_category_link', 'product_id="'.$entry_id.'"');
				foreach($_POST['inputs']['categories_id'] as $key=>$val){
					$dl->insert('mod_product_category_link', array(
						'product_id'				=>$entry_id,
						'category_id'				=>$val
					));
				}
			}

			$dl->delete('mod_'.$prefix.'_document_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['inputs']['document_media_files_id']) ){
				$exp = explode(',', $_POST['inputs']['document_media_files_id']);
				if(  is_array($exp) && sizeof($exp)>0 ){
					foreach($exp as $key=>$val){
						$dl->insert('mod_'.$prefix.'_document_link', array(
							$prefix.'_id'				=>$entry_id,
							'media_files_id'			=>$val,
							$prefix.'_media_link_sort'	=>$key
						));
					}
				}
			}

			if(isset($_POST['inputs']['blog_in_the_news'])){
				$dl->update('mod_blog', array(
					'blog_in_the_news'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['inputs']['blog_latest_blog'])){
				$dl->update('mod_blog', array(
					'blog_latest_blog'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['inputs']['blog_latest_article'])){
				$dl->update('mod_blog', array(
					'blog_latest_article'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}


			if(isset($_POST['inputs']['blog_latest_article_1'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}
			
			if(isset($_POST['inputs']['blog_latest_article_2'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}

			if(isset($_POST['inputs']['blog_latest_article_3'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}

			echo json_encode(array('success'=>1));

		break;			

		case '3' : //update sort
			foreach($_POST[$_POST['prefix'].'_id'] as $key=>$val){
				$dl->update('mod_'.$_POST['prefix'], array(
					$_POST['prefix'].'_sort'=>$key
				), $_POST['prefix'].'_id='.$val);
			}
		break;
	}
}