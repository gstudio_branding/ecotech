<?php
function fixJson($json){

	echo '<pre>';
	print_r($json);
	echo '</pre>';

	if( isset($json['content']) && is_array($json['content']) && sizeof($json['content'])>0 ){
		foreach ($json['content'] as $rowKey => $row) {
			if( isset($row) && is_array($row) && sizeof($row)>0 ){
				foreach ($row as $colKey => $col) {
					if( isset($col['data']) && $col['data']!='' ){
						//echo 'found: ['.$rowKey.']['.$colKey.']['.$json[$rowKey][$colKey].']';
						if( !is_array($col['data']) && get_magic_quotes_gpc() ){
							$json['content'][$rowKey][$colKey]['data'] = stripslashes(stripslashes($col['data']));
						}
						else{
							$json['content'][$rowKey][$colKey]['data'] = ($col['data']);
						}
					}
				}
			}
		}
	}
	echo '<hr>';
	echo '<pre>';
	print_r($json);
	echo '</pre>';

	$json = json_encode($json);
	return $json;
}
$json = (isset($_POST['rows']))?$_POST['rows']:'';
fixJson($json);