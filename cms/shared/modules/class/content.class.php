<?php 
class Content { 

	private $dl;
	public $webRoot;
	public $dbIncludePath;

	public $prefix;

	public function __construct($dbIncludePath='../../../includes/dbal/dlinc.php', $webRoot='', $prefix="") {

		if(is_object($dbIncludePath)){
			$this->dl = $dbIncludePath;
		}		
		else{
			$this->dbIncludePath = $dbIncludePath;
			$this->connectDB();
		}
		
        $this->webRoot = $webRoot;
        $this->prefix = $prefix;
    }   

    private function connectDB(){
		if(!is_object($this->dl)){ //if the connection hasnt already been made, make connection
			include_once($this->dbIncludePath);
			$this->dl = new DataLayer();
			$this->dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
			$this->dl->debug = false;
		}	
	}

	function getDl(){
		return $this->dl;
	}

	function getHTMLIntro(){
		$intro = $this->dl->select('mod_'.$this->prefix.'_introduction', $this->prefix.'_introduction_archived=0');
		if($this->dl->totalrows>0){ $intro = $intro[0][$this->prefix.'_introduction_description']; }
		else{ $intro = ''; }
		return $intro;
	}

	function getCategoriesHTML($layout=1, $imgWidth='', $imgHeight='', $page=''){
		if($page==''){ $page = $this->prefix.'-info'; }
		$ret = '';

		$cats = $this->dl->select('mod_'.$this->prefix.'_category', $this->prefix.'_category_archived=0');
		if($this->dl->totalrows>0){
			foreach($cats as $c){

				switch($layout){
					case 1 : 
						$ret .= 
						'<div class="project-entry col-md-4 col-sm-6">
							<h3 class="project-heading">'.$c[$this->prefix.'_category_heading'].'</h3>
							<ul>
								<li>
									<a href="'.$this->webRoot.'pages/'.$page.'.php?cat_id='.$c[$this->prefix.'_category_id'].'">Read more</a>
								</li>
							</ul>					
		    			</div>';
					break;

					case 2 :
						$ret .= 
						'<div class="icon client col-md-3 col-sm-4 col-xs-12">
							<a href="'.$this->webRoot.'pages/'.$page.'.php?cat_id='.$c[$this->prefix.'_category_id'].'">
								<div class="icon_image">
									<img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$c['media_files_id'].(($imgWidth!='')?'&width='.$imgWidth.'':'').(($imgHeight!='')?'&height='.$imgHeight.'':'').'">
								</div>'.$c[$this->prefix.'_category_heading'].'
							</a>
						</div>';
					break;

					case 3 :
						$ret .= 
						'<div class="icon client col-md-3 col-sm-4 col-xs-12">
							<a href="'.$this->webRoot.'pages/'.$page.'.php?cat_id='.$c[$this->prefix.'_category_id'].'">
								<div class="icon_image">
									<img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$c['media_files_id'].(($imgWidth!='')?'&width='.$imgWidth.'':'').(($imgHeight!='')?'&height='.$imgHeight.'':'').'">
								</div>
							</a>
						</div>';
					break;
				}
			}
		}

		return $ret;
	}

	function getClientCategoriesHTML($layout=1, $imgWidth='', $imgHeight='', $page=''){
		if($page==''){ $page = $this->prefix.'-info'; }
		$ret = '';

		$cats = $this->dl->select('mod_'.$this->prefix.'_category', $this->prefix.'_category_archived=0');
		if($this->dl->totalrows>0){
			foreach($cats as $c){

				switch($layout){
					case 1 : 
						$ret .= 
						'<div class="project-entry col-md-4 col-sm-6">
							<h3 class="project-heading">'.$c[$this->prefix.'_category_heading'].'</h3>
							<ul>
								<li>
									<a href="'.$this->webRoot.'pages/'.$page.'.php?cat_id='.$c[$this->prefix.'_category_id'].'">Read more</a>
								</li>
							</ul>					
		    			</div>';
					break;

					case 2 :
						$ret .= 
						'<div class="icon client col-md-3 col-sm-4 col-xs-12">
							<a href="'.$c[$this->prefix.'_category_link'].'" target="_blank">
								<div class="icon_image">
									<img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$c['media_files_id'].(($imgWidth!='')?'&width='.$imgWidth.'':'').(($imgHeight!='')?'&height='.$imgHeight.'':'').'">
								</div>'.$c[$this->prefix.'_category_heading'].'
							</a>
						</div>';
					break;
				}
			}
		}

		return $ret;
	}

	function getContentHTMLByID($id='', $showImages=false, $imgWidth='', $imgHeight=''){
		if($id!=''){
			$ret = '';

			//$content = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_category_id='.$cat_id.' AND '.$this->prefix.'_archived=0');
			$content = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_id='.$id.' AND '.$this->prefix.'_archived=0');
			if($this->dl->totalrows>0){
				foreach($content as $c){
					$ret .= 
					'<h2 class="page-heading">'.$c[$this->prefix.'_heading'].'</h2>
					<div class="cols">'.$c[$this->prefix.'_description'].'</div>';

					if($showImages){
						$images = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id='.$c[$this->prefix.'_id']);
						if($this->dl->totalrows>0){
							$ret .= '<div id="gallery">';
							foreach($images as $i){
								
								$ret .= '<div class="col-xs-4"><a href="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$i['media_files_id'].'" class="fancy-image" rel="'.$c[$this->prefix.'_id'].'"><img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$i['media_files_id'].'&aspect_r=4x3'.(($imgWidth!='')?'&width='.$imgWidth.'':'').(($imgHeight!='')?'&height='.$imgHeight.'':'').'" class="img-responsive" style="margin-bottom:20px;"></a></div>';
							}
							$ret .= '</div>';
						}
					}
				}
			}
			else{
				$ret = 'No content found';
			}

			return $ret;
		}
		else{
			return 'Category not set';
		}
	}

	function getContentHTML($cat_id='', $showImages=false, $imgWidth='', $imgHeight=''){
		if($cat_id!=''){
			$ret = '';

			$content = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_category_id='.$cat_id.' AND '.$this->prefix.'_archived=0');
			if($this->dl->totalrows>0){
				foreach($content as $c){
					$ret .= 
					'<h2 class="page-heading">'.$c[$this->prefix.'_heading'].'</h2>
					<div class="cols">'.$c[$this->prefix.'_description'].'</div>';

					if($showImages){
						$images = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id='.$c[$this->prefix.'_id']);
						if($this->dl->totalrows>0){
							$ret .= '<div id="gallery">';
							foreach($images as $i){
								
								$ret .= '<div class="col-xs-4"><img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$i['media_files_id'].'&aspect_r=4x3'.(($imgWidth!='')?'&width='.$imgWidth.'':'').(($imgHeight!='')?'&height='.$imgHeight.'':'').'" class="img-responsive"></div>';
							}
							$ret .= '</div>';
						}
					}
				}
			}
			else{
				$ret = 'No content found';
			}

			return $ret;
		}
		else{
			return 'Category not set';
		}
	}

} 

// $entry = new Content('../../../includes/dbal/dlinc.php', '../../../../');
// echo $entry->getHTMLEntries();
?>