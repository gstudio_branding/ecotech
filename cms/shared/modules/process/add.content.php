<?php
$folder = '';
$prefix = '';
$filename = '';
$options = '';

//options for the cms
$optionsArray['date'] = false;
$optionsArray['category'] = false;
$optionsArray['heading'] = false;
$optionsArray['price'] = false;
$optionsArray['short_description'] = false;
$optionsArray['description'] = false;
$optionsArray['link'] = false;
$optionsArray['link_video'] = false;
$optionsArray['image'] = false;
$optionsArray['gallery'] = false;

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		if(isset($optionsArray[$t])){
			$optionsArray[$t] = true;
		}
	}
}
else{
	foreach($optionsArray as $key=>$val){
		$optionsArray[$key] = true;
	}
}

if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = true;
	
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key
	
	switch($_GET['a']){
		case '1' : //add entry

			if($prefix == 'blog'){
				if(isset($_POST[$prefix.'_special'])){
					$dl->update('mod_'.$prefix, array($prefix.'_special'=>0));
				}	
				
				if(isset($_POST[$prefix.'_in_the_news'])){
					$dl->update('mod_'.$prefix, array($prefix.'_in_the_news'=>0));
				}	
				
				if(isset($_POST[$prefix.'_latest_blog'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_blog'=>0));
				}	
				
				if(isset($_POST[$prefix.'_latest_article'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article'=>0));
				}		

				if(isset($_POST[$prefix.'_latest_article_1'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_1'=>0));
				}	
				if(isset($_POST[$prefix.'_latest_article_2'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_2'=>0));
				}	
				if(isset($_POST[$prefix.'_latest_article_3'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_3'=>0));
				}		

			}

			$dl->insert('mod_'.$prefix.'', array(
				$prefix.'_timestamp'			=>	time(),
				'media_files_id'				=>	($_POST['media_file_id']),
				$prefix.'_date'					=>	strtotime($_POST[$prefix.'_date']),
				$prefix.'_heading'				=>	stripslashes($_POST[$prefix.'_heading']),
				$prefix.'_author'				=>	stripslashes($_POST[$prefix.'_author']),
				$prefix.'_slug'				=>	stripslashes($_POST[$prefix.'_slug']),
				$prefix.'_price'				=>	stripslashes($_POST[$prefix.'_price']),
				$prefix.'_quantity'				=>	stripslashes($_POST[$prefix.'_quantity']),
				$prefix.'_code'					=>	stripslashes($_POST[$prefix.'_code']),
				$prefix.'_offset'				=>	stripslashes($_POST[$prefix.'_offset']),
				$prefix.'_category_id'			=>	stripslashes($_POST[$prefix.'_category_id']),
				$prefix.'_short_description'	=>	stripslashes($_POST[$prefix.'_short_description']),
				$prefix.'_description'			=>	stripslashes($_POST[$prefix.'_description']),
				$prefix.'_special'				=>	((isset($_POST[$prefix.'_special']))?1:0),
				$prefix.'_link'					=>	stripslashes($_POST[$prefix.'_link']),
				$prefix.'_link_video'			=>	($_POST[$prefix.'_link_video'])
			));

			$entry_id = $dl->insert_id;
		

			if(isset($_POST['media_files_id']) && is_array($_POST['media_files_id']) && sizeof($_POST['media_files_id'])>0){
				foreach($_POST['media_files_id'] as $key=>$val){
					$dl->insert('mod_'.$prefix.'_image_link', array(
						$prefix.'_id'				=>$entry_id,
						'media_files_id'			=>$val,
						$prefix.'_media_link_sort'	=>$key
					));
				}
			}

			if(isset($_POST['document_media_files_id']) && is_array($_POST['document_media_files_id']) && sizeof($_POST['document_media_files_id'])>0){
				foreach($_POST['document_media_files_id'] as $key=>$val){
					$dl->insert('mod_'.$prefix.'_document_link', array(
						$prefix.'_id'				=>$entry_id,
						'media_files_id'			=>$val,
						$prefix.'_media_link_sort'	=>$key
					));
				}
			}

			if(isset($_POST['categories_id']) && is_array($_POST['categories_id']) && sizeof($_POST['categories_id'])>0){
				$dl->delete('mod_product_category_link', 'product_id="'.$entry_id.'"');
				foreach($_POST['categories_id'] as $key=>$val){
					$dl->insert('mod_product_category_link', array(
						'product_id'				=>$entry_id,
						'category_id'				=>$val
					));
				}
			}

			if(isset($_POST['blog_in_the_news'])){
				$dl->update('mod_blog', array(
					'blog_in_the_news'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['blog_latest_blog'])){
				$dl->update('mod_blog', array(
					'blog_latest_blog'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['blog_latest_article'])){
				$dl->update('mod_blog', array(
					'blog_latest_article'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}


			if(isset($_POST['blog_latest_article_1'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			if(isset($_POST['blog_latest_article_2'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			if(isset($_POST['blog_latest_article_3'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			
		break;
		
		case '2' : //edit entry

			if($prefix == 'blog'){
				if(isset($_POST[$prefix.'_special'])){
					$dl->update('mod_'.$prefix, array($prefix.'_special'=>0));
				}	
				
				if(isset($_POST[$prefix.'_in_the_news'])){
					$dl->update('mod_'.$prefix, array($prefix.'_in_the_news'=>0));
				}	
				
				if(isset($_POST[$prefix.'_latest_blog'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_blog'=>0));
				}	
				
				if(isset($_POST[$prefix.'_latest_article'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article'=>0));
				}	

				if(isset($_POST[$prefix.'_latest_article_1'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_1'=>0));
				}	
				if(isset($_POST[$prefix.'_latest_article_2'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_2'=>0));
				}	
				if(isset($_POST[$prefix.'_latest_article_3'])){
					$dl->update('mod_'.$prefix, array($prefix.'_latest_article_3'=>0));
				}		
			}
			
			$dl->update('mod_'.$prefix.'', array(
				$prefix.'_timestamp'			=>	time(),
				'media_files_id'				=>	($_POST['media_file_id']),
				$prefix.'_date'					=>	strtotime($_POST[$prefix.'_date']),
				$prefix.'_heading'				=>	stripslashes($_POST[$prefix.'_heading']),
				$prefix.'_author'				=>	stripslashes($_POST[$prefix.'_author']),
				$prefix.'_slug'					=>	stripslashes($_POST[$prefix.'_slug']),
				$prefix.'_price'				=>	stripslashes($_POST[$prefix.'_price']),
				$prefix.'_quantity'				=>	stripslashes($_POST[$prefix.'_quantity']),
				$prefix.'_code'					=>	stripslashes($_POST[$prefix.'_code']),
				$prefix.'_offset'				=>	stripslashes($_POST[$prefix.'_offset']),
				$prefix.'_category_id'			=>	stripslashes($_POST[$prefix.'_category_id']),
				$prefix.'_short_description'	=>	stripslashes($_POST[$prefix.'_short_description']),
				$prefix.'_description'			=>	stripslashes($_POST[$prefix.'_description']),
				$prefix.'_special'				=>	((isset($_POST[$prefix.'_special']))?1:0),
				$prefix.'_link'					=>	stripslashes($_POST[$prefix.'_link']),
				$prefix.'_link_video'			=>	($_POST[$prefix.'_link_video'])
			), $prefix.'_id="'.$_POST[$prefix.'_id'].'"');

			$entry_id = $_POST[$prefix.'_id'];


			$dl->delete('mod_'.$prefix.'_image_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['media_files_id']) && is_array($_POST['media_files_id']) && sizeof($_POST['media_files_id'])>0){
				foreach($_POST['media_files_id'] as $key=>$val){
					$dl->insert('mod_'.$prefix.'_image_link', array(
						$prefix.'_id'				=>$entry_id,
						'media_files_id'			=>$val,
						$prefix.'_media_link_sort'	=>$key
					));
				}
			}

			if(isset($_POST['categories_id']) && is_array($_POST['categories_id']) && sizeof($_POST['categories_id'])>0){
				$dl->delete('mod_product_category_link', 'product_id="'.$entry_id.'"');
				foreach($_POST['categories_id'] as $key=>$val){
					$dl->insert('mod_product_category_link', array(
						'product_id'				=>$entry_id,
						'category_id'				=>$val
					));
				}
			}

			$dl->delete('mod_'.$prefix.'_document_link', $prefix.'_id="'.$entry_id.'"');
			if(isset($_POST['document_media_files_id']) && is_array($_POST['document_media_files_id']) && sizeof($_POST['document_media_files_id'])>0){
				foreach($_POST['document_media_files_id'] as $key=>$val){
					$dl->insert('mod_'.$prefix.'_document_link', array(
						$prefix.'_id'				=>$entry_id,
						'media_files_id'			=>$val,
						$prefix.'_media_link_sort'	=>$key
					));
				}
			}

			if(isset($_POST['blog_in_the_news'])){
				$dl->update('mod_blog', array(
					'blog_in_the_news'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['blog_latest_blog'])){
				$dl->update('mod_blog', array(
					'blog_latest_blog'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}

			if(isset($_POST['blog_latest_article'])){
				$dl->update('mod_blog', array(
					'blog_latest_article'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}


			if(isset($_POST['blog_latest_article_1'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_1'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}
			
			if(isset($_POST['blog_latest_article_2'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_2'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}

			if(isset($_POST['blog_latest_article_3'])){
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 1
				), 'blog_id="'.$entry_id.'"');
			}
			else{
				$dl->update('mod_blog', array(
					'blog_latest_article_3'	=> 0
				), 'blog_id="'.$entry_id.'"');	
			}


		break;			

		case '3' : //update sort
			foreach($_POST[$_GET['prefix'].'_id'] as $key=>$val){
				$dl->update('mod_'.$_GET['prefix'], array(
					$_GET['prefix'].'_sort'=>$key
				), $_GET['prefix'].'_id='.$val);
			}
		break;
	}
}