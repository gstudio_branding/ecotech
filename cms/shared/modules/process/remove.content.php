<?php
$folder = '';
$prefix = '';
$filename = '';

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }

$table = 'mod_'.$prefix;

if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = true;
	
	switch($_GET['a']){
		case '1' : //delete staff
			if(isset($_POST[$prefix.'_id']) && $_POST[$prefix.'_id']!=''){ 
				$story = $dl->select($table, $prefix.'_id='.$_POST[$prefix.'_id']); 
				if($story[0][$prefix.'_archived']==0){
					$dl->update($table, array($prefix.'_archived' => 1), $prefix.'_id='.$_POST[$prefix.'_id']); 
				}
				else{
					$dl->update($table, array($prefix.'_archived' => 0), $prefix.'_id='.$_POST[$prefix.'_id']); 
				}
				
			}
		break;

		case '2' : //delete
			if(isset($_POST[$prefix.'_id']) && $_POST[$prefix.'_id']!=''){ 
				$story = $dl->select($table, $prefix.'_id='.$_POST[$prefix.'_id']); 
				
				if( strpos($prefix, '_category') !== false ){ //check if there is content associated with the cateogry
					$contentPrefix = str_replace('_category', '', $prefix);
					//remove associated content
					$dl->delete('mod_'.$contentPrefix, $prefix.'_id='.$_POST[$prefix.'_id']); 

					//remove document links
					$dl->delete('mod_'.$prefix.'_document_link', $prefix.'_id='.$_POST[$prefix.'_id']); 

					//remove image links
					$dl->delete('mod_'.$prefix.'_image_link', $prefix.'_id='.$_POST[$prefix.'_id']); 
				}

				//remove entry
				$dl->delete($table, $prefix.'_id='.$_POST[$prefix.'_id']); 
				
			}
		break;
	}
	
}