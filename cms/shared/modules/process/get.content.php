<?php
session_start();

$folder = '';
$prefix = '';
$filename = '';
$options = '';

//options for the cms
$optionsArray['date'] = false;
$optionsArray['category'] = false;
$optionsArray['heading'] = false;
$optionsArray['price'] = false;
$optionsArray['short_description'] = false;
$optionsArray['description'] = false;
$optionsArray['link'] = false;
$optionsArray['link_video'] = false;
$optionsArray['image'] = false;
$optionsArray['gallery'] = false;

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		if(isset($optionsArray[$t])){
			$optionsArray[$t] = true;
		}
	}
}
else{
	foreach($optionsArray as $key=>$val){
		$optionsArray[$key] = true;
	}
}


if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	switch($_GET['a']){
		case '1' : //
		break;

		case '2' : //get story content
			include_once('../../../includes/settings.php');

			$msg = '';
			$status = 0;
			$ret = '';
			
			if($optionsArray['category']) {
				if($prefix=='blog'){
					$story = $dl->select('mod_'.$prefix.' AS content LEFT JOIN mod_'.$prefix.'_category AS category ON content.'.$prefix.'_category_id=category.'.$prefix.'_category_id', '', $prefix.'_sort ASC');
				}
				else{
					$story = $dl->select('mod_'.$prefix.' AS content LEFT JOIN mod_'.$prefix.'_category AS category ON content.'.$prefix.'_category_id=category.'.$prefix.'_category_id', '', $prefix.'_date ASC');
				}

			}
			else{
				$story = $dl->select('mod_'.$prefix, '', $prefix.'_date ASC');
			}

			
			if($dl->totalrows>0){
				$status = 1;
				foreach($story as $s){
					$ret .= '
					<div class="row'.(($s[$prefix.'_archived']==0)?'':' bg-danger').'" '.$prefix.'_id="'.$s[$prefix.'_id'].'" id="'.$prefix.'_id_'.$s[$prefix.'_id'].'" style="padding-top:10px;">';

					if($optionsArray['category']) {
					$ret .=	'<div class="col-xs-8">'.($s[$prefix.'_category_heading']).'</div>
					<div class="col-xs-7">'.($s[$prefix.'_heading']).'</div>';
					} 
					else{
					$ret .=	'<div class="col-xs-15">'.($s[$prefix.'_heading']).'</div>';
					}


					$ret .=	'<div class="col-xs-3">
							<a href="#" class="tools edit"><span class="glyphicon glyphicon-pencil"></span></a>
							<a href="#" class="tools archive"><span class="glyphicon glyphicon-briefcase"></span></a>
							<a href="#" class="tools remove"><span class="glyphicon glyphicon-trash"></span></a>
							<a href="#" class="tools move hidden"><span class="glyphicon glyphicon-move"></span></a>
						</div>
						<div class="col-xs-18" style="border-bottom:1px solid #ccc; margin-top:10px;"></div>
					</div>';
				}
			}
			else{
				$status = 2;
				$ret = '';
			}
			
			echo json_encode(array('status'=>$status, 'msg'=>$msg, 'data'=>utf8_encode($ret)));
			
		break;

	}
	
}