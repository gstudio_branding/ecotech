<?php 
$folder = '';
$prefix = '';
$filename = '';

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }


$mode = 'add';

if(isset($_POST['mode']) && $_POST['mode']!=''){
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('mod_'.$prefix, $prefix.'_id="'.$_POST[$prefix.'_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}
?>

<form role="form" class="add-entry">
	<div class="row">
		<!-- Group 1 -->
        
        <div class="col-xs-4">
			Heading
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_heading" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_heading']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->

		<!-- Group 1 End -->		
		
		<?php if($mode=='edit'){ ?>
		<input type="hidden" name="<?php echo $prefix; ?>_id" class="form-control input-sm" value="<?php echo $entry[$prefix.'_id']; ?>">
		<?php } ?>
		
	</div><!-- row -->
	
	<button type="submit" class="btn btn-default">Submit</button>
</form>

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	$('.add-entry').submit(function(){
		
		<?php if($mode=='add'){ ?>
			var url = 'shared/modules/process/add.<?php echo $filename; ?>.php?a=1&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename?>',
				msg = 'Your content has been added';
		<?php } ?>
		<?php if($mode=='edit'){ ?>
			var url = 'shared/modules/process/add.<?php echo $filename; ?>.php?a=2&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename?>',
				msg = 'Your content has been updated';
		<?php } ?>
		
		$.ajax({
			url : url,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
				changeSystemModal('<span class="text-success">Success!</span>', msg, '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
				<?php if($mode=='edit'){ ?>
				$('.go-back').trigger('click');
				<?php } ?>
			}
		});
		
		return false;
	});

	var today = new Date();
	var month = '';
	
	//set the date to today
	<?php if($mode=='add'){ ?>
	switch(today.getMonth()){
		case 0 : month ='January'; break;
		case 1 : month ='February'; break;
		case 2 : month ='March'; break;
		case 3 : month ='April'; break;
		case 4 : month ='May'; break;
		case 5 : month ='June'; break;
		case 6 : month ='July'; break;
		case 7 : month ='August'; break;
		case 8 : month ='September'; break;
		case 9 : month ='October'; break;
		case 10 : month ='November'; break;
		case 11 : month ='December'; break;
	}
	var date = today.getDate() + ' ' +month+ ' ' +today.getFullYear();
	$('.date').val(date);
	<?php } 
	else if($mode=='edit'){ ?>
	<?php } ?>

	$('.date').datepicker({
		dateFormat: "dd MM yy"
	});

	$('.tinymce').tinymce({
		//toolbar: 'link',
		toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink",
        plugins: 'link,paste',
		menubar: "false",
		height : 400,
		paste_text_sticky : true,
	    paste_use_dialog : false,
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : true,
		paste_remove_styles : true,
		paste_retain_style_properties : ""
	});


	function refreshBindings(){
		$('.image-container').unbind();
		$('.image-container').sortable({
			helper: "clone"
		});
		
		$('.crop-image').unbind();
		$('.crop-image').click(function(){
			var $this = $(this);
			
			changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-container').eq(0).find('input').val()+'&aspect_r='+$this.attr('aspect_r'), 'Cancel', 'Crop Image', function(){  }, function(){ 
				var src = $this.parents('div.img-container').eq(0).find('img').attr('src'),
					timestamp = Math.round(new Date().getTime() / 1000);
				
				$this.parents('div.img-container').eq(0).find('img').attr('src', src+'&timestamp='+timestamp);
			});
			$('#system-modal').modal('show');
		});
	
	}
	refreshBindings();

	$('.select-pillar').click(function(){
		var $this = $(this);
		var data = $('.add-entry').find('[name="pillar_id[]"]').serialize();

		changeSystemModal('choose objectives', 'loading pillars', 'shared/modules/window.add.pillars.php?'+data, 'Cancel', 'Choose objectives', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					var img_src = imgs[i].split(':');

					img_html += '<div class="col-xs-2 img-container"><input type="hidden" name="pillar_id[]" value="'+img_src[1]+'"><img src="'+img_src[0]+'" class="img-responsive" /></div>';
				}

				$('.pillar-container .pillars').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});


	$('.select-foundation').click(function(){
		var $this = $(this);
		var data = $('.add-entry').find('[name="foundation_id"]').serialize();

		changeSystemModal('choose programmes', 'loading foundations/icons', 'shared/modules/window.add.foundations.php?'+data, 'Cancel', 'Choose programmes/icon', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					var img_src = imgs[i].split(':');

					img_html += '<div class="col-xs-2 img-container"><input type="hidden" name="foundation_id" value="'+img_src[1]+'"><img src="'+img_src[0]+'" class="img-responsive" /></div>';
				}

				$('.foundation-container .foundations').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});


	$('.select-images').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.image-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.multi.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="4x3">crop image</div><input type="hidden" name="media_files_id[]" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=4x3" class="img-responsive" /></div>';
				}
				
				$('.image-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.remove-pillar').click(function(){
		$('.pillar-container .pillars').html('');
	});
	$('.remove-foundation').click(function(){
		$('.foundation-container .foundations').html('');
	});
	$('.remove-images').click(function(){
		$('.image-container').html('');
	});
	
});
</script>