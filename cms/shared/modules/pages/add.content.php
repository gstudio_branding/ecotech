<?php 
$folder = '';
$prefix = '';
$filename = '';
$options = '';

//options for the cms
$optionsArray['date'] = false;
$optionsArray['category'] = false;
$optionsArray['heading'] = false;
$optionsArray['short_description'] = false;
$optionsArray['description'] = false;
$optionsArray['link'] = false;
$optionsArray['link_video'] = false;
$optionsArray['image'] = false;
$optionsArray['gallery'] = false;
$optionsArray['documents'] = false;

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

if($options!=''){
	$temp = explode(',', $options);
	foreach($temp as $t){
		if(isset($optionsArray[$t])){
			$optionsArray[$t] = true;
		}
	}
}
else{
	foreach($optionsArray as $key=>$val){
		$optionsArray[$key] = true;
	}
}



$mode = 'add';

if(isset($_POST['mode']) && $_POST['mode']!=''){
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('mod_'.$prefix, $prefix.'_id="'.$_POST[$prefix.'_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}

if(is_array($optionsArray) && (!isset($dl))){
	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
}
?>

<form role="form" class="add-entry">
	<div class="row">
		<?php if( $optionsArray['date'] ) { ?>
		<!-- Group 1 -->
		<div class="col-xs-4">
			Date
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_date" class="form-control input-sm date" value="<?php if($mode=='edit' && $entry[$prefix.'_date']!=0 && $entry[$prefix.'_date']!=''){ echo date('d F Y', $entry[$prefix.'_date']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>
        

		<?php if( $optionsArray['category'] ) { ?>
		<div class="col-xs-4">
			Category
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<select name="<?php echo $prefix; ?>_category_id" class="form-control">
					<?php 
					$cats = $dl->select('mod_'.$prefix.'_category');
					if($cats>0){
						foreach($cats as $c){
							if($mode=='edit' && $c[$prefix.'_category_id']==$entry[$prefix.'_category_id']){
								?><option value="<?php echo $c[$prefix.'_category_id']; ?>" selected="selected"><?php echo $c[$prefix.'_category_heading']; ?></option><?php 
							}
							else{
								?><option value="<?php echo $c[$prefix.'_category_id']; ?>"><?php echo $c[$prefix.'_category_heading']; ?></option><?php 
							}
						}
					}
					else{
						?><option value="0">There are no categories</option><?php 
					}
					?>
				</select>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
        <?php } ?>


        <?php if( $optionsArray['heading'] ) { ?>
        <div class="col-xs-4">
			Heading
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_heading" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_heading']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( $optionsArray['short_description'] ) { ?>
        <div class="col-xs-4">
			Short Intro
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_short_description" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_short_description']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( $optionsArray['description'] ) { ?>
		<div class="col-xs-4">
			Article
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<textarea name="<?php echo $prefix; ?>_description" class="form-control input-sm tinymce"><?php if($mode=='edit'){ echo $entry[$prefix.'_description']; } ?></textarea>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		<?php } ?>

        
        <?php if( $optionsArray['link'] ) { ?>
        <div class="col-xs-4">
			Link
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="<?php echo $prefix; ?>_link" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry[$prefix.'_link']); } ?>">
			</div><!-- form-group -->

		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( $optionsArray['link_video'] ) { ?>
        <div class="col-xs-4">
			Youtube/Video Link 
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="checkbox" name="<?php echo $prefix; ?>_link_video" class="" <?php if($mode=='edit' && $entry[$prefix.'_link_video']=="1"){ echo 'checked="checked"'; } ?> value="1">
			</div><!-- form-group -->

		</div><!-- col-xs-14 -->
		<?php } ?>


		<?php if( $optionsArray['image'] ) { ?>
		<div class="col-xs-4">
			image
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-image" style="margin-top:10px;">select image</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-image" style="margin-top:10px;">remove image</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 image-container">
			<?php 
			if($mode=='edit' && $entry['media_files_id']!="" && $entry['media_files_id']!="0"){ 
				?>
				<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="1x1">crop image</div><input type="hidden" name="media_file_id" value="<?php echo $entry['media_files_id']; ?>"><img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $entry['media_files_id']; ?>&width=200&aspect_r=1x1" class="img-responsive" /></div><?php
			}
			?>
		</div><!-- col-xs-18 -->
		<?php } ?>

			
		<?php if( $optionsArray['gallery'] ) { ?>
		<div class="col-xs-4">
			Gallery
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-images" style="margin-top:10px;">select images</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-images" style="margin-top:10px;">remove images</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 gallery-images-container">
			<?php 
			if($mode=='edit'){ 
				$media_files = $dl->select('mod_'.$prefix.'_image_link', $prefix.'_id="'.$_POST[$prefix.'_id'].'"', $prefix.'_media_link_sort'.' ASC');
				if($dl->totalrows>0){
					foreach ($media_files as $m){
						?><div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="4x3">crop image</div><input type="hidden" name="media_files_id[]" value="<?php echo $m['media_files_id']; ?>"><img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $m['media_files_id']; ?>&width=200&aspect_r=4x3" class="img-responsive" /></div><?php
					}
				}
			?>
			
			<?php } ?>
		</div><!-- col-xs-18 -->
		<?php } ?>


		<?php if( $optionsArray['documents'] ) { ?>
		<div class="col-xs-4">
			Documents
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-documents" style="margin-top:10px;">select documents</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-documents" style="margin-top:10px;">remove documents</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 documents-container">
			<?php 
			if($mode=='edit'){ 
				$media_files = $dl->select('mod_'.$prefix.'_document_link', $prefix.'_id="'.$_POST[$prefix.'_id'].'"', $prefix.'_media_link_sort'.' ASC');
				if($dl->totalrows>0){
					foreach ($media_files as $m){
						?><div class="col-xs-3 img-container"><input type="hidden" name="document_media_files_id[]" value="<?php echo $m['media_files_id']; ?>"><img src="elements/icons/icon-pdf.png" class="img-responsive" /></div><?php
					}
				}
			?>
			
			<?php } ?>
		</div><!-- col-xs-18 -->
		<?php } ?>


		<!-- Group 1 End -->		
		
		<?php if($mode=='edit'){ ?>
		<input type="hidden" name="<?php echo $prefix; ?>_id" class="form-control input-sm" value="<?php echo $entry[$prefix.'_id']; ?>">
		<?php } ?>
		
	</div><!-- row -->
	
	<button type="submit" class="btn btn-default">Submit</button>
</form>

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	$('.add-entry').submit(function(){
		
		<?php if($mode=='add'){ ?>
			var url = 'shared/modules/process/add.<?php echo $filename; ?>.php?a=1&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename?>',
				msg = 'Your content has been added';
		<?php } ?>
		<?php if($mode=='edit'){ ?>
			var url = 'shared/modules/process/add.<?php echo $filename; ?>.php?a=2&folder=<?php echo $folder; ?>&prefix=<?php echo $prefix; ?>&filename=<?php echo $filename?>',
				msg = 'Your content has been updated';
		<?php } ?>
		
		$.ajax({
			url : url,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
				changeSystemModal('<span class="text-success">Success!</span>', msg, '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
				<?php if($mode=='edit'){ ?>
				$('.go-back').trigger('click');
				<?php }
				else{ ?>
				$('input,textarea').val('');
				$('input:checkbox').attr('checked',false);
				$('.image-container, .gallery-images-container, .documents-container').html('');
				<?php } ?>
			}
		});
		
		return false;
	});

	var today = new Date();
	var month = '';
	
	//set the date to today
	<?php if($mode=='add'){ ?>
	switch(today.getMonth()){
		case 0 : month ='January'; break;
		case 1 : month ='February'; break;
		case 2 : month ='March'; break;
		case 3 : month ='April'; break;
		case 4 : month ='May'; break;
		case 5 : month ='June'; break;
		case 6 : month ='July'; break;
		case 7 : month ='August'; break;
		case 8 : month ='September'; break;
		case 9 : month ='October'; break;
		case 10 : month ='November'; break;
		case 11 : month ='December'; break;
	}
	var date = today.getDate() + ' ' +month+ ' ' +today.getFullYear();
	$('.date').val(date);
	<?php } 
	else if($mode=='edit'){ ?>
	<?php } ?>

	$('.date').datepicker({
		dateFormat: "dd MM yy"
	});

	$('.tinymce').tinymce({
		//toolbar: 'link',
		toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink",
        plugins: 'link,paste',
		menubar: "false",
		height : 400,
		paste_text_sticky : true,
	    paste_use_dialog : false,
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : true,
		paste_remove_styles : true,
		paste_retain_style_properties : ""
	});

	function refreshBindings(){
		$('.gallery-images-container').unbind();
		$('.gallery-images-container').sortable({
			helper: "clone"
		});
		
		$('.crop-image').unbind();
		$('.crop-image').click(function(){
			var $this = $(this);
			
			changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-container').eq(0).find('input').val()+'&aspect_r='+$this.attr('aspect_r'), 'Cancel', 'Crop Image', function(){  }, function(){ 
				var src = $this.parents('div.img-container').eq(0).find('img').attr('src'),
					timestamp = Math.round(new Date().getTime() / 1000);
				
				$this.parents('div.img-container').eq(0).find('img').attr('src', src+'&timestamp='+timestamp);
			});
			$('#system-modal').modal('show');
		});
	
	}
	refreshBindings();	

	$('.select-image').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.image-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="1x1">crop image</div><input type="hidden" name="media_file_id" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=1x1" class="img-responsive" /></div>';
				}
				
				$('.image-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.select-images').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.gallery-images-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.multi.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="4x3">crop image</div><input type="hidden" name="media_files_id[]" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=4x3" class="img-responsive" /></div>';
				}
				
				$('.gallery-images-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.select-documents').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.documents-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose documents', 'loading documents', 'shared/system/window.add.multi.document.php?'+data, 'Cancel', 'Choose Documents', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><input type="hidden" name="document_media_files_id[]" value="'+imgs[i]+'"><img src="elements/icons/icon-pdf.png" class="img-responsive" /></div>';
				}
				
				$('.documents-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});
	

	$('.remove-image').click(function(){
		$('.image-container').html('');
	});
	$('.remove-images').click(function(){
		$('.gallery-images-container').html('');
	});
	$('.remove-documents').click(function(){
		$('.documents-container').html('');
	});
	
});
</script>