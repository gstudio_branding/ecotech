<?php session_start(); 
	
include_once('../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>
<div class="row media-container">
	<?php
	$media = $dl->select('cms_media_files', 'media_files_type=2', 'media_files_timestamp DESC');
	$selected_images = array();
	
	if(isset($_GET['document_media_files_id']) && $_GET['document_media_files_id']!=''){
		foreach($_GET['document_media_files_id'] as $m){ array_push($selected_images, $m); }
	}

	if($dl->totalrows>0){
		foreach($media as $m){
			$isSelected = false;
			if(is_array($selected_images) && sizeof($selected_images)>0){
				if(array_search($m['media_files_id'], $selected_images)!==FALSE){ $isSelected = true; }
			}
			?>
			<div class="col-xs-3 img-container <?php if($isSelected){ ?>img-selected<?php } ?>" media_files_id="<?php echo $m['media_files_id']; ?>">
				<div class="tick-icon"><span class="glyphicon glyphicon-ok"></span></div>
				<img src="elements/icons/icon-pdf.png" class="img-responsive" />

				<div class="title-container"><?php echo $m['media_files_original_name'] ?></div>

				<div></div>
			</div><!-- col-xs-3 img-container -->

			<?php
		}
	}
	else{
		?>
		<div class="col-xs-18">
			<p>There are no documents, please upload first.</p>
		</div><!-- col-xs-3 img-container -->
		<?php
	}
	?>
</div>
<div class="ret-data"></div>

<style>
.title-container{
	font-size:10px; 
	position:absolute; 
	bottom:0; 
	width:100%; 
	background-color:#fff; 
	opacity:0.8; 
	-ms-word-break: break-all; 
	word-break: break-all; 
	word-break: break-word; 
	padding:5px;
}

.tick-icon{
	display: none;
	position: absolute;
	top: 5px;
	right: 15px;
	font-size: 30px;
	text-shadow: -1px 0 #fff, 0 1px #fff, 1px 0 #fff, 0 -1px #fff;
}

.img-selected .tick-icon{
	display: block;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('.img-container').click(function(){
		if($(this).hasClass('img-selected')){ $(this).removeClass('img-selected'); }
		else{ $(this).addClass('img-selected'); }
	});
	
	$('.modal-save-btn').click(function(){
		var imgs = del = '';
		$('.img-selected').each(function(){
			imgs += del+$(this).attr('media_files_id');
			del = ',';
		});
		
		$('.ret-data').html(imgs);
		$('#system-modal').modal('hide');
	});
});
</script>