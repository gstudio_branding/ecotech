<?php session_start(); 
	
include_once('../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>

<style type="text/css">
.folder{
	/*border: 2px solid #000;
	padding:10px;
	margin:10px 0;*/
	margin-right:0px;
	margin-left:0px;
	margin-bottom: 10px;

	display: block;
    padding: 6px 12px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

.folder .folder{
	border-right: none;
	border-left: none;
}

.sub-folders{
	display:none;
}
</style>

<div class="row media-container">
	<?php
	
	//$media = $dl->select('cms_media_files', '', 'media_files_timestamp DESC');


	function getFolders($dl, $folder_id=0){
		$temp_arr = array();
		$folder_arr = array(
			'media_folder_id'=>'',
			'media_folder_name'=>'',
			'folders'=>array()
		);

		$folders = $dl->select('cms_media_folder', 'media_folder_link="'.$folder_id.'"');
		if($dl->totalrows>0){
			foreach($folders as $f){
				$folder_arr['media_folder_id'] = $folder_arr['media_folder_name'] = $folder_arr['folders'] = '';

				$folder_arr['media_folder_id'] = $f['media_folder_id'];
				$folder_arr['media_folder_name'] = $f['media_folder_name'];
				
				$temp = getFolders($dl, $f['media_folder_id']);
				if($temp != false){
					$folder_arr['folders'] = $temp;
				}

				array_push($temp_arr, $folder_arr);
			}
			
			return $temp_arr;
		}
		else{
			return false;
		}
	}

	function getImages($dl, $folder_id=0){
		$folder_str = '';

		$where = 'mf.media_files_type=1 AND mfl.media_folder_id="'.$folder_id.'"';
		if($folder_id == '-1'){
			$where = $del = '';
			$all_folders = $dl->select('cms_media_folder_link');

			if($dl->totalrows > 0){
				foreach($all_folders as $f){
					$where .= $del.'mfl.media_files_id!='.$f['media_files_id'];
					$del = ' AND ';
				}
			}
		}

		$media = $dl->select('cms_media_folder_link AS mfl LEFT JOIN cms_media_files AS mf ON mfl.media_files_id=mf.media_files_id', $where, 'mf.media_files_timestamp DESC');

		if($dl->totalrows>0){
			foreach($media as $m){
				$folder_str .= '
				<div class="col-xs-3 img-container" media_files_id="'.$m['media_files_id'].'">
					<div class="tick-icon"><span class="glyphicon glyphicon-ok"></span></div>
					<img src="modules/media/scripts/image/image.handler.php?media_files_id='.$m['media_files_id'].'&width=200&aspect_r=1x1" class="img-responsive" />

					<div class="title-container">'.$m['media_files_original_name'].'</div>
					<div></div>
				</div><!-- col-xs-3 img-container -->';
			}
		}

		return $folder_str;
	}

	function buildFolders($dl, $folder_id=0){
		$folders = $dl->select('cms_media_folder', 'media_folder_link="'.$folder_id.'" AND media_folder_name!=""', 'media_folder_name ASC');
		$folder_str = '';

		if($dl->totalrows>0){
			foreach($folders as $f){
				$folder_str .= '<div data-folder-id="'.$f['media_folder_id'].'" class="folder row">'.$f['media_folder_name'].'<div class="sub-folders col-xs-18">';

				$folder_str .= getImages($dl, $f['media_folder_id']);

				$folder_str .= buildFolders($dl, $f['media_folder_id']);
				$folder_str .= '</div></div>';
			}
			
			return $folder_str;
		}
		else{
			return '';
		}
	}

	echo buildFolders($dl);
	echo getImages($dl);

	/*
	if($dl->totalrows>0){
		foreach($media as $m){
			?>
			<div class="col-xs-3 img-container" media_files_id="<?php echo $m['media_files_id']; ?>">
				<div class="tick-icon"><span class="glyphicon glyphicon-ok"></span></div>
				<img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $m['media_files_id']; ?>&width=200&aspect_r=1x1" class="img-responsive" />

				<div class="title-container"><?php echo $m['media_files_original_name']; ?></div>
				<div></div>
			</div><!-- col-xs-3 img-container -->


			<?php
		}
	}
	else{
		?>
		<div class="col-xs-18">
			<p>There is no media, please upload first.</p>
		</div><!-- col-xs-3 img-container -->
		<?php
	}
	*/
	?>
</div>
<div class="ret-data"></div>

<style>
.title-container{
	font-size:10px; 
	position:absolute; 
	bottom:0; 
	width:100%; 
	background-color:#fff; 
	opacity:0.8; 
	-ms-word-break: break-all; 
	word-break: break-all; 
	word-break: break-word; 
	padding:5px;
}
.tick-icon{
	display: none;
	position: absolute;
	top: 5px;
	right: 15px;
	font-size: 30px;
	text-shadow: -1px 0 #fff, 0 1px #fff, 1px 0 #fff, 0 -1px #fff;
}

.img-selected .tick-icon{
	display: block;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('.img-container').click(function(){
		$('.img-selected').removeClass('img-selected');
		$(this).addClass('img-selected');
		return false;
	});
	
	$('.modal-save-btn').unbind();
	$('.modal-save-btn').click(function(){
		$('.ret-data').html($('.img-selected').attr('media_files_id'));
		$('#'+$(this).parents('.modal').eq(0).attr('id')).modal('hide');
	});

	$('.folder').click(function(){
		$(this).children('.sub-folders').slideToggle();
		return false;
	});

});
</script>