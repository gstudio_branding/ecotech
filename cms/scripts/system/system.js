/* system js */

var confirmMainNavigate = false;

function init(){
	refreshSystemBindings();
	resizeSideNav();
	
	$(window).on('orientationchange, resize', function(){
		resizeSideNav();
	});
}

function refreshSystemBindings(){
	/* kills and rebinds all system related bindings affected by ajax */
	$('.nav-item>a, .subnav-item>a').unbind();
	
	$('.nav-item>a, .subnav-item>a').bind('click', function(){
		if(confirmMainNavigate && 1==2){
			$this = $(this);
			changeSystemModal('Confirm Navigation', 'Any unsaved changes will be lost if you navigate away.', '', 'Cancel', 'Continue', function(){}, function(){});
			$('#system-modal').modal('show');
			$('.modal-save-btn').unbind();
			$('.modal-save-btn').click(function(){
				confirmMainNavigate = false;
				$this.trigger('click');
				$('#system-modal').modal('hide'); 
			});
		}
		else{
			loadMenuItem($(this));
			if($(this).parents('div').eq(0).hasClass('subnav-item')){ $('.subnav-item').find('.btn-primary').removeClass('btn-primary'); }
			else{ $('.nav-item').find('.btn-primary').removeClass('btn-primary'); }
			$(this).addClass('btn-primary');
		}

		return false; //dont navigate away
	});
}

function loadMenuItem($this){
	var href = $this.attr('href'),
		ajaxLocation = $('.'+$this.attr('data-location'));
	
	if(href!='' && href!='#'){
		$.ajax({
			url : href,
			beforeSend : function(){
				showLoader();
			},
			success : function(data){
				ajaxLocation.html(data);
			},
			complete : function(){
				hideLoader();
				refreshSystemBindings();
				resizeSideNav();
			}
		});
	}
	else{
		alert('The menu item is still under construction');
	}
}

function changeSystemModal(title, body, url, closeText, saveText, loadedFunc, closedFunc){
	var modalContainer = $('#system-modal'),
		titleContainer = modalContainer.find('.modal-title'),
		bodyContainer = modalContainer.find('.modal-body');
	
	titleContainer.html(title);
	bodyContainer.html(body);
	
	if(url!='' && url!=undefined){
		$.ajax({
			url : url,
			success : function(data){
				bodyContainer.html(data);
			},
			complete : function(){
				if($.isFunction(loadedFunc)){
					loadedFunc();
				}
			}
		});
	}
	
	if(closeText!=''){ modalContainer.find('.modal-close-btn').html(closeText).show(); }
	else{ modalContainer.find('.modal-close-btn').hide(); }
	
	if(saveText!=''){ modalContainer.find('.modal-save-btn').html(saveText).show(); }
	else{ modalContainer.find('.modal-save-btn').hide(); }
	
	modalContainer.unbind();
	modalContainer.on('hidden.bs.modal', function (e) {
		if($.isFunction(closedFunc)){
			closedFunc();
		}
	});
}

function changeSystemModal2(title, body, url, closeText, saveText, loadedFunc, closedFunc){
	var modalContainer = $('#system-modal-2'),
		titleContainer = modalContainer.find('.modal-title'),
		bodyContainer = modalContainer.find('.modal-body');
	
	titleContainer.html(title);
	bodyContainer.html(body);
	
	if(url!='' && url!=undefined){
		$.ajax({
			url : url,
			success : function(data){
				bodyContainer.html(data);
			},
			complete : function(){
				if($.isFunction(loadedFunc)){
					loadedFunc();
				}
			}
		});
	}
	
	if(closeText!=''){ modalContainer.find('.modal-close-btn').html(closeText).show(); }
	else{ modalContainer.find('.modal-close-btn').hide(); }
	
	if(saveText!=''){ modalContainer.find('.modal-save-btn').html(saveText).show(); }
	else{ modalContainer.find('.modal-save-btn').hide(); }
	
	modalContainer.unbind();
	modalContainer.on('hidden.bs.modal', function (e) {
		if($.isFunction(closedFunc)){
			closedFunc();
		}
	});
}


function showLoader(){ $('#loader').modal('show'); }
function hideLoader(){ $('#loader').modal('hide'); $('.modal-backdrop').hide(); resizeSideNav(); }

$('#system-modal, #loader').modal({
	show : false,
	backdrop : 'static' //stops the modal from closing on click (stops loader + cropper closing)
});

function resizeSideNav(){

	$('.sidenav').css({ 
		'height': 'auto'
	});

	var height = $(window).outerHeight(),
		margin = 0;
	
	if($('.nav-item-content').outerHeight() > height){ height = $('.nav-item-content').outerHeight(); }
	
	$('.sidenav').css({ 
		'height': (height+margin)+'px'
	});
}

//start the system
init();

//stop enter in modals
$(document).keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13' && $('.modal').css('display')=='block'){
		return false;
	}
});