function strEncode(str) {
	$.ajax({
		url : 'shared/modules/responsive/process/functions.php?a=1',
		type : 'post',
		async : false,
		data : 'str='+str,
		success : function(data){ str = data; }
	});

	return str;
}
function strDecode(str) {
	$.ajax({
	 	url : 'shared/modules/responsive/process/functions.php?a=2',
	 	type : 'post',
	 	async : false,
	 	data : 'str='+str,
	 	success : function(data){ str = data; }
	 });

	 return str;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}
function reverseAll(str, find, replaceWith) {
	// while(str.indexOf(find)>0){
	// 	str = str.replace(find, replaceWith);
	// }
	// return str;
}

var cmsOptions = {};

cmsOptions = {
	// Input : {
	// 	template : '<div class="col-xs-4">[*title*]</div><div class="col-xs-18"><div class="form-group"><input type="text" name="[*name*]" class="form-control input-sm [*class*]" value="[*value*]"></div></div>'
	// },
	// Select : {
	// 	template : '<div class="col-xs-4">[*title*]<div><div class="col-xs-18"><div class="form-group"><select name="[*name*]" class="form-control [*class*]">[*value*]</select></div></div>'
	// },
	Text : {
		template : '<form class="text-area-form"><div class="col-xs-4">[*title*]</div><div class="col-xs-18 [*class*]"><div class="form-group"><textarea class="form-control input-sm tinymce" id="tinymce-modal" name="text-area-form">[*value*]</textarea></div></div></form>',
		functions : function(){ richtextBinding(); }
	},
	// Checkbox : {
	// 	template : '<div class="col-xs-4">[*title*]</div><div class="col-xs-18"><div class="form-group"><input type="checkbox" name="[*name*]" class="[*class*]" value="[*value*]" [*checked*]></div></div>'
	// },

	/*
	<div class="col-xs-4">[*title*]</div>
	<div class="col-xs-18">
		<div class="btn btn-primary btn-xs btn-bt-margin select-image" style="margin-top:10px;">select image</div>
		<div class="btn btn-primary btn-xs btn-bt-margin remove-image" style="margin-top:10px;">remove image</div>
	
		aspect ratio
		width
		quality
	
		<div class="col-xs-18">Image Properties</div>
		<div class="col-xs-18 col-md-9">
			<div class="col-xs-18 col-md-9">
					Aspect Ratio
			</div>
			<div class="col-xs-18 col-md-9">
				<div class="form-group">
					<select name="aspect_r" class="form-control">
						<option value="1x1">1x1</option>
						<option value="2x1">2x1</option>
						<option value="4x3">4x3</option>
						<option value="16x10">16x10</option>
						<option value="1x1">1x1</option>
						<option value="1x2">1x2</option>
						<option value="3x4">3x4</option>
						<option value="10x16">10x16</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-xs-18 col-md-9">
			<div class="col-xs-18 col-md-9">
					Image width <small>(leave blank for 100%)</small>
			</div>
			<div class="col-xs-18 col-md-9">
				<div class="form-group">
					<input type="text" name="abc_short_description" class="form-control input-sm" value="">
				</div>
			</div>
		</div>
		<div class="col-xs-18 col-md-9">
			<div class="col-xs-18 col-md-9">
					Quality
			</div>
			<div class="col-xs-18 col-md-9">
				<div class="form-group">
					<input type="text" name="abc_short_description" class="form-control input-sm" value="">
				</div>
			</div>
		</div>

	</div>
	<div class="col-xs-18 image-container">[*value*]</div>
	*/


	Image : {
		//template : '<div class="col-xs-4">[*title*]</div><div class="col-xs-18"><div class="btn btn-primary btn-xs btn-bt-margin select-image" style="margin-top:10px;">select image</div><div class="btn btn-primary btn-xs btn-bt-margin remove-image" style="margin-top:10px;">remove image</div></div><div class="col-xs-18 image-container">[*value*]</div>',
		template : '<div class="row"><div class="spacer20"><hr></div><div class="col-xs-8"><div class="col-xs-18 image-container">[*value*]</div></div>	<div class="col-xs-10 image-property-container"><div class="btn btn-primary btn-bt-margin select-image">select image</div> <!-- <div class="btn btn-primary btn-bt-margin crop-image" aspect_r="1x1">crop image</div> --> <div class="btn btn-primary btn-bt-margin remove-image">remove image</div><div class="col-xs-18">Image Properties</div><div class="col-xs-18"><div class="col-xs-18 col-md-9">Aspect Ratio</div><div class="col-xs-18 col-md-9"><div class="form-group"><select name="image-aspect_r" class="form-control"><option value="0">Original</option><option value="1x1">1x1</option><option value="2x1">2x1</option><option value="4x3">4x3</option><option value="16x10">16x10</option><option value="1x2">1x2</option><option value="3x4">3x4</option><option value="10x16">10x16</option></select></div></div><div class="col-xs-18 col-md-9">Image width <small>(leave blank for 100%)</small></div><div class="col-xs-18 col-md-9"><div class="form-group"><input type="text" name="image-width" class="form-control input-sm" value="600"></div></div><div class="col-xs-18 col-md-9">Quality</div><div class="col-xs-18 col-md-9"><div class="form-group"><input type="text" name="image-quality" class="form-control input-sm" value="100"></div></div></div></div></div>',
		functions : function(){ imageBinding(); }
	}//,
	// Gallery : {
	// 	template : '<div class="col-xs-4">[*title*]</div><div class="col-xs-18"><div class="btn btn-primary btn-xs btn-bt-margin select-images" style="margin-top:10px;">select images</div><div class="btn btn-primary btn-xs btn-bt-margin remove-images" style="margin-top:10px;">remove images</div></div><div class="col-xs-18 gallery-images-container">			[*value*]</div>',
	// 	functions : function(){ galleryBinding(); }
	// },
	// Document : {
	// 	template : '<div class="col-xs-4">[*title*]</div><div class="col-xs-18"><div class="btn btn-primary btn-xs btn-bt-margin select-documents" style="margin-top:10px;">select documents</div><div class="btn btn-primary btn-xs btn-bt-margin remove-documents" style="margin-top:10px;">remove documents</div></div><div class="col-xs-18 documents-container">[*value*]</div>',
	// 	functions : function(){ documentBinding(); }
	// }
}


function refreshBindings(){
	$('.gallery-images-container').unbind();
	$('.gallery-images-container').sortable({
		helper: "clone"
	});
	
	$('.crop-image').unbind();
	$('.crop-image').click(function(){
		var $this = $(this);
		
		changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-container').eq(0).find('input').val()+'&aspect_r='+$this.attr('aspect_r'), 'Cancel', 'Crop Image', function(){  }, function(){ 
			var src = $this.parents('div.img-container').eq(0).find('img').attr('src'),
				timestamp = Math.round(new Date().getTime() / 1000);
			
			$this.parents('div.img-container').eq(0).find('img').attr('src', src+'&timestamp='+timestamp);
		});
		$('#system-modal').modal('show');
	});

	$('.image-property-container').find('[name="image-aspect_r"]').unbind();
	$('.image-property-container').find('[name="image-width"]').unbind();
	$('.image-property-container').find('[name="image-quality"]').unbind();

	$('.image-property-container').find('[name="image-aspect_r"]').change(function(){ updateImageData(''); });
	$('.image-property-container').find('[name="image-width"]').change(function(){ updateImageData(''); });
	$('.image-property-container').find('[name="image-quality"]').change(function(){ updateImageData(''); });

}

function updateImageData(imgs){
	if(imgs!=''){
		imgId = imgs.split(',');
		imgId = imgId[0];
	}
	else{
		var temp = $('#system-modal .image-container, #system-modal-2 .image-container').html(),
			temp2 = temp,
			offsetStart = 0,
			offsetEnd = 0;

			offsetStart = temp.indexOf('media_files_id=')+15;
			temp2.substring(offsetStart);
			offsetEnd = temp2.indexOf('&');

		imgId = temp.substring(offsetStart, offsetEnd);
	}
	
	var propContainer = $('.image-property-container'),
		aspectR = propContainer.find('[name="image-aspect_r"]').val(),
		width = propContainer.find('[name="image-width"]').val(),
		quality = propContainer.find('[name="image-quality"]').val(),
		img_html = '';

	if(width == "" || parseInt(width)<0 ){ width = 1000; }
	if(quality == "" || parseInt(quality)<0 || parseInt(quality)>100){ quality = 100; }

	img_html += '<div class="col-xs-18 img-container"><input type="hidden" name="media_file_id" value="'+imgId+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgId+'&width='+width;
		if(aspectR!=0){ img_html += '&aspect_r='+aspectR; }
		img_html += '&quality='+quality+'" class="img-responsive" /></div>';

	var html = '<div class="col-xs-18 img-container"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgId+'&width='+width;
	if(aspectR!=0){ html += '&aspect_r='+aspectR; }
		html += '&quality='+quality+'" class="img-responsive" /></div>';

	passedInformation = { 
		type : 'image',
		html : html, 
		data : {
			id : imgId,
			width : width,
			quality : quality,
			aspect_r : aspectR
		} 
	};

	$('#system-modal .image-container, #system-modal-2 .image-container').html(img_html);
	refreshBindings();
}

function imageBinding(){
	$('.select-image').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.image-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		$('.ret-data').html('');

		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){ updateImageData(imgs); }
			$('#system-modal-2').css('z-index', 10000);
			
		});
		$('#system-modal-2').css('z-index', 0);
		$('#system-modal').modal('show');
	});

	$('#system-modal .remove-image, #system-modal-2 .remove-image').click(function(){
		$('#system-modal .image-container, #system-modal-2 .image-container').html('');
		passedInformation = { 
			html : '',
			data : 'empty' 
		};
	});

	if( passedInformation!=undefined ){

		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.image.php', 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){ updateImageData(imgs); }
			$('#system-modal-2').css('z-index', 10000);
			
		});

		if('data' in passedInformation){
			if('data' in passedInformation.data){
				var propContainer = $('.image-property-container');

				if('aspect_r' in passedInformation.data.data){ propContainer.find('[name="image-aspect_r"]').val(passedInformation.data.data.aspect_r); }
				if('width' in passedInformation.data.data){ propContainer.find('[name="image-width"]').val(passedInformation.data.data.width); }
				if('quality' in passedInformation.data.data){ propContainer.find('[name="image-quality"]').val(passedInformation.data.data.quality); }

				if('id' in passedInformation.data.data){ updateImageData(passedInformation.data.data.id); }
			}
		}
		//passedInformation = {};	
	}
}

function galleryBinding(){
	$('.select-images').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.gallery-images-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.multi.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="4x3">crop image</div><input type="hidden" name="media_files_id[]" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=4x3" class="img-responsive" /></div>';
				}
				
				$('.gallery-images-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.remove-images').click(function(){
		$('.gallery-images-container').html('');
	});
}

function documentBinding(){
	$('.select-documents').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.documents-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose documents', 'loading documents', 'shared/system/window.add.multi.document.php?'+data, 'Cancel', 'Choose Documents', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><input type="hidden" name="document_media_files_id[]" value="'+imgs[i]+'"><img src="elements/icons/icon-pdf.png" class="img-responsive" /></div>';
				}
				
				$('.documents-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.remove-documents').click(function(){
		$('.documents-container').html('');
	});
}

function richtextBinding(){
	$('.tinymce').html(passedInformation.html);
	var tinyMCESettings = {
		toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | strikethrough superscript subscript | hr | bullist numlist | outdent indent | link unlink pdflink imglink | table | code",
        plugins: 'link,paste,table,hr,code',
		menubar: "false",
		height : 400,
		paste_text_sticky : true,
	    paste_use_dialog : false,
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : true,
		paste_remove_styles : true,
		paste_retain_style_properties : "",
		style_formats : [
			{title : 'Headers', items: [
				{title : 'Header 1', block : 'h1'},
				{title : 'Header 2', block : 'h2'},
				{title : 'Header 3', block : 'h3'},
				{title : 'Header 4', block : 'h4'},
				{title : 'Header 5', block : 'h5'},
				{title : 'Header 6', block : 'h6'}
			]},
			{title : 'Inline', items: [
				{title : 'Bold'         , icon : "bold"         , inline : 'strong'},
				{title : 'Italic'       , icon : "italic"       , inline : 'em'},
				{title : 'Underline'    , icon : "underline"    , inline : 'span', styles : {'text-decoration' : 'underline'}},
				{title : 'Strikethrough', icon : "strikethrough", inline : 'span', styles : {'text-decoration' : 'line-through'}},
				{title : 'Superscript'  , icon : "superscript"  , inline : 'sup'},
				{title : 'Subscript'    , icon : "subscript"    , inline : 'sub'},
				{title : 'Code'         , icon : "code"         , inline : 'code'}
			]},
			{title : 'Blocks', items: [
				{title : 'Paragraph' , block : 'p'},
				{title : 'Blockquote', block : 'blockquote'},
				{title : 'Div'       , block : 'div'},
				{title : 'Pre'       , block : 'pre'}
			]},
			{title : 'Alignment', items: [
				{title : 'Left'   , icon : "alignleft"   , block : 'div', styles : {'text-align' : 'left'}},
				{title : 'Center' , icon : "aligncenter" , block : 'div', styles : {'text-align' : 'center'}},
				{title : 'Right'  , icon : "alignright"  , block : 'div', styles : {'text-align' : 'right'}},
				{title : 'Justify', icon : "alignjustify", block : 'div', styles : {'text-align' : 'justify'}}
			]},
			{title : 'Styles', items: [
				{title: 'Call to action', block: 'div', classes: 'call-to-action', styles: { 'background-color': '#fbbf0f', color: '#fff', 'padding' : '10px', 'text-transform':'uppercase', 'display':'inline-block' } },
				{title: 'Yellow text', inline: 'span', classes: 'text-yellow', styles: { color: '#fbbf0f' } },
			]}
		],
		setup : function(ed) {
	        ed.addButton('pdflink', {
	            title : 'Link PDF',
	            image : 'scripts/tinymce/themes/icon-pdf.png',
	            onclick : function() {
	    			// $('#system-modal').find('.modal-save-btn').click(function(){
					// 	console.log('asd');
					// 	return false;
					// });

	            	changeSystemModal('choose document', 'loading documents', 'shared/system/window.add.multi.document.php', 'Cancel', 'Insert Document', function(){  }, function(){
						$('#system-modal-2').css('z-index', 1050);	
						
						ed.focus();
	                	ed.selection.setContent('<a href="[basepath]cms/modules/media/scripts/documents/document.handler.php?media_files_id='+$('#system-modal').find('.ret-data').html()+'">Download PDF</a>');
					});
					$('#system-modal-2').css('z-index', 0);
					$('#system-modal').modal('show');
	            }
	        });

	        ed.addButton('imglink', {
	            title : 'Insert Image',
	            image : 'scripts/tinymce/themes/icon-img.png',
	            onclick : function() {
	            	changeSystemModal('choose document', 'loading images', 'shared/system/window.add.image.php', 'Cancel', 'Insert Image', function(){  }, function(){
						$('#system-modal-2').css('z-index', 1050);	
						
						var imageID = $('#system-modal').find('.ret-data').html();
						ed.focus();
	                	
	                	if(imageID!='' && imageID!='0'  && imageID!=undefined){
	                		ed.selection.setContent('<img src="modules/media/scripts/image/image.handler.php?media_files_id='+imageID+'&width=1080" class="img-responsive" />');
	                	}

	                	rebindContentManagement();
					});
					$('#system-modal-2').css('z-index', 0);
					$('#system-modal').modal('show');
	            }
	        });
	    }
	};

	if($('#tinymce-modal_ifr').length>0){ $(".tinymce").tinymce().remove(); }
	$('.tinymce').tinymce(tinyMCESettings);
}