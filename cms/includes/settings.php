<?php
	$client = 'Ecotech';
	$logo = '../elements/ecotech-logo.png';

	$version = '1.0';
	
	$modules = array(
		array(
			'mod'=>'Admin',
			'path'=>'modules/admin/index.php',
			'prefix'=>'admin'
		),
		array(
			'mod'=>'Media',
			'path'=>'modules/media/index.php',
			'prefix'=>'media'
		),
		array(
			'mod'=>'Pages',
			'path'=>'modules/pages/index.php',
			'prefix'=>'page'
		),
		// array(
		// 	'mod'=>'Services',
		// 	'path'=>'modules/services/index.php',
		// 	'prefix'=>'service'
		// )
	);
?>