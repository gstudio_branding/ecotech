<?php 

class pandora {

	#===================================#
	# 			Data Encryption			#
	#===================================#
	
	private $cryptKey = '';
	
	//encrypts data on specified key
	function enterPandora($toEncrypt) {
	    $encoded = base64_encode(mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5($this->cryptKey), $toEncrypt, MCRYPT_MODE_CBC, md5( md5($this->cryptKey))));
	    return($encoded);
	}
	
	//decrypts data on specified key
	function exitPandora($toDencrypt) {
	    $decoded = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $this->cryptKey ), base64_decode( $toDencrypt ), MCRYPT_MODE_CBC, md5( md5( $this->cryptKey ) ) ), "\0");
	    return($decoded);
	}
	
	//set the key that is used for d/encryption
	function setCryptKey($cryptKey){
		$this->cryptKey = $cryptKey;
	}	
	
	
	#===================================#
	# 			Access Control			#
	#===================================#
	
	private $loginSuccess = 'User authenticated, logging in.';
	
	private $loginFail = 'User credentials incorrect or insufficient permissions, please make sure the username and password are correct and that you are permitted to login to the system.';
	
	function setLoginSuccessMessage($msg){ $this->loginSuccess = $msg; }
	
	function setLoginFailMessage($msg){ $this->loginFail = $msg; }
	
	function startSesion(){
		if(!isset($_SESSION)){ session_start(); } //if there isnt a session, create one
	}
	
	function unsetSesion(){
		if(!isset($_SESSION['pa'])){ unset($_SESSION['pa']); }
	}

	function attemptLogin($username, $password){
		//endrypt the users password and look for the user in the database
		$attempt = $this->checkUser($this->enterPandora($username), $this->enterPandora($password));
		
		//handle login attempt 
		if($attempt['access']){ //if the login was successful
			$this->startSesion(); //start the session now the we know the user exists
			$_SESSION['pa'] = $attempt; //store the returned data into at session for later use
			
			/*foreach($modules as $m){
				$_SESSION['pa'][$m['access'].'_access'] = $this->buildAccess($m);
			}*/
			
			$return = array("status"=>1, "msg"=>$this->loginSuccess);
		}
		else{ //if the login was unsuccessful
			$this->unsetSesion();
			$return = array("status"=>0, "msg"=>$this->loginFail);
		}
		
		return $return;
	}
	
	function checkUser($uname, $pword){
		$admin = $this->dl->select('cms_admin', 'admin_email="'.$uname.'" AND admin_password="'.$pword.'" AND admin_archived=0') ;
		
		if($this->dl->totalrows>0){
			$pa['id'] = $admin[0]['admin_id'];
			$pa['name'] = $admin[0]['admin_name'];
			$pa['surname'] = $admin[0]['admin_surname'];
			$pa['email'] = $admin[0]['admin_email'];
			$pa['access'] = true;
		}
		else{
			$pa['access'] = false;
		}
		return $pa;
	}
	
	function buildAccess($mod){
		$level = 'n';
		$access = $this->dl->select('access_level_'.$mod['access'], $mod['access'].'_acc_admin_id="'.$_SESSION['pa']['id'].'"');
		if($this->dl->totalrows>0){
			$access = $access[0];
			$level = $access[$mod['access'].'_acc_level'];
		}
		return $level;
	}
		
	
	#===================================#
	# 			   Database				#
	#===================================#

	private $dl;
	
	function connectDB($dlhostname, $dlusername, $dlpassword, $dldbname){
		if(!is_object($this->dl)){ //if the connection hasnt already been made, make connection
			$this->dl = new DataLayer();
			$this->dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
			$this->dl->debug = false;
		}	
	}
}