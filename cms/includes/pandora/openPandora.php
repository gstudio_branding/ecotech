<?php
session_start();
include_once('../dbal/dlinc.php');
include_once('pandora.php');
include_once('hope.php');

$pandora = new pandora();
$pandora->setCryptKey($cryptKey); //set the encryption key
$pandora->connectDB($dlhostname, $dlusername, $dlpassword, $dldbname); //connect to the database

//attempt to log the user in
echo json_encode($pandora->attemptLogin($_POST['uname'], $_POST['pword']));
?>