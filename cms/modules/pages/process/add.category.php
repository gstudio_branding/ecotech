<?php
$folder = '';
$prefix = '';
$filename = '';
$options = '';
$optionsArray = array();

if(isset($_GET['folder'])){ $folder=$_GET['folder']; }
if(isset($_GET['prefix'])){ $prefix=$_GET['prefix']; }
if(isset($_GET['filename'])){ $filename=$_GET['filename']; }
if(isset($_GET['options'])){ $options=$_GET['options']; }

if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = true;
	
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key
	
	switch($_GET['a']){
		case '1' : //add entry
			$dl->insert('mod_'.$prefix.'_category', array(
				$prefix.'_category_timestamp'			=>	time(),
				$prefix.'_category_category_id'			=>	@stripslashes($_POST[$prefix.'_category_category_id']),
				$prefix.'_category_heading'				=>	@stripslashes($_POST[$prefix.'_category_heading']),
				$prefix.'_category_short_description'	=>	@stripslashes($_POST[$prefix.'_category_short_description']),
				$prefix.'_category_slug'				=>	@stripslashes($_POST[$prefix.'_category_slug']),
				'media_files_id'					=> $_POST['media_file_id']
			));

			$entry_id = $dl->insert_id;
			
		break;
		
		case '2' : //edit entry
			$dl->update('mod_'.$prefix.'_category', array(
				$prefix.'_category_timestamp'			=>	time(),
				$prefix.'_category_category_id'			=>	@stripslashes($_POST[$prefix.'_category_category_id']),
				$prefix.'_category_heading'				=>	@stripslashes($_POST[$prefix.'_category_heading']),
				$prefix.'_category_short_description'	=>	@stripslashes($_POST[$prefix.'_category_short_description']),
				$prefix.'_category_slug'				=>	@stripslashes($_POST[$prefix.'_category_slug']),
				'media_files_id'					=> $_POST['media_file_id']
			), $prefix.'_category_id="'.$_POST[$prefix.'_category_id'].'"');

			$entry_id = $_POST[$prefix.'_category_id'];

		break;				

		case '3' : //update sort
			foreach($_POST[$_GET['prefix'].'_id'] as $key=>$val){
				$dl->update('mod_'.$_GET['prefix'], array(
					$_GET['prefix'].'_sort'=>$key
				), $_GET['prefix'].'_id='.$val);
			}
		break;
	}
}