<?php
$mode = 'add';

if(isset($_POST['mode']) && $_POST['mode']!=''){
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('cms_admin', 'admin_id="'.$_POST['admin_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}
?>

<form role="form" class="add-entry">
	<div class="row">
		<!-- Group 1 -->
		<div class="col-xs-4">
			Name
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="admin_name" class="form-control input-sm" value="<?php if($mode=='edit'){ echo $pandora->exitPandora($entry['admin_name']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
        
        <div class="col-xs-4">
			Surname
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="admin_surname" class="form-control input-sm" value="<?php if($mode=='edit'){ echo $pandora->exitPandora($entry['admin_surname']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
        
        <div class="col-xs-4">
			Email
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="admin_email" class="form-control input-sm" value="<?php if($mode=='edit'){ echo $pandora->exitPandora($entry['admin_email']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
        
        <div class="col-xs-4">
			Password
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="password" name="admin_password" class="form-control input-sm" value="<?php if($mode=='edit'){ echo $pandora->exitPandora($entry['admin_password']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->	
        
		<!-- Group 1 End -->		
		
		<?php if($mode=='edit'){ ?>
		<input type="hidden" name="admin_id" class="form-control input-sm" value="<?php echo $entry['admin_id']; ?>">
		<?php } ?>
		
	</div><!-- row -->
	
	<button type="submit" class="btn btn-default">Submit</button>
</form>

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	$('.add-entry').submit(function(){
		
		<?php if($mode=='add'){ ?>
			var url = 'modules/admin/process/add.admin.php?a=1',
				msg = 'Your content has been added';
		<?php } ?>
		<?php if($mode=='edit'){ ?>
			var url = 'modules/admin/process/add.admin.php?a=2',
				msg = 'Your content has been updated';
		<?php } ?>
		
		$.ajax({
			url : url,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
				changeSystemModal('<span class="text-success">Success!</span>', msg, '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
				<?php if($mode=='edit'){ ?>
				$('.go-back').trigger('click');
				<?php } ?>
			}
		});
		
		return false;
	});
	
});
</script>