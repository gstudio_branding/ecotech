<?php
if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = true;
	
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key
	
	switch($_GET['a']){
		case '1' : //save admin
			$dl->insert('cms_admin', array(
				'admin_name'		=>	$pandora->enterPandora($_POST['admin_name']),
				'admin_surname'		=>	$pandora->enterPandora($_POST['admin_surname']),
				'admin_email'		=>	$pandora->enterPandora($_POST['admin_email']),
				'admin_password'	=>	$pandora->enterPandora($_POST['admin_password'])
			));
		break;
		
		case '2' : //save admin
			$dl->update('cms_admin', array(
				'admin_name'		=>	$pandora->enterPandora($_POST['admin_name']),
				'admin_surname'		=>	$pandora->enterPandora($_POST['admin_surname']),
				'admin_email'		=>	$pandora->enterPandora($_POST['admin_email']),
				'admin_password'	=>	$pandora->enterPandora($_POST['admin_password'])
			), 'admin_id="'.$_POST['admin_id'].'"');
		break;
		
		case '3' : //edit admin permissions
			$acc_level = 0;
			if($_POST['mod_state']==0){ $acc_level=1; }
			
			$ia = array(
				$_POST['mod_prefix'].'_acc_level'		=>	$acc_level,
				$_POST['mod_prefix'].'_acc_admin_id'	=>	$_POST['admin_id']
			);
			
			$dl->select('access_level_'.$_POST['mod_prefix'], $_POST['mod_prefix'].'_acc_admin_id="'.$_POST['admin_id'].'"');
			if($dl->totalrows>0){ $dl->update('access_level_'.$_POST['mod_prefix'], $ia, $_POST['mod_prefix'].'_acc_admin_id="'.$_POST['admin_id'].'"'); }
			else{ $dl->insert('access_level_'.$_POST['mod_prefix'], $ia); }
			
		break;
	}
}