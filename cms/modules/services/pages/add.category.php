<?php 

$mode = 'add';

if(isset($_POST['mode']) && $_POST['mode']!=''){
	include_once('../../../includes/pandora/pandora.php');
	include_once('../../../includes/pandora/hope.php');
	$pandora = new pandora();
	$pandora->setCryptKey($cryptKey); //set the encryption key

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('mod_blog_category', 'blog_category_id="'.$_POST['blog_category_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}

if(!isset($dl)){
	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
}

$instructions = $dl->select('mod_instruction', '', 'instruction_heading ASC');


$categories = $dl->select('mod_category', '', 'category_heading ASC');

function getRecursiveCategories($dl, $cat_id, $indent, $selected){
	$cats = $dl->select('mod_blog_category', 'blog_category_category_id="'.$cat_id.'" AND blog_category_archived="0"', 'blog_category_sort ASC');
	$data = '';
	$indent .= '&nbsp;&nbsp;&nbsp;&nbsp;';

	if($dl->totalrows>0){
		foreach ($cats as $c){
			$selectOption = '';
			if($selected == $c['blog_category_id']){
				$selectOption = 'selected="selected"';
			}
			$data .= '<option value="'.$c['blog_category_id'].'" '.$selectOption.'>';
			if($cat_id!=0){ 
				
				$data .= $indent; 
			}
			$data .= $c['blog_category_heading'].'</option>';			
			$data .= getRecursiveCategories($dl, $c['blog_category_id'], $indent, $selected);
		}
		return $data;
	}
	else{
	 	return '';
	}
}

?>

<form role="form" class="add-entry">
	<div class="row">
		

		<div class="col-xs-4">
			Parent Category
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<select name="blog_category_category_id" class="form-control">
					<option value="0">Top level category</option>
					<?php 
					$selectedCat = '';
					if($mode=='edit'){ $selectedCat = $entry['blog_category_category_id']; }
					echo getRecursiveCategories($dl, 0, '', $selectedCat); 
					?>
				</select>
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->



        <div class="col-xs-4">
			Category
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="blog_category_heading" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry['blog_category_heading']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->



		<div class="col-xs-4">
			Slug
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="blog_category_slug" class="form-control input-sm" value="<?php if($mode=='edit'){ echo ($entry['blog_category_slug']); } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->


		<div class="col-xs-4">
			Banner Image
		</div><!-- col-xs-4 -->
		<div class="col-xs-14">
			<div class="btn btn-primary btn-xs btn-bt-margin select-image" style="margin-top:10px;">select image</div>
			<div class="btn btn-primary btn-xs btn-bt-margin remove-image" style="margin-top:10px;">remove image</div>
		</div>
		<div class="col-xs-14 col-xs-offset-4 image-container">
			<?php 
			if($mode=='edit' && $entry['media_files_id']!="" && $entry['media_files_id']!="0"){ 
				?>
				<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="16x10">crop image</div><input type="hidden" name="media_file_id" value="<?php echo $entry['media_files_id']; ?>"><img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $entry['media_files_id']; ?>&width=200&aspect_r=16x10" class="img-responsive" /></div><?php
			}
			?>
		</div><!-- col-xs-18 -->



		<?php if($mode=='edit'){ ?>
		<input type="hidden" name="blog_category_id" class="form-control input-sm" value="<?php echo $entry['blog_category_id']; ?>">
		<?php } ?>



	</div><!-- row -->
	
	<button type="submit" class="btn btn-default">Submit</button>
</form>

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	$('.add-entry').submit(function(){
		
		<?php if($mode=='add'){ ?>
			var url = 'modules/blog/process/add.category.php?a=1',
				msg = 'Your content has been added';
		<?php } ?>
		<?php if($mode=='edit'){ ?>
			var url = 'modules/blog/process/add.category.php?a=2',
				msg = 'Your content has been updated';
		<?php } ?>
		
		$.ajax({
			url : url,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
				changeSystemModal('<span class="text-success">Success!</span>', msg, '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
				<?php if($mode=='edit'){ ?>
				$('.go-back').trigger('click');
				<?php } ?>
			}
		});
		
		return false;
	});

	function refreshBindings(){		
		$('.crop-image').unbind();
		$('.crop-image').click(function(){
			var $this = $(this);
			
			changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-container').eq(0).find('input').val()+'&aspect_r='+$this.attr('aspect_r'), 'Cancel', 'Crop Image', function(){  }, function(){ 
				var src = $this.parents('div.img-container').eq(0).find('img').attr('src'),
					timestamp = Math.round(new Date().getTime() / 1000);
				
				$this.parents('div.img-container').eq(0).find('img').attr('src', src+'&timestamp='+timestamp);
			});
			$('#system-modal').modal('show');
		});
	
	}
	refreshBindings();	

	$('[name="blog_category_heading"]').change(function(){		
		if($('[name="blog_category_slug"]').val()==''){
			var find = ' ';
			var re = new RegExp(find, 'g');
			var slug = (($('[name="blog_category_heading"]').val().toLowerCase()).replace(/[^\w\s]/gi, '')).replace(re, '-');
			$('[name="blog_category_slug"]').val(slug);
		}
	});

	$('.select-image').click(function(){
		var $this = $(this);
		
		var data = del = '';
		$(this).parents('div').eq(0).next('div.image-container').find('input').each(function(){
			data += del+$(this).attr('name')+'='+$(this).val();
			del = '&';
		});

		//var data = $('.add-entry').serialize();
		
		changeSystemModal('choose image', 'loading images', 'shared/system/window.add.image.php?'+data, 'Cancel', 'Choose Image', function(){  }, function(){
			var imgs = $('.ret-data').html();
			if(imgs!=''){
				imgs = imgs.split(',');
				var img_html = '';
				
				for(var i=0; i<imgs.length; i++){
					img_html += '<div class="col-xs-3 img-container"><div class="btn btn-primary btn-xs btn-bt-margin crop-image" aspect_r="16x10">crop image</div><input type="hidden" name="media_file_id" value="'+imgs[i]+'"><img src="modules/media/scripts/image/image.handler.php?media_files_id='+imgs[i]+'&width=200&aspect_r=16x10" class="img-responsive" /></div>';
				}
				
				$('.image-container').html(img_html);
				refreshBindings();
			}
			
		});
		$('#system-modal').modal('show');
	});

	$('.remove-image').click(function(){
		$('.image-container').html('');
	});
});
</script>