<div class="row">
	<!-- Group 1 -->
	
	<div class="col-xs-18 manage-area">
		<div class="col-xs-15"><strong>Heading</strong></div>
		<div class="col-xs-3"><strong>Tools</strong></div>
		<div class="col-xs-18 managed-content"></div>
	</div><!-- col-xs-18 -->
	
	
	<div class="col-xs-18 edit-area" style="display:none;">
		<div class="col-xs-18" style="border-bottom: 2px solid #fff; margin-bottom:20px;"><a href="#" class="go-back pull-right btn btn-warning btn-bt-margin">Go Back</a></div>
		<div class="col-xs-18 ajax-area"></div>
	</div><!-- col-xs-18 -->
	<!-- Group 1 End -->		
	
</div><!-- row -->

<style>
.thumb{
	width:25%;
}
</style>

<script type="text/javascript">
$(document).ready(function(){	
	function goBack(){
		$('.go-back').click(function(){
			$('.edit-area').slideUp(300, function(){ $('.manage-area').slideDown(300); $('.edit-area .ajax-area').html(''); });
			getStory();
		});
		
		return false;
	}
	
	function refreshBindings(){
		$('.tools.edit').unbind();
		$('.tools.edit').bind('click', function(){ editEntry($(this)); });
		
		$('.tools.remove').unbind();
		$('.tools.remove').bind('click', function(){ removeEntry($(this)); return false; });
		
		$('.mod-perm').unbind();
		$('.mod-perm').bind('click', function(){ updatePermission($(this)); return false; });

		<?php 
		if(1==1){ 
			$prefix = 'blog_category'; 
			$filename = 'content';
		?> 
		$('a.move').removeClass('hidden');
		$('.managed-content').unbind();
		$('.managed-content').sortable({
			placeholder: "ui-state-highlight",
			handle: "a.move",
			helper: "clone",
			update : function(){
				$.ajax({
					url : 'shared/modules/process/add.<?php echo $filename; ?>.php?a=3&prefix=<?php echo $prefix; ?>',
					beforeSend : function(){
						showLoader();
					},
					type : 'post',
					data : $( ".managed-content" ).sortable( "serialize"),
					complete : function(){
						hideLoader();
					}
				});
			}
		});
		<?php } ?>
	}
	
	function editEntry($this){
		$('.manage-area').slideUp(300, function(){ $('.edit-area').slideDown(300); });
		
		
		$.ajax({
			url : 'modules/blog/pages/add.category.php',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'mode=edit&blog_category_id='+$this.parents('div.row').eq(0).attr('blog_category_id'),
			success : function(data){
				$('.edit-area .ajax-area').html(data);
			},
			complete : function(){
				hideLoader();
			}
		});
		
		return false;
	}
	
	function removeEntry($this){
		$.ajax({
			url : 'modules/blog/process/remove.category.php?a=1',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'blog_category_id='+$this.parents('div.row').eq(0).attr('blog_category_id'),
			success : function(data){
				getStory();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function updatePermission($this){
		$.ajax({
			url : '#',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'blog_category_id='+$this.parents('div.row').eq(0).attr('blog_category_id')+'&mod_prefix='+$this.attr('mod-prefix')+'&mod_state='+$this.attr('mod-state'),
			success : function(data){
				getStory();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function getStory(){
		$.ajax({
			url : 'modules/blog/process/get.category.php?a=2',
			beforeSend : function(){
				showLoader();
			},
			dataType : 'json',
			success : function(data){
				if(data.status==1){
					$('.managed-content').html(data.data);
				}
				
				refreshBindings();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});
	}
	
	goBack();
	getStory();
});
</script>