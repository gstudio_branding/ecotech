<?php 
class Content { 

	private $dl;
	public $webRoot;
	public $dbIncludePath;

	public $prefix;

	public function __construct($dbIncludePath='../../../includes/dbal/dlinc.php', $webRoot='', $prefix="what_we_do") {
        $this->dbIncludePath = $dbIncludePath;
        $this->webRoot = $webRoot;
        $this->prefix = $prefix;

        $this->connectDB();
    }   

    private function connectDB(){
		if(!is_object($this->dl)){ //if the connection hasnt already been made, make connection
			include_once($this->dbIncludePath);
			$this->dl = new DataLayer();
			$this->dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
			$this->dl->debug = false;
		}	
	}

	function getHTMLIntro(){
		$intro = $this->dl->select('mod_'.$this->prefix.'_introduction', $this->prefix.'_introduction_archived=0');
		if($this->dl->totalrows>0){ $intro = $intro[0][$this->prefix.'_introduction_description']; }
		else{ $intro = ''; }
		return $intro;
	}

	function getContent($entry_id=0){ //gets the full story
		if($entry_id==0){
			$entry = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_archived=0', $this->prefix.'_date ASC', array(0,1));
			$entry_id = $entry[0][$this->prefix.'_id'];
		}
		else{
			$entry = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_id="'.$entry_id.'" AND '.$this->prefix.'_archived=0', $this->prefix.'_date ASC');
		}
		
		if($this->dl->totalrows>0){
			$entry = $entry[0];

			$entryImages = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id="'.$entry_id.'"', $this->prefix.'_media_link_sort ASC');
			if($this->dl->totalrows>0){ 
				$entry[$this->prefix.'_images'] = $entryImages;
			}

			$pillarImages = $this->dl->select('mod_'.$this->prefix.'_pillar_link AS spl LEFT JOIN mod_pillar AS p ON spl.pillar_id=p.pillar_id', $this->prefix.'_id="'.$entry_id.'"', 'mod_'.$this->prefix.'_pillar_link_sort ASC');
			if($this->dl->totalrows>0){ 
				$entry['pillar_images'] = $pillarImages;
			}
			return $entry;
		}
		else{
			return false;
		}
	}

	function getHTMLContent($entry_id=0, $container_id=''){ //gets the full story
		if($entry_id==0){
			//$entry = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_archived=0', $this->prefix.'_date ASC', array(0,1));
			//$entry_id = $entry[0][$this->prefix.'_id'];
			$entry = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_archived=0', $this->prefix.'_date ASC');
			//$entry_id = $entry[0][$this->prefix.'_id'];
		}
		else{
			$entry = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_id="'.$entry_id.'" AND '.$this->prefix.'_archived=0', $this->prefix.'_date ASC');

		}

		$content = '';
		
		if($this->dl->totalrows>0){
			foreach($entry as $e){

				$entryImages = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id="'.$e[$this->prefix.'_id'].'"', $this->prefix.'_media_link_sort ASC');
				if($this->dl->totalrows>0){ 
					$entry[$this->prefix.'_images'] = $entryImages;
				}

				$pillarImages = $this->dl->select('mod_'.$this->prefix.'_pillar_link AS spl LEFT JOIN mod_pillar AS p ON spl.pillar_id=p.pillar_id', $this->prefix.'_id="'.$e[$this->prefix.'_id'].'"', 'mod_'.$this->prefix.'_pillar_link_sort ASC');
				if($this->dl->totalrows>0){ 
					$e['pillar_images'] = $pillarImages;
				}


				$pillars = false;
				if(sizeof($pillarImages)>0){ $pillars = true; }

				

				$content .= '<div class="col-xs-30 content-block margin-bottom-20" id="'.$container_id.'">
					<div class="col-xs-29  col-sm-28 text-area">
					  <div class="col-xs-28 col-xs-offset-1 col-sm-28 col-sm-offset-1 text-area-inner">
					    <h2'.(($pillars)?' class="margin-bottom-0"':'').'>'.$e[$this->prefix.'_heading'].'</h2>';

				if($pillars){
					$content .= '<div class="clearfix margin-bottom-5 margin-top-0" style="">';
					foreach($pillarImages as $p){
						$content .= '<img src="'.$this->webRoot.$p['pillar_image'].'" width="40" class="pull-right" style="" />';
					}
					$content .= '</div>';
		    	}
		        
		        $content .= $e[$this->prefix.'_description'];

				if(strtolower($container_id) == 'page_pillars'){ $content .= $this->getPillars(); }
				
				$content .='<div class="col-xs-30">';

					    if(is_array($entryImages) && sizeof($entryImages)>0){
					    	$col_count = 0;
					    	foreach($entryImages as $i){
					    		if($col_count==3){ $col_count=0; }
					    		$content .='<div class="col-xs-10">
				                    <div class="col-xs-30 news-container">
				                      <div class="col-xs-28 col-xs-offset-'.($col_count).' margin-top-10"">
				                        <img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$i['media_files_id'].'&width=200&aspect_r=5x3" class="img-responsive" />
				                      </div>
				                    </div>
				                  </div><!-- col-xs-10 -->';

				                  $col_count++;
					    	}
					    }
		                $content .='</div><!-- col-xs-30 -->


					  </div>
					</div><!-- col-xs-28 -->';

					if($e['foundation_id']!='' && $e['foundation_id']!='0'){
						$foundation_icon = $this->dl->select('mod_foundation', 'foundation_id="'.$e['foundation_id'].'"');
						$foundation_icon = $foundation_icon[0];
						$content .= '<div class="col-xs-1 col-sm-2">
						  <div class="logo-icon">
						  <img src="'.$this->webRoot.$foundation_icon['foundation_image'].'" />

						  </div>
						</div>';
					}

				$content .= '</div><!-- content-block Mission -->';

				//return $entry;
					
			}	
			return $content;	
		}
		else{
			return false;
		}
	}

	function getPillars(){
		$pillars = $this->dl->select('mod_pillar');
		$pillar_html = '';

		$pillar_html .= '<div class="pillars">';
		if($this->dl->totalrows>0){
			foreach($pillars as $p){
				$pillar_html .= '<p class="to-upper champagne"><img src="'.$this->webRoot.$p['pillar_image'].'" height="75" /> <strong>'.strtoupper($p['pillar_name']).'</strong></p>';
			}
		}
		$pillar_html .= '</div>';

		return $pillar_html;
	}

	function getEntries($numEntries=3){ //gets information for the thumbnail view of entries
		$entries = $this->dl->select('mod_'.$this->prefix, $this->prefix.'_archived=0', $this->prefix.'_date ASC', array(0, $numEntries));
		if($this->dl->totalrows>0){
			return $entries;
		}
		else{
			return false;
		}
	}

	function getHTMLEntries($numEntries=3, $layout=1, $urlPath=''){
		$entries = $this->getEntries($numEntries);
		$entriesHTML = '';
		$count = 0;

		if($entries!=false){
			foreach($entries as $s){
				switch($layout){
					case '1' : //block layout for article page
						$entryImage = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id="'.$s[$this->prefix.'_id'].'"', $this->prefix.'_media_link_sort ASC', array(0,1));
						if($this->dl->totalrows>0){ $entryImage = $entryImage[0]; }
						else{ $entryImage = false; }
						
						$entryHTML .= '
							<div class="col-xs-14 col-sm-10 margin-top-10">
						      <div class="col-xs-30 news-container">
						        <div class="col-xs-30 col-sm-28">';
						 if($entryImage!=false){ $entryHTML .= '<img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$entryImage['media_files_id'].'&width=200&aspect_r=4x3" class="img-responsive" />'; }
						$entryHTML .= '
								  <h4 class="margin-bottom-0">'.$s[$this->prefix.'_heading'].'</h4>
						          <div class="line green margin-top-5 margin-bottom-5"></div>
						          <p class="intro-text">'.$s[$this->prefix.'_short_description'].'</p>
						          <a href="'.$this->webRoot.$urlPath.'?eid='.$s[$this->prefix.'_id'].'" class="read-more-block margin-top-10">Read more ></a>
						        </div>
						      </div>
						    </div><!-- col-xs-30 col-xs-10 -->
							';
					break;

					case '2' : //block layout for article page
						$entryImage = $this->dl->select('mod_'.$this->prefix.'_image_link', $this->prefix.'_id="'.$s[$this->prefix.'_id'].'"', $this->prefix.'_media_link_sort ASC', array(0,1));
						if($this->dl->totalrows>0){ $entryImage = $entryImage[0]; }
						else{ $entryImage = false; }
						
						if($count==0){ $entryHTML .= '<div class="col-xs-30 col-sm-15">'; }
						else if($count==2){ 
							$entryHTML .= '</div><!-- col-xs-30 --><div class="col-xs-30 col-sm-15">'; 
							$count = 0;
						}

						$entryHTML .= '
							<div class="col-xs-15 ">
								<div class="story-block">
								  <img src="'.$this->webRoot.'cms/modules/media/scripts/image/image.handler.php?media_files_id='.$entryImage['media_files_id'].'&width=200&aspect_r=1x1" class="" width="100%" />
								  <div class="col-xs-30 story-overlay">
								    <h4>'.$s[$this->prefix.'_heading'].'</h4>
								    <div class="line"></div>
								    <div>
								      <p>'.$s[$this->prefix.'_short_description'].'</p>
								    </div>
								    <a href="'.$this->webRoot.$urlPath.'?eid='.$s[$this->prefix.'_id'].'">read more</a>
								  </div><!-- story-overlay -->
								</div>
							</div>
						';

						$count ++; //keeps track of how many articles have been loaded

					break;
				}

				$entriesHTML .= $entryHTML;
			}

			if($layout==2){
				$entryHTML .= '</div>';
			}

			$entriesHTML .= $entryHTML;
		}
		return $entriesHTML;
	}

} 

// $entry = new Content('../../../includes/dbal/dlinc.php', '../../../../');
// echo $entry->getHTMLEntries();
?>