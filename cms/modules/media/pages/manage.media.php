<?php session_start(); ?>
<div class="row media-container">
	
</div>

<script type="text/javascript">
$(document).ready(function(){
	var currFolderId = 0;
	
	function refreshBindings(){
		$('.tools.remove').unbind();
		$('.tools.remove').bind('click', function(){ removeEntry($(this)); return false; });
		
		$('.tools.move').unbind();
		$('.tools.move').bind('click', function(){ 
			var $this = $(this);
			changeSystemModal('move media', 'loading content', 'modules/media/pages/window.move.media.php?mode=add&media_files_id='+$this.parents('div.img-tools').eq(0).attr('media_files_id'), 'Cancel', 'Move Media', function(){  }, function(){
				getEntries(currFolderId);
			});
			$('#system-modal').modal('show');
		
			return false; 
		});

		$('.tools.crop').unbind();
		$('.tools.crop').bind('click', function(){ 
			var $this = $(this);
			changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-tools').eq(0).attr('media_files_id'), 'Cancel', 'Crop Image', function(){  }, function(){
			});
			$('#system-modal').modal('show');
		});

		$('.ap-tool-container').unbind();
		$('.ap-tool-container').hover(
			function(){
				$(this).find('.img-tools').stop().slideDown();
			},
			function(){
				$(this).find('.img-tools').stop().slideUp();
			}
		);

		$('.img-responsive').load(function(){ sizeToolContainer(); resizeSideNav(); });

		sizeToolContainer();
		
		$('.add-folder').unbind();
		$('.add-folder').bind('click', function(){ addFolder($(this)); return false; });
		
		$('.open-folder').unbind();
		$('.open-folder').bind('click', function(){ openFolder($(this)); return false; });
		
		$('.edit-folder').unbind();
		$('.edit-folder').bind('click', function(){ editFolder($(this)); return false; });
		
		$('.remove-folder').unbind();
		$('.remove-folder').bind('click', function(){ removeFolder($(this)); return false; });
	}
	
	function removeEntry($this){
		$.ajax({
			url : 'modules/media/process/remove.media.php?a=1',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'media_files_id='+$this.parents('div.img-tools').eq(0).attr('media_files_id'),
			success : function(data){
				getEntries(currFolderId);
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function removeFolder($this){
		var media_folder_id = $this.parents('.media-folder').attr('media_folder_id');
		
		$.ajax({
			url : 'modules/media/process/remove.folder.php?a=1',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'media_folder_id='+media_folder_id,
			success : function(data){
				getEntries(currFolderId);
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function getEntries(media_folder_id){
		var getUrl = 'modules/media/process/get.media.php?a=1';
		
		$.ajax({
			url : getUrl,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'media_folder_id='+currFolderId,
			success : function(data){
				$('.media-container').html(data);
				refreshBindings();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});
	}
	
	function sizeToolContainer(){
		var innerWidth = $('.img-container').innerWidth(),
			width = $('.img-container').width(),
			height = $('.img-container').height();
			
					
		$('.ap-tool-container').css({
			'width'		: width+'px',
			'height'	: height+'px',
			'left'		: ((innerWidth - width)/2)+'px'
		});
	}	
	
	$(window).on('orientationchange, resize', function(){
		sizeToolContainer();
	});
	
	function openFolder($this){
		currFolderId = $this.parents('.media-folder').attr('media_folder_id');
		getEntries(currFolderId);
	}
	
	function addFolder($this){
		//currFolderId
		changeSystemModal('add folder', 'loading content', 'modules/media/pages/window.add.folder.php?mode=add&media_folder_id='+$this.parents('.media-folder').attr('media_folder_id'), 'Cancel', 'Add Folder', function(){  }, function(){
			getEntries(currFolderId);
		});
		$('#system-modal').modal('show');
	}
	
	function editFolder($this){
		//currFolderId
		changeSystemModal('edit folder', 'loading content', 'modules/media/pages/window.add.folder.php?mode=edit&media_folder_id='+$this.parents('.media-folder').attr('media_folder_id'), 'Cancel', 'Edit Folder', function(){  }, function(){
			getEntries(currFolderId);
		});
		$('#system-modal').modal('show');
	}

	//get the media
	getEntries(currFolderId);
});
</script>