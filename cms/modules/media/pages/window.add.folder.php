<?php session_start(); 

$mode = 'add';

if(isset($_GET['mode']) && $_GET['mode']!='' && $_GET['mode']!='add'){
	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	$entry = $dl->select('cms_media_folder', 'media_folder_id="'.$_GET['media_folder_id'].'"');
	if($dl->totalrows>0){ $entry = $entry[0]; }
	
	$mode = 'edit';
}


include_once('../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>

<form role="form" class="add-entry">
	
	<div class="row">
		<!-- Group 1 -->
		<div class="col-xs-4">
			Folder Name
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="text" name="media_folder_name" class="form-control input-sm" value="<?php if($mode=='edit'){ echo $entry['media_folder_name']; } ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		
		<div class="col-xs-4">
			
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="hidden" name="media_folder_id" class="form-control input-sm" value="<?php echo $_GET['media_folder_id']; ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
                
		<!-- Group 1 End -->		
		
	</div><!-- row -->
</form>

<script type="text/javascript">
$(document).ready(function(){
	$('.modal-save-btn').unbind();
	$('.modal-save-btn').click(function(){
		<?php if($mode=='add'){ ?>
		var url = 'modules/media/process/add.media.folder.php?a=1';
		<?php } else{ ?>
		var url = 'modules/media/process/add.media.folder.php?a=2';
		<?php } ?>
		
		$.ajax({
			url : url,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
			},
			error : function(){
			},
			complete : function(){
				hideLoader();
				$('.modal-close-btn').trigger('click');
			}
		});
		
		return false;
	});

});
</script>