<?php session_start(); 

$mode = 'add';

include_once('../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>

<form role="form" class="add-entry">
	
	<div class="row">
		<!-- Group 1 -->
		<div class="col-xs-4">
			Folder Name
		</div>
		<div class="col-xs-14">
			<div class="form-group folder-list">
				
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
		
		<div class="col-xs-4">
			
		</div>
		<div class="col-xs-14">
			<div class="form-group">
				<input type="hidden" name="media_files_id" class="form-control input-sm" value="<?php echo $_GET['media_files_id']; ?>">
			</div><!-- form-group -->
		</div><!-- col-xs-14 -->
                
		<!-- Group 1 End -->		
		
	</div><!-- row -->
</form>

<script type="text/javascript">
$(document).ready(function(){
	$('.folder-list').load('modules/media/process/get.media.php?a=2');

	$('.modal-save-btn').unbind();
	$('.modal-save-btn').click(function(){
		$.ajax({
			url : 'modules/media/process/add.media.folder.php?a=3',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : $('.add-entry').serialize(),
			success : function(){
			},
			error : function(){
			},
			complete : function(){
				hideLoader();
				$('.modal-close-btn').trigger('click');
			}
		});
		
		return false;
	});

});
</script>