<?php session_start(); ?>
<div class="row media-container">
	
</div>

<script type="text/javascript">
$(document).ready(function(){
	function refreshBindings(){
		$('.tools.remove').unbind();
		$('.tools.remove').bind('click', function(){ removeEntry($(this)); return false; });

		$('.tools.crop').unbind();
		$('.tools.crop').bind('click', function(){ 
			var $this = $(this);
			changeSystemModal('crop image', 'loading image', 'modules/media/pages/window.crop.image.php?media_files_id='+$this.parents('div.img-tools').eq(0).attr('media_files_id'), 'Cancel', 'Crop Image', function(){  }, function(){
			});
			$('#system-modal').modal('show');
		});

		$('.ap-tool-container').unbind();
		$('.ap-tool-container').hover(
			function(){
				$(this).find('.img-tools').stop().slideDown();
			},
			function(){
				$(this).find('.img-tools').stop().slideUp();
			}
		);

		$('.img-responsive').load(function(){ sizeToolContainer(); resizeSideNav(); });

		sizeToolContainer();
	}
	
	function removeEntry($this){
		$.ajax({
			url : 'modules/media/process/remove.media.php?a=1',
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			data : 'media_files_id='+$this.parents('div.img-tools').eq(0).attr('media_files_id'),
			success : function(data){
				getEntries();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});		
	}
	
	function getEntries(){
		var getUrl = 'modules/media/process/get.media.php?a=1';
		
		$.ajax({
			url : getUrl,
			beforeSend : function(){
				showLoader();
			},
			type : 'post',
			success : function(data){
				$('.media-container').html(data);
				refreshBindings();
			},
			error : function(){
				changeSystemModal('<span class="text-danger">Error</span>', 'There was a problem processing your request, please try again later', '', '', 'OK');
				$('#system-modal').modal('show');
				$('.modal-save-btn').unbind();
				$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
			},
			complete : function(){
				hideLoader();
			}
		});
	}
	
	function sizeToolContainer(){
		var innerWidth = $('.img-container').innerWidth(),
			width = $('.img-container').width(),
			height = $('.img-container').height();
					
		$('.ap-tool-container').css({
			'width'		: width+'px',
			'height'	: height+'px',
			'left'		: ((innerWidth - width)/2)+'px'
		});
	}
	
	$(window).on('orientationchange, resize', function(){
		sizeToolContainer();
	});

	//get the media
	getEntries();
});
</script>