<?php 
session_start();
unset($_SESSION['scripts']);
?>
<?php if(!isset($_SESSION['scripts']['blueimp_ui_widget'])){ $_SESSION['scripts']['blueimp_ui_widget'] = time(); ?> <script type="text/javascript" src="modules/media/scripts/blueimp/jquery.ui.widget.js"></script> <?php } ?>
<?php if(!isset($_SESSION['scripts']['blueimp_iframe_trans'])){ $_SESSION['scripts']['blueimp_iframe_trans'] = time(); ?> <script type="text/javascript" src="modules/media/scripts/blueimp/jquery.iframe-transport.js"></script> <?php } ?>
<?php if(!isset($_SESSION['scripts']['blueimp_fileupload'])){ $_SESSION['scripts']['blueimp_fileupload'] = time(); ?> <script type="text/javascript" src="modules/media/scripts/blueimp/jquery.fileupload.js"></script> <?php } ?>


<script type="text/javascript">
$(document).ready(function(){	
	var curr_id = 1;
	
	$('.select-trigger').click(function(){
		$('#fileupload').trigger('click');
	});
	
	function getUniqueID(){
		return curr_id++;
	}
	
	function convertFileSize(file_size){
		var file = Array();
		var file_unit = 'KB';
		
		file_size = file_size/1024;
		
		if(file_size>1024){
			file_size = Math.round(file_size/1024*100)/100;
			file_unit = 'MB';
			
			if(file_size>1024){
				file_size = Math.round(file_size/1024*100)/100;
				file_unit = 'GB';
			}
		}
		else{
			file_size = Math.round(file_size, file_unit)
		}
		
		file['size'] = file_size;
		file['unit'] = file_unit;
		return file;
	}
	
	$('.upload-trigger').click(function(){
		uploadMedia();
	});
	
	function uploadMedia(){
		var btn = $('.upload-btn:not(.disabled)').eq(0);
		if(btn.length>0){
			btn.trigger('click').addClass('disabled');
		}
		else{
			$('.upload-trigger').addClass('disabled');
		}
	}
	
	$(function () {
	
	    $('#fileupload').fileupload({
	        //dataType: 'json',
	        add: function (e, data) {
        		var fileInfo = data.files[0];
				var conversion = convertFileSize(fileInfo.size);
				var file_size = conversion['size'];
				var file_unit = conversion['unit'];

				var container_id = getUniqueID();
	            $('<div class="row" data-container="'+container_id+'"><div class="col-xs-8 filename">'+fileInfo.name+' ('+file_size+' '+file_unit+')</div>  <div class="col-xs-6"><div class="progress"><div class="bar" style="width: 0%;"></div></div></div>  <div class="col-xs-4"><span class="status pull-right">Queued</span> <a href="#" class="btn btn-default btn-xs upload-btn" style="display:none;">Upload</a><input type="hidden" name="folder" value="'+$('[name="media_folder_id"]').val()+'" /></div>  <div class="col-xs-18" style="border-bottom: 2px solid #fff; margin-bottom:20px;"></div></div>').appendTo($('#queue'));
	            
	            data.context = $('[data-container="'+container_id+'"]');
	            
	            $('.upload-trigger').removeClass('disabled');
                
                data.context.find('.upload-btn').click(function () {
                	if(!$(this).find('.btn').hasClass('disabled')){
	                	$(this).find('.btn').addClass('disabled');
	                    data.context.find('.status').addClass('text-warning').html('Uploading.');
	                    data.submit();
                    }
                    return false;
                });
	        },
	        done: function (e, data) {
	            data.context.find('.status').removeClass('text-warning').addClass('text-success').html('Upload finished.');
	            uploadMedia();
	        },
	        progress : function (e, data) {
			    var progress = parseInt(data.loaded / data.total * 100, 10);
			    
			    data.context.find('.progress .bar').css(
		            'width',
		            progress + '%'
		        );
			}
	        /* progressall: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#progress .bar').css(
		            'width',
		            progress + '%'
		        );
		    } */
	    });
	    
	    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
		    var inputs = data.context.find(':input');
		    if (inputs.filter(function () {
		            return !this.value && $(this).prop('required');
		        }).first().focus().length) {
		        data.context.find('button').prop('disabled', false);
		        return false;
		    }
		    data.formData = inputs.serializeArray();
		});
	});
	
	$('.folder-list').load('modules/media/process/get.media.php?a=2', function(){
		$('[name="media_folder_id"]').change(function(){
			$('[name="folder"]').val($(this).val());
		});
	});
});
</script>

<style type="text/css">
.bar {
	background-color: rgb(39, 121, 192);
	height: 100%;}
</style>


<input id="fileupload" type="file" name="files[]" data-url="modules/media/scripts/upload_manager.php?a=1" multiple style="display:none;">

<div class="row">
	<div class="col-xs-4">
		<div class="btn btn-primary select-trigger">Select Files</div>
		<div class="btn btn-primary upload-trigger disabled">Upload Files</div>
	</div>
	<div class="col-xs-14"><label class="col-xs-4" style="margin-top:7px;">Upload Images To:</label> <div class="col-xs-14 folder-list"></div><!-- form-group --></div>
</div>


<div id="progress">
    <div class="bar" style="width: 0%;"></div>
</div>

<div id="queue" class="">
	<div class="col-xs-18" style="border-bottom: 2px solid #fff; margin-bottom:20px; margin-top:20px;"></div>
</div>