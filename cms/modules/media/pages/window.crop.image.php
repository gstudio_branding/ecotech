<?php session_start(); 
	
include_once('../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;
?>
<div class="row media-container">
	<?php
	$media = $dl->select('cms_media_files', 'media_files_id="'.$_GET['media_files_id'].'"', 'media_files_timestamp DESC');

	if($dl->totalrows>0){
		$media = $media[0];
		?>
		<div class="col-xs-18 img-container" media_files_id="<?php echo $media['media_files_id']; ?>">
			<form name="crop.form">
				<div class="tick-icon"><span class="glyphicon glyphicon-ok"></span></div>
				<div class="crop-container text-center">
					<img src="modules/media/scripts/image/image.handler.php?media_files_id=<?php echo $media['media_files_id']; ?>" class="crop-img" />
				</div>
				
				<div style="display:none;">
	                x: <input type="text" name="x" id="x" value="0"/>
	                y: <input type="text" name="y" id="y" value="0"/>
	                <br />
	                w: <input type="text" name="w" id="w" value="0"/>
	                h: <input type="text" name="h" id="h" value="0"/>
	                <br />
	                imgw: <input type="text" name="img_w" id="img_w" value="0"/>
	                imgh: <input type="text" name="img_h" id="img_h" value="0"/>
	                aspect_ratio: <input type="text" name="aspect_r" value="<?php echo (isset($_GET['aspect_r']))?$_GET['aspect_r']:1; ?>" />
	                media_files_id: <input type="text" name="media_files_id" value="<?php echo $_GET['media_files_id']; ?>" />
	            </div>
			</form>
		</div><!-- col-xs-18 img-container -->
		<?php
	}
	else{
		?>
		<div class="col-xs-18">
			<p>The image could not be found.</p>
		</div><!-- col-xs-3 img-container -->
		<?php
	}
	?>
</div>
<div class="ret-data"></div>

<style>
.tick-icon{
	display: none;
	position: absolute;
	top: 5px;
	right: 15px;
	font-size: 30px;
	text-shadow: -1px 0 #fff, 0 1px #fff, 1px 0 #fff, 0 -1px #fff;
}

.img-selected .tick-icon{
	display: block;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
		
	function updateCoords(c){
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h); 
	}
	
	function resize_image(con_height, con_width, img_height, img_width, img){
		var final_width = final_height = 0;
		
		if(img_height>con_height || img_width>con_width){
			if(img_height>con_height){
				img.attr('height', con_height);
				img.attr('width', (img_width/img_height)*con_height);
				
				final_height = con_height;
				final_width = (img_width/img_height)*con_height;
				
				
				if(img_width>con_width){
					img.attr('width', con_width);
					img.attr('height', (img_height/img_width)*con_width);
					
					final_height = (img_height/img_width)*con_width;
					final_width = con_width;
				}
				
			}
			else{
				img.attr('width', con_width);
				img.attr('height', (img_height/img_width)*con_width);
				
				final_height = (img_height/img_width)*con_width;
				final_width = con_width;
				
				if(img_height>con_height){
					img.attr('height', con_height);
					img.attr('width', (img_width/img_height)*con_height);
					
					final_height = con_height;
					final_width = (img_width/img_height)*con_height;
				}
			}

			$('#img_w').val(final_width);
			$('#img_h').val(final_height);
			
			setTimeout(function(){
				$('.crop-container').css({
					'width': final_width+'px',
					'height': final_height+'px',
					'margin-left': (($('#system-modal').find('.img-container').width()-final_width)/2)+'px'
				});
			}, 150);
		}
		else{
			img.attr('height', img_height);
			img.attr('width', img_width);
			
			$('#img_w').val(img_width);
			$('#img_h').val(img_height);
			
			setTimeout(function(){
				$('.crop-container').css({
					'width': img_width+'px',
					'height': img_height+'px',
					'margin-left': (($('#system-modal').find('.img-container').width()-img_width)/2)+'px'
				});
			}, 150);
		}
	}
	
	$('.modal-save-btn').unbind();	
	$('.modal-save-btn').click(function(){
		$('[name="crop.form"]').submit();
	});

	//after the image has loaded, jcrop
	$('.crop-img').load(function(){
		$('#system-modal').modal('show');

		var containerHeight = ($(window).innerHeight()*0.9);
			containerWidth = ($('#system-modal').find('.modal-dialog').width()*0.9);

		if( containerWidth <=0 ){
			containerWidth = (parseInt($(window).width())-40)*0.9 ;
		}

		resize_image(containerHeight, containerWidth, this.height, this.width, $('.crop-img'));
		
		$('.crop-img').Jcrop({
			aspectRatio: <?php echo (isset($_GET['aspect_r']))?str_replace('x', '/', $_GET['aspect_r']):1; ?>,
			<?php /*
			$ar = $dl->select('cms_media_dimensions', 'media_files_id="'.$_GET['media_files_id'].'" AND media_dimensions_aspect_ratio="'.$_GET['aspect_r'].'"');
			if($dl->totalrows>0){ 
				$ar = $ar[0];
			?>
			setSelect:   [ <?php echo $ar['media_dimensions_x']; ?>, <?php echo $ar['media_dimensions_y']; ?>, <?php echo $ar['media_dimensions_x2']; ?>, <?php echo $ar['media_dimensions_y2']; ?> ],
			<?php } */ ?>
			onSelect: updateCoords
		});	
		
		
	});

	$('[name="crop.form"]').submit(function(){
		if($('[name="x"]').val()!='' && $('[name="h"]').val()!=''){
			$.ajax({
				url : 'modules/media/scripts/image/image.cropper.php?a=2',
				beforeSend : function(){
					showLoader();
				},
				type : 'post',
				data : $('[name="crop.form"]').serialize(),
				success : function(data){
					$('#system-modal').modal('hide');
				},
				complete : function(){
					hideLoader();
				}
			});
		}
		else{
			changeSystemModal('<span class="text-success">Success!</span>', 'Please crop the image', '', '', 'OK');
			$('#system-modal').modal('show');
			$('.modal-save-btn').unbind();
			$('.modal-save-btn').click(function(){ $('#system-modal').modal('hide'); });
		}
		
		return false;
	});
});
</script>