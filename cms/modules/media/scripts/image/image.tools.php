<?php
class imageTools {
	
	function get_aspect_crop($aspect_x, $aspect_y, $src_width, $src_height){
		$rx = $aspect_x;
		$ry = $aspect_y;
		
		$max_height;
		$max_width;
		
		if($src_width>$src_height){
			$max_height = $src_height;
			$max_width = ($src_height/$ry)*$rx;
			
			if($max_width>$src_width){
				$max_width=$src_width;
				$max_height = ($src_width/$rx)*$ry;
			}
		}
		else{
			$max_width = $src_width;
			$max_height = ($src_width/$rx)*$ry;
		}
		
		$src_ratio = $src_width/$src_height;
		
		$width = $max_width;
		$height = $max_height;
		
		$ratio = $max_width/$max_height;
		
		$crop_height = 0;
		$crop_width = 0;
		
		if ($src_height > $src_width)
		{
			$new_height = $width/$src_ratio;
			$crop_height = ($new_height>0)?0:($new_height-$height);
		}
		else
		{
			$new_width = $height*$src_ratio;
			$crop_width = ($new_width>0)?0:($new_width-$width);
		}
		
		return array('width'=>((int)($width-$crop_width)), 'height'=>((int)($height-$crop_height)));
	}
	
	/*$rx = 1;
	$ry = 1;
	
	$src_width = rand(10, 100);
	$src_height = rand(10, 100);
	
	$size = get_size($rx, $ry, $src_width, $src_height);*/

}
