<?php 
set_time_limit(3600);
ini_set('memory_limit','128M');

session_start();
include_once('../../../../includes/dbal/dlinc.php');
include_once('../../../../scripts/media/wideimage/lib/WideImage.php');

switch($_GET['a']){

	case 2 : /*  ! case 2 : custom crop */
		$dl = new DataLayer();
		$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
		$dl->debug = false;
		
		//store posted variables
		$img_id = $_POST['media_files_id'];
		$aspect = $_POST['aspect_r'];
		$aspect_code = 0;
		$x_point = $_POST['x'];
		$y_point = $_POST['y'];
		$crop_width = $_POST['w'];
		$crop_height = $_POST['h'];
		$src_width = $_POST['img_w'];
		$src_height = $_POST['img_h'];
		
		
		$image = $dl->select('cms_media_files', 'media_files_id="'.$img_id.'"', '', '', '');
		$image = $image[0];
		
		$img = WideImage::loadFromFile('../../uploads/original/'.$image['media_files_filename']);
		
		/* ! scale points to correct size */
		if($img->getWidth()>$src_width){
			$scale_correct = $img->getWidth()/$src_width;
			
			$x_point = (int)($x_point*$scale_correct);
			$y_point = (int)($y_point*$scale_correct);
			$crop_width = (int)($crop_width*$scale_correct);
			$crop_height = (int)($crop_height*$scale_correct);	
		}
		else{
			$scale_correct = $src_width/$img->getWidth();
			
			$x_point = (int)($x_point/$scale_correct);
			$y_point = (int)($y_point/$scale_correct);
			$crop_width = (int)($crop_width/$scale_correct);
			$crop_height = (int)($crop_height/$scale_correct);	
		}
				
		$ia = array(
			'media_files_id' 				=> $img_id,
			'media_dimensions_x' 			=> $x_point,
			'media_dimensions_y' 			=> $y_point,
			'media_dimensions_x2' 			=> $crop_width,
			'media_dimensions_y2' 			=> $crop_height,
			'media_dimensions_aspect_ratio'	=> $aspect,
			'media_dimensions_time'			=> time()
		);
		
		$dimensions = $dl->select('cms_media_dimensions', 'media_files_id="'.$img_id.'" AND media_dimensions_aspect_ratio="'.$aspect.'"');
		if($dl->totalrows>0){
			$dl->update('cms_media_dimensions', $ia, 'media_dimensions_id="'.$dimensions[0]['media_dimensions_id'].'"');
		}
		else{
			$dl->insert('cms_media_dimensions', $ia);
		}
		
		$img->destroy();
	break;
}
?>