<?php
session_start(); 
set_time_limit(3600);
ini_set('memory_limit','128M');

include_once('image.tools.php');
include_once('../../../../scripts/media/wideimage/lib/WideImage.php');
include_once('../../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

$img_tool = new imageTools();

//WideImage::loadFromFile('../../uploads/original/'.$filename);
//$filename='', $width=300, $height=300, $x=0, $y=0, $x2=0, $y2=0, $quality=100, $aspectR='1x1'


function setHeaders($timestamp){
	//header("Cache-Control: private, max-age=10800, pre-check=10800");
	
	header("Cache-Control: private, max-age=0, pre-check=0");
	header("Pragma: private");
	header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));
	$img = "some_image.png"; 
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) 
	       && 
	  (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $timestamp)) {
	  // send the last mod time of the file back
	  header('Last-Modified: '.gmdate('D, d M Y H:i:s', $timestamp).' GMT', 
	  true, 304);
	  exit;
	}
	// generate a thumbnail off the $img file, and tell the browser to cache the result.
	// and here we send the image to the browser with all the stuff required for tell it to cache
	header("Content-type: image/jpeg");
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $timestamp) . ' GMT');
	
}



//filename handed in, proceed to generate image
if(isset($_GET['media_files_id']) && $_GET['media_files_id']!=''){

	$p_width; 		//stores the passed in width
	$p_height; 		//stores the passed in height
	$p_aspect_r;	//stores the passed in aspect ratio
	$p_aspect_r_x;	//stores the passed in aspect ratio x
	$p_aspect_r_y;	//stores the passed in aspect ratio y
	$p_quality;		//stores the passed in quality %
	
	$o_width;		//stores the original width of the image
	$o_height;		//stores the original width of the image
	
	$img_ext;		//the extension of the image
	
	$timestamp; 	//last updated

	//check db for file
	$media_item = $dl->select('cms_media_files', 'media_files_id="'.$_GET['media_files_id'].'"');
	if($dl->totalrows>0){
		$media_item = $media_item[0];
		$timestamp = $media_item['media_files_timestamp'];
		$filename = $media_item['media_files_filename'];
		#
		$exp_char = '.';
		$temp_ext = explode($exp_char, $filename);
		$img_ext = strtolower(end($temp_ext));
	
		$image = WideImage::loadFromFile('../../uploads/original/'.$filename);
		$o_width = $image->getWidth();
		$o_height = $image->getHeight();
		
		if(
			(isset($_GET['width']) && $_GET['width']!='')
			||
			(isset($_GET['height']) && $_GET['height']!='')
		){
			if(isset($_GET['width']) && $_GET['width']!=''){
				$p_width = $_GET['width'];
			}
			else{
				$p_height = $_GET['height'];
			}
			
			if(
				(isset($_GET['aspect_r']) && $_GET['aspect_r']!='')
			){ //
				#################################################
				#	resize within aspect ratio constraints		#
				#################################################
				
				
				//check db for aspect ratio
				$ar = $dl->select('cms_media_dimensions', 'media_files_id="'.$_GET['media_files_id'].'" AND media_dimensions_aspect_ratio="'.$_GET['aspect_r'].'"');
				if($dl->totalrows>0){
					$ar = $ar[0];
					$timestamp = $ar['media_dimensions_time'];
					$image = $image->crop($ar['media_dimensions_x'], $ar['media_dimensions_y'], $ar['media_dimensions_x2'], $ar['media_dimensions_y2']);

					if(isset($p_width)){
						$image = $image->resize($p_width);
					}
					else{
						$image = $image->resize(NULL, $p_height);
					}
				}
				else{ //no aspect ration found
					$p_aspect_r = explode('x', $_GET['aspect_r']);
					$p_aspect_r_x = $p_aspect_r[0];
					$p_aspect_r_y = $p_aspect_r[1];
					
					$sizes = $img_tool->get_aspect_crop($p_aspect_r_x, $p_aspect_r_y, $o_width, $o_height);
					
					$image = $image->crop('center', 'center', $sizes['width'], $sizes['height']);
					
					if(isset($p_width)){
						$image = $image->resize($p_width);
					}
					else{
						$image = $image->resize(NULL, $p_height);
					}
				}
			}
			else{ 
				#################################################
				#		plain resize to provided dimension		#
				#################################################
				
				
				if(isset($p_width)){
					$image = $image->resize($p_width);
				}
				else{
					$image = $image->resize(NULL, $p_height);
				}
				
				
			}
			
			//$image->output('jpg', $quality);
			if(isset($_GET['filter']) && $_GET['filter']!=''){ 
				switch($_GET['filter']){
					case 'grayscale' : 
						$image = $image->applyFilter(IMG_FILTER_CONTRAST, 20);
						$image = $image->applyFilter(IMG_FILTER_BRIGHTNESS, 70);
						$image = $image->applyFilter(IMG_FILTER_GRAYSCALE);
					break;
				}
			}
			
			setHeaders($timestamp);
			if($img_ext=='png'){
				$image->output('png');
			}
			else{
				if(isset($_GET['quality']) && $_GET['quality']!=''){ $image->output('jpg', $_GET['quality']); }
				else{ $image->output('jpg'); }
			}
		}
		else{
			#################################################
			#			return original image				#
			#################################################
			//echo 'width/height not set';
			setHeaders($timestamp);
			if($img_ext=='png'){
				$image->output('png');
			}
			else{
				if(isset($_GET['quality']) && $_GET['quality']!=''){ $image->output('jpg', $_GET['quality']); }
				else{ $image->output('jpg'); }
			}
		}
	}
	else{
		echo 'no db record';
	}
}
else{
	echo 'media_files_id not set';
}