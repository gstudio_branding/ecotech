<?php
ini_set('max_input_time', 1200);
ini_set('max_execution_time', 1200);
set_time_limit(1200);

include_once('../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

session_start();

switch($_GET['a']){
	case 1 : //image upload
		
		# 0 = Script did not process
		# 1 = Wrong file type
		# 2 = File did not upload
		# 3 = File uploaded
		
		$status = '0';
		$msg = 'The script did not process.';
		$file_name = '';
		
		$profile = 'original';
		if(isset($_GET['folder']) && $_GET['folder']!=''){
			$profile = $_GET['folder'];
		}
		
		$targetFolder = '../uploads/'.$profile;
				
		if (!empty($_FILES)) {
			// Validate the file type
			$imageExt = array('jpg','jpeg','gif','png'); // File extensions
			$documentExt = array('pdf','doc','docx'); // File extensions

			//$fileTypes = array('jpg','jpeg','gif','png'); // File extensions

			$fileTypes = array();
			foreach ($imageExt as $i) { array_push($fileTypes, $i); }
			foreach ($documentExt as $i) { array_push($fileTypes, $i); }

			//$fileParts = pathinfo($_FILES['Filedata']['name']);
			$fileParts = pathinfo($_FILES['files']['name'][0]);
			
			$fileParts['extension'] = strtolower($fileParts['extension']);
			
			if (in_array($fileParts['extension'],$fileTypes)) {
				//$tempFile = $_FILES['Filedata']['tmp_name'];
				$tempFile = $_FILES['files']['tmp_name'][0];
				
				$image_name = md5(time());
				$targetFile = rtrim($targetFolder,'/') . '/' . $image_name.'.'.$fileParts['extension'];
				
				while(file_exists($targetFile)){
					$image_name = md5(time());
					$targetFile = rtrim($targetFolder,'/') . '/' . $image_name.'.'.$fileParts['extension'];
				}
				
				
				if(file_exists($tempFile)){
					move_uploaded_file($tempFile,$targetFile);
					$file_name = $image_name.'.'.$fileParts['extension'];
					$status = '3';
					$msg = 'The file uploaded successfully.';

					$mediaType=0;
					if (in_array($fileParts['extension'],$imageExt)) { //image type
						$mediaType = 1;
					}
					else if (in_array($fileParts['extension'],$documentExt)) { //document type
						$mediaType = 2;
					}
					
					//add content to db
					$dl->insert('cms_media_files', array(
						'admin_id'				=>	$_SESSION['pa']['id'],
						'media_files_filename'	=>	$image_name.'.'.$fileParts['extension'],
						'media_files_type'		=>	$mediaType,
						'media_files_timestamp'	=>	time(),
						'media_files_original_name'	=>	$fileParts['filename']
					));
					
					$dl->insert('cms_media_folder_link', array(
						'media_folder_id'		=>	$_POST['folder'],
						'media_files_id'	=>	$dl->insert_id
					));
				}
				else{
					$status = '2';
					$msg = 'The file was not found on the server.';
					echo '<pre>';
					print_r($_FILES);
					print_r($_POST);
					echo '</pre>';
				}
			}
			else {
				$status = '1';
				$msg = 'Invalid file type.';
			}
		}
		echo json_encode(array('status'=>$status, 'msg'=>$msg, 'file'=>$file_name));
	break;
}
?>