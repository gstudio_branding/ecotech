<?php
session_start(); 
set_time_limit(3600);
ini_set('memory_limit','128M');

include_once('../../../../includes/dbal/dlinc.php');
$dl = new DataLayer();
$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
$dl->debug = false;

if(isset($_GET['media_files_id']) && $_GET['media_files_id']!=''){
	$media_item = $dl->select('cms_media_files', 'media_files_id="'.$_GET['media_files_id'].'"');
	if($dl->totalrows>0){

		$file = $media_item[0];
		$ext = pathinfo($file['media_files_filename'], PATHINFO_EXTENSION);

		$mimeTypes = array(
	    	'pdf' => 'application/pdf',
	    	'txt' => 'text/plain',
	    	'html' => 'text/html',
	    	'exe' => 'application/octet-stream',
	    	'zip' => 'application/zip',
	    	'doc' => 'application/msword',
	    	'xls' => 'application/vnd.ms-excel',
	    	'ppt' => 'application/vnd.ms-powerpoint',
	    	'gif' => 'image/gif',
	    	'png' => 'image/png',
	    	'jpeg' => 'image/jpg',
	    	'jpg' => 'image/jpg',
	    	'php' => 'text/plain'
	    );

		if(isset($mimeTypes[$ext])){
			header("Content-type: ".$mimeTypes[$ext]);
			header('Content-Disposition: attachment; filename="'.$file['media_files_original_name'].'.'.$ext.'"');
			//header('Content-Disposition: attachment; filename="'.$file['media_files_original_name'].'.'.$ext.'"');
			readfile('../../uploads/original/'.$file['media_files_filename']);

			//header("Content-type:application/pdf");
			//header("Content-Disposition:attachment;filename='downloaded.pdf'");
			//readfile("original.pdf");
		}
	}
}
else{
	echo 'media_files_id not set';
}