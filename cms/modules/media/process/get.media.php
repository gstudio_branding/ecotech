<?php
if(isset($_GET['a']) && $_GET['a']!=''){

	include_once('../../../includes/dbal/dlinc.php');
	$dl = new DataLayer();
	$dl->dbCon($dlhostname, $dlusername, $dlpassword, $dldbname);
	$dl->debug = false;
	
	
	function getMedia($dl, $media_folder_id){
		$ret = '';
		
		if($media_folder_id==0){ //if we are in the root, get the unlinked files
			$where = $del = '';
			$dns = $dl->select('cms_media_folder_link', 'media_folder_id!=0');
			if($dl->totalrows>0){
				foreach($dns as $d){
					$where .= $del.'media_files_id!='.$d['media_files_id'];
					$del = ' AND ';
				}
			}
			
			$media = $dl->select('cms_media_files', $where, 'media_files_timestamp DESC');
		}
		else{		
			$media = $dl->select('cms_media_files AS mf LEFT JOIN cms_media_folder_link AS mfl ON mf.media_files_id=mfl.media_files_id', 'mfl.media_folder_id="'.$media_folder_id.'"', 'media_files_timestamp DESC');
		}
	
		if($dl->totalrows>0){
			foreach($media as $m){
				$ret .= 
				'<div class="col-xs-3 img-container">';

				if($m['media_files_type'] == 1){
					$ret .= '<img src="modules/media/scripts/image/image.handler.php?media_files_id='.$m['media_files_id'].'&width=200&aspect_r=1x1" class="img-responsive" />';
				}
				else{
					$ext = explode('.', $m['media_files_filename']);
					$ext = end($ext);
					switch ($ext) {
						case 'pdf':  $ret .= '<img src="elements/icons/icon-pdf.png" class="img-responsive" />'; break;
						case 'doc':  $ret .= '<img src="elements/icons/icon-word.png" class="img-responsive" />'; break;
						case 'docx':  $ret .= '<img src="elements/icons/icon-word.png" class="img-responsive" />'; break;
						
						default: $ret .= '<img src="elements/icons/icon-pdf.png" class="img-responsive" />'; break;
					}
				}

					

			$ret .= '<div class="ap-tool-container">
						<div class="img-tools bg-info" media_files_id="'.$m['media_files_id'].'">
							<!-- <a href="#" class="tools crop"><span class="glyphicon glyphicon-resize-small"></span></a>  -->
							<a href="#" class="tools remove"><span class="glyphicon glyphicon-trash"></span></a> 
							<a href="#" class="tools move"><span class="glyphicon glyphicon-move"></span></a>
						</div><!-- img-tools row -->
					</div>
				</div><!-- col-xs-3 img-container -->';
			}
		}
		
		return $ret;
	}
	
	function getBreadCrumb($dl, $media_folder_id){
		$ret = '<div class="col-xs-18"><span class="media-folder" media_folder_id="0"><a href="#" class="open-folder">./</a></span>';
		
		$folders = array();
		$searchDone = false;
		$currFolderId = $media_folder_id;
		
		if($media_folder_id!=0){
			while(!$searchDone){
				$folder = $dl->select('cms_media_folder', 'media_folder_id="'.$currFolderId.'"');
				array_unshift($folders, array($folder[0]['media_folder_id'] => $folder[0]['media_folder_name']));
				
				if($folder[0]['media_folder_link']!=0){
					$currFolderId = $folder[0]['media_folder_link'];
				}
				else{
					$searchDone = true;
				}
			}
			
			foreach($folders as $folder){
				foreach($folder as $key=>$val){
					$ret .= '<span class="media-folder" media_folder_id="'.$key.'"><a href="#" class="open-folder"> '.$val.'/</a></span>';
				}
			}
		}
		
		$ret .= '</div>';
		
		return $ret;
	}
	
	function getFolders($dl, $media_folder_id){
		$ret = '';
		$folders = $dl->select('cms_media_folder', 'media_folder_link="'.$media_folder_id.'" AND media_folder_name!=""', 'media_folder_name ASC');
		
		if($dl->totalrows>0){
			foreach($folders as $f){
				$ret .= '
				<div class="col-xs-3 media-folder" media_folder_id="'.$f['media_folder_id'].'">
					<a href="#" class="open-folder"><img src="elements/folder/folder.png" class="img-responsive" /></a>
					<div class="col-xs-offset-1 col-xs-16">
						<a href="#" class="open-folder">'.$f['media_folder_name'].'</a>
						<a href="#" class="remove-folder"><span class="glyphicon glyphicon-trash pull-right"></span></a> 
						<a href="#" class="edit-folder"><span class="glyphicon glyphicon-edit pull-right"></span></a>
					</div>
					<div style="height:30px;">&nbsp;</div>
				</div>';
			}
		}
		$ret .= '<div class="col-xs-3 media-folder text-center" media_folder_id="'.$media_folder_id.'"><a href="#" class="add-folder"><img src="elements/folder/add_folder.png" class="img-responsive" />add folder</a></div>';
		
		$ret .= '<div class="col-xs-18" style="height:20px; border-bottom: 2px solid #fff; margin-bottom:20px;"></div>'.getMedia($dl, $media_folder_id);
		
		return $ret;
	}
	
	function addTab($tabs){
		$ret = '';
		
		for($i=0; $i<($tabs-1); $i++){
			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		
		return $ret;
	}
	
	function getFolderList($dl, $media_folder_link, $media_folder_name, $depth){
		$ret = '';
		$folders = $dl->select('cms_media_folder', 'media_folder_link="'.$media_folder_link.'" AND media_folder_name!=""', 'media_folder_name ASC');
		
		if($media_folder_link!=0){ 
			$ret .= '<option value="'.$media_folder_link.'">'.addTab($depth).$media_folder_name.'</option>'; 
		}
		
		if($dl->totalrows>0){
			foreach($folders as $f){
				$ret .= getFolderList($dl, $f['media_folder_id'], $f['media_folder_name'], ($depth+1));
			}
		}
		else{
			$searchDone = true;
		}
		
		return $ret;
	}
	
	switch($_GET['a']){
		case '1' : //get media files
			echo getBreadCrumb($dl, $_POST['media_folder_id']);
			echo getFolders($dl, $_POST['media_folder_id']);
		break;
		
		case '2' : 
			echo '<select name="media_folder_id" class="form-control"><option value="0">./</option>'.getFolderList($dl, 0, '', 0).'</select>';
		break;
	}
	
}