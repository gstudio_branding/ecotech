var thisScreen = 'lg';

function adjustScreenType(){
  var screens = $('#screen-type').find('.screen-type');

  screens.each(function(){
    if($(this).css('display') == 'block'){
      thisScreen = $(this).attr('screen');
    }
  });
}

function sizeFlyout(){
	$('.flyout-info').each(function(){
		var innerContainer = $(this),
			container = innerContainer.parents('.flyout-container').eq(0),
			animWidth = 72;

		container.css('width', '100%');
		innerContainer.css('width', 'auto');
		animWidth += innerContainer.outerWidth();
		innerContainer.css('width', animWidth+'px');
		container.css('width', '0');
	});

}

function closeSidebar(){
	$('#services-submenu').removeClass('active');
	$('.services-link.active').removeClass('active').find('.service-info').stop().fadeOut();
	$('#overlay').fadeOut();	
	$('#services-submenu').find('.curved-text').stop().fadeIn();
}

$(document).ready(function(){

	$('#nav li').hover(
		function(){
			if(thisScreen!='xs'){
				var navContainer = $(this).find('ul').eq(0);
				if(navContainer.size()>0 && !navContainer.hasClass('mini-category-list')){
					navContainer.show();
				}
			}
		},
		function(){
			if(thisScreen!='xs'){
				var navContainer = $(this).find('ul').eq(0);
				if(navContainer.size()>0 && !navContainer.hasClass('mini-category-list')){
					navContainer.hide();
				}
			}
		}
	);

	$('.services-link').click(function(){
		if($(this).hasClass('active')){
			$('.services-link.active').removeClass('active').find('.service-info').stop().fadeOut();
			$('#overlay').fadeOut();	
		}
		else{
			$('.services-link.active').removeClass('active').find('.service-info').stop().fadeOut();
			$(this).addClass('active').find('.service-info').stop().fadeIn();
			$('#overlay').fadeIn();
		}

		
	});
	
	sizeFlyout();


	$('.flyout-info').hover(
		function(){

			if($(this).attr('data-tab')=='partners'){
				$('[data-tab="awards"]').parents('.flyout-container').eq(0).stop().animate({
					'width' : 0,
					'margin-top' : '90px'
				}, 300);
			}

			var innerContainer = $(this),
				container = innerContainer.parents('.flyout-container').eq(0),
				animWidth = 72;

			$('.flyout-info').find('.curved-text').stop().fadeIn();
			$(this).find('.curved-text').stop().fadeOut();

			container.css('width', '100%');
			innerContainer.css('width', 'auto');
			animWidth += innerContainer.outerWidth();
			innerContainer.css('width', animWidth+'px');
			container.css('width', '0');

			container.css('z-index', '1004');
			container.stop().animate({
				'width' : animWidth+'px',
				'margin-top' : '0px'
			}, 300);
		},
		function(){
			if($(this).attr('data-tab')=='partners'){
				$('[data-tab="awards"]').parents('.flyout-container').eq(0).stop().animate({
					'width' : 0,
					'margin-top' : '0px'
				}, 300);
			}

			var innerContainer = $(this),
				container = innerContainer.parents('.flyout-container').eq(0);

			$('.flyout-info').find('.curved-text').stop().fadeIn();

			container.css('z-index', '1001');
			container.stop().animate({
				'width' : 0,
				'margin-top' : '0px'
			}, 300);
		}
	);

	$('#services-submenu .closeToggle').click(function(){
		if(parseInt($('#services-submenu').css('left'))<0){
			$(this).find('.curved-text').stop().fadeOut();
			$('#services-submenu').addClass('active');
		}
		else{
			closeSidebar();
		}
	});

	$('.burger-toggle').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#menu-overlay').stop().fadeOut();
		}
		else{
			$(this).addClass('active');
			$('#menu-overlay').stop().fadeIn();
		}
	});
	$('.footer-slider').owlCarousel({
	    loop : true,
	    items : 2,
	    margin:0,
	    // center:true,
	    nav : true,
	    navText : [('<img src="'+basepath+'elements/slider-prev.png" />'),('<img src="'+basepath+'elements/slider-next.png" />')],
	    // responsive:{
	    //     0:{ items:1 },
	    //     600:{ items:1 },
	    //     1000:{ items:1 }
	    // },
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		// animateOut: 'fadeOut'
	});

	$('.submenu-toggle').click(function(){
		if($(this).hasClass('open')){
			$(this).removeClass('open');
			$(this).parents('li').eq(0).find('ul').eq(0).slideUp();
		}
		else{
			$(this).addClass('open');
			$(this).parents('li').eq(0).find('ul').eq(0).slideDown();
		}
	});


	$(window).click(function(e){
		if( $(e.target).parents('#services-submenu').size() ==0 ){ //close sidebar menu
			closeSidebar();
		}
	})

	$(window).resize(function(){
		adjustScreenType();
		
		//collapse burger
		$('.burger-toggle').removeClass('active');
		$('#menu-overlay').stop().fadeOut();
	});

	$(window).load(function(){
		adjustScreenType();
	});
});