# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.20)
# Database: ecotech
# Generation Time: 2016-08-11 07:32:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table access_level_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_admin`;

CREATE TABLE `access_level_admin` (
  `admin_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `admin_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`admin_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_admin` WRITE;
/*!40000 ALTER TABLE `access_level_admin` DISABLE KEYS */;

INSERT INTO `access_level_admin` (`admin_acc_id`, `admin_acc_level`, `admin_acc_admin_id`)
VALUES
	(9,'1',11);

/*!40000 ALTER TABLE `access_level_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_media`;

CREATE TABLE `access_level_media` (
  `media_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `media_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`media_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_media` WRITE;
/*!40000 ALTER TABLE `access_level_media` DISABLE KEYS */;

INSERT INTO `access_level_media` (`media_acc_id`, `media_acc_level`, `media_acc_admin_id`)
VALUES
	(7,'1',11);

/*!40000 ALTER TABLE `access_level_media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_page`;

CREATE TABLE `access_level_page` (
  `page_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `page_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`page_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_page` WRITE;
/*!40000 ALTER TABLE `access_level_page` DISABLE KEYS */;

INSERT INTO `access_level_page` (`page_acc_id`, `page_acc_level`, `page_acc_admin_id`)
VALUES
	(20,'1',11);

/*!40000 ALTER TABLE `access_level_page` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_service`;

CREATE TABLE `access_level_service` (
  `service_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `service_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`service_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_service` WRITE;
/*!40000 ALTER TABLE `access_level_service` DISABLE KEYS */;

INSERT INTO `access_level_service` (`service_acc_id`, `service_acc_level`, `service_acc_admin_id`)
VALUES
	(20,'1',11);

/*!40000 ALTER TABLE `access_level_service` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_admin`;

CREATE TABLE `cms_admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(200) DEFAULT NULL COMMENT 'name of the user',
  `admin_surname` varchar(200) DEFAULT NULL COMMENT 'surname of the user',
  `admin_dob` int(11) DEFAULT NULL,
  `admin_gender` int(11) DEFAULT NULL COMMENT '1:male; 0:female',
  `admin_email` varchar(200) DEFAULT NULL COMMENT 'email address of the user',
  `admin_cell_private` varchar(12) DEFAULT NULL,
  `admin_cell_work` varchar(12) DEFAULT NULL,
  `admin_tel_private` varchar(12) DEFAULT NULL,
  `admin_tel_work` varchar(12) DEFAULT NULL,
  `admin_fax` varchar(12) DEFAULT NULL,
  `admin_address` varchar(200) DEFAULT NULL,
  `admin_password` varchar(200) DEFAULT NULL COMMENT 'password for the user',
  `admin_added_by_id` int(11) DEFAULT NULL COMMENT 'the id of the user that added the user',
  `admin_archived` int(11) DEFAULT '0',
  `admin_picture` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `cms_admin` WRITE;
/*!40000 ALTER TABLE `cms_admin` DISABLE KEYS */;

INSERT INTO `cms_admin` (`admin_id`, `admin_name`, `admin_surname`, `admin_dob`, `admin_gender`, `admin_email`, `admin_cell_private`, `admin_cell_work`, `admin_tel_private`, `admin_tel_work`, `admin_fax`, `admin_address`, `admin_password`, `admin_added_by_id`, `admin_archived`, `admin_picture`)
VALUES
	(11,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=','Qg42ym9OyRJVDrJyru5wx+SUgyiAUNtXRRnvYTJue6o=',NULL,NULL,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=',NULL,NULL,NULL,NULL,NULL,NULL,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=',NULL,0,NULL);

/*!40000 ALTER TABLE `cms_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_media_dimensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_dimensions`;

CREATE TABLE `cms_media_dimensions` (
  `media_dimensions_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `media_dimensions_x` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_x2` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y2` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_aspect_ratio` varchar(11) DEFAULT NULL COMMENT 'aspect ratio eg 1x1',
  `media_dimensions_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_dimensions_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table cms_media_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_files`;

CREATE TABLE `cms_media_files` (
  `media_files_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `media_files_filename` varchar(400) DEFAULT NULL,
  `media_files_type` int(11) DEFAULT NULL,
  `media_files_title` varchar(200) DEFAULT NULL,
  `media_files_description` text,
  `media_files_timestamp` int(11) DEFAULT NULL,
  `media_files_original_name` text NOT NULL,
  PRIMARY KEY (`media_files_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `cms_media_files` WRITE;
/*!40000 ALTER TABLE `cms_media_files` DISABLE KEYS */;

INSERT INTO `cms_media_files` (`media_files_id`, `admin_id`, `media_files_filename`, `media_files_type`, `media_files_title`, `media_files_description`, `media_files_timestamp`, `media_files_original_name`)
VALUES
	(17,0,'cc696d2d52c8c9b9fe9db3344164aad0.png',1,NULL,NULL,1467900487,'AngloGold'),
	(18,0,'198e48fd6b67e39216fde87014c2d982.png',1,NULL,NULL,1467900488,'aquarius'),
	(19,0,'dbed5459da16a47a3670485b231c0d68.png',1,NULL,NULL,1467900489,'cb2659a5dd0232fcb4bb86c0a41deea981e143b1f093e2763e1ba5482d92b586'),
	(13,0,'fe91beda4251bfb51e13a2dc5cf8a87a.png',1,NULL,NULL,1467900480,'6594915_orig'),
	(14,0,'1501d3e2effc2ac1f683630a89fff716.png',1,NULL,NULL,1467900481,'AAEAAQAAAAAAAAI_AAAAJDI5MTZmN2EyLWQwYzktNDRhOC05ODlhLWFlZDRjYzE3YjViNA'),
	(15,0,'f341c71f1828b2abb3663a7bc288065f.png',1,NULL,NULL,1467900482,'AAEAAQAAAAAAAAZUAAAAJGQxYWQ0MzY5LTM0MzQtNDdlNi1hN2JjLTlmY2NjZjc0NzQxNw'),
	(16,0,'0037b714d4b12da910406a5993597c9c.png',1,NULL,NULL,1467900484,'ACSA'),
	(12,11,'8d8e707f81bc70d09234c358fa0e259b.png',1,NULL,NULL,1467877824,'Gold_Partner_Insignia'),
	(20,0,'3944af3dc6ee2d12095bdae77766e42a.png',1,NULL,NULL,1467900491,'Eskom logo lrg_1'),
	(21,0,'bf1cb0151a42ead4cc906ac5d956f4e2.png',1,NULL,NULL,1467900492,'Eurasian_Natural_Resources_Corporation_(ENRC)'),
	(22,0,'bf6bb054c0027b45fbabc3ccf9d56df8.png',1,NULL,NULL,1467900494,'login-logo'),
	(23,0,'19aed3016868c0bf00088cd9af8295d4.png',1,NULL,NULL,1467900496,'Microsoft-PESTEL-Analysis'),
	(24,0,'37356101c61dd28ab6eb626e21750629.jpg',1,NULL,NULL,1467900498,'MTN-logo'),
	(25,0,'adc8f787dd409fc9ccc902dc93fe9ad1.png',1,NULL,NULL,1467900499,'mukam5'),
	(26,0,'dc3a75ee088e0508b1683f6c6ffa1bff.png',1,NULL,NULL,1467900501,'natsure-logo_news_18517_10278'),
	(27,0,'edc105f590e356557f2be15cd0057481.png',1,NULL,NULL,1467900502,'Neotel-logo-page'),
	(28,0,'f692892a3961f92216d8f0b3ce80b183.png',1,NULL,NULL,1467900504,'petra-diamonds-limited-logo'),
	(29,0,'6ce911e8cbee8485635e3c3cdf3f48e8.png',1,NULL,NULL,1467900507,'psg-konsult-logo-e1399475814749'),
	(30,0,'0fba8aabd3527a0e17beed9422200e0c.png',1,NULL,NULL,1467900508,'Rezidor_Hotel_Group'),
	(31,0,'67d5d5fefa5c486d6d2a614e651372c7.png',1,NULL,NULL,1467900511,'Stead Law Logo Final Logo'),
	(32,0,'3e1b576ef89af7e16448c85809daa52d.png',1,NULL,NULL,1467900513,'taste-holdings-logo-e1415610893848'),
	(33,0,'2d1b983fba8dc1cd890a83f8c6de8fce.png',1,NULL,NULL,1467900514,'TelkomSA'),
	(34,0,'a7bf7799e3f2418954a9fbfc47377eaf.png',1,NULL,NULL,1467900515,'thumb_300x300_670d9df5-fb04-49af-9a94-7019f3414f0a'),
	(35,0,'5c1da837c4dd8f730ae62203e3616710.png',1,NULL,NULL,1467900517,'unisa-logo'),
	(36,0,'f709a8dd84e83a96c595b878577250d9.png',1,NULL,NULL,1467900518,'University_of_Johannesburg_Logo'),
	(37,11,'17ea5d3310dbed381a34c41668046a71.png',1,NULL,NULL,1467926344,'icon-channel'),
	(38,11,'4b0f743170d7646eb463ed501d4eca5c.png',1,NULL,NULL,1467926345,'icon-cloud'),
	(39,11,'1c844af65473c12e72adc8399cd2ce76.png',1,NULL,NULL,1467926346,'icon-connectivity'),
	(40,11,'f58c1c5a0fad3a13865ce2649ab4ab1c.png',1,NULL,NULL,1467926347,'icon-consulting'),
	(41,11,'c6686ea38d4aaed7f8e225784bb27542.png',1,NULL,NULL,1467926348,'icon-infrastructure'),
	(42,11,'abb0ec4315b9659e1fa54254609428e2.png',1,NULL,NULL,1467926349,'icon-microsoft'),
	(43,11,'375a903d653e1f771e1307af8bbeabec.png',1,NULL,NULL,1467926350,'icon-portals'),
	(44,11,'81845e74e91caf3da1266502ce18c480.png',1,NULL,NULL,1467926351,'icon-security'),
	(45,11,'5464de8a6d635b17b0318abe4f323970.png',1,NULL,NULL,1467926352,'icon-servicedesk'),
	(46,11,'066614153e2817835eef7f7bde5f992b.png',1,NULL,NULL,1467926353,'icon-voice'),
	(47,11,'bdce487e069b2a4cc0fa13e321643048.png',1,NULL,NULL,1467957425,'microsoftpartner'),
	(48,11,'3dd23c37a22933cd11a270a752ab7ac9.jpg',1,NULL,NULL,1467957427,'Neotel Channel partner logo'),
	(49,11,'f9034c699e11df0625b3245339ca8dce.jpg',1,NULL,NULL,1467957428,'cisco_registered_Partner'),
	(50,11,'95a0bf53094d143589e0397e93db4025.png',1,NULL,NULL,1467957429,'dell_partnerdirect_preferred_rgb3'),
	(51,11,'117304f2e556f7375838e469fa09a789.jpg',1,NULL,NULL,1467957432,'hp_logo_preferred_partner_w'),
	(52,11,'e003e220580ebb96b3d6bad43f292b81.png',1,NULL,NULL,1467957435,'microsoft-small-business-specialist'),
	(53,11,'c8aa1175fac0c3578e45d9b3ef6c8db8.jpg',1,NULL,NULL,1467957436,'SEACOM_OfficialPartnerlogo-300x138'),
	(54,11,'83462e7a1ded84951890301c0bbebda9.pdf',2,NULL,NULL,1468226984,'Blank_SLA Schedules Ver 1'),
	(55,11,'b2343d0197212bf222ee518a6bdca54c.png',1,NULL,NULL,1468232872,'certifications_awards'),
	(56,11,'8aac0632b1275d9b93fb1b4c93703767.png',1,NULL,NULL,1468232874,'channel'),
	(57,11,'ebc07f520c8e06433cec8aec44d1f92c.png',1,NULL,NULL,1468232877,'client_project_list'),
	(58,11,'f233d55354c49e9c00b6f168c7a73906.png',1,NULL,NULL,1468232879,'cloud'),
	(59,11,'8360d158b6d8204da8afd58f01a78d1a.png',1,NULL,NULL,1468232882,'connectivity_solutions'),
	(60,11,'1c3c9d60c4ca57657096af498f72452d.png',1,NULL,NULL,1468232884,'find_us'),
	(61,11,'ee19c0e2c681325c87a9da6cdd4a3c4a.png',1,NULL,NULL,1468232886,'history_operations'),
	(62,11,'4fdcf6d8941b13127d1d8880d2bb4e17.png',1,NULL,NULL,1468232892,'infrastructure'),
	(63,11,'6a04ac9cb91480384696b3d84c45e0d4.png',1,NULL,NULL,1468232893,'management_bios'),
	(64,11,'b07798b67bb80dcd1cc074e5af921f7c.png',1,NULL,NULL,1468232895,'microsoft'),
	(65,11,'46cd65f5d936f78ec91c5c8206d46021.png',1,NULL,NULL,1468232898,'migration_solutions'),
	(66,11,'15561e659fb37b119d08dd7ffef437e0.png',1,NULL,NULL,1468232901,'partners'),
	(67,11,'9e2da296e5585a2643df0d8bd47df645.png',1,NULL,NULL,1468232904,'scalability'),
	(68,11,'2315d5982606e1970a8733d5c0abbbc7.png',1,NULL,NULL,1468232907,'service_desk'),
	(69,11,'5b295a9a518df60abf73198c65a21a57.png',1,NULL,NULL,1468232913,'voice_telephony'),
	(70,11,'1112100f59c08ade43c63df3d79aeaba.png',1,NULL,NULL,1468232918,'consulting'),
	(71,11,'eacacbd2d147c83e30ef762de0d35e77.png',1,NULL,NULL,1468234333,'home'),
	(72,0,'d5f4e1ff514a286d90270a7018d12fd3.png',1,NULL,NULL,1469801180,'MicrosoftSQLServer'),
	(73,0,'22be097b01a2ac26c915649940ae6604.png',1,NULL,NULL,1469801181,'ms-azure transparent');

/*!40000 ALTER TABLE `cms_media_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_media_folder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_folder`;

CREATE TABLE `cms_media_folder` (
  `media_folder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_name` varchar(200) DEFAULT NULL,
  `media_folder_link` int(11) DEFAULT '0',
  PRIMARY KEY (`media_folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `cms_media_folder` WRITE;
/*!40000 ALTER TABLE `cms_media_folder` DISABLE KEYS */;

INSERT INTO `cms_media_folder` (`media_folder_id`, `media_folder_name`, `media_folder_link`)
VALUES
	(1,'Client logos',0),
	(2,'service icons',0),
	(3,'Partner logos',0),
	(4,'Service images',0);

/*!40000 ALTER TABLE `cms_media_folder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_media_folder_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_folder_link`;

CREATE TABLE `cms_media_folder_link` (
  `media_folder_link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_id` int(11) DEFAULT NULL,
  `media_files_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_folder_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `cms_media_folder_link` WRITE;
/*!40000 ALTER TABLE `cms_media_folder_link` DISABLE KEYS */;

INSERT INTO `cms_media_folder_link` (`media_folder_link_id`, `media_folder_id`, `media_files_id`)
VALUES
	(18,1,18),
	(19,1,19),
	(20,1,20),
	(21,1,21),
	(22,1,22),
	(13,1,13),
	(14,1,14),
	(15,1,15),
	(16,1,16),
	(17,1,17),
	(12,0,12),
	(23,1,23),
	(24,1,24),
	(25,1,25),
	(26,1,26),
	(27,1,27),
	(28,1,28),
	(29,1,29),
	(30,1,30),
	(31,1,31),
	(32,1,32),
	(33,1,33),
	(34,1,34),
	(35,1,35),
	(36,1,36),
	(37,2,37),
	(38,2,38),
	(39,2,39),
	(40,2,40),
	(41,2,41),
	(42,2,42),
	(43,2,43),
	(44,2,44),
	(45,2,45),
	(46,2,46),
	(47,3,47),
	(48,3,48),
	(49,3,49),
	(50,3,50),
	(51,3,51),
	(52,3,52),
	(53,3,53),
	(54,0,54),
	(55,4,55),
	(56,4,56),
	(57,4,57),
	(58,4,58),
	(59,4,59),
	(60,4,60),
	(61,4,61),
	(62,4,62),
	(63,4,63),
	(64,4,64),
	(65,4,65),
	(66,4,66),
	(67,4,67),
	(68,4,68),
	(69,4,69),
	(70,4,70),
	(71,4,71),
	(72,0,72),
	(73,0,73);

/*!40000 ALTER TABLE `cms_media_folder_link` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mod_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page`;

CREATE TABLE `mod_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_timestamp` int(11) NOT NULL,
  `page_date` int(11) NOT NULL,
  `page_price` double DEFAULT NULL,
  `page_category_id` int(11) DEFAULT NULL,
  `page_heading` varchar(400) NOT NULL,
  `page_short_description` text NOT NULL,
  `page_description` text NOT NULL,
  `page_link` varchar(400) NOT NULL,
  `page_link_video` int(1) NOT NULL,
  `page_archived` int(11) NOT NULL DEFAULT '0',
  `page_quantity` int(11) DEFAULT NULL,
  `page_code` varchar(100) DEFAULT NULL,
  `page_offset` int(11) DEFAULT NULL,
  `page_featured` int(11) DEFAULT '0',
  `page_hidden_on_site` int(11) DEFAULT '0',
  `page_sort` int(99) DEFAULT NULL,
  `page_colour` varchar(200) DEFAULT NULL,
  `page_anchor` varchar(400) DEFAULT NULL,
  `page_include` text,
  `page_align` varchar(100) DEFAULT NULL,
  `page_slug` varchar(400) DEFAULT NULL,
  `page_author` varchar(200) DEFAULT NULL,
  `page_special` int(11) DEFAULT '0',
  `page_in_the_news` int(11) DEFAULT '0',
  `page_latest_page` int(11) DEFAULT '0',
  `page_latest_article` int(11) DEFAULT '0',
  `page_latest_article_1` int(11) DEFAULT '0',
  `page_latest_article_2` int(11) DEFAULT '0',
  `page_latest_article_3` int(11) DEFAULT '0',
  `page_json` text,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `mod_page` WRITE;
/*!40000 ALTER TABLE `mod_page` DISABLE KEYS */;

INSERT INTO `mod_page` (`page_id`, `media_files_id`, `page_timestamp`, `page_date`, `page_price`, `page_category_id`, `page_heading`, `page_short_description`, `page_description`, `page_link`, `page_link_video`, `page_archived`, `page_quantity`, `page_code`, `page_offset`, `page_featured`, `page_hidden_on_site`, `page_sort`, `page_colour`, `page_anchor`, `page_include`, `page_align`, `page_slug`, `page_author`, `page_special`, `page_in_the_news`, `page_latest_page`, `page_latest_article`, `page_latest_article_1`, `page_latest_article_2`, `page_latest_article_3`, `page_json`)
VALUES
	(338,0,1470133598,0,0,337,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"%3Cp%3EOur+goal+is+to+exceed+the+expectations+of+every+client+by+offering+outstanding+customer+service%2C+increased+flexibility%2C+and+greater+value%2C+thus+optimizing+solution+functionality%2C+and+improving+operational+efficiency.+Our+associates+are+distinguished+by+their+functional+and+technical+expertise%2C+combined+with+their+hands-on+experience%2C+thereby+ensuring+that+our+clients+receive+the+most+effective+and+professional+service.%26nbsp%3B%3C%2Fp%3E%0D%0A%3Cp%3E%3Cimg+class%3D%22img-responsive%22+style%3D%22display%3A+block%3B+margin-left%3A+auto%3B+margin-right%3A+auto%3B%22+src%3D%22%5Bbasepath%5D%2Fcms%2Fmodules%2Fmedia%2Fscripts%2Fimage%2Fimage.handler.php%3Fmedia_files_id%3D70%26amp%3Bwidth%3D1080%22+alt%3D%22%22+width%3D%22853%22+height%3D%22702%22+%2F%3E%3C%2Fp%3E%0D%0A%3Cp%3E%3Cstrong%3EWhy+Us%3F%3C%2Fstrong%3E%3C%2Fp%3E%0D%0A%3Cp%3EOur+extensive+skills+encompass+all+aspects+of+ICT+solution+architecture%2C+design%2C+implementation+and+operation%2C+including+strategic+advisory+and+technology+roadmap+planning+services+coupled+with+ICT+rationalization+and+modernization+expertise.%3C%2Fp%3E%0D%0A%3Cp%3E%3Cstrong%3ESpecifics%3C%2Fstrong%3E%3C%2Fp%3E%0D%0A%3Cp%3EAs+part+of+the+architectural+methodology+a+due+diligence+will+be+done+to+assess+the+current+environment+to+determine+the+most+effective+and+effective+method+of+migration+or+solution+implementation.%3C%2Fp%3E%0D%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E%0D%0A%3Cdiv+class%3D%22call-to-action%22+style%3D%22background-color%3A+%23fbbf0f%3B+color%3A+%23ffffff%3B+padding%3A+10px%3B+text-transform%3A+uppercase%3B+display%3A+inline-block%3B%22%3ECALL+US+TODAY!%3C%2Fdiv%3E%0D%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E%0D%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E\"}}}}'),
	(337,0,1468233816,0,0,335,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>Our dedicated service desk functions as our central services center. It is also our &lsquo;single point of contact center&rsquo; (SPoC). From here we provide the full managed-services package, remote management as well as monitoring services to all our clients. We offer ad hoc as well as permanent, on-site based technicians and SLA support.&nbsp;<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=68&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"850\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p><strong>Why Us?<\\/strong><\\/p><p>ECOTECH Converge makes use of the Autotask and Solarwinds N-Able range of products to provide managed, dedicated or shared service desk capabilities coupled to the full remote management, monitoring and support services capabilities. The ECOTECH Converge &lsquo;Managed Services&rsquo; and &lsquo;Operational Support Services&rsquo; teams are fully aligned with the ITILv3 best practices.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>The ITIL (Information Infrastructure Library) approach is a set of best practices for IT service management that focus on aligning IT services with the needs of your business.<\\/p><p>ITIL describes processes, procedures, tasks and checklists that are used by an organization for establishing integration with strategy, delivering value and maintaining a minimum level of competency. It provides a baseline from which one can plan, implement, and measure, and is used to demonstrate compliance and measure improvement.<\\/p><ul><li><strong>Incident Management:<\\/strong> Restores normal service operation as quickly as possible.<\\/li><li><strong>Problem Management:<\\/strong> Resolves the root causes of incidents, prevents recurrence of incidents related to these errors and includes problem identification and recording, classification, investigation and diagnosis.<\\/li><li><strong>Change Management:<\\/strong> Ensures that standardized methods and procedures are used for efficient handling of all changes.<\\/li><li><strong>Asset Management:<\\/strong> The management and traceability of every aspect of a configuration from beginning to end, which includes identification, planning, change control, change management, release management and maintenance.<\\/li><\\/ul><p>&nbsp;<\\/p><p><strong>Our service desk is located in Pretoria at the ECOTECH Converge HQ. You can <span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\"><a href=\\\\\\\"..\\/content\\/contact-us\\\\\\\">contact our service desk<\\/a><\\/span>&nbsp;to discuss different options for support.&nbsp;<\\/strong><\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(317,0,1468234879,0,0,325,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=71&amp;width=478\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"478\\\\\\\" height=\\\\\\\"436\\\\\\\" \\/><\\/p>\"}}}}'),
	(318,0,1467838503,0,0,325,'','','','',0,1,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>At ECOTECH Converge we regard partnerships as a highly valued attribute to our business. Strategic partnerships empower us to leverage of leading industry knowledge, thus enabling us to deliver &ldquo;best of breed&rdquo; solutions to our clients.<\\\\\\/p><p>We will work with various suppliers and partners to deliver products and services as and when required by our clients. This will include amongst others the following:<\\\\\\/p>\"}},\"row-1\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"10\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"empty\":\"\"},\"col3\":{\"empty\":\"\"}}}}'),
	(319,0,1470133759,0,0,333,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>We provide hosting, hosted private cloud, hosted multi-tenant cloud and public could services to support and deliver numerous private and commercial platforms. Our cloud and hosting facilities and distributed service delivery nodes are managed and operated 24\\/7\\/365.<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=58&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"880\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p><strong>Why Us?<br \\/><\\/strong>We have dedicated world-class hosting facilities in our Bryanston and Pretoria premises. Our cloud and hosting team manages all facets, from inception through the lifecycle of the service\\/solutions. We leverage industry-leading technology, hardware, storage, network and connectivity solutions to craft solutions that meet our client&rsquo;s explicit needs. This capability and competency provides all facets of ICT services and provides adaptive scale and aligned licensing models to provide a utility based, consumption based ICT service<\\/p><p><strong><strong>Specifics<br \\/><\\/strong><\\/strong>The two ECOTECH Converge datacenters deliver high-tech, geo resilient services. The distance between these centers absolutely meets the compliance mandates for disaster recovery. They are hosted within the borders of Southern Africa, thereby also complying with data sovereignty legislation. These facilities form the core of the ECOTECH Converge cloud services staging, R&amp;D, solution and service development platforms. Through our partnership with Teraco, and the distributed datacenters they provide &ndash; nationally and internationally &ndash; we have the reach and capability to delivery cloud-based or hosted ICT services to all clients, regardless of the geography, and the capabi<strong><strong>l<\\/strong><\\/strong>ity to operate and support these services.<\\/p><p><strong>Cloud Services:<\\/strong><\\/p><ul><li>IaaS (Infrastructure as a service).<\\/li><li>SaaS (Software as a service).<\\/li><ul><li>Hosted Exchange.<\\/li><li>O365.<\\/li><li>Skype for Business.<\\/li><\\/ul><li>PaaS (Platform as a service).<\\/li><ul><li>SharePoint.<\\/li><\\/ul><li>DBaaS (Database as a service).<\\/li><\\/ul><p>&nbsp;<\\/p><div class=\\\\\\\"call-to-action\\\\\\\" style=\\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\\"><strong>CALL US TODAY!&nbsp;<\\/strong><\\/div><p>&nbsp;<\\/p><p>&nbsp;<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(365,0,1470133403,0,0,358,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">Infrastructure as a service (IaaS)<\\/span><\\/h3><p>IaaS provides on demand computing infrastructure (server and storage) up to the level of operating system. Our initial offering covers physical or virtual servers on Intel technologies. The options are as follows:<\\/p><ul><li>Server on Demand &ndash; Microsoft Windows Server 2008, 2012, 2012 R2 or Red Hat<\\/li><li>Storage on demand &ndash; Selectable in Gigabyte blocks<\\/li><li>Virtualisation &ndash; VMWare or Microsoft Hyper-V<\\/li><li>Optional Backup<\\/li><\\/ul>\"}}}}'),
	(340,0,1468233159,0,0,341,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>ECOTECH Converge offers a wide range of information technology hardware and peripheral devices catering for most IT requirements. &nbsp;<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=56&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"703\\\\\\\" \\/><\\/p><p><strong>Why us?<\\/strong><\\/p><p>ECOTECH Converge provides a wide range of services and products to choose from, catering for all customer requirements. We believe in maintain excellent relationships, not only with our clients, but as importantly, with our vendors and suppliers. This ensures superior supplier support, enabling us to provide clients with excellent products and customer aftersales support at all times.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>We offer hardware and peripherals for the following fields:<\\/p><ul><li>Hardware components.<\\/li><li>Peripherals.<\\/li><li>Networking hardware.<\\/li><li>Cabling.<\\/li><li>Consumables.<\\/li><li>IP phones.<\\/li><li>Printers.<\\/li><li>Notebooks.<\\/li><li>Personal computers.<\\/li><li>Software.<\\/li><li>Uninterruptable power solutions.<\\/li><li>Surveillance.<\\/li><li>Security software.<\\/li><li>Audio and visual.<\\/li><li>Server solutions.<\\/li><li>Backup hardware solutions.<\\/li><\\/ul><p><strong>ECOTECH converge welcomes the opportunity to provide this excellent service for your company. All quotes and requests can be mailed to <span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\"><a href=\\\\\\\"\\\\\\\\\\\\\\\">reps@ecocorp.co.za<\\/a><\\/span><\\/strong><\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(339,0,1468233362,0,0,340,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>ECOTECH Converge supply various turnkey connectivity solutions including, but not limited to, managed connectivity, broadband, V-Sat and internet service provider services.&nbsp;<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=59&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" \\/><\\/p><p><strong>Why us?<\\/strong><\\/p><p>ECOTECH Converge has built up a highly competent and reliable network team that focuses on quality and doing the cut over right first time without repeats or returns. We utilize a proven methodology, providing a niche service offering.<\\/p><p>We have developed a dedicated group of experts dealing with all segments of networking and connectivity and industry aligned solutions and offerings. Our strategic partnerships with key carriers such as NEOTEL, SEACOM, TELKOM, and our relationships with key delivery partners makes it possible for us to deliver world-class connectivity solutions which span the globe and which provide true value.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>ECOTECH Converge currently services clients across more than 8 countries globally, leveraging solutions that range from low cost broadband, through cellular\\/GSM, Fibre Optic and V-SAT technologies to provide customized, turnkey connectivity solutions that align to the needs and budgets of our clients.<\\/p><ul><li>V-SAT connectivity into African countries, or remote locations where fixed line or other services are not available.<\\/li><li>Local, shaped and unshaped broadband services.<\\/li><li>Business fibre connectivity ranging from best effort to full service level agreement services.<\\/li><li>Hosted service delivered over fibre cable.<\\/li><li>Local Area and Wide Area network solutions, design, planning, implementation, equipment, infrastructure and services.<\\/li><li>Virtual private network solutions.<\\/li><li>Bandwidth optimization, prioritization and filtering\\/management solutions.<\\/li><\\/ul><p>&nbsp;<\\/p>\"}}}}'),
	(324,0,1468234036,0,0,343,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=65&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p>\"}}}}'),
	(325,0,1468233781,0,0,344,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>IaaS provides on-demand computing infrastructure (compute and storage) up to the level of the operating system. The base offering covers physical or virtual servers on Intel technologies. The solution provides resources from a single server up to hundreds of server depending on your unique requirements. The IaaS is run across numerous blade chassis and SAN enclosure to ensure enterprise scale.<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=67&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p><strong>The options are as follows:<\\/strong><\\/p><ul><li>Server on-demand &ndash; Linux or Microsoft operating systems.<\\/li><li>Storage on-demand &ndash; selectable in gigabyte blocks.<\\/li><li>Virtualisation &ndash; Microsoft Hyper-V.<\\/li><li>Optional snapshot, replication or backup services.<\\/li><li>Optional off-site storage.<\\/li><\\/ul><p><br \\/><strong>Customer Scenario:<\\/strong><\\/p><ol><li><strong>Business Challenge:<\\/strong> Need compute processing power and storage for test and development, or Quality assurance and end user acceptance testing platforms? Need additional resources for go to market and sales initiatives? Does month end processing deadline put your existing infrastructure under strain? Is there a rapid infrastructure deployment requirement to launch a product or service to market?<\\/li><li><strong>Solution:<\\/strong> ECOTECH Infrastructure as a Service. Utilise efficiently provisioned physical or virtual servers hosted in our secure, highly available cloud data centres. Rent a configured server(s), add extra storage and options such as backup\\/recovery and within minutes you are ready to go. And you pay only for what you specify and use on a per month basis.<\\/li><li><strong>Service Description:<\\/strong> A Physical Server or a Virtualised Server. Either instance is provisioned on a server blade farm with Windows Server 2008\\/2012R2 or Linux Server derivatives as the guest OS. Microsoft Hyper-V is the virtualisation technology used to host the virtual machines.<\\/li><\\/ol><p>&nbsp;<\\/p>\"}}}}'),
	(326,0,1468233640,0,0,338,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>Microsoft technologies and solutions are widely deployed and utilized today, which includes everything from server platform solutions and tools, to systems management and desktop infrastructure. ECOTECH Converge, through our partnership and relationship with Microsoft, helps customers design and implement secure, optimized and well managed Microsoft infrastructure solutions in a practical way.<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=64&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p>We are a Microsoft certified partner and carry a number of Microsoft Gold competencies. Our Microsoft Windows Azure Pack Cloud hosting platform provides resilient and reliable utility-based computing services that have been developed with our clients&rsquo; explicit needs for reliability and security in mind.&nbsp;<\\/p><p><strong>Why Us?<\\/strong><\\/p><p>Our cloud and datacenter competencies now leverage Microsoft Windows Azure Pack in all ECOTECH Datacenters, and provide extended reach, capability and on-demand capacity and scale through Microsoft Windows Azure. With datacenters located across the globe, this strategic and symbiotic relationship equips us with the ability to provide a wide range of highly advanced, highly distributed and global cloud services and solutions.<\\/p><p>In addition, ECOTECH Converge has experience with large hosting facilities and datacenters, having been responsible for the successful implementation of not only our own world-class hosting and cloud facility, but also client infrastructures and datacenters across the globe.<\\/p><p><strong>Specifics:<\\/strong><\\/p><p>In order to meet the individual needs of clients, ECOTECH Converge maintains a wide range of expertise and has aligned very strategic partnerships. We excel in the provision and delivery of Microsoft-based traditional and cloud solutions, and underpin the services we deliver with highly skilled staff.<\\/p><p><strong>Services:<\\/strong><\\/p><ul><li>Hyper-V<\\/li><li>Office 365<\\/li><li>SQL 2016<\\/li><li>Azure<\\/li><li>Exchange<\\/li><li>Sharepoint<\\/li><li>Azure<\\/li><\\/ul><p>&nbsp;<\\/p>\"}}}}'),
	(327,0,1468233544,0,0,339,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>Due to a growing demand from business environments, organizations are placing more and more pressure on IT to deliver reliable and cost effective solutions. ECOCORP Cloud Services addresses these requirements by providing &lsquo;anywhere access&rsquo; to your employees&rsquo; basic applications. ECOTECH &lsquo;Infrastructure as a Service&rsquo; allows companies to free up resources and allow IT to focus on more strategic objectives&hellip;<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=62&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"1070\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p><strong>Why us?<\\/strong><\\/p><p>A large number organizations are realizing that IT infrastructure cost and complexity are increasing each year, making it increasingly difficult to recognize value while making use of limited resources. ECOTECH Converge believe that our solution offers the most effective route to productive use at the lowest total cost of ownership in the South African market. In addition, the solution seamlessly migrates the current operating environment with the least amount of disruption. Virtual machines and storage can be provisioned in real time using the self-service IaaS portal using highly automated processes.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>IaaS provides on demand computing infrastructure (server and storage) up to the level of operating system. Our initial offering covers physical or virtual servers on Intel technologies. The options are as follows:<\\/p><ul><li>Server on Demand &ndash; Microsoft Windows Server 2008, 2012, 2012 R2 or Red Hat<\\/li><li>Storage on demand &ndash; Selectable in Gigabyte blocks<\\/li><li>Virtualisation &ndash; VMware or Microsoft Hyper-V<\\/li><li>Optional Backup<\\/li><\\/ul>\"}}}}'),
	(350,0,1468234129,0,0,348,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><strong>Talk to a consultant now:&nbsp;<\\/strong>0861 ECOTECH (326 8324) \\/&nbsp;Int: +27 12 843 9700<\\/h3><p>&nbsp;<img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=60&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"807\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p>\"}},\"row-1\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\" text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Pretoria (Head Office):<\\/span><\\/h3><p>62 Cussonia Ave, Val de Grace, Pretoria, 0184&nbsp;<br \\/>Tel: +27 (12) 843 9700<br \\/>Fax: +27 (12) 843 9701<br \\/>Email:&nbsp;<a href=\\\"\\\">info@ecotechconverge.co.za<\\/a>&nbsp;<\\/p><p>&nbsp;<\\/p>\"},\"col2\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\" text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Johannesburg:<\\/span><\\/h3><p>The Campus, 57 Sloan Street, Bryanston, The Gabba Building<br \\/>Tel: +27 (12) 843 9777<br \\/>Email:&nbsp;<a href=\\\"\\\">corporate@ecotechconverge.co.za<\\/a><\\/p><p>&nbsp;&nbsp;<\\/p>\"}},\"row-2\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\" text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Cape Town:<\\/span><\\/h3><p>2nd Floor, Imperial Terraces, Tyger Waterfront, Carl Cronje Drive, Bellville, 7530<br \\/>Tel: 086 144 7338<br \\/>Fax: +27 (21) 555 0276<br \\/>Email:&nbsp;<a href=\\\"\\\">capetown@ecotechconverge.co.za<\\/a><\\/p>\"},\"col2\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\"text-yellow\\\" style=\\\"color: #fbbf0f;\\\"><span class=\\\"\\\">Quick Contacts:<\\/span><\\/span><\\/h3><p>Channel Services:<\\/p><p>Tel: +27 (12) 843 9700<br \\/>Email: channel@ecotechconverge.co.za&nbsp;<\\/p><p><span class=\\\"\\\">Service Desk:<\\/span><\\/p><p>Tel: +27 (12) 843 9777<br \\/>Email:&nbsp;<a>servicedesk@ecotechconverge.co.za<\\/a><\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(335,0,1468233847,0,0,334,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>These services provide businesses with Voice Over IP (VOIP) and least cost routing options that have been designed to work &ldquo;on premise&rdquo; as well as &ldquo;in the cloud&rdquo; as online services at the same time.&nbsp;<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=69&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"625\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p><strong>Why Us?<\\/strong><\\/p><p>ECOTECH Converge provides a hosted VOIP PBX solution, delivering productive phone calling features, traditionally only available to larger enterprises. Our VOIP services come with the added benefit of having no in-house systems or software to procure, manage or maintain. We do it all!<\\/p><p>Our VOIP solution provides businesses with access to advanced functionality, easy maintenance from remote locations, and a user-friendly system, which adds up to advantages in worker productivity. Low maintenance demands also free up internal IT staff to focus on more high-value pursuits.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>ECOTECH Converge provides an on-Premise VOIP PBX solution, which delivers the same services and benefits as the Hosted VOIP PBX, but with the added capability of being able to combine the VOIP PBX \\/ Gateway with traditional PBX functionality. This enables businesses to utilize existing infrastructure, realizing further cost savings, and greater mobility.<\\/p>\"}}}}'),
	(342,0,1468233508,0,0,317,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>Exceptional functional and technical expertise, coupled with extensive industry knowledge makes ECOTECH Converge the ideal choice in a consulting firm. Choose us to manage the implementation and operations of your ICT infrastructure requirements.<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=61&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"755\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p><p>ECOTECH Converge provides superior consulting services for IT Infrastructure and services management. We work in the financial, mining, logistics, agriculture, hospitality, telecoms and multiple other industries, serving both the domestic and international sectors. Our headquarters is in Pretoria, South Africa.<\\/p><p>We pride ourselves on our proven track record, effectively administering multiple corporate ICT infrastructures. We have an acute understanding of each of our client&rsquo;s unique challenges, and are able to provide turnkey solutions for managing people, processes and technology.<\\/p><p>ECOTECH Converge has experience with large hosting facilities and datacenters. We successfully implement client infrastructures and datacenters across the globe, as well as having created our very own world-class infrastructures to manage our own business.<\\/p><p>In order to meet the individual needs of clients, ECOTECH Converge maintains a wide range of expertise:<\\/p><ul><li>We have established numerous strategic partnerships to assist us in delivering the highest quality and range of services.<\\/li><li>We excel in the Microsoft competency.<\\/li><li>We back this up with core understanding and delivery of managed hosting, broadband connectivity and cutting edge data solutions.<\\/li><\\/ul><p>&nbsp;<\\/p><p><strong>What makes ECOTECH different?<\\/strong><\\/p><p>ECOTECH Converge brings a fresh and innovative approach to consulting services, acting as liaison between the end-user and the software provider. Our goal is to exceed the expectations of every client by offering outstanding customer service, increased flexibility, and greater value, thus optimizing system functionality and improving operational efficiency. Our associates are distinguished by their functional and technical expertise, combined with their hands-on experience, thereby ensuring that our clients receive the most effective and professional service.<\\/p><p>Our extensive skills encompass all aspects of ICT implementation and operation, including business requirements definition, development of functional specifications for client approval, system design, and overseeing vendor and solution selection to fit specific client needs.<\\/p><p>ECOTECH Converge identifies with the following fundamental ethics:<\\/p><ul><li>Operational brilliance.<\\/li><li>A solid ethos of reliability, truthfulness and honour.<\\/li><li>Respect for each individual at each business rank.<\\/li><li>Taking responsibility at an organisational and employee level.<\\/li><\\/ul><p>&nbsp;<\\/p><p><strong>History<\\/strong><\\/p><p>A converged unification of talents brought together by market conditions, acquired skills and business acumen. ECOTECH Converge had a very humble start as less than a hand full of people originating from several ITC sectors focused on distribution in a garage size premises before 2001.<\\/p><p>Clients needed a more comprehensive provider and dealers needed support which led to the organic growth of the Corporate Services division to compliment distribution and expand the business into other market segments.<\\/p><p>The garage grew to several neighbouring premises, host to multi storey buildings and even Data Centre Facilities that in conjunction with the national support offices and data centres deliver the broad yet specialised spectrum of services and solutions to our clients all over the globe.<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(341,0,1468233304,0,0,347,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>ECOTECH Converge is extremely proud of the consistently positive feedback we receive from our clients. We currently have more than 1600 active clients varying from small to international corporates in all segments of the economy.<\\/p><p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=57&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"878\\\\\\\" height=\\\\\\\"701\\\\\\\" \\/><\\/p><p>&nbsp;Our operational business sectors &amp; client list:<\\/p><ul><li>Transportation<\\/li><li>Mining<\\/li><li>Insurance<\\/li><li>Property Investment<\\/li><li>Financial<\\/li><li>Retail<\\/li><li>Utilities<\\/li><li>ICT<\\/li><li>Telecommunications<\\/li><li>Public<\\/li><li>Resources<\\/li><li>Legal<\\/li><li>Hospitality<\\/li><li>Tourism<\\/li><li>Education<\\/li><\\/ul>\"}},\"row-1\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"22\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"16\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"17\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"34\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-2\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"23\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"27\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"33\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"24\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-3\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"15\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"20\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"29\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"36\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-4\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"25\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"35\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"13\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"14\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-5\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"32\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"30\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"28\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"26\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-6\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"21\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"19\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"18\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"31\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}}}}'),
	(351,0,1468233744,0,0,320,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>ECOTECH partners with choice international service providers &amp; key players, ensuring quality &amp; reliable service to all clients.<\\/p><p>&nbsp;<img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=66&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p>\"}},\"row-1\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"47\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"52\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"50\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"type\":\"image\",\"data\":{\"id\":\"53\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}}},\"row-2\":{\"col1\":{\"type\":\"image\",\"data\":{\"id\":\"48\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col2\":{\"type\":\"image\",\"data\":{\"id\":\"49\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col3\":{\"type\":\"image\",\"data\":{\"id\":\"51\",\"width\":\"600\",\"quality\":\"100\",\"aspect_r\":\"0\"}},\"col4\":{\"empty\":\"\"}}}}'),
	(352,0,1469800214,0,0,349,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">SQL 2016<\\/span><\\/h3><p><strong>Query Store:<\\/strong><\\/p><p>One common problem many organizations face when upgrading versions of SQL Server is changes in the query optimizer (which happen from version to version) negatively impacting performance. Without comprehensive testing, this has traditionally been a hard problem to identify and then resolve. The Query Store feature maintains a history of query execution plans with their performance data, and quickly identifies queries that have gotten slower recently, allowing administrators or developers to force the use of an older, better plan if needed. The Query Store is configured at the individual database level.<\\/p><p><strong>Polybase:<\\/strong><\\/p><p>Hadoop and Big Data have been all the rage in the last several years. I think some of that was industry hype, but Hadoop is a scalable, cost-effective way to store large amounts of data. Microsoft had introduced Polybase, a SQL Server connector to Hadoop (and Azure Blob Storage) to its data warehouse appliance Analytics Platform System in 2015. But now Microsoft has incorporated that functionality into the regular on-premises product. This feature will benefit you if your regular data processing involves dealing with a lot of large text files -- they can be stored in Azure Blob Storage or Hadoop, and queried as if they were database tables. A common scenario where you might use this would be an extract, transform and load (ETL) process, where you were taking a subset of the text file to load into your database.<\\/p><p><strong>Stretch Database:<\\/strong><\\/p><p>One common idiom in recent years, is how cheap storage is. While it may be cheap to buy a 3TB drive from Amazon, if you are buying enterprise-class SAN storage or enterprise SSDs, you will know that storage is still very expensive. Microsoft is trying to help reduce your storage (and processing costs) with a hybrid feature called \\\\\\\\\\\\\\\"Stretch Database.\\\\\\\\\\\\\\\" The basics of Stretch Database are that some part of your tables (configurable or automated) will be moved into an Azure SQL Database in the cloud in a secure fashion. When you query those tables, the query optimizer knows which rows are on your server and which rows are in Azure, and divides the workload accordingly. The query processing on the Azure rows takes place in Azure so the only latency is for the return of the rows over the network. As an additional enhancement, you are only charged for the SQL Database in Azure when it is used for queries. You do, however, pay for the Azure Blob storage, which, generally speaking, is much cheaper than enterprise storage.<\\/p><p><strong>JSON Support:<\\/strong><\\/p><p>In addition to supporting direct querying to Hadoop, SQL Server 2016 adds support for the lingua franca of Web applications: Java Script Object Notation (JSON). Several other large databases have added this support in recent years as the trend towards Web APIs using JSON has increased. The way this is implemented in SQL 2016 is very similar to the way XML support is built in with FOR JSON and OPENJSON -- providing the ability to quickly move JSON data into tables.<\\/p><p><strong>Row Level Security:<\\/strong><\\/p><p>A feature that other databases have had for many years, and SQL Server has lacked natively is the ability to provide row-level security (RLS). This restricts which users can view what data in a table, based on a function. SQL Server 2016 introduces this feature, which is very useful in multi-tenant environments where you may want to limit data access based on customer ID. I\\\\\\\\\\\\&#39;ve seen some customized implementations of RLS at clients in the past, and they weren\\\\\\\\\\\\&#39;t pretty. It is hard to execute at scale. The implementation of RLS in SQL 2016 still has it limits (updates and inserts are not covered), but it is good start on a much-needed feature.<\\/p><p><strong>Always Encrypted:<\\/strong><\\/p><p>It seems like every month, we hear about some company having a major data breach. Encryption works, but many companies do not or cannot implement it all the way through the stack, leaving some layer data available for the taking as plain text. SQL Server has long supported both column-level encryption, encryption at rest, and encryption in transit. However these all had to be configured independently and were frequently misconfigured. Always Encrypted is new functionality through the use of an enhanced client library at the application so the data stays encrypted in transit, at rest and while it is alive in the database. Also given Microsoft\\\\\\\\\\\\&#39;s push towards the use of Azure, easy encryption makes for a much better security story.<\\/p><p><strong>In-Memory Enhancements:<\\/strong><\\/p><p>SQL Server 2014 introduced the concept of in-memory tables. These were optimally designed for high-speed loading of data with no locking issues or high-volume session state issues. While this feature sounded great on paper, there were a lot of limitations particularly around constraints and procedures. In SQL Server 2016, this feature is vastly improved, supporting foreign keys, check and unique constraints and parallelism. Additionally, tables up to 2TB are now supported (up from 256GB). Another part of in-memory is column store indexes, which are commonly used in data warehouse workloads. This feature was introduced in SQL 2012 and has been enhanced in each version since. In 2016 it receives some enhancements around sorting and better support with AlwaysOn Availability Groups.<\\/p><p><strong>SQL Server R Services:<\\/strong><\\/p><p>SQL Server R Services is a new feature in SQL Server 2016 that supports enterprise-scale data science. SQL Server R Services helps you embrace the highly popular open source R language in your business. R is the most popular programming language for advanced analytics, and offers an incredibly rich set of packages and a vibrant and fast-growing developer community.<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(356,0,1470042428,0,0,353,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<ul><li>Server Hosting<\\/li><li>Server Platform<\\/li><li>Management and Virtualisation<\\/li><li>Identity and Access<\\/li><li>Hosted Infrastructure Services<\\/li><li>Data Centre<\\/li><li>Public, Private or Hybrid Cloud Services<ul><li>Managed Hosting<\\/li><li>Application Managed Hosting.<ul><li>Remote Desktop Services.<\\/li><li>Windows 7\\/8\\/10.<\\/li><li>Office 2013\\/2016.<\\/li><li>Sage ERP.<\\/li><\\/ul><\\/li><li>Disaster Recovery Planning and Facilitation<\\/li><\\/ul><\\/li><\\/ul>\"}}}}'),
	(353,0,1467975842,0,0,352,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">Azure<\\/span><\\/h3><p>Any developer or IT professional can be productive with Azure. The integrated tools, pre-built templates and managed services make it easier to build and manage enterprise, mobile, Web and Internet of Things (IoT) apps faster, using skills you already have and technologies you already know. Microsoft is also the only vendor positioned as a Leader across Gartner&rsquo;s Magic Quadrants for Cloud Infrastructure as a Service, Application Platform as a Service, and Cloud Storage Services for the second consecutive year.<\\/p><p><strong>Use an open and flexible cloud service platform<\\/strong><\\/p><p>Azure supports the broadest selection of operating systems, programming languages, frameworks, tools, databases and devices. Run Linux containers with Docker integration; build apps with JavaScript, Python, .NET, PHP, Java and Node.js; build back-ends for iOS, Android and Windows devices. Azure cloud service supports the same technologies that millions of developers and IT professionals already rely on and trust.<\\/p><p><strong>Extend your existing IT<\\/strong><\\/p><p>Some cloud providers make you choose between your data centre and the cloud. Not Azure, which easily integrates with your existing IT environment through the largest network of secure private connections, hybrid database and storage solutions, and data residency and encryption features &ndash; so your assets stay right where you need them. And with&nbsp;Azure Stack, you can bring the Azure model of application development and deployment to your data centre. Azure hybrid cloud solutions give you the best of both worlds: more IT options, less complexity and cost. That&rsquo;s why it&rsquo;s one of the best&nbsp;cloud computing services&nbsp;available.<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(354,0,1468227019,0,0,346,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>ECOTECH\\\\&#39;s Service Level Agreement ensures transparency, service efficiency and consistency. This SLA is generic and serves as a guideline to that of a typical agreement.&nbsp;<\\/p><p><a href=\\\\\\\"[basepath]cms\\/modules\\/media\\/scripts\\/documents\\/document.handler.php?media_files_id=54\\\\\\\">Download PDF<\\/a><\\/p>\"}}}}'),
	(364,0,1470133173,0,0,357,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">Public, Private or Hybrid Hosting?<\\/span><\\/h3><p>ECOTECH Converge provides public, private or hybrid cloud solutions. Consulting and professional services help clients assess different technological and methodological strategies and aids in the alignment of business in the cloud.<\\/p><ul><li><strong> Semi managed hosting:<\\/strong><\\/li><\\/ul><p>Services typically include monitoring of hardware and software infrastructure to ensure uptime and availability, as well as timeous notification to customers or vendors in case of failures and facilitation of incident management processes.<\\/p><ul><li><strong> Fully managed hosting:<\\/strong><\\/li><\\/ul><p>Services include &ldquo;semi-managed hosting services&rdquo;, operating systems and update management and support, as well as the management and support of all additional service add-ons that the customer has subscribed to. Such services include the above-mentioned managed data protection and recovery, endpoint security,managed perimeter security etc.<\\/p><ul><li><strong> Application managed hosting:<\\/strong><\\/li><\\/ul><p>Management and support services of standard Microsoft server side applications, systems and tools related to server platform, management and virtualization, messaging, communications and identity and access management.<\\/p><p><a href=\\\\\\\"..\\/content\\/contact-us\\\\\\\">TALK TO A CONSULTANT NOW!<\\/a><\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(349,0,1468233601,0,0,318,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=63&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p>\"}},\"row-1\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\"text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Simon Martyn, Principal Solutions Architect<\\/span><\\/h3><p><strong>Highly experienced specialist in Cloud, Datacenter and ICT Transformation, IT Strategy &amp; Managed and Professional Services<\\/strong><\\/p><p>Simon is an enterprise solutions-oriented, &ldquo;Principal Solutions Architect&rdquo; and architecture lead of the ECOTECH Converge &ldquo;Solutions Architecture&rdquo; practice. Simon has notable success in defining strategic IT architectural roadmaps and associated strategies in order to leverage new and existing organizational ICT infrastructure to meet new and changing business requirements.<\\/p><p>Simon has 22 years of experience in enterprise information technology . He boasts an impressive track record of directing a broad range of large -scale corporate information technology initiatives and strategies.<\\/p>\"},\"col2\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\"text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Zac Hooper, Cloud Infrastructure Business Manager.<\\/span><\\/h3><p><strong>Highly experienced specialist in Cloud, Datacenter and ICT Transformation, IT Strategy &amp; Managed and Professional Services.<\\/strong><\\/p><p>Zac has been in the IT services industry for the last 23 years and has spent the last few years working as a solutions\\/infrastructure architect for ECOTECH, Datacentrix and Gijima. His main focus has been on core infrastructure and business productivity solutions with later emphasis on cloud, application and virtualization solutions. Primarily focus in the standardization and optimization of the infrastructure technology domain of enterprise architecture as well as technology road-maps and strategy.<\\/p><p>Zac has 23 years of experience in enterprise information technology. He has a proven track record of directing a broad range of large-scale, corporate information technology initiatives and strategies.<\\/p><p>&nbsp;<\\/p>\"}},\"row-2\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\"text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Alex Pohl, Support Services Manager.<\\/span><\\/h3><p><strong>Highly experienced in Consulting, Projects and Managed Services.<\\/strong><\\/p><p>Alex is the &ldquo;Managed and Support Services&rdquo; lead within ECOTECH Converge. He is a service orientated individual with vast experience in excess of 10 years, across several IT fields. Alex&rsquo;s focus is on developing strong support and delivery teams, capabilities and the competencies to support the highly complex traditional and modern\\/cloud driven ITC landscapes.<\\/p><p>The ECOTECH Converge support services team that Alex has developed over the last 9 years excels at Call Center and First Call resolution, on-site and remote, re-active and pro-active ICT support services, all of which aim to address, manage and support all facets of our clients ICT landscape.<\\/p><p>With the evolution of ICT into Cloud services, Voice over IP telephony and remote deployment of systems and services, coupled to management and operational support services, the service provided by Alex&rsquo;s team is aligned to providing &ldquo;everything as a service&rdquo; with a key focus on customer satisfaction and world class service delivery.<\\/p>\"},\"col2\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\"text-yellow\\\" style=\\\"color: #fbbf0f;\\\">Thys Smith, Business Development Manager.<\\/span><\\/h3><p><strong>Highly Experienced In Managing Operations, Solution Design and Solution Sales of Corporate &amp; Enterprise Solutions.<\\/strong><\\/p><p>Thys is the ECOTECH Converge Corporate Operations Manager. Thys was instrumental in the development the first ECOTECH Converge Data Centers and the ECOTECH Cloud and Hosting Business. Thys has spent over a decade with ECOTECH Converge, focusing on the development of solutions and services for the enterprise and corporate ICT market.<\\/p><p>Thys has developed a world-class Cloud, Hosting and Datacenter practice that now serves in excess of a 1000 ECOTECH Converge clients. Its focus is on delivering &ldquo;everything as a service&rdquo;, aligned to the needs of the current enterprise and corporate client landscape.<\\/p><p>Thys&rsquo; efforts in developing unique to market offerings and services focused on the needs and requirements of the financial services industry has provided ECOTECH Converge with a very well known and respected Cloud and Datacenter Competency. He and his team&rsquo;s acknowledged excellence in service delivery, service quality, business enabling and secure ICT services have been widely recognized.<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(355,0,1468234083,0,0,345,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><img class=\\\\\\\"img-responsive\\\\\\\" style=\\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\\" src=\\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=65&amp;width=1080\\\\\\\" alt=\\\\\\\"\\\\\\\" width=\\\\\\\"679\\\\\\\" height=\\\\\\\"702\\\\\\\" \\/><\\/p>\"}}}}'),
	(357,0,1470133704,0,0,325,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<ul><li>Server Hosting.<\\/li><li>Server Platform.<\\/li><li>Management and Virtualisation.<\\/li><li>Identity and Access.<\\/li><li>Hosted Infrastructure Services.<\\/li><li>Data Centre.<\\/li><li>Public, Private or Hybrid Cloud Services.<ul><li>Managed Hosting.<\\/li><li>Application Managed Hosting.<ul><li>Remote Desktop Services.<\\/li><li>Windows 7\\/8\\/10.<\\/li><li>Office 2013\\/2016.<\\/li><li>Sage ERP.<\\/li><\\/ul><\\/li><li>Disaster Recovery Planning and Facilitation.<\\/li><\\/ul><\\/li><\\/ul><div class=\\\\\\\"call-to-action\\\\\\\" style=\\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\\">CALL US TODAY!<\\/div>\"}}}}'),
	(358,0,1470133656,0,0,355,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<ul><li><p>Desktop Service:<\\/p><ul><li><p>Windows 7\\/8\\/10.<\\/p><\\/li><li><p>Office 2013\\/2016.<\\/p><\\/li><li><p>Anti-Virus.<\\/p><\\/li><\\/ul><\\/li><li><p>Consulting, Professional Services &amp; Solutions.<\\/p><\\/li><li><p>Connectivity Solutions.<\\/p><\\/li><\\/ul><p>&nbsp;<\\/p><div class=\\\\\\\"call-to-action\\\\\\\" style=\\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\\">CALL US TODAY!<\\/div>\"}}}}'),
	(359,0,1470130974,0,0,356,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><strong>Overview<\\/strong><\\/p><p>ECOTECH Converge supply various turnkey connectivity solutions including, but not limited to, managed connectivity, broadband, V-Sat and internet service provider services.&nbsp;<\\/p><p><strong>Why us?<\\/strong><\\/p><p>ECOTECH Converge has built up a highly competent and reliable network team that focuses on quality and doing the cut over right first time without repeats or returns. We utilize a proven methodology, providing a niche service offering.<\\/p><p>We have developed a dedicated group of experts dealing with all segments of networking and connectivity and industry aligned solutions and offerings. Our strategic partnerships with key carriers such as NEOTEL, SEACOM, TELKOM, and our relationships with key delivery partners makes it possible for us to deliver world-class connectivity solutions which span the globe and which provide true value.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>ECOTECH Converge currently services clients across more than 8 countries globally, leveraging solutions that range from low cost broadband, through cellular\\/GSM, Fibre Optic and V-SAT technologies to provide customized, turnkey connectivity solutions that align to the needs and budgets of our clients.&nbsp;<\\/p><ul><li>V-SAT connectivity into African countries, or remote locations where fixed line or other services are not available.<\\/li><li>Local, shaped and unshaped broadband services.<\\/li><li>Business fibre connectivity ranging from best effort to full service level agreement services.<\\/li><li>Hosted service delivered over fibre cable.<\\/li><li>Local Area and Wide Area network solutions, design, planning, implementation, equipment, infrastructure and services.<\\/li><li>Virtual private network solutions.<\\/li><li>Bandwidth optimization, prioritization and filtering\\/management solutions.<\\/li><li>ISP &amp; Broadband.<\\/li><li>MPLS.<\\/li><li>Dynamic Bandwidth.<\\/li><li>Broadband.<\\/li><li>Metro Ethernet.<\\/li><li>IP Connect (IPC).<\\/li><li>Voice, Telephony and Unified Communications.<\\/li><li>VOIP Voice over IP.<\\/li><\\/ul><div class=\\\\\\\"call-to-action\\\\\\\" style=\\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\\">TALK TO A CONSULTANT NOW!&nbsp;<\\/div><p>&nbsp;<\\/p><p>&nbsp;<\\/p>\"}}}}'),
	(367,0,1470133438,0,0,361,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\"><strong>Database as a Service (DBaaS) (SQL)<\\/strong><\\/span><\\/h3><p><strong>What is DBaaS?<\\/strong><\\/p><p>DBaaS is a cloud-based rapid provisioning of databases in the context of a database cloud rather than in the context of individual servers.<\\/p><p>DBaaS delivers database functionality similar to what is found in relational database management systems (RDBMS&rsquo;s) such as SQL Server, MySQL and Oracle. Being cloud-based, on the other hand, DBaaS provides a flexible, scalable, on-demand platform that\\\\&#39;s oriented toward self-service and easy management, particularly in terms of provisioning a business\\\\&#39; own environment.<\\/p><p>A managed service, offered on a pay-per-usage basis, that provides on-demand access to a database for the storage of application data.<\\/p><p><strong>Why DBaaS?<\\/strong><\\/p><p>In simple terms, it&rsquo;s a method of storing and managing your data in the cloud that is simple to use and easy to update. You won&rsquo;t have to devote hours of your time to install, maintain and monitor in-house databases. That&rsquo;s the cloud provider&rsquo;s job. Your cloud database can be deployed in just a few clicks and in the end, it will be a lot more secure than your current system. And your hardware? You can stop worrying about that too. You pay only for what you use so you don&rsquo;t have to make big investments in new technology. Speaking of technology, there are a lot of advantages to having a strong infrastructure in the cloud. Further to the aforementioned, you have access to all the enterprise features without incurring the costs associated with licensing your own enterprise instance!<\\/p><p><strong>The advantages of DBaaS?<\\/strong><\\/p><p>That&rsquo;s fine, you might say, but will it work for my business? We think so and here are a few reasons why:<\\/p><p><strong>Security<\\/strong><\\/p><p>If you&rsquo;re running your databases on in-house servers, you have to think seriously about security. You&rsquo;ll have to ensure that your database is up to date to keep it safe from digital threats. You also need to make sure your on-premises location is secure from any possible disaster that could interrupt services. This requires lot of work and resources that your business might not have. If you have a cloud solution, your provider handles security, which means you don&rsquo;t have to worry about it.<\\/p><p><strong>Rapid provisioning<\\/strong><\\/p><p>Need a new database in a hurry? No problem. You can scale your usage up or down whenever you need it. This can range from 10GB to 1000GB, depending on the provider.<\\/p><p><strong>Reduced cost<\\/strong><\\/p><p>Instead of investing large amounts in technology, you pay as you go. You can even start with hourly pay-as-you-go options, depending on your cloud provider. You&rsquo;re also free of the problems associated with providing new software for each user.<\\/p><p><strong>Performance<\\/strong><\\/p><p>One of the common complaints we&rsquo;ve heard is that databases just aren&rsquo;t fast enough. Queries that should take a few minutes sometimes end up taking hours to process. And in some cases, the data isn&rsquo;t even valid! Cloud-based databases can provide much better performance than traditional on-premises systems. This is because they can gather as many instances as required to complete the database processing as quickly as possible. There&rsquo;s also the matter of uptime to consider! Most cloud providers have high availability configurations of 99.99% or higher, which is hard to achieve with an in-house solution.<\\/p><p><strong>Continuous monitoring and maintenance:<\\/strong><\\/p><p>Stop worrying about finding the right IT expert to deal with your database problems. The cloud provider has technical experts working around the clock to monitor and optimize your databases.<\\/p><p><strong>Database consumer:<\\/strong><\\/p><ul><li>On-demand;<\\/li><li>Quick access to a database allows for greater agility and innovation.<\\/li><li>Simple<\\/li><li>No need to manage a full server ;<\\/li><li>Abstracted from management of HA, performance.<\\/li><li>Cost-efficient<\\/li><li>Pay for what you use, when you use it.<\\/li><\\/ul><p><strong>&nbsp;DBaaS Products:<\\/strong><\\/p><ul><li>SQL 2016<\\/li><li>MySQL<\\/li><\\/ul>\"}}}}'),
	(366,0,1470133374,0,0,359,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">Software as a Service (SaaS)<\\/span><\\/h3><ul><li>Hosted Exchange<\\/li><li>O365<\\/li><li>Skype for Business<\\/li><\\/ul>\"}}}}'),
	(362,0,1470133894,0,0,354,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<ul><li>Server Hosting.<\\/li><li>Server Platform.<\\/li><li>Management and Virtualisation.<\\/li><li>Identity and Access.<\\/li><li>Hosted Infrastructure Services.<\\/li><li>Data Centre.<\\/li><li>Public, Private or Hybrid Cloud Services.<\\/li><ul><li>Managed Hosting.<\\/li><li>Application Managed Hosting.<\\/li><ul><li>Remote Desktop Services.<\\/li><li>Windows 7\\/8\\/10.<\\/li><li>Office 2013\\/2016.<\\/li><li>Sage ERP.<\\/li><\\/ul><li>Disaster Recovery Planning and Facilitation.<\\/li><\\/ul><\\/ul><div class=\\\\\\\"call-to-action\\\\\\\" style=\\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\\">CALL US TODAY!<\\/div>\"}}}}'),
	(368,0,1470133516,0,0,360,'','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<h3><span class=\\\\\\\"text-yellow\\\\\\\" style=\\\\\\\"color: #fbbf0f;\\\\\\\">Platform as a Service (PaaS)<\\/span><\\/h3><p>- Sharepoint<\\/p>\"}}}}');

/*!40000 ALTER TABLE `mod_page` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mod_page_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_category`;

CREATE TABLE `mod_page_category` (
  `page_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_category_timestamp` int(11) NOT NULL,
  `page_category_date` int(11) NOT NULL,
  `page_category_price` double DEFAULT NULL,
  `page_category_category_id` int(11) DEFAULT NULL,
  `page_category_heading` varchar(400) NOT NULL,
  `page_category_short_description` text NOT NULL,
  `page_category_description` text NOT NULL,
  `page_category_link` varchar(400) NOT NULL,
  `page_category_link_video` int(1) NOT NULL,
  `page_category_archived` int(11) NOT NULL DEFAULT '0',
  `page_category_quantity` int(11) DEFAULT NULL,
  `page_category_code` varchar(100) DEFAULT NULL,
  `page_category_offset` int(11) DEFAULT NULL,
  `page_category_featured` int(11) DEFAULT '0',
  `page_category_hidden_on_site` int(11) DEFAULT '0',
  `page_category_sort` int(99) DEFAULT NULL,
  `page_category_colour` varchar(200) DEFAULT NULL,
  `page_category_anchor` varchar(400) DEFAULT NULL,
  `page_category_include` text,
  `page_category_align` varchar(100) DEFAULT NULL,
  `page_category_slug` varchar(400) DEFAULT NULL,
  `page_category_author` varchar(200) DEFAULT NULL,
  `page_category_special` int(11) DEFAULT '0',
  `page_category_in_the_news` int(11) DEFAULT '0',
  `page_category_latest_page` int(11) DEFAULT '0',
  `page_category_latest_article` int(11) DEFAULT '0',
  `page_category_latest_article_1` int(11) DEFAULT '0',
  `page_category_latest_article_2` int(11) DEFAULT '0',
  `page_category_latest_article_3` int(11) DEFAULT '0',
  `page_category_json` text,
  PRIMARY KEY (`page_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `mod_page_category` WRITE;
/*!40000 ALTER TABLE `mod_page_category` DISABLE KEYS */;

INSERT INTO `mod_page_category` (`page_category_id`, `media_files_id`, `page_category_timestamp`, `page_category_date`, `page_category_price`, `page_category_category_id`, `page_category_heading`, `page_category_short_description`, `page_category_description`, `page_category_link`, `page_category_link_video`, `page_category_archived`, `page_category_quantity`, `page_category_code`, `page_category_offset`, `page_category_featured`, `page_category_hidden_on_site`, `page_category_sort`, `page_category_colour`, `page_category_anchor`, `page_category_include`, `page_category_align`, `page_category_slug`, `page_category_author`, `page_category_special`, `page_category_in_the_news`, `page_category_latest_page`, `page_category_latest_article`, `page_category_latest_article_1`, `page_category_latest_article_2`, `page_category_latest_article_3`, `page_category_json`)
VALUES
	(317,0,1467901597,0,NULL,316,'Operations & History','','','',0,0,NULL,NULL,NULL,0,0,25,NULL,NULL,NULL,NULL,'operations--history',NULL,0,0,0,0,0,0,0,NULL),
	(316,0,1467190005,0,NULL,0,'This is us','','','',0,0,NULL,NULL,NULL,0,0,24,NULL,NULL,NULL,NULL,'this-is-us',NULL,0,0,0,0,0,0,0,NULL),
	(318,0,1467901709,0,NULL,316,'Management','','','',0,0,NULL,NULL,NULL,0,0,26,NULL,NULL,NULL,NULL,'management',NULL,0,0,0,0,0,0,0,NULL),
	(319,0,1467190641,0,NULL,316,'Certifications & Awards','','','',0,1,NULL,NULL,NULL,0,0,31,NULL,NULL,NULL,NULL,'certifications--awards',NULL,0,0,0,0,0,0,0,NULL),
	(320,0,1467190650,0,NULL,316,'Partners','','','',0,0,NULL,NULL,NULL,0,0,32,NULL,NULL,NULL,NULL,'partners',NULL,0,0,0,0,0,0,0,NULL),
	(321,0,1467190663,0,NULL,318,'Simon Martyn','','','',0,1,NULL,NULL,NULL,0,0,27,NULL,NULL,NULL,NULL,'simon-martyn',NULL,0,0,0,0,0,0,0,NULL),
	(322,0,1467190698,0,NULL,318,'Zac Hooper','','','',0,1,NULL,NULL,NULL,0,0,28,NULL,NULL,NULL,NULL,'zac-hooper',NULL,0,0,0,0,0,0,0,NULL),
	(323,0,1467190708,0,NULL,318,'Alex Pohl','','','',0,1,NULL,NULL,NULL,0,0,29,NULL,NULL,NULL,NULL,'alex-pohl',NULL,0,0,0,0,0,0,0,NULL),
	(324,0,1467190718,0,NULL,318,'Thys Smith','','','',0,1,NULL,NULL,NULL,0,0,30,NULL,NULL,NULL,NULL,'thys-smith',NULL,0,0,0,0,0,0,0,NULL),
	(325,0,1467190755,0,NULL,0,'Home','','','',0,0,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,'home',NULL,0,0,0,0,0,0,0,NULL),
	(326,0,1467190766,0,NULL,0,'Services','','','',0,0,NULL,NULL,NULL,0,0,3,NULL,NULL,NULL,NULL,'services',NULL,0,0,0,0,0,0,0,NULL),
	(327,0,1467700140,0,NULL,326,'Cloud','','','',0,1,NULL,NULL,NULL,0,0,5,NULL,NULL,NULL,NULL,'cloud',NULL,0,0,0,0,0,0,0,NULL),
	(328,0,1467788750,0,NULL,327,'Quick description','<h1>Heading</h1>\r\n<p>content</p>','','',0,1,NULL,NULL,NULL,0,0,6,NULL,NULL,NULL,NULL,'quick-description',NULL,0,0,0,0,0,0,0,NULL),
	(329,0,1467191248,0,NULL,328,'2nd level info - How it changes how you currently do things','','','',0,1,NULL,NULL,NULL,0,0,7,NULL,NULL,NULL,NULL,'2nd-level-info--how-it-changes-how-you-currently-do-things',NULL,0,0,0,0,0,0,0,NULL),
	(330,0,1467191262,0,NULL,328,'3rd level technical info','','','',0,1,NULL,NULL,NULL,0,0,8,NULL,NULL,NULL,NULL,'3rd-level-technical-info',NULL,0,0,0,0,0,0,0,NULL),
	(331,0,1467191284,0,NULL,327,'Get this now','','','',0,1,NULL,NULL,NULL,0,0,9,NULL,NULL,NULL,NULL,'get-this-now',NULL,0,0,0,0,0,0,0,NULL),
	(332,0,1467293653,0,NULL,326,'Microsoft','','','',0,1,NULL,NULL,NULL,0,0,10,NULL,NULL,NULL,NULL,'microsoft',NULL,0,0,0,0,0,0,0,NULL),
	(333,38,1467926708,0,NULL,326,'Cloud','<h4>Hosted private cloud, hosted multi-tenant cloud and public could services to support and deliver numerous private and commercial platforms.</h4>','','',0,0,NULL,NULL,NULL,0,0,11,NULL,NULL,NULL,NULL,'cloud',NULL,0,0,0,0,0,0,0,NULL),
	(334,46,1467926719,0,NULL,326,'Voice & Telephony','<h4>Voice Over IP (VOIP) and least cost routing options that have been designed to work simultaneously &ldquo;on premise&rdquo; as well as &ldquo;in the cloud&rdquo; as online services.</h4>','','',0,1,NULL,NULL,NULL,0,0,17,NULL,NULL,NULL,NULL,'voice--telephony',NULL,0,0,0,0,0,0,0,NULL),
	(335,45,1467926732,0,NULL,326,'Service Desk','<h4>A dedicated service desk functions as ECOTECH central services center. It is also our single point of contact center (SPoC). From here we provide the full managed-services package, remote management and monitoring services to all our clients. We offer ad hoc, as well as permanent on-site based technicians and SLA support.</h4>','','',0,0,NULL,NULL,NULL,0,0,18,NULL,NULL,NULL,NULL,'service-desk',NULL,0,0,0,0,0,0,0,NULL),
	(336,0,1467803762,0,NULL,326,'Service Desk','<h4>A dedicated service desk functions as ECOTECH central services center. It is also our single point of contact center (SPoC). From here we provide the full managed-services package, remote management and monitoring services to all our clients. We offer ad hoc, as well as permanent on-site based technicians and SLA support.</h4>','','',0,1,NULL,NULL,NULL,0,0,19,NULL,NULL,NULL,NULL,'service-desk',NULL,0,0,0,0,0,0,0,NULL),
	(337,40,1467926745,0,NULL,326,'Consulting','<h4>Our goal is to exceed the expectations of every client by offering outstanding customer service, increased flexibility, and greater value, thus optimizing solution functionality, and improving operational efficiency.</h4>','','',0,0,NULL,NULL,NULL,0,0,20,NULL,NULL,NULL,NULL,'consulting',NULL,0,0,0,0,0,0,0,NULL),
	(338,42,1467926756,0,NULL,326,'Microsoft','<h4>As a Microsoft certified partner we carry a number of Microsoft Gold competencies. Our Microsoft Windows Azure Pack Cloud hosting platform provides resilient and reliable utility-based computing services that have been developed with our clients&rsquo; explicit needs for reliability and security in mind.</h4>','','',0,0,NULL,NULL,NULL,0,0,12,NULL,NULL,NULL,NULL,'microsoft',NULL,0,0,0,0,0,0,0,NULL),
	(339,41,1467926767,0,NULL,326,'Infrastructure','<h4>ECOCORP Cloud Services addresses these requirements by providing &lsquo;anywhere access&rsquo; to your employees&rsquo; basic applications. ECOTECH &lsquo;Infrastructure as a Service&rsquo; allows companies to free up resources and allow IT to focus on more strategic objectives&hellip;</h4>','','',0,1,NULL,NULL,NULL,0,0,21,NULL,NULL,NULL,NULL,'infrastructure',NULL,0,0,0,0,0,0,0,NULL),
	(340,39,1467974824,0,NULL,326,'Connectivity','<h4>ECOTECH Converge supply various turnkey connectivity solutions including, but not limited to, managed connectivity, broadband, V-Sat and internet service provider services...</h4>','','',0,0,NULL,NULL,NULL,0,0,22,NULL,NULL,NULL,NULL,'connectivity',NULL,0,0,0,0,0,0,0,NULL),
	(341,37,1467926793,0,NULL,326,'Channel','<h4>ECOTECH Converge offers a wide range of information technology hardware and peripheral devices catering for most IT requirements...</h4>','','',0,0,NULL,NULL,NULL,0,0,23,NULL,NULL,NULL,NULL,'channel',NULL,0,0,0,0,0,0,0,NULL),
	(342,0,1467873710,0,NULL,325,'Why us','','','',0,1,NULL,NULL,NULL,0,0,2,NULL,NULL,NULL,NULL,'why-us',NULL,0,0,0,0,0,0,0,NULL),
	(343,0,1467873904,0,NULL,0,'Why us','','','',0,0,NULL,NULL,NULL,0,0,33,NULL,NULL,NULL,NULL,'why-us',NULL,0,0,0,0,0,0,0,NULL),
	(344,0,1467873982,0,NULL,343,'Scalibility','','','',0,0,NULL,NULL,NULL,0,0,36,NULL,NULL,NULL,NULL,'scalibility',NULL,0,0,0,0,0,0,0,NULL),
	(345,0,1467874011,0,NULL,343,'Migration solutions','','','',0,0,NULL,NULL,NULL,0,0,34,NULL,NULL,NULL,NULL,'migration-solutions',NULL,0,0,0,0,0,0,0,NULL),
	(346,0,1467874057,0,NULL,343,'Service Level Agreement','','','',0,0,NULL,NULL,NULL,0,0,35,NULL,NULL,NULL,NULL,'service-level-agreement',NULL,0,0,0,0,0,0,0,NULL),
	(347,0,1467900027,0,NULL,343,'Client & Industry list','','','',0,0,NULL,NULL,NULL,0,0,37,NULL,NULL,NULL,NULL,'client--industry-list',NULL,0,0,0,0,0,0,0,NULL),
	(348,0,1467902451,0,NULL,0,'Contact Us','','','',0,0,NULL,NULL,NULL,0,0,38,NULL,NULL,NULL,NULL,'contact-us',NULL,0,0,0,0,0,0,0,NULL),
	(349,0,1467958810,0,NULL,338,'MS SQL','','','',0,0,NULL,NULL,NULL,0,0,13,NULL,NULL,NULL,NULL,'ms-sql',NULL,0,0,0,0,0,0,0,NULL),
	(350,0,1467959635,0,NULL,338,'Azure','<p>Any developer or IT professional can be productive with Azure. The integrated tools, pre-built templates and managed services make it easier to build and manage enterprise, mobile, Web and Internet of Things (IoT) apps faster, using skills you already have and technologies you already know. Microsoft is also the only vendor positioned as a Leader across Gartner&rsquo;s Magic Quadrants for Cloud Infrastructure as a Service, Application Platform as a Service, and Cloud Storage Services for the second consecutive year.</p>\r\n<p><strong>Use an open and flexible cloud service platform</strong></p>\r\n<p>Azure supports the broadest selection of operating systems, programming languages, frameworks, tools, databases and devices. Run Linux containers with Docker integration; build apps with JavaScript, Python, .NET, PHP, Java and Node.js; build back-ends for iOS, Android and Windows devices. Azure cloud service supports the same technologies that millions of developers and IT professionals already rely on and trust.</p>\r\n<p><strong>Extend your existing IT</strong></p>\r\n<p>Some cloud providers make you choose between your data centre and the cloud. Not Azure, which easily integrates with your existing IT environment through the largest network of secure private connections, hybrid database and storage solutions, and data residency and encryption features &ndash; so your assets stay right where you need them. And with&nbsp;Azure Stack, you can bring the Azure model of application development and deployment to your data centre. Azure hybrid cloud solutions give you the best of both worlds: more IT options, less complexity and cost. That&rsquo;s why it&rsquo;s one of the best&nbsp;cloud computing services&nbsp;available.</p>','','',0,1,NULL,NULL,NULL,0,0,15,NULL,NULL,NULL,NULL,'azure',NULL,0,0,0,0,0,0,0,NULL),
	(351,0,1467959646,0,NULL,338,'Azure','','','',0,1,NULL,NULL,NULL,0,0,16,NULL,NULL,NULL,NULL,'azure',NULL,0,0,0,0,0,0,0,NULL),
	(352,0,1467974920,0,NULL,338,'Azure','','','',0,0,NULL,NULL,NULL,0,0,14,NULL,NULL,NULL,NULL,'azure',NULL,0,0,0,0,0,0,0,NULL),
	(353,0,1470042376,0,NULL,0,'Hosting','<p>Hosting services... Find out more....</p>','','',0,1,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,'hosting',NULL,0,0,0,0,0,0,0,NULL),
	(354,0,1470130726,0,NULL,326,'Hosting','<h4>Hosting services...&nbsp;</h4>','','',0,0,NULL,NULL,NULL,0,0,4,NULL,NULL,NULL,NULL,'hosting',NULL,0,0,0,0,0,0,0,NULL),
	(356,0,1470130849,0,NULL,355,'Connectivity Solutions','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'connectivity-solutions',NULL,0,0,0,0,0,0,0,NULL),
	(355,0,1470130709,0,NULL,326,'Managed Services','<h4>ECOTECH Converge supplies various turnkey management solutions including, but not limited to Desktop Services, Connectivity Solutions &amp; Consulting.&nbsp;</h4>','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'managed-services',NULL,0,0,0,0,0,0,0,NULL),
	(357,0,1470131739,0,NULL,333,'Public, Private or Hybrid','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'public-private-or-hybrid',NULL,0,0,0,0,0,0,0,NULL),
	(358,0,1470132475,0,NULL,333,'IaaS','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'iaas',NULL,0,0,0,0,0,0,0,NULL),
	(359,0,1470132554,0,NULL,333,'SaaS','<ul>\r\n<li>Hosted Exchange.</li>\r\n<li>O365.</li>\r\n<li>Skype for Business.</li>\r\n</ul>','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'saas',NULL,0,0,0,0,0,0,0,NULL),
	(360,0,1470132673,0,NULL,333,'PaaS','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'paas',NULL,0,0,0,0,0,0,0,NULL),
	(361,0,1470133033,0,NULL,333,'DBaaS (SQL)','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'dbaas',NULL,0,0,0,0,0,0,0,NULL);

/*!40000 ALTER TABLE `mod_page_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mod_page_document_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_document_link`;

CREATE TABLE `mod_page_document_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_page_image_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_image_link`;

CREATE TABLE `mod_page_image_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service`;

CREATE TABLE `mod_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_timestamp` int(11) NOT NULL,
  `service_date` int(11) NOT NULL,
  `service_price` double DEFAULT NULL,
  `service_category_id` int(11) DEFAULT NULL,
  `service_heading` varchar(400) NOT NULL,
  `service_short_description` text NOT NULL,
  `service_description` text NOT NULL,
  `service_link` varchar(400) NOT NULL,
  `service_link_video` int(1) NOT NULL,
  `service_archived` int(11) NOT NULL DEFAULT '0',
  `service_quantity` int(11) DEFAULT NULL,
  `service_code` varchar(100) DEFAULT NULL,
  `service_offset` int(11) DEFAULT NULL,
  `service_featured` int(11) DEFAULT '0',
  `service_hidden_on_site` int(11) DEFAULT '0',
  `service_sort` int(99) DEFAULT NULL,
  `service_colour` varchar(200) DEFAULT NULL,
  `service_anchor` varchar(400) DEFAULT NULL,
  `service_include` text,
  `service_align` varchar(100) DEFAULT NULL,
  `service_slug` varchar(400) DEFAULT NULL,
  `service_author` varchar(200) DEFAULT NULL,
  `service_special` int(11) DEFAULT '0',
  `service_in_the_news` int(11) DEFAULT '0',
  `service_latest_service` int(11) DEFAULT '0',
  `service_latest_article` int(11) DEFAULT '0',
  `service_latest_article_1` int(11) DEFAULT '0',
  `service_latest_article_2` int(11) DEFAULT '0',
  `service_latest_article_3` int(11) DEFAULT '0',
  `service_json` text,
  PRIMARY KEY (`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_category`;

CREATE TABLE `mod_service_category` (
  `service_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_category_timestamp` int(11) NOT NULL,
  `service_category_date` int(11) NOT NULL,
  `service_category_price` double DEFAULT NULL,
  `service_category_category_id` int(11) DEFAULT NULL,
  `service_category_heading` varchar(400) NOT NULL,
  `service_category_short_description` text NOT NULL,
  `service_category_description` text NOT NULL,
  `service_category_link` varchar(400) NOT NULL,
  `service_category_link_video` int(1) NOT NULL,
  `service_category_archived` int(11) NOT NULL DEFAULT '0',
  `service_category_quantity` int(11) DEFAULT NULL,
  `service_category_code` varchar(100) DEFAULT NULL,
  `service_category_offset` int(11) DEFAULT NULL,
  `service_category_featured` int(11) DEFAULT '0',
  `service_category_hidden_on_site` int(11) DEFAULT '0',
  `service_category_sort` int(99) DEFAULT NULL,
  `service_category_colour` varchar(200) DEFAULT NULL,
  `service_category_anchor` varchar(400) DEFAULT NULL,
  `service_category_include` text,
  `service_category_align` varchar(100) DEFAULT NULL,
  `service_category_slug` varchar(400) DEFAULT NULL,
  `service_category_author` varchar(200) DEFAULT NULL,
  `service_category_special` int(11) DEFAULT '0',
  `service_category_in_the_news` int(11) DEFAULT '0',
  `service_category_latest_page` int(11) DEFAULT '0',
  `service_category_latest_article` int(11) DEFAULT '0',
  `service_category_latest_article_1` int(11) DEFAULT '0',
  `service_category_latest_article_2` int(11) DEFAULT '0',
  `service_category_latest_article_3` int(11) DEFAULT '0',
  `service_category_json` text,
  PRIMARY KEY (`service_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_document_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_document_link`;

CREATE TABLE `mod_service_document_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_image_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_image_link`;

CREATE TABLE `mod_service_image_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
