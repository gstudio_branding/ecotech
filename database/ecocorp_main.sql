-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2017 at 09:22 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecocorp_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_level_admin`
--

CREATE TABLE IF NOT EXISTS `access_level_admin` (
  `admin_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `admin_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`admin_acc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `access_level_admin`
--

INSERT INTO `access_level_admin` (`admin_acc_id`, `admin_acc_level`, `admin_acc_admin_id`) VALUES
(9, '1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `access_level_media`
--

CREATE TABLE IF NOT EXISTS `access_level_media` (
  `media_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `media_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`media_acc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `access_level_media`
--

INSERT INTO `access_level_media` (`media_acc_id`, `media_acc_level`, `media_acc_admin_id`) VALUES
(7, '1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `access_level_page`
--

CREATE TABLE IF NOT EXISTS `access_level_page` (
  `page_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `page_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`page_acc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `access_level_page`
--

INSERT INTO `access_level_page` (`page_acc_id`, `page_acc_level`, `page_acc_admin_id`) VALUES
(20, '1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `access_level_service`
--

CREATE TABLE IF NOT EXISTS `access_level_service` (
  `service_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `service_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`service_acc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `access_level_service`
--

INSERT INTO `access_level_service` (`service_acc_id`, `service_acc_level`, `service_acc_admin_id`) VALUES
(20, '1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `cms_admin`
--

CREATE TABLE IF NOT EXISTS `cms_admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(200) DEFAULT NULL COMMENT 'name of the user',
  `admin_surname` varchar(200) DEFAULT NULL COMMENT 'surname of the user',
  `admin_dob` int(11) DEFAULT NULL,
  `admin_gender` int(11) DEFAULT NULL COMMENT '1:male; 0:female',
  `admin_email` varchar(200) DEFAULT NULL COMMENT 'email address of the user',
  `admin_cell_private` varchar(12) DEFAULT NULL,
  `admin_cell_work` varchar(12) DEFAULT NULL,
  `admin_tel_private` varchar(12) DEFAULT NULL,
  `admin_tel_work` varchar(12) DEFAULT NULL,
  `admin_fax` varchar(12) DEFAULT NULL,
  `admin_address` varchar(200) DEFAULT NULL,
  `admin_password` varchar(200) DEFAULT NULL COMMENT 'password for the user',
  `admin_added_by_id` int(11) DEFAULT NULL COMMENT 'the id of the user that added the user',
  `admin_archived` int(11) DEFAULT '0',
  `admin_picture` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `cms_admin`
--

INSERT INTO `cms_admin` (`admin_id`, `admin_name`, `admin_surname`, `admin_dob`, `admin_gender`, `admin_email`, `admin_cell_private`, `admin_cell_work`, `admin_tel_private`, `admin_tel_work`, `admin_fax`, `admin_address`, `admin_password`, `admin_added_by_id`, `admin_archived`, `admin_picture`) VALUES
(11, 'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=', 'Qg42ym9OyRJVDrJyru5wx+SUgyiAUNtXRRnvYTJue6o=', NULL, NULL, 'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=', NULL, NULL, NULL, NULL, NULL, NULL, 'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_media_dimensions`
--

CREATE TABLE IF NOT EXISTS `cms_media_dimensions` (
  `media_dimensions_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `media_dimensions_x` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_x2` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y2` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_aspect_ratio` varchar(11) DEFAULT NULL COMMENT 'aspect ratio eg 1x1',
  `media_dimensions_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_dimensions_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_media_files`
--

CREATE TABLE IF NOT EXISTS `cms_media_files` (
  `media_files_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `media_files_filename` varchar(400) DEFAULT NULL,
  `media_files_type` int(11) DEFAULT NULL,
  `media_files_title` varchar(200) DEFAULT NULL,
  `media_files_description` text,
  `media_files_timestamp` int(11) DEFAULT NULL,
  `media_files_original_name` text NOT NULL,
  PRIMARY KEY (`media_files_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `cms_media_files`
--

INSERT INTO `cms_media_files` (`media_files_id`, `admin_id`, `media_files_filename`, `media_files_type`, `media_files_title`, `media_files_description`, `media_files_timestamp`, `media_files_original_name`) VALUES
(17, 0, 'cc696d2d52c8c9b9fe9db3344164aad0.png', 1, NULL, NULL, 1467900487, 'AngloGold'),
(18, 0, '198e48fd6b67e39216fde87014c2d982.png', 1, NULL, NULL, 1467900488, 'aquarius'),
(19, 0, 'dbed5459da16a47a3670485b231c0d68.png', 1, NULL, NULL, 1467900489, 'cb2659a5dd0232fcb4bb86c0a41deea981e143b1f093e2763e1ba5482d92b586'),
(13, 0, 'fe91beda4251bfb51e13a2dc5cf8a87a.png', 1, NULL, NULL, 1467900480, '6594915_orig'),
(14, 0, '1501d3e2effc2ac1f683630a89fff716.png', 1, NULL, NULL, 1467900481, 'AAEAAQAAAAAAAAI_AAAAJDI5MTZmN2EyLWQwYzktNDRhOC05ODlhLWFlZDRjYzE3YjViNA'),
(15, 0, 'f341c71f1828b2abb3663a7bc288065f.png', 1, NULL, NULL, 1467900482, 'AAEAAQAAAAAAAAZUAAAAJGQxYWQ0MzY5LTM0MzQtNDdlNi1hN2JjLTlmY2NjZjc0NzQxNw'),
(16, 0, '0037b714d4b12da910406a5993597c9c.png', 1, NULL, NULL, 1467900484, 'ACSA'),
(12, 11, '8d8e707f81bc70d09234c358fa0e259b.png', 1, NULL, NULL, 1467877824, 'Gold_Partner_Insignia'),
(20, 0, '3944af3dc6ee2d12095bdae77766e42a.png', 1, NULL, NULL, 1467900491, 'Eskom logo lrg_1'),
(21, 0, 'bf1cb0151a42ead4cc906ac5d956f4e2.png', 1, NULL, NULL, 1467900492, 'Eurasian_Natural_Resources_Corporation_(ENRC)'),
(22, 0, 'bf6bb054c0027b45fbabc3ccf9d56df8.png', 1, NULL, NULL, 1467900494, 'login-logo'),
(23, 0, '19aed3016868c0bf00088cd9af8295d4.png', 1, NULL, NULL, 1467900496, 'Microsoft-PESTEL-Analysis'),
(24, 0, '37356101c61dd28ab6eb626e21750629.jpg', 1, NULL, NULL, 1467900498, 'MTN-logo'),
(25, 0, 'adc8f787dd409fc9ccc902dc93fe9ad1.png', 1, NULL, NULL, 1467900499, 'mukam5'),
(26, 0, 'dc3a75ee088e0508b1683f6c6ffa1bff.png', 1, NULL, NULL, 1467900501, 'natsure-logo_news_18517_10278'),
(27, 0, 'edc105f590e356557f2be15cd0057481.png', 1, NULL, NULL, 1467900502, 'Neotel-logo-page'),
(28, 0, 'f692892a3961f92216d8f0b3ce80b183.png', 1, NULL, NULL, 1467900504, 'petra-diamonds-limited-logo'),
(29, 0, '6ce911e8cbee8485635e3c3cdf3f48e8.png', 1, NULL, NULL, 1467900507, 'psg-konsult-logo-e1399475814749'),
(30, 0, '0fba8aabd3527a0e17beed9422200e0c.png', 1, NULL, NULL, 1467900508, 'Rezidor_Hotel_Group'),
(31, 0, '67d5d5fefa5c486d6d2a614e651372c7.png', 1, NULL, NULL, 1467900511, 'Stead Law Logo Final Logo'),
(32, 0, '3e1b576ef89af7e16448c85809daa52d.png', 1, NULL, NULL, 1467900513, 'taste-holdings-logo-e1415610893848'),
(33, 0, '2d1b983fba8dc1cd890a83f8c6de8fce.png', 1, NULL, NULL, 1467900514, 'TelkomSA'),
(34, 0, 'a7bf7799e3f2418954a9fbfc47377eaf.png', 1, NULL, NULL, 1467900515, 'thumb_300x300_670d9df5-fb04-49af-9a94-7019f3414f0a'),
(35, 0, '5c1da837c4dd8f730ae62203e3616710.png', 1, NULL, NULL, 1467900517, 'unisa-logo'),
(36, 0, 'f709a8dd84e83a96c595b878577250d9.png', 1, NULL, NULL, 1467900518, 'University_of_Johannesburg_Logo'),
(37, 11, '17ea5d3310dbed381a34c41668046a71.png', 1, NULL, NULL, 1467926344, 'icon-channel'),
(38, 11, '4b0f743170d7646eb463ed501d4eca5c.png', 1, NULL, NULL, 1467926345, 'icon-cloud'),
(39, 11, '1c844af65473c12e72adc8399cd2ce76.png', 1, NULL, NULL, 1467926346, 'icon-connectivity'),
(40, 11, 'f58c1c5a0fad3a13865ce2649ab4ab1c.png', 1, NULL, NULL, 1467926347, 'icon-consulting'),
(41, 11, 'c6686ea38d4aaed7f8e225784bb27542.png', 1, NULL, NULL, 1467926348, 'icon-infrastructure'),
(42, 11, 'abb0ec4315b9659e1fa54254609428e2.png', 1, NULL, NULL, 1467926349, 'icon-microsoft'),
(43, 11, '375a903d653e1f771e1307af8bbeabec.png', 1, NULL, NULL, 1467926350, 'icon-portals'),
(44, 11, '81845e74e91caf3da1266502ce18c480.png', 1, NULL, NULL, 1467926351, 'icon-security'),
(45, 11, '5464de8a6d635b17b0318abe4f323970.png', 1, NULL, NULL, 1467926352, 'icon-servicedesk'),
(46, 11, '066614153e2817835eef7f7bde5f992b.png', 1, NULL, NULL, 1467926353, 'icon-voice'),
(47, 11, 'bdce487e069b2a4cc0fa13e321643048.png', 1, NULL, NULL, 1467957425, 'microsoftpartner'),
(48, 11, '3dd23c37a22933cd11a270a752ab7ac9.jpg', 1, NULL, NULL, 1467957427, 'Neotel Channel partner logo'),
(49, 11, 'f9034c699e11df0625b3245339ca8dce.jpg', 1, NULL, NULL, 1467957428, 'cisco_registered_Partner'),
(50, 11, '95a0bf53094d143589e0397e93db4025.png', 1, NULL, NULL, 1467957429, 'dell_partnerdirect_preferred_rgb3'),
(51, 11, '117304f2e556f7375838e469fa09a789.jpg', 1, NULL, NULL, 1467957432, 'hp_logo_preferred_partner_w'),
(52, 11, 'e003e220580ebb96b3d6bad43f292b81.png', 1, NULL, NULL, 1467957435, 'microsoft-small-business-specialist'),
(53, 11, 'c8aa1175fac0c3578e45d9b3ef6c8db8.jpg', 1, NULL, NULL, 1467957436, 'SEACOM_OfficialPartnerlogo-300x138'),
(54, 11, '83462e7a1ded84951890301c0bbebda9.pdf', 2, NULL, NULL, 1468226984, 'Blank_SLA Schedules Ver 1'),
(55, 11, 'b2343d0197212bf222ee518a6bdca54c.png', 1, NULL, NULL, 1468232872, 'certifications_awards'),
(56, 11, '8aac0632b1275d9b93fb1b4c93703767.png', 1, NULL, NULL, 1468232874, 'channel'),
(57, 11, 'ebc07f520c8e06433cec8aec44d1f92c.png', 1, NULL, NULL, 1468232877, 'client_project_list'),
(58, 11, 'f233d55354c49e9c00b6f168c7a73906.png', 1, NULL, NULL, 1468232879, 'cloud'),
(59, 11, '8360d158b6d8204da8afd58f01a78d1a.png', 1, NULL, NULL, 1468232882, 'connectivity_solutions'),
(60, 11, '1c3c9d60c4ca57657096af498f72452d.png', 1, NULL, NULL, 1468232884, 'find_us'),
(61, 11, 'ee19c0e2c681325c87a9da6cdd4a3c4a.png', 1, NULL, NULL, 1468232886, 'history_operations'),
(62, 11, '4fdcf6d8941b13127d1d8880d2bb4e17.png', 1, NULL, NULL, 1468232892, 'infrastructure'),
(63, 11, '6a04ac9cb91480384696b3d84c45e0d4.png', 1, NULL, NULL, 1468232893, 'management_bios'),
(64, 11, 'b07798b67bb80dcd1cc074e5af921f7c.png', 1, NULL, NULL, 1468232895, 'microsoft'),
(65, 11, '46cd65f5d936f78ec91c5c8206d46021.png', 1, NULL, NULL, 1468232898, 'migration_solutions'),
(66, 11, '15561e659fb37b119d08dd7ffef437e0.png', 1, NULL, NULL, 1468232901, 'partners'),
(67, 11, '9e2da296e5585a2643df0d8bd47df645.png', 1, NULL, NULL, 1468232904, 'scalability'),
(68, 11, '2315d5982606e1970a8733d5c0abbbc7.png', 1, NULL, NULL, 1468232907, 'service_desk'),
(69, 11, '5b295a9a518df60abf73198c65a21a57.png', 1, NULL, NULL, 1468232913, 'voice_telephony'),
(70, 11, '1112100f59c08ade43c63df3d79aeaba.png', 1, NULL, NULL, 1468232918, 'consulting'),
(71, 11, 'eacacbd2d147c83e30ef762de0d35e77.png', 1, NULL, NULL, 1468234333, 'home'),
(72, 0, 'd5f4e1ff514a286d90270a7018d12fd3.png', 1, NULL, NULL, 1469801180, 'MicrosoftSQLServer'),
(73, 0, '22be097b01a2ac26c915649940ae6604.png', 1, NULL, NULL, 1469801181, 'ms-azure transparent'),
(75, 11, 'b8a72f31bab8f9bdc48b761aae538dc8.png', 1, NULL, NULL, 1470382657, 'home-page'),
(76, 11, '6f4876ed7e98e1e3daf812ad97fc6ad7.png', 1, NULL, NULL, 1470383029, 'hosting-icon'),
(77, 0, 'd7c2f2f8b789daefa35ed71113e0211d.png', 1, NULL, NULL, 1470384831, 'certifications_awards'),
(78, 0, 'e547b0ea290ac1ba85634e496d44e665.png', 1, NULL, NULL, 1470384832, 'channel'),
(79, 0, 'ef9fba958a17c421b537e6f7e46720f4.png', 1, NULL, NULL, 1470384833, 'client_list'),
(80, 0, 'c8d784328ab20828cbf9ee1c22ebf5ac.png', 1, NULL, NULL, 1470384834, 'cloud'),
(81, 0, 'a99e81101740ebe3613f1c1be652adaa.png', 1, NULL, NULL, 1470384835, 'connectivity_solutions'),
(82, 0, 'dd11d4c5f5bbf1351b3d13456251ccbe.png', 1, NULL, NULL, 1470384836, 'consulting'),
(83, 0, '52f9b7395de117f75a73a6e967a3d513.png', 1, NULL, NULL, 1470384837, 'contact'),
(84, 0, 'c3577ec9901c104e25bfebf3daefa67f.png', 1, NULL, NULL, 1470384838, 'history_operations'),
(85, 0, '6020ecb85365b9c88fef3f0b03ebb2ec.png', 1, NULL, NULL, 1470384839, 'hosting'),
(86, 0, '4801ef93ed3f6e7fc160416f514689ff.png', 1, NULL, NULL, 1470384840, 'infrastructure'),
(87, 0, '3a4d4c7aef44f823edb542883b9f33b8.png', 1, NULL, NULL, 1470384841, 'management'),
(88, 0, 'a2ff60c05d7832742824b92c1a7b1999.png', 1, NULL, NULL, 1470384842, 'microsoft'),
(89, 0, '2c62fed38b5539ef32fd37fc547dcccf.png', 1, NULL, NULL, 1470384843, 'migration'),
(90, 0, '00a1090f2677705c00f8e9b96882237b.png', 1, NULL, NULL, 1470384844, 'partners'),
(91, 0, '3389fce5a79802971ff5206bf53bbc59.png', 1, NULL, NULL, 1470384845, 'scalibility'),
(92, 0, 'df6345fe4caaf38979f0c93d6153e2b9.png', 1, NULL, NULL, 1470384846, 'service_desk'),
(93, 0, '8bdaab82f5b85077d3f5bef3cbab8d16.png', 1, NULL, NULL, 1470384847, 'sql_microsoft'),
(94, 0, 'b5938dffa454d1628120550ad0be56d1.png', 1, NULL, NULL, 1470384848, 'voice_telephony'),
(95, 11, '61c7bb2cf6f13215c6e23dca664b7cb0.png', 1, NULL, NULL, 1470385196, 'at the forefront of tech bubble'),
(96, 11, 'aebcd66f1d105bf6f1ab7be1917d2eb5.png', 1, NULL, NULL, 1470385197, 'cloud'),
(97, 11, 'c63253cd0e85632a00bad7e602307550.png', 1, NULL, NULL, 1470385198, 'consulting'),
(98, 11, '32a8ac61cc4a112617266cd2d2b9c9b2.png', 1, NULL, NULL, 1470385199, 'contact'),
(99, 11, '91c8c61785301823aa22d464e59ca2cc.png', 1, NULL, NULL, 1470385200, 'exceptional function'),
(100, 11, '14ae2f5b1f5de973794d6e704eb400e0.png', 1, NULL, NULL, 1470385201, 'forefront of tech'),
(101, 11, '7fefd19a4ff5698c12a65f2d6dc5bd9d.png', 1, NULL, NULL, 1470385202, 'hardandsoftbrands'),
(102, 11, 'f9415b1e53e7a5dcabcced6ff7cfc38b.png', 1, NULL, NULL, 1470385203, 'highly availablescalibility'),
(103, 11, 'd7a9465d8fca618879d4e8ff73cbdaf0.png', 1, NULL, NULL, 1470385204, 'infrastructure'),
(104, 11, '017a3dcca1f900731809fd9c9bfb4c39.png', 1, NULL, NULL, 1470385205, 'microsoft'),
(105, 11, '8198a3a01cc36fe6e5fea50b8a5ac35b.png', 1, NULL, NULL, 1470385206, 'migration'),
(106, 11, 'ca37356e6049c03490279a4cab585349.png', 1, NULL, NULL, 1470385207, 'operationalbrilliance'),
(107, 11, '426dbfd421d6adad9ee5107b8e44ae89.png', 1, NULL, NULL, 1470385208, 'partners'),
(108, 11, '2d5699a221a3a08f46455d939b9a55ca.png', 1, NULL, NULL, 1470385209, 'prescribed archi'),
(109, 11, 'f6195a56d555fba54b1b263fe2e404dd.png', 1, NULL, NULL, 1470385210, 'superior consulting'),
(110, 11, '96043286d50376969ac788351ba7da25.png', 1, NULL, NULL, 1470385211, 'topnotchhosting'),
(112, 0, '2b4b727277f4e2a028049e13eb53495b.png', 1, NULL, NULL, 1470386569, 'available-05'),
(113, 0, 'd3dad2b53b4968faa0aa22079b0b0fab.png', 1, NULL, NULL, 1470386570, 'consulting-01'),
(114, 0, 'ceadb6de604b99dbcba449dce94e4202.png', 1, NULL, NULL, 1470386571, 'customer-07'),
(115, 0, '6191937470c784474be1193f45c441f2.png', 1, NULL, NULL, 1470386572, 'enhancing-08'),
(116, 0, '7fa0c5c9636d1f2da11630eb638cb6c1.png', 1, NULL, NULL, 1470386573, 'forefront tech-12'),
(117, 0, '22b13c2bb59281308498e0a2fd32ba5e.png', 1, NULL, NULL, 1470386574, 'functional-14'),
(118, 0, 'ced85dbe113996b89adf9f181f8f53f4.png', 1, NULL, NULL, 1470386575, 'hardware-11'),
(119, 0, 'ea44f09e1ad0889d90afd4773b382591.png', 1, NULL, NULL, 1470386576, 'infrastructure-04'),
(120, 0, '223947f26cbab85c4686c1985b8cab72.png', 1, NULL, NULL, 1470386577, 'infrastructure-16'),
(121, 0, '58293e34115c217375c8744e951b2966.png', 1, NULL, NULL, 1470386579, 'microsoft-10'),
(122, 0, 'a3add601c231955bcc78cb98f3a6ffad.png', 1, NULL, NULL, 1470386580, 'operational-02'),
(123, 0, '016ae5093bdf38909e94b9cea09231a8.png', 1, NULL, NULL, 1470386581, 'public-09'),
(124, 0, '48ae52518e99592c298a0808736ee260.png', 1, NULL, NULL, 1470386582, 'service-15'),
(125, 0, '7c671847f1bbaf795c06ed192e2bbd76.png', 1, NULL, NULL, 1470386583, 'shape-18'),
(126, 0, '7f64d48d1be79030e26afa811a5e6db5.png', 1, NULL, NULL, 1470386584, 'solution-06'),
(127, 0, '29c1c7522e15e52f00a01595f7050852.png', 1, NULL, NULL, 1470386585, 'superior-13'),
(128, 0, '32b2cd72a5ea28ae6c9df54c2c357721.png', 1, NULL, NULL, 1470386586, 'technology-03'),
(129, 0, 'ac4286ebe59e92970f83829381903d7a.png', 1, NULL, NULL, 1470386587, 'top-notch-17'),
(130, 11, 'c1f7b7786ee322c6e7c5b36fa36e13fd.png', 1, NULL, NULL, 1470388399, 'home-page-trans'),
(131, 11, 'ebecbfa750dc9562a10c602bec434f7c.png', 1, NULL, NULL, 1470910874, 'home-ms-sql'),
(132, 11, '9e371bb96065e3f4d31a7a4baa2bf63e.png', 1, NULL, NULL, 1470910875, 'home-ms-azure'),
(133, 11, '1bae981f5f21d639458bec39fc533186.png', 1, NULL, NULL, 1470910876, 'home-top'),
(134, 11, '0f0d265104dc09ad27c4dc062d7d8f8b.png', 1, NULL, NULL, 1470995310, 'hosting-grey'),
(135, 11, '2434ac8d0c627b6349380dc940563d41.png', 1, NULL, NULL, 1479802159, 'Cloud'),
(136, 11, '20eaf6455197158b1356ca5efe561637.png', 1, NULL, NULL, 1479802400, 'Cloud smaller'),
(137, 11, '8e4dec05ad94816a9c065495d796c032.png', 1, NULL, NULL, 1479803892, 'SQL server 2016 editions'),
(138, 11, 'ec31fc3cc54c8b76ec25eb949c68b51b.png', 1, NULL, NULL, 1479803893, 'SQL server 2016');

-- --------------------------------------------------------

--
-- Table structure for table `cms_media_folder`
--

CREATE TABLE IF NOT EXISTS `cms_media_folder` (
  `media_folder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_name` varchar(200) DEFAULT NULL,
  `media_folder_link` int(11) DEFAULT '0',
  PRIMARY KEY (`media_folder_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `cms_media_folder`
--

INSERT INTO `cms_media_folder` (`media_folder_id`, `media_folder_name`, `media_folder_link`) VALUES
(1, 'Client logos', 0),
(2, 'service icons', 0),
(3, 'Partner logos', 0),
(4, 'Service images', 0),
(6, 'Newsmall images', 0),
(7, 'horizontal', 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_media_folder_link`
--

CREATE TABLE IF NOT EXISTS `cms_media_folder_link` (
  `media_folder_link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_id` int(11) DEFAULT NULL,
  `media_files_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_folder_link_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `cms_media_folder_link`
--

INSERT INTO `cms_media_folder_link` (`media_folder_link_id`, `media_folder_id`, `media_files_id`) VALUES
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(12, 0, 12),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 2, 37),
(38, 2, 38),
(39, 2, 39),
(40, 2, 40),
(41, 2, 41),
(42, 2, 42),
(43, 2, 43),
(44, 2, 44),
(45, 2, 45),
(46, 2, 46),
(47, 3, 47),
(48, 3, 48),
(49, 3, 49),
(50, 3, 50),
(51, 3, 51),
(52, 3, 52),
(53, 3, 53),
(54, 0, 54),
(55, 4, 55),
(56, 4, 56),
(57, 4, 57),
(58, 4, 58),
(59, 4, 59),
(60, 4, 60),
(61, 4, 61),
(62, 4, 62),
(63, 4, 63),
(64, 4, 64),
(65, 4, 65),
(66, 4, 66),
(67, 4, 67),
(68, 4, 68),
(69, 4, 69),
(70, 4, 70),
(71, 4, 71),
(72, 0, 72),
(73, 0, 73),
(75, 0, 75),
(76, 2, 76),
(113, 7, 113),
(112, 7, 112),
(110, 0, 110),
(109, 0, 109),
(108, 0, 108),
(107, 0, 107),
(106, 0, 106),
(105, 0, 105),
(104, 0, 104),
(103, 0, 103),
(102, 0, 102),
(101, 0, 101),
(100, 0, 100),
(99, 0, 99),
(98, 0, 98),
(97, 0, 97),
(96, 0, 96),
(95, 0, 95),
(114, 7, 114),
(115, 7, 115),
(116, 7, 116),
(117, 7, 117),
(118, 7, 118),
(119, 7, 119),
(120, 7, 120),
(121, 7, 121),
(122, 7, 122),
(123, 7, 123),
(124, 7, 124),
(125, 7, 125),
(126, 7, 126),
(127, 7, 127),
(128, 7, 128),
(129, 7, 129),
(130, 0, 130),
(131, 0, 131),
(132, 0, 132),
(133, 0, 133),
(134, 2, 134),
(135, 0, 135),
(136, 0, 136),
(137, 0, 137),
(138, 0, 138);

-- --------------------------------------------------------

--
-- Table structure for table `mod_page`
--

CREATE TABLE IF NOT EXISTS `mod_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_timestamp` int(11) NOT NULL,
  `page_date` int(11) NOT NULL,
  `page_price` double DEFAULT NULL,
  `page_category_id` int(11) DEFAULT NULL,
  `page_heading` varchar(400) NOT NULL,
  `page_short_description` text NOT NULL,
  `page_description` text NOT NULL,
  `page_link` varchar(400) NOT NULL,
  `page_link_video` int(1) NOT NULL,
  `page_archived` int(11) NOT NULL DEFAULT '0',
  `page_quantity` int(11) DEFAULT NULL,
  `page_code` varchar(100) DEFAULT NULL,
  `page_offset` int(11) DEFAULT NULL,
  `page_featured` int(11) DEFAULT '0',
  `page_hidden_on_site` int(11) DEFAULT '0',
  `page_sort` int(99) DEFAULT NULL,
  `page_colour` varchar(200) DEFAULT NULL,
  `page_anchor` varchar(400) DEFAULT NULL,
  `page_include` text,
  `page_align` varchar(100) DEFAULT NULL,
  `page_slug` varchar(400) DEFAULT NULL,
  `page_author` varchar(200) DEFAULT NULL,
  `page_special` int(11) DEFAULT '0',
  `page_in_the_news` int(11) DEFAULT '0',
  `page_latest_page` int(11) DEFAULT '0',
  `page_latest_article` int(11) DEFAULT '0',
  `page_latest_article_1` int(11) DEFAULT '0',
  `page_latest_article_2` int(11) DEFAULT '0',
  `page_latest_article_3` int(11) DEFAULT '0',
  `page_json` text,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=382 ;

--
-- Dumping data for table `mod_page`
--

INSERT INTO `mod_page` (`page_id`, `media_files_id`, `page_timestamp`, `page_date`, `page_price`, `page_category_id`, `page_heading`, `page_short_description`, `page_description`, `page_link`, `page_link_video`, `page_archived`, `page_quantity`, `page_code`, `page_offset`, `page_featured`, `page_hidden_on_site`, `page_sort`, `page_colour`, `page_anchor`, `page_include`, `page_align`, `page_slug`, `page_author`, `page_special`, `page_in_the_news`, `page_latest_page`, `page_latest_article`, `page_latest_article_1`, `page_latest_article_2`, `page_latest_article_3`, `page_json`) VALUES
(338, 0, 1485939877, 0, 0, 337, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 0, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3 style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Consulting<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge provides Consulting and Professional Services and Solutions that consist of advisory, assessment, strategic, architectural and design planning, as well as implementation and management services to align business and IT.<\\\\\\/p><p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=127\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p><strong>Areas of specialisation:<\\\\\\/strong><\\\\\\/p><ul><li>Best practice &amp; solution architectures.<\\\\\\/li><li>Budget &amp; Services roadmap.<\\\\\\/li><li>Research and Development of customers&rsquo; IT roadmap.<\\\\\\/li><li>Microsoft Services.<\\\\\\/li><li>Roadmap and Software Asset Management.<\\\\\\/li><li>Application Services.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Talk to a consultant!<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(337, 0, 1485940549, 0, 0, 335, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 1, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p style=\\\\\\"text-align: justify;\\\\\\">Our dedicated service desk functions as our central services center. It is also our &lsquo;single point of contact center&rsquo; (SPoC). From here we provide the full managed-services package, remote management as well as monitoring services to all our clients. We offer ad hoc as well as permanent, on-site based technicians and SLA support.&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=113\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p><strong>Why Us?<\\\\\\/strong><\\\\\\/p><p>ECOTECH Converge makes use of the Autotask and Solarwinds N-Able range of products to provide managed, dedicated or shared service desk capabilities coupled to the full remote management, monitoring and support services capabilities. The ECOTECH Converge &lsquo;Managed Services&rsquo; and &lsquo;Operational Support Services&rsquo; teams are fully aligned with the ITILv3 best practices.<\\\\\\/p><p><strong>Specifics<\\\\\\/strong><\\\\\\/p><p>The ITIL (Information Infrastructure Library) approach is a set of best practices for IT service management that focus on aligning IT services with the needs of your business.<\\\\\\/p><p>ITIL describes processes, procedures, tasks and checklists that are used by an organization for establishing integration with strategy, delivering value and maintaining a minimum level of competency. It provides a baseline from which one can plan, implement, and measure, and is used to demonstrate compliance and measure improvement.<\\\\\\/p><ul><li><strong>Incident Management:<\\\\\\/strong> Restores normal service operation as quickly as possible.<\\\\\\/li><li><strong>Problem Management:<\\\\\\/strong> Resolves the root causes of incidents, prevents recurrence of incidents related to these errors and includes problem identification and recording, classification, investigation and diagnosis.<\\\\\\/li><li><strong>Change Management:<\\\\\\/strong> Ensures that standardized methods and procedures are used for efficient handling of all changes.<\\\\\\/li><li><strong>Asset Management:<\\\\\\/strong> The management and traceability of every aspect of a configuration from beginning to end, which includes identification, planning, change control, change management, release management and maintenance.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Call us today!&nbsp;<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}}}}'),
(317, 0, 1471875343, 0, 0, 325, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 2, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><a href=\\\\\\"content\\\\\\/contact-us\\\\\\"><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"margin: auto; padding-left: 40px;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=133\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/a><\\\\\\/p><p style=\\\\\\"text-align: center;\\\\\\"><a href=\\\\\\"https:\\\\\\/\\\\\\/azure.microsoft.com\\\\\\/en-us\\\\\\/\\\\\\" target=\\\\\\"_blank\\\\\\"><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: inline-block;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=132\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/a>&nbsp;<a href=\\\\\\"https:\\\\\\/\\\\\\/www.microsoft.com\\\\\\/en\\\\\\/server-cloud\\\\\\/products\\\\\\/sql-server\\\\\\/default.aspx\\\\\\" target=\\\\\\"_blank\\\\\\"><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: inline-block;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=131\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/a><\\\\\\/p>"}}}}'),
(318, 0, 1467838503, 0, 0, 325, '', '', '', '', 0, 1, 0, '', 0, 0, 0, 3, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>At ECOTECH Converge we regard partnerships as a highly valued attribute to our business. Strategic partnerships empower us to leverage of leading industry knowledge, thus enabling us to deliver &ldquo;best of breed&rdquo; solutions to our clients.<\\\\\\/p><p>We will work with various suppliers and partners to deliver products and services as and when required by our clients. This will include amongst others the following:<\\\\\\/p>"}},"row-1":{"col1":{"type":"image","data":{"id":"10","width":"600","quality":"100","aspect_r":"0"}},"col2":{"empty":""},"col3":{"empty":""}}}}'),
(319, 0, 1485952202, 0, 0, 333, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 4, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p style=\\\\\\"text-align: justify;\\\\\\">We provide hosting, hosted private cloud, hosted multi-tenant cloud and public could services to support and deliver numerous private and commercial platforms. Our cloud and hosting facilities and distributed service delivery nodes are managed and operated 24\\\\\\/7\\\\\\/365.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=123\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Cloud makes computing resources available in a remote location and accessible over a network. It entrusts remote services with a user&#39;s data, software and computation, avoiding upfront infrastructure costs and focusing on projects that differentiate a business. Public, Private and Hybrid cloud computing services and solutions to enable customers to implement and provide scalable and adaptable &ldquo;Software as a Service&rdquo; services to their customers and business, or both. Prescribed architecture is customized and implemented to provide a complete, effective and simplified hosted and managed services infrastructure solution.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Cloud Services:<\\\\\\/strong><\\\\\\/p><ul><li style=\\\\\\"text-align: justify;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/iaas\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">IaaS (Infrastructure as a service).<\\\\\\/span><\\\\\\/a><\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/saas\\\\\\">SaaS (Software as a service).<\\\\\\/a><\\\\\\/span><\\\\\\/li><ul style=\\\\\\"text-align: justify;\\\\\\"><li>Hosted Exchange.<\\\\\\/li><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/office-365\\\\\\">Office 365.<\\\\\\/a><\\\\\\/span><\\\\\\/li><\\\\\\/ul><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/dbaas\\\\\\">DBaaS (Database as a service).<\\\\\\/a><\\\\\\/span><\\\\\\/li><ul style=\\\\\\"text-align: justify;\\\\\\"><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/ms-sql\\\\\\">SQL 2016<\\\\\\/a><\\\\\\/span><\\\\\\/li><li>MySQL<\\\\\\/li><\\\\\\/ul><li style=\\\\\\"text-align: justify;\\\\\\">SQLaaS (SQL as a Service).<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">BIaaS (Business Intelligence as a Service).<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Hybrid Cloud Infrastructure.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Cloud hosted VOIP\\\\\\/PABX\\\\\\/Voice Logging\\\\\\/Least Cost Routing.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Journaling of Hosted Exchange e-mail.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Backup as a Service (RedStore).<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Cloud hosted RMM (Remote Management &amp; Monitoring) with Anti-Virus Security,<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Notifications and Reporting.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">SPLA (Service Provider Licensing Agreements) Microsoft Licensing.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/office-365\\\\\\">Office 365<\\\\\\/a><\\\\\\/span>&nbsp;with various options including deployment and Mimecast integration.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">CSP (Cloud Service provider) Office 365 products, Services and Licenses direct from ECOTECH.<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Why Us?<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">We provide hosting, hosted private cloud, hosted multi-tenant cloud as well as public cloud services to support numerous private and commercial platforms. Our cloud and hosting facilities and distributed service delivery nodes are managed and operated 24\\\\\\/7\\\\\\/365.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge has dedicated world-class hosting facilities in both our Bryanston and Pretoria premises. Our cloud and hosting teams manage all facets, from inception through the lifecycle of the service\\\\\\/solutions. We leverage industry-leading technology, hardware, storage, network and connectivity solutions to craft solutions that meet our clients&rsquo; explicit needs. This capability and competency provides all facets of ICT services and provides adaptive scale and aligned licensing models to provide a utility based, consumption based ICT service.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Specifics:<\\\\\\/strong>&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">The two ECOTECH Converge datacenters deliver high-tech, geo-resilient services. The distance between these centers absolutely meets the compliance mandates for disaster recovery. They are hosted within the borders of Southern Africa, thereby also complying with data sovereignty legislation. These facilities form the core of the ECOTECH Converge cloud services staging, R&amp;D, solution and service development platforms. Through our partnership with Teraco, and the distributed datacenters they provide &ndash; nationally and internationally &ndash; we have the reach and capability to delivery cloud-based or hosted ICT services to all clients, regardless of the geography, and the capability to operate and support these services.<\\\\\\/p><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><strong><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a>&nbsp;<\\\\\\/strong><\\\\\\/div><p>&nbsp;&nbsp;<\\\\\\/p>"}}}}'),
(365, 0, 1485952319, 0, 0, 358, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 5, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3 style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Infrastructure as a service (IaaS)<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">Infrastructure as a Service (IaaS) is used to delivering Cloud Computing infrastructure &ndash; servers, storage, network and operating systems, as an on-demand service. Instead than purchasing servers, software, datacenter space or network equipment, clients rather buy those resources as an outsourced service on demand.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Also known as &lsquo;Hosted Infrastructure&rsquo;, it provides businesses with Dynamic Datacentre infrastructure architecture that is robust, scalable, secure and highly available.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Why us?<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Our Hosting Infrastructure is monitored and maintained through integrated management toolsets together with the required management and maintenance processes and practices. It utilises the latest server platforms and virtualisation technologies as well as hardware and storage solutions.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Specifics:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">IaaS provides on demand computing infrastructure (server and storage) up to the level of operating system. Our initial offering covers physical or virtual servers on Intel technologies. The options are as follows:<\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>Server on Demand &ndash; <span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/windows-server\\\\\\">Microsoft Windows Server 2008, 2012, 2012 R2<\\\\\\/a><\\\\\\/span>&nbsp;or Red Hat.<\\\\\\/li><li>Storage on demand &ndash; Selectable in Gigabyte blocks.<\\\\\\/li><li>Virtualisation &ndash; VMWare or&nbsp;<span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/hyperv\\\\\\">Microsoft Hyper-V.<\\\\\\/a><\\\\\\/span><\\\\\\/li><li>Optional Backup.<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\"><strong>The following core characteristics describe what IaaS is:<\\\\\\/strong><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>Resources are distributed as a service.<\\\\\\/li><li>Allows for dynamic scaling.<\\\\\\/li><li>Has a variable cost, utility pricing model.<\\\\\\/li><li>Generally, includes multiple users on a single piece of hardware.<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"text-align: justify; background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><strong><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a>&nbsp;<\\\\\\/strong><\\\\\\/div><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;&nbsp;<\\\\\\/p>"}}}}'),
(340, 0, 1485940890, 0, 0, 341, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 6, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>ECOTECH Converge offers a wide range of information technology hardware and peripheral devices catering for most IT requirements.&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=120\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/>ECOTECH Converge is a value-added IT Infrastructure Distributor and provides leading and best of breed information technology hardware and software brands to enterprises and small to medium enterprise businesses. ECOTECH Converge offers a wide range of information technology hardware and peripheral devices catering for most IT requirements. &nbsp;<\\\\\\/p><p><strong>Why us?<\\\\\\/strong><\\\\\\/p><p>ECOTECH Converge provides a wide range of services and products to choose from, catering for all customer requirements. We believe in maintain excellent relationships, not only with our clients, but as importantly, with our vendors and suppliers. This ensures superior supplier support, enabling us to provide clients with excellent products and customer aftersales support at all times.<\\\\\\/p><p>A custom-made solution is provided via a semi, fully and\\\\\\/or application-managed physical or virtual server.<strong>&nbsp;<\\\\\\/strong><\\\\\\/p><p><strong>Specifics<\\\\\\/strong><\\\\\\/p><p>We offer hardware and peripherals:<\\\\\\/p><ul><li>IT Hardware<\\\\\\/li><li>IT Software&nbsp;<\\\\\\/li><\\\\\\/ul><p>ECOTECH converge welcomes the opportunity to provide this excellent service for your company. All quotes and requests can be mailed to <a href=\\\\\\"mailto:reps@ecocorp.co.za\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">reps@ecocorp.co.za<\\\\\\/span>&nbsp;<\\\\\\/a><\\\\\\/p><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Call us today!<\\\\\\/a><\\\\\\/div><p>&nbsp;&nbsp;<\\\\\\/p>"}}}}'),
(339, 0, 1471597227, 0, 0, 340, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 7, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Connectivity<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge supply various turnkey connectivity solutions including, but not limited to, managed connectivity, broadband, V-Sat and internet service provider services.&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=124\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge provides corporate access and connectivity solutions and services to address and accommodate business requirements for highly available accessible networks between sites, offices, data centres and the Internet.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Our comprehensive range of solutions and services includes Wireless, Broadband, Metro Fibre \\\\\\/ Ethernet, MPLS and IPConnect (IPC) connectivity and access options<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Why us?<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge has built up a highly competent and reliable network team that focuses on quality and doing the cut over right first time without repeats or returns. We utilize a proven methodology, providing a niche service offering.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">We have developed a dedicated group of experts dealing with all segments of networking and connectivity and industry aligned solutions and offerings. Our strategic partnerships with key carriers such as NEOTEL, SEACOM, TELKOM, and our relationships with key delivery partners makes it possible for us to deliver world-class connectivity solutions which span the globe and which provide true value.<\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>V-SAT connectivity into African countries, or remote locations where fixed line or other services are not available.<\\\\\\/li><li>Local, shaped and unshaped broadband services.<\\\\\\/li><li>Business fibre connectivity ranging from best effort to full service level agreement services.<\\\\\\/li><li>Hosted service delivered over fibre cable.<\\\\\\/li><li>Local Area and Wide Area network solutions, design, planning, implementation, equipment, infrastructure and services.<\\\\\\/li><li>Virtual private network solutions.<\\\\\\/li><li>Bandwidth optimization, prioritization and filtering\\\\\\/management solutions.<strong>&nbsp;<\\\\\\/strong><\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Specifics:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge currently services clients across more than 8 countries globally, leveraging solutions that range from low cost broadband, through cellular\\\\\\/GSM, Fibre Optic and V-SAT technologies to provide customized, turnkey connectivity solutions that align to the needs and budgets of our clients.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Connectivity Solutions:&nbsp;<\\\\\\/strong><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li><strong><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">ISP &amp; Broadband<\\\\\\/span> -&nbsp;<\\\\\\/strong>Through our corporate services division, we have a dedicated group of experts dealing with all segments of networking and connectivity. Our strategic partnerships with all major providers have enabled us to deliver on world class connectivity solutions that span the globe.&nbsp;<\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>MPLS<\\\\\\/strong> <\\\\\\/span>- Designed to provide a unified, highly scalable and protocol agnostic data-carrying service medium that is utilised to deliver IP Virtual Private Network services, Metro Ethernet services as well as Voice over IP, Video and Data services.<\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><p><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Dynamic Bandwidth <\\\\\\/strong><\\\\\\/span>- Internet bandwidth provided in a shared medium where business can take advantage of several attributes of shared networks and consider that not all users are connected to the network at any one time when most traffic occurs in bursts.<\\\\\\/p><\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><strong><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Broadband&nbsp;<\\\\\\/span><\\\\\\/strong>- Dependable and trustworthy business broadband solutions offering wireless broadband, ADSL broadband, SDSL broadband and Fibre broadband access options.<\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><strong><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Metro Ethernet<\\\\\\/span><\\\\\\/strong> - Metropolitan area network based on Ethernet standards, which offers multipoint connectivity between sites, is quality-of-service enabled and provides business Real Time, Business Data and General Data Classes of Service.&nbsp;<\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>IP Connect (IPC) <\\\\\\/strong><\\\\\\/span>- A network access and connectivity service that provides business with a diginet-like point-to-point solution without the need for a leased line. IPC infrastructure allows for the utilisation of existing ADSL lines to access corporate hosted services at the hosting facility with no associated bandwidth costs. From our Datacenters in Bryanston and Pretoria we are managing networks to 8 International countries.<\\\\\\/li><\\\\\\/ul><ul style=\\\\\\"text-align: justify;\\\\\\"><li><strong><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Voice, Telephony and Unified Communications <\\\\\\/span><\\\\\\/strong>- Our VOIP connectivity is designed to meet our client&rsquo;s needs: we supply unified communications via all mediums of connectivity including V-SAT and Broadband. We design, implement and manage VOIP PABX solutions.<\\\\\\/li><\\\\\\/ul><ul><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>VOIP Voice over IP<\\\\\\/strong><\\\\\\/span> - Voice over IP (VOIP) and Least Cost Routing solutions for streamlined communications over data networks enable businesses to lower costs for phone services, provides users with more features to boost productivity and provides the business with improved collaboration and simplified administration. Our Voice and Telephony consulting and professional services and solutions include &ldquo;In the Cloud&rdquo; and &ldquo;On Premise&rdquo; options.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Get connected today!<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(324, 0, 1468234036, 0, 0, 343, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 8, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=65&amp;width=1080\\\\\\" alt=\\\\\\"\\\\\\" width=\\\\\\"679\\\\\\" height=\\\\\\"702\\\\\\" \\/><\\/p>"}}}}'),
(325, 0, 1471597490, 0, 0, 344, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 9, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p style=\\\\\\"text-align: justify;\\\\\\">IaaS provides on-demand computing infrastructure (compute and storage) up to the level of the operating system. The base offering covers physical or virtual servers on Intel technologies. The solution provides resources from a single server up to hundreds of server depending on your unique requirements. The IaaS is run across numerous blade chassis and SAN enclosure to ensure enterprise scale.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=112\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>The options are as follows:<\\\\\\/strong><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>Server on-demand &ndash; Linux or Microsoft operating systems.<\\\\\\/li><li>Storage on-demand &ndash; selectable in gigabyte blocks.<\\\\\\/li><li>Virtualisation &ndash; Microsoft Hyper-V.<\\\\\\/li><li>Optional snapshot, replication or backup services.<\\\\\\/li><li>Optional off-site storage.<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\"><br \\\\\\/><strong>Customer Scenario:<\\\\\\/strong><\\\\\\/p><ol><li style=\\\\\\"text-align: justify;\\\\\\"><strong>Business Challenge:<\\\\\\/strong> Need compute processing power and storage for test and development, or Quality assurance and end user acceptance testing platforms? Need additional resources for go to market and sales initiatives? Does month end processing deadline put your existing infrastructure under strain? Is there a rapid infrastructure deployment requirement to launch a product or service to market?<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><strong>Solution:<\\\\\\/strong> ECOTECH Infrastructure as a Service. Utilise efficiently provisioned physical or virtual servers hosted in our secure, highly available cloud data centres. Rent a configured server(s), add extra storage and options such as backup\\\\\\/recovery and within minutes you are ready to go. And you pay only for what you specify and use on a per month basis.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><strong>Service Description:<\\\\\\/strong> A Physical Server or a Virtualised Server. Either instance is provisioned on a server blade farm with Windows Server 2008\\\\\\/2012R2 or Linux Server derivatives as the guest OS. Microsoft Hyper-V is the virtualisation technology used to host the virtual machines.<\\\\\\/li><\\\\\\/ol><p>&nbsp;<\\\\\\/p>"}}}}'),
(326, 0, 1485954644, 0, 0, 338, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 10, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p style=\\\\\\"text-align: justify;\\\\\\">Microsoft technologies and solutions are widely deployed and utilized today, which includes everything from server platform solutions and tools, to systems management and desktop infrastructure. ECOTECH Converge, through our partnership and relationship with Microsoft, helps customers design and implement secure, optimized and well managed Microsoft infrastructure solutions in a practical way.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=121\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Why Us?<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">We are a Microsoft certified partner and carry a number of Microsoft Gold competencies. Our Microsoft Windows Azure Pack Cloud hosting platform provides resilient and reliable utility-based computing services that have been developed with our clients&rsquo; explicit needs for reliability and security in mind.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Our cloud and datacenter competencies now leverage Microsoft Windows Azure Pack in all ECOTECH Datacenters, and provide extended reach, capability and on-demand capacity and scale through Microsoft Windows Azure. With datacenters located across the globe, this strategic and symbiotic relationship equips us with the ability to provide a wide range of highly advanced, highly distributed and global cloud services and solutions.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">In addition, ECOTECH Converge has experience with large hosting facilities and datacenters, having been responsible for the successful implementation of not only our own world-class hosting and cloud facility, but also client infrastructures and datacenters across the globe<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Specifics:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">In order to meet the individual needs of clients, ECOTECH Converge maintains a wide range of expertise and has aligned very strategic partnerships. We excel in the provision and delivery of Microsoft-based traditional and cloud solutions, and underpin the services we deliver with highly skilled staff.<\\\\\\/p><ul><li style=\\\\\\"text-align: justify;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/windows-server\\\\\\">Windows Server.<\\\\\\/a><\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/windows-10\\\\\\">Windows 10.<\\\\\\/a><\\\\\\/span><\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/office-365\\\\\\">Office 365.<\\\\\\/a>&nbsp;<\\\\\\/span>(Word, Excel, PowerPoint, Outlook, OneNote, Publisher, Sharepoint, OneDrive, Skype for Business, Access)<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/azure\\\\\\">Azure.<\\\\\\/a><\\\\\\/span><\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/ms-sql\\\\\\">MS SQL.<\\\\\\/a><\\\\\\/span><\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/hyperv\\\\\\">Hyper-V<\\\\\\/a><\\\\\\/span><\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Speak to a consultant!<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(327, 0, 1468233544, 0, 0, 339, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 11, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>Due to a growing demand from business environments, organizations are placing more and more pressure on IT to deliver reliable and cost effective solutions. ECOCORP Cloud Services addresses these requirements by providing &lsquo;anywhere access&rsquo; to your employees&rsquo; basic applications. ECOTECH &lsquo;Infrastructure as a Service&rsquo; allows companies to free up resources and allow IT to focus on more strategic objectives&hellip;<\\/p><p><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=62&amp;width=1080\\\\\\" alt=\\\\\\"\\\\\\" width=\\\\\\"1070\\\\\\" height=\\\\\\"702\\\\\\" \\/><\\/p><p><strong>Why us?<\\/strong><\\/p><p>A large number organizations are realizing that IT infrastructure cost and complexity are increasing each year, making it increasingly difficult to recognize value while making use of limited resources. ECOTECH Converge believe that our solution offers the most effective route to productive use at the lowest total cost of ownership in the South African market. In addition, the solution seamlessly migrates the current operating environment with the least amount of disruption. Virtual machines and storage can be provisioned in real time using the self-service IaaS portal using highly automated processes.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>IaaS provides on demand computing infrastructure (server and storage) up to the level of operating system. Our initial offering covers physical or virtual servers on Intel technologies. The options are as follows:<\\/p><ul><li>Server on Demand &ndash; Microsoft Windows Server 2008, 2012, 2012 R2 or Red Hat<\\/li><li>Storage on demand &ndash; Selectable in Gigabyte blocks<\\/li><li>Virtualisation &ndash; VMware or Microsoft Hyper-V<\\/li><li>Optional Backup<\\/li><\\/ul>"}}}}'),
(350, 0, 1485941175, 0, 0, 348, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 12, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><strong>Talk to a consultant now:&nbsp;<\\\\\\/strong><\\\\\\/h3><h3>+27 12 843 9700<\\\\\\/h3><p>&nbsp;<\\\\\\/p>"}},"row-1":{"col1":{"type":"text","data":"<h3><span class=\\\\\\" text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Pretoria (Head Office):<\\\\\\/span><\\\\\\/h3><p>62 Cussonia Ave<\\\\\\/p><p>Val de Grace<\\\\\\/p><p>Pretoria<\\\\\\/p><p>0184&nbsp;<\\\\\\/p><p><br \\\\\\/>Tel: +27 (12) 843 9700<\\\\\\/p><p>Service desk: 0861 ECOTECH (3268324)<\\\\\\/p><p>Fax: +27 (12) 843 9701<\\\\\\/p><p>Email:&nbsp;<a href=\\\\\\"mailto:info@ecotechconverge.co.za\\\\\\">info@ecotechconverge.co.za<\\\\\\/a>&nbsp;<\\\\\\/p><p><iframe src=\\\\\\"https:\\\\\\/\\\\\\/www.google.com\\\\\\/maps\\\\\\/embed?pb=!1m18!1m12!1m3!1d3593.806075173587!2d28.289881951604002!3d-25.74392588356459!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e956070ea65606f%3A0x5c25a33b3018866d!2s62+Cussonia+Ave%2C+Pretoria%2C+0184!5e0!3m2!1sen!2sza!4v1470385181217\\\\\\" width=\\\\\\"100%\\\\\\" height=\\\\\\"200\\\\\\" frameborder=\\\\\\"0\\\\\\" allowfullscreen=\\\\\\"\\\\\\"><\\\\\\/iframe><\\\\\\/p><p>&nbsp;&nbsp;<\\\\\\/p>"},"col2":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><span class=\\\\\\"\\\\\\">Quick Contacts:<\\\\\\/span><\\\\\\/span><\\\\\\/h3><p>Channel Services:<\\\\\\/p><p>Tel: +27 (12) 843 9700<\\\\\\/p><p>Email: <a href=\\\\\\"mailto:channel@ecotechconverge.co.za\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">channel@ecotechconverge.co.za<\\\\\\/span>&nbsp;<\\\\\\/a><\\\\\\/p><p><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"mailto:reps@ecocorp.co.za\\\\\\">reps@ecocorp.co.za<\\\\\\/a><\\\\\\/span>&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p><p><span class=\\\\\\"\\\\\\">Service Desk:<\\\\\\/span><\\\\\\/p><p>Tel: +27 (12) 843 9777<\\\\\\/p><p>0861 ECOTECH (3268324)<\\\\\\/p><p>Email:&nbsp;<span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"mailto:servicedesk@ecotechconverge.co.za\\\\\\">servicedesk@ecotechconverge.co.za<\\\\\\/a><\\\\\\/span><\\\\\\/p>"}},"row-2":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Cape Town:<\\\\\\/span><\\\\\\/h3><p>2nd Floor, Imperial Terraces<\\\\\\/p><p>Tyger Waterfront<\\\\\\/p><p>Carl Cronje Drive<\\\\\\/p><p>Bellville, 7530<\\\\\\/p><p>&nbsp;<\\\\\\/p><p>Tel: 086 144 7338<\\\\\\/p><p>Service desk: 0861 ECOTECH (3268324)<\\\\\\/p><p>Fax: +27 (21) 555 0276<\\\\\\/p><p>Email:&nbsp;<span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"mailto:capetown@ecotechconverge.co.za\\\\\\">capetown@ecotechconverge.co.za<\\\\\\/a><\\\\\\/span><\\\\\\/p><p><iframe style=\\\\\\"border: 0;\\\\\\" src=\\\\\\"https:\\\\\\/\\\\\\/www.google.com\\\\\\/maps\\\\\\/embed?pb=!1m18!1m12!1m3!1d342.2017092828864!2d18.629891470885966!3d-33.876881928495635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe36bb8c8de21babb!2sTygerwaterfront+Canal+Edge!5e0!3m2!1sen!2sza!4v1470385649046\\\\\\" width=\\\\\\"100%\\\\\\" height=\\\\\\"200\\\\\\" frameborder=\\\\\\"0\\\\\\" allowfullscreen=\\\\\\"\\\\\\"><\\\\\\/iframe><\\\\\\/p><p>&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p>"},"col2":{"type":"text","data":"<h3><span class=\\\\\\" text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Johannesburg:<\\\\\\/span><\\\\\\/h3><p>The Campus<\\\\\\/p><p>57 Sloan Street<\\\\\\/p><p>The Gabba Building<\\\\\\/p><p>Bryanston<\\\\\\/p><p>&nbsp;<\\\\\\/p><p>Tel: +27 (12) 843 9777<\\\\\\/p><p>Service desk: 0861 ECOTECH (3268324)<\\\\\\/p><p>Email:&nbsp;<span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"mailto:corporate@ecotechconverge.co.za\\\\\\">corporate@ecotechconverge.co.za<\\\\\\/a><\\\\\\/span><\\\\\\/p><p>&nbsp;<\\\\\\/p><p><iframe style=\\\\\\"border: 0;\\\\\\" src=\\\\\\"https:\\\\\\/\\\\\\/www.google.com\\\\\\/maps\\\\\\/embed?pb=!1m18!1m12!1m3!1d1792.3835554819198!2d28.022926925290946!3d-26.041174995681505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e9573f74ebcbfbd%3A0xc1f755b6ea59697d!2sThe+Gabba+Building%2C+57+Sloane+St%2C+Sandton%2C+2191!5e0!3m2!1sen!2sza!4v1470385293727\\\\\\" width=\\\\\\"100%\\\\\\" height=\\\\\\"200\\\\\\" frameborder=\\\\\\"0\\\\\\" allowfullscreen=\\\\\\"\\\\\\"><\\\\\\/iframe><\\\\\\/p><p>&nbsp;&nbsp;<\\\\\\/p>"}}}}'),
(335, 0, 1468233847, 0, 0, 334, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 13, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>These services provide businesses with Voice Over IP (VOIP) and least cost routing options that have been designed to work &ldquo;on premise&rdquo; as well as &ldquo;in the cloud&rdquo; as online services at the same time.&nbsp;<\\/p><p><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\/cms\\/modules\\/media\\/scripts\\/image\\/image.handler.php?media_files_id=69&amp;width=1080\\\\\\" alt=\\\\\\"\\\\\\" width=\\\\\\"625\\\\\\" height=\\\\\\"702\\\\\\" \\/><\\/p><p><strong>Why Us?<\\/strong><\\/p><p>ECOTECH Converge provides a hosted VOIP PBX solution, delivering productive phone calling features, traditionally only available to larger enterprises. Our VOIP services come with the added benefit of having no in-house systems or software to procure, manage or maintain. We do it all!<\\/p><p>Our VOIP solution provides businesses with access to advanced functionality, easy maintenance from remote locations, and a user-friendly system, which adds up to advantages in worker productivity. Low maintenance demands also free up internal IT staff to focus on more high-value pursuits.<\\/p><p><strong>Specifics<\\/strong><\\/p><p>ECOTECH Converge provides an on-Premise VOIP PBX solution, which delivers the same services and benefits as the Hosted VOIP PBX, but with the added capability of being able to combine the VOIP PBX \\/ Gateway with traditional PBX functionality. This enables businesses to utilize existing infrastructure, realizing further cost savings, and greater mobility.<\\/p>"}}}}'),
(342, 0, 1485941301, 0, 0, 317, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 14, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>Exceptional functional and technical expertise, coupled with extensive industry knowledge makes ECOTECH Converge the ideal choice in a consulting firm. Choose us to manage the implementation and operations of your ICT infrastructure requirements.<\\\\\\/p><p><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=122\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p>&nbsp;<\\\\\\/p><p>ECOTECH Converge provides superior consulting services for IT Infrastructure and services management. We work in the financial, mining, logistics, agriculture, hospitality, telecoms and multiple other industries, serving both the domestic and international sectors. Our headquarters is in Pretoria, South Africa.<\\\\\\/p><p>We pride ourselves on our proven track record, effectively administering multiple corporate ICT infrastructures. We have an acute understanding of each of our client&rsquo;s unique challenges, and are able to provide turnkey solutions for managing people, processes and technology.<\\\\\\/p><p>ECOTECH Converge has experience with large hosting facilities and datacenters. We successfully implement client infrastructures and datacenters across the globe, as well as having created our very own world-class infrastructures to manage our own business.<\\\\\\/p><p>In order to meet the individual needs of clients, ECOTECH Converge maintains a wide range of expertise:<\\\\\\/p><ul><li>We have established numerous strategic partnerships to assist us in delivering the highest quality and range of services.<\\\\\\/li><li>We excel in the Microsoft competency.<\\\\\\/li><li>We back this up with core understanding and delivery of managed hosting, broadband connectivity and cutting edge data solutions.<\\\\\\/li><\\\\\\/ul><p><strong>What makes ECOTECH different?<\\\\\\/strong><\\\\\\/p><p>ECOTECH Converge brings a fresh and innovative approach to consulting services, acting as liaison between the end-user and the software provider. Our goal is to exceed the expectations of every client by offering outstanding customer service, increased flexibility, and greater value, thus optimizing system functionality and improving operational efficiency.<\\\\\\/p><p>Our associates are distinguished by their functional and technical expertise, combined with their hands-on experience, thereby ensuring that our clients receive the most effective and professional service.<\\\\\\/p><p>ECOTECH Converge extensive skills encompass all aspects of ICT implementation and operation, including business requirements definition, development of functional specifications for client approval, system design, and overseeing vendor and solution selection to fit specific client needs.<\\\\\\/p><p>ECOTECH Converge identifies with the following fundamental ethics:<\\\\\\/p><ul><li>Operational brilliance.<\\\\\\/li><li>A solid ethos of reliability, truthfulness and honour.<\\\\\\/li><li>Respect for each individual at each business rank.<\\\\\\/li><li>Taking responsibility at an organisational and employee level.<\\\\\\/li><\\\\\\/ul><p><strong>History<\\\\\\/strong><\\\\\\/p><p>A converged unification of talents brought together by market conditions, acquired skills and business acumen. ECOTECH Converge had a very humble start as less than a hand full of people originating from several ITC sectors focused on distribution in a garage size premises before 2001.&nbsp;<\\\\\\/p><p>Clients needed a more comprehensive provider and dealers needed support, which led to the organic growth of the Corporate Services division to compliment distribution and expand the business into other market segments.<\\\\\\/p><p>&nbsp;The garage grew to several neighbouring premises, host to multi storey buildings and even Data Centre Facilities that in conjunction with the national support offices and data centres deliver the broad yet specialised spectrum of services and solutions to our clients all over the globe.<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}}}}'),
(341, 0, 1485941772, 0, 0, 347, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 15, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>ECOTECH Converge is extremely proud of the consistently positive feedback we receive from our clients. We currently have more than 1600 active clients varying from small to international corporates in all segments of the economy.<\\\\\\/p><p>&nbsp;Our operational business sectors &amp; industries include:<\\\\\\/p><ul><li>Education.<\\\\\\/li><li>Financial.<\\\\\\/li><li>Hospitality.<\\\\\\/li><li>Insurance.<\\\\\\/li><li>Legal.<\\\\\\/li><li>Mining &amp; Resources.&nbsp;<\\\\\\/li><li>Property.<\\\\\\/li><li>Public.<\\\\\\/li><li>Retail.<\\\\\\/li><li>Telecommunications.<\\\\\\/li><li>Tourism.<\\\\\\/li><li>Transportation.<\\\\\\/li><li>Utilities.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p>"}}}}'),
(351, 0, 1470386728, 0, 0, 320, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 16, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>ECOTECH partners with choice international service providers &amp; key players, ensuring quality &amp; reliable service to all clients.<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}},"row-1":{"col1":{"type":"image","data":{"id":"47","width":"600","quality":"100","aspect_r":"0"}},"col2":{"type":"image","data":{"id":"52","width":"600","quality":"100","aspect_r":"0"}},"col3":{"type":"image","data":{"id":"50","width":"600","quality":"100","aspect_r":"0"}},"col4":{"type":"image","data":{"id":"53","width":"600","quality":"100","aspect_r":"0"}}},"row-2":{"col1":{"type":"image","data":{"id":"48","width":"600","quality":"100","aspect_r":"0"}},"col2":{"type":"image","data":{"id":"49","width":"600","quality":"100","aspect_r":"0"}},"col3":{"type":"image","data":{"id":"51","width":"600","quality":"100","aspect_r":"0"}},"col4":{"empty":""}},"row-3":{"col1":{"empty":""}}}}');
INSERT INTO `mod_page` (`page_id`, `media_files_id`, `page_timestamp`, `page_date`, `page_price`, `page_category_id`, `page_heading`, `page_short_description`, `page_description`, `page_link`, `page_link_video`, `page_archived`, `page_quantity`, `page_code`, `page_offset`, `page_featured`, `page_hidden_on_site`, `page_sort`, `page_colour`, `page_anchor`, `page_include`, `page_align`, `page_slug`, `page_author`, `page_special`, `page_in_the_news`, `page_latest_page`, `page_latest_article`, `page_latest_article_1`, `page_latest_article_2`, `page_latest_article_3`, `page_json`) VALUES
(352, 0, 1469800214, 0, 0, 349, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 17, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">SQL 2016<\\/span><\\/h3><p><strong>Query Store:<\\/strong><\\/p><p>One common problem many organizations face when upgrading versions of SQL Server is changes in the query optimizer (which happen from version to version) negatively impacting performance. Without comprehensive testing, this has traditionally been a hard problem to identify and then resolve. The Query Store feature maintains a history of query execution plans with their performance data, and quickly identifies queries that have gotten slower recently, allowing administrators or developers to force the use of an older, better plan if needed. The Query Store is configured at the individual database level.<\\/p><p><strong>Polybase:<\\/strong><\\/p><p>Hadoop and Big Data have been all the rage in the last several years. I think some of that was industry hype, but Hadoop is a scalable, cost-effective way to store large amounts of data. Microsoft had introduced Polybase, a SQL Server connector to Hadoop (and Azure Blob Storage) to its data warehouse appliance Analytics Platform System in 2015. But now Microsoft has incorporated that functionality into the regular on-premises product. This feature will benefit you if your regular data processing involves dealing with a lot of large text files -- they can be stored in Azure Blob Storage or Hadoop, and queried as if they were database tables. A common scenario where you might use this would be an extract, transform and load (ETL) process, where you were taking a subset of the text file to load into your database.<\\/p><p><strong>Stretch Database:<\\/strong><\\/p><p>One common idiom in recent years, is how cheap storage is. While it may be cheap to buy a 3TB drive from Amazon, if you are buying enterprise-class SAN storage or enterprise SSDs, you will know that storage is still very expensive. Microsoft is trying to help reduce your storage (and processing costs) with a hybrid feature called \\\\\\\\\\\\\\"Stretch Database.\\\\\\\\\\\\\\" The basics of Stretch Database are that some part of your tables (configurable or automated) will be moved into an Azure SQL Database in the cloud in a secure fashion. When you query those tables, the query optimizer knows which rows are on your server and which rows are in Azure, and divides the workload accordingly. The query processing on the Azure rows takes place in Azure so the only latency is for the return of the rows over the network. As an additional enhancement, you are only charged for the SQL Database in Azure when it is used for queries. You do, however, pay for the Azure Blob storage, which, generally speaking, is much cheaper than enterprise storage.<\\/p><p><strong>JSON Support:<\\/strong><\\/p><p>In addition to supporting direct querying to Hadoop, SQL Server 2016 adds support for the lingua franca of Web applications: Java Script Object Notation (JSON). Several other large databases have added this support in recent years as the trend towards Web APIs using JSON has increased. The way this is implemented in SQL 2016 is very similar to the way XML support is built in with FOR JSON and OPENJSON -- providing the ability to quickly move JSON data into tables.<\\/p><p><strong>Row Level Security:<\\/strong><\\/p><p>A feature that other databases have had for many years, and SQL Server has lacked natively is the ability to provide row-level security (RLS). This restricts which users can view what data in a table, based on a function. SQL Server 2016 introduces this feature, which is very useful in multi-tenant environments where you may want to limit data access based on customer ID. I\\\\\\\\\\\\&#39;ve seen some customized implementations of RLS at clients in the past, and they weren\\\\\\\\\\\\&#39;t pretty. It is hard to execute at scale. The implementation of RLS in SQL 2016 still has it limits (updates and inserts are not covered), but it is good start on a much-needed feature.<\\/p><p><strong>Always Encrypted:<\\/strong><\\/p><p>It seems like every month, we hear about some company having a major data breach. Encryption works, but many companies do not or cannot implement it all the way through the stack, leaving some layer data available for the taking as plain text. SQL Server has long supported both column-level encryption, encryption at rest, and encryption in transit. However these all had to be configured independently and were frequently misconfigured. Always Encrypted is new functionality through the use of an enhanced client library at the application so the data stays encrypted in transit, at rest and while it is alive in the database. Also given Microsoft\\\\\\\\\\\\&#39;s push towards the use of Azure, easy encryption makes for a much better security story.<\\/p><p><strong>In-Memory Enhancements:<\\/strong><\\/p><p>SQL Server 2014 introduced the concept of in-memory tables. These were optimally designed for high-speed loading of data with no locking issues or high-volume session state issues. While this feature sounded great on paper, there were a lot of limitations particularly around constraints and procedures. In SQL Server 2016, this feature is vastly improved, supporting foreign keys, check and unique constraints and parallelism. Additionally, tables up to 2TB are now supported (up from 256GB). Another part of in-memory is column store indexes, which are commonly used in data warehouse workloads. This feature was introduced in SQL 2012 and has been enhanced in each version since. In 2016 it receives some enhancements around sorting and better support with AlwaysOn Availability Groups.<\\/p><p><strong>SQL Server R Services:<\\/strong><\\/p><p>SQL Server R Services is a new feature in SQL Server 2016 that supports enterprise-scale data science. SQL Server R Services helps you embrace the highly popular open source R language in your business. R is the most popular programming language for advanced analytics, and offers an incredibly rich set of packages and a vibrant and fast-growing developer community.<\\/p><p>&nbsp;<\\/p>"}}}}'),
(356, 0, 1470310433, 0, 0, 325, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 18, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><strong>Server Hosting<\\\\\\/strong><\\\\\\/p><p><strong>Server Platform<\\\\\\/strong><\\\\\\/p><p>Windows Server and Windows Server-based infrastructure solutions that provide a solid foundation for all server workload and application requirements, complete with tools for virtualisation, web and management in order to integrate, help save time, reduce costs and provide for efficient management of the Datacentre.<\\\\\\/p><p><strong>Management and Virtualisation<\\\\\\/strong><\\\\\\/p><p>Highly virtualised and well managed Datacentre solutions to provide benefits such as improved infrastructure and facilities utilisation with reduced associated costs with agile, flexible and highly available services.<\\\\\\/p><p><strong>Identity and Access<\\\\\\/strong><\\\\\\/p><p>Secure, identity-based access to systems. Applications and data both &ldquo;On premises&rdquo; and &ldquo;In the Cloud&rdquo; from virtually any location or device with the use of a single identity.<\\\\\\/p><p><strong>Hosted Infrastructure Services<\\\\\\/strong><\\\\\\/p><p>We have a dedicated world-class hosting facility in our Bryanston and Pretoria premises from where we are hosting numerous private and commercial platforms. The hosting facilities are managed by our onsite dedicated personnel and support logging is via our managed helpdesk via a service desk.<\\\\\\/p><p><strong>Data Centre<\\\\\\/strong><\\\\\\/p><p>Dynamic Datacentre services and solutions to enable customers to implement and provide robust, scalable and adaptable &ldquo;Infrastructure as a Service&rdquo; services to customers and business, or both.<\\\\\\/p><p>All factors including Facilities, Hardware and Software Infrastructure, Virtualisation, Connectivity, Storage, Networks, Security and Management are taken into consideration to provide a standardised, highly automated, agile, cost effective and complete infrastructure as a service solution.<\\\\\\/p><p><strong>Public, Private or Hybrid Cloud Services<\\\\\\/strong><\\\\\\/p><ul><li>Managed Hosting<\\\\\\/li><li>Application Managed Hosting.<ul><li>Remote Desktop Services.<\\\\\\/li><li>Windows 7\\\\\\/8\\\\\\/10.<\\\\\\/li><li>Office 2013\\\\\\/2016.<\\\\\\/li><li>Sage ERP.<\\\\\\/li><\\\\\\/ul><\\\\\\/li><li>Disaster Recovery Planning and Facilitation<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"padding-left: 30px;\\\\\\">Having access to the ECOTECH Converge Hosting facilities provides us with cost effective solutions for IT disaster recovery and business continuity. In partnership with the LCR providers, we develop and maintain a DRP for our clients and through virtualization and consolidation a cost effective alternative will be architected and tested.<\\\\\\/p><p style=\\\\\\"padding-left: 30px;\\\\\\">&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"padding: 10px; background-color: #fbbf0f; color: #ffffff; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Get hosted today!<\\\\\\/a><\\\\\\/div><p style=\\\\\\"padding-left: 30px;\\\\\\">&nbsp;<\\\\\\/p><p style=\\\\\\"padding-left: 30px;\\\\\\">&nbsp;<\\\\\\/p><p style=\\\\\\"padding-left: 30px;\\\\\\">&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}}}}'),
(353, 0, 1467975842, 0, 0, 352, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 19, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Azure<\\/span><\\/h3><p>Any developer or IT professional can be productive with Azure. The integrated tools, pre-built templates and managed services make it easier to build and manage enterprise, mobile, Web and Internet of Things (IoT) apps faster, using skills you already have and technologies you already know. Microsoft is also the only vendor positioned as a Leader across Gartner&rsquo;s Magic Quadrants for Cloud Infrastructure as a Service, Application Platform as a Service, and Cloud Storage Services for the second consecutive year.<\\/p><p><strong>Use an open and flexible cloud service platform<\\/strong><\\/p><p>Azure supports the broadest selection of operating systems, programming languages, frameworks, tools, databases and devices. Run Linux containers with Docker integration; build apps with JavaScript, Python, .NET, PHP, Java and Node.js; build back-ends for iOS, Android and Windows devices. Azure cloud service supports the same technologies that millions of developers and IT professionals already rely on and trust.<\\/p><p><strong>Extend your existing IT<\\/strong><\\/p><p>Some cloud providers make you choose between your data centre and the cloud. Not Azure, which easily integrates with your existing IT environment through the largest network of secure private connections, hybrid database and storage solutions, and data residency and encryption features &ndash; so your assets stay right where you need them. And with&nbsp;Azure Stack, you can bring the Azure model of application development and deployment to your data centre. Azure hybrid cloud solutions give you the best of both worlds: more IT options, less complexity and cost. That&rsquo;s why it&rsquo;s one of the best&nbsp;cloud computing services&nbsp;available.<\\/p><p>&nbsp;<\\/p>"}}}}'),
(354, 0, 1471597457, 0, 0, 346, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 20, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=115\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH&#39;s Service Level Agreement ensures transparency, service efficiency and consistency. This SLA is generic and serves as a guideline to that of a typical agreement.&nbsp;<\\\\\\/p><p><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"modules\\\\\\/media\\\\\\/scripts\\\\\\/documents\\\\\\/document.handler.php?media_files_id=54\\\\\\">Download PDF<\\\\\\/a><\\\\\\/span><\\\\\\/p>"}}}}'),
(364, 0, 1479806981, 0, 0, 357, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 21, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3 style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Public, Private or Hybrid Hosting?<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">As a traditional hoster, infrastructure and services provider ECOTECH has developed turn key solutions to provide cost effective infrastructure, software and system services called cloud computing. Before diving into our services lets first decompose the cloud computing buzz word and all its associated components.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Cloud Computing is the term used to describe a broad range of services. Other relevant developments in technology have caused vendors to use the term &ldquo;Cloud&rdquo; for products that fall outside the proper definition. To understand cloud and the value it offers organisations one needs to understand what cloud is and that it has different components.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">As cloud is a collection of services, organisations can choose how, where and when they utilise Cloud Computing. The different type of Cloud Computing services is generally referred to as Software as a Service (SaaS), Platform as a Service (PaaS), Infrastructure as a Service (IaaS) and Hybrid cloud.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">The ECOTECH Cloud platform is a 3<sup>rd<\\\\\\/sup> generation, Microsoft Windows Hyper-V based, fault tolerant and highly available virtual server hosting platform. This platform is built upon enterprise grade server, storage and networking equipment and additionally is architected to provide maximum uptime\\\\\\/availability and very high performance. Further, this platform is holistically encompassed by advanced management, automation and operational health systems thereby providing an &ldquo;always on&rdquo;, secure and dynamic utility based capability.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Cloud Computing Components can be described as follows:<\\\\\\/strong><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>SaaS applications are designed for end-users and provided over the internet.<\\\\\\/li><li>PaaS is the set of tools and services designed to make coding and deploying those applications quick and efficient.<\\\\\\/li><li>IaaS is the hardware and software that everything run on &ndash; servers, storage, networks, operating systems.<\\\\\\/li><li>Hybrid Cloud is a combination of On Premise infrastructure and IaaS for additional capacity or short term projects.&nbsp;<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\">Due to the different as a services layer&rsquo;s cloud computing is known as a stack since the services are built one on top of another under the general term &ldquo;Cloud&rdquo;. The generally accepted definition of Cloud Computing comes from the National Institute of Standards and Technology (NIST).<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">In summary NIST defines cloud computing as &ldquo;a model for enabling convenient, on-demand network access to a shared pool of configurable computing resources (e.g., networks, servers, storage, applications, and services) that can be quickly be provisioned and released with minimal management effort or service provider interaction&rdquo;.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>The characteristics of Cloud offered by NIST include:<\\\\\\/strong><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>On-demand self-service. The ability for an end user to sign up and receive services without the long delays that have characterized traditional IT.<\\\\\\/li><li>Broad network access. Ability to access the service via standard platforms (desktop, laptop, mobile etc).<\\\\\\/li><li>Resource pooling. Resources are pooled across multiple customers.<\\\\\\/li><li>Rapid elasticity. Capability can scale to cope with demand peaks.<\\\\\\/li><li>Measured Service. Billing is metered and delivered as a utility service.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH Converge provides public, private or hybrid cloud solutions. Consulting and professional services help clients assess different technological and methodological strategies and aids in the alignment of business in the cloud.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Semi managed hosting:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Services typically include monitoring of hardware and software infrastructure to ensure uptime and availability, as well as timeous notification to customers or vendors in case of failures and facilitation of incident management processes.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Fully managed hosting:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Services include &ldquo;semi-managed hosting services&rdquo;, operating systems and update management and support, as well as the management and support of all additional service add-ons that the customer has subscribed to. Such services include the above-mentioned managed data protection and recovery, endpoint security,managed perimeter security etc.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Application managed hosting:<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Management and support services of standard Microsoft server side applications, systems and tools related to server platform, management and virtualization, messaging, communications and identity and access management.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Business requirements are analysed in terms of connectivity, storage, security and network infrastructure and services management.<br \\\\\\/> It&rsquo;s robust, scalable, secure and highly available.It notifies customers or vendors in case of failures and facilitates incident management processes. A custom-made solution is provided via a semi, fully and\\\\\\/or application-managed physical or virtual server.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p>"}},"row-1":{"col1":{"type":"image","data":{"id":"135","width":"500","quality":"100","aspect_r":"0"}}},"row-2":{"col1":{"type":"text","data":"<p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><strong><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a><\\\\\\/strong><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(349, 0, 1485955869, 0, 0, 318, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 22, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '""'),
(380, 0, 1486030070, 0, 0, 364, 'Windows Server', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-server', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<div id=''dccn-emdedENUdistributorwindowsserver2016''></div>\\n<script type=''text/javascript''>\\nvar ccs_cc_args = ccs_cc_args || [];\\n(function () {\\n   var o = ccs_cc_args; o.push([''_SKey'', ''b7c681fa'']); o.push([''_ZoneId'', ''emdedENUdistributorwindowsserver2016'']); \\n   var sc = document.createElement(''script''); sc.type = ''text/javascript''; sc.async = true;\\n   sc.src = (''https:'' == document.location.protocol ? ''https://'' : ''http://'') + ''cdn.cnetcontent.com/jsc/h.js'';\\n   var n = document.getElementsByTagName(''script'')[0]; n.parentNode.insertBefore(sc, n);\\n})();\\n</script>"}}}}'),
(355, 0, 1485832067, 0, 0, 345, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 23, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=126\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">The transition and migration from an on-premise ICT service delivery capability is a very complex process, this is mainly due to the potential disruption of service that is implied by such a transition. To minimise the impact as far as possible to your business, ECOTECH has placed specific focus on the transition and migration planning portion of this type of initiative. A tried and tested approach of assessing, planning and designing the migration forms part of our methodology with superior support and managed services.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Typically, existing company server equipment that falls within maintenance or in good working condition can be re-provisioned to fulfil new roles, this allows for the cost impact to be minimized through utilizing the existing company investment in &ldquo;ICT infrastructure and to compliment this infrastructure with cloud services where require to provide a complete capability&rdquo;.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">The re-provisioning aims to re-deploy the existing company equipment on the latest supported Operating system as well as change the roles of these servers within the company ICT landscape to derive a modernized, fully managed and cost effective capability.<\\\\\\/p><p>&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}}}}'),
(357, 0, 1470133704, 0, 0, 325, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 24, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<ul><li>Server Hosting.<\\/li><li>Server Platform.<\\/li><li>Management and Virtualisation.<\\/li><li>Identity and Access.<\\/li><li>Hosted Infrastructure Services.<\\/li><li>Data Centre.<\\/li><li>Public, Private or Hybrid Cloud Services.<ul><li>Managed Hosting.<\\/li><li>Application Managed Hosting.<ul><li>Remote Desktop Services.<\\/li><li>Windows 7\\/8\\/10.<\\/li><li>Office 2013\\/2016.<\\/li><li>Sage ERP.<\\/li><\\/ul><\\/li><li>Disaster Recovery Planning and Facilitation.<\\/li><\\/ul><\\/li><\\/ul><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\">CALL US TODAY!<\\/div>"}}}}'),
(358, 0, 1485939818, 0, 0, 355, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 26, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=117\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p><ul><li>Connectivity Solutions.<\\\\\\/li><li>Consulting, Professional Services &amp; Solutions.<\\\\\\/li><li>Desktop Services:<\\\\\\/li><ul><li>Windows 7\\\\\\/8\\\\\\/10.<\\\\\\/li><li>Office 2013\\\\\\/2016.<\\\\\\/li><li>Anti-Virus.<\\\\\\/li><\\\\\\/ul><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Contact us today!<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(367, 0, 1485952769, 0, 0, 361, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 25, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Database as a Service (DBaaS) (SQL)<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">ECOTECH a local south African company have partnered with <span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/microsoft\\\\\\">Microsoft<\\\\\\/a><\\\\\\/span>&nbsp;South Africa to provide a cloud based SQL platform based on <span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/ms-sql\\\\\\">SQL 2016<\\\\\\/a><\\\\\\/span>&nbsp;and <span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/azure\\\\\\">Windows Azure<\\\\\\/a>&nbsp;<\\\\\\/span>Platform on premise. Together with ECOTECH&rsquo;s SQL partner and Microsoft this is the first SQL2016 on demand SQL instance offering in South Africa. This platform is built upon enterprise grade server, storage and networking equipment and additionally is architected to provide maximum uptime\\\\\\/availability and very high performance, supported by a team of high skilled specialists, service desk and automated monitoring and alerting systems.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>What is DBaaS?<\\\\\\/strong><\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">DBaaS is a cloud-based rapid provisioning of databases in the context of a database cloud rather than in the context of individual servers.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">DBaaS delivers database functionality similar to what is found in relational database management systems (RDBMS&rsquo;s) such as SQL Server, MySQL and Oracle. Being cloud-based, on the other hand, DBaaS provides a flexible, scalable, on-demand platform that&#39;s oriented toward self-service and easy management, particularly in terms of provisioning a business&#39; own environment.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">A managed service, offered on a pay-per-usage basis, that provides on-demand access to a database for the storage of application data. &nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>Why DBaaS?<\\\\\\/strong><\\\\\\/p><p>In simple terms, it&rsquo;s a method of storing and managing your data in the cloud that is simple to use and easy to update. You won&rsquo;t have to devote hours of your time to install, maintain and monitor in-house databases. That&rsquo;s the cloud provider&rsquo;s job. Your cloud database can be deployed in just a few clicks and in the end, it will be a lot more secure than your current system. And your hardware? You can stop worrying about that too. You pay only for what you use so you don&rsquo;t have to make big investments in new technology. Speaking of technology, there are a lot of advantages to having a strong infrastructure in the cloud. Further to the aforementioned, you have access to all the enterprise features without incurring the costs associated with licensing your own enterprise instance!<\\\\\\/p><p><strong>The advantages of DBaaS?<\\\\\\/strong><\\\\\\/p><p>That&rsquo;s fine, you might say, but will it work for my business? We think so and here are a few reasons why:<\\\\\\/p><p><strong>Security<\\\\\\/strong><\\\\\\/p><p>If you&rsquo;re running your databases on in-house servers, you have to think seriously about security. You&rsquo;ll have to ensure that your database is up to date to keep it safe from digital threats. You also need to make sure your on-premises location is secure from any possible disaster that could interrupt services. This requires lot of work and resources that your business might not have. If you have a cloud solution, your provider handles security, which means you don&rsquo;t have to worry about it.<\\\\\\/p><p><strong>Rapid provisioning<\\\\\\/strong><\\\\\\/p><p>Need a new database in a hurry? No problem. You can scale your usage up or down whenever you need it. This can range from 10GB to 1000GB, depending on the provider.<\\\\\\/p><p><strong>Reduced cost<\\\\\\/strong><\\\\\\/p><p>Instead of investing large amounts in technology, you pay as you go. You can even start with hourly pay-as-you-go options, depending on your cloud provider. You&rsquo;re also free of the problems associated with providing new software for each user.<\\\\\\/p><p><strong>Performance<\\\\\\/strong><\\\\\\/p><p>One of the common complaints we&rsquo;ve heard is that databases just aren&rsquo;t fast enough. Queries that should take a few minutes sometimes end up taking hours to process. And in some cases, the data isn&rsquo;t even valid! Cloud-based databases can provide much better performance than traditional on-premises systems. This is because they can gather as many instances as required to complete the database processing as quickly as possible. There&rsquo;s also the matter of uptime to consider! Most cloud providers have high availability configurations of 99.99% or higher, which is hard to achieve with an in-house solution.<\\\\\\/p><p><strong>Continuous monitoring and maintenance:<\\\\\\/strong><\\\\\\/p><p>Stop worrying about finding the right IT expert to deal with your database problems. The cloud provider has technical experts working around the clock to monitor and optimize your databases.<\\\\\\/p><p><strong>Database consumer:<\\\\\\/strong><\\\\\\/p><ul><li>On-demand: Quick access to a database allows for greater agility and innovation.<\\\\\\/li><li>Simple: No need to manage a full server ;<\\\\\\/li><li>Abstracted from management of HA, performance.<\\\\\\/li><li>Cost-efficient: Pay for what you use, when you use it.<\\\\\\/li><\\\\\\/ul><p><strong>&nbsp;DBaaS Products:<\\\\\\/strong><\\\\\\/p><ul><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/ms-sql\\\\\\">SQL 2016<\\\\\\/a><\\\\\\/span><\\\\\\/li><li>MySQL<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Get DBaas today!&nbsp;<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p><p>&nbsp;<\\\\\\/p>"}}}}'),
(366, 0, 1485952399, 0, 0, 359, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 27, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Software as Service (SaaS)<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">Software as a service (or SaaS) is a way of delivering applications over the Internet&mdash;as a service. Instead of installing and maintaining software, you simply access it via the Internet, freeing yourself from complex software and hardware management known as &ldquo;on demand software&rdquo;.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Software as a Service (SaaS) users have access to application software and databases. SaaS is sometimes referred to as \\\\\\"on-demand software\\\\\\" and is usually priced on a pay-per-use basis. The SaaS platform provides a solution designed to work &ldquo;On-Premise&rdquo; or &ldquo;In the Cloud&rdquo;. Our Cloud Services infrastructure is based on prescribed architecture and is mapped to customer requirements, providing streamlined communications, effective collaboration services, simplified management, security and availability.<\\\\\\/p><ul><li>Hosted Exchange.<\\\\\\/li><li><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/office-365\\\\\\">MS Office 360.<\\\\\\/a><\\\\\\/span><\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><strong><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a>&nbsp;<\\\\\\/strong><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(362, 0, 1485951362, 0, 0, 354, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 28, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=129\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><\\\\\\/p>"}},"row-1":{"col1":{"type":"text","data":"<p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Server Hosting<\\\\\\/strong><\\\\\\/span><strong> - <\\\\\\/strong>A dedicated server gives you the control, power and security. With your own server, you&#39;ll have access to all the server resources and horsepower you need. Plus, with our dedicated server experts managing and monitoring the performance of your server 24x7, you can focus on your business.&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Server Platform<\\\\\\/strong><\\\\\\/span><strong> -&nbsp;<\\\\\\/strong>Windows Server and Windows Server-based infrastructure solutions that provide a solid foundation for all server workload and application requirements, complete with tools for virtualization, web and management in order to integrate, help save time, reduce costs and provide for efficient management of the Datacenter.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Management and Virtualization<\\\\\\/strong><\\\\\\/span><strong> -&nbsp;<\\\\\\/strong>Highly virtualized and well managed Datacenter solutions to provide benefits such as improved infrastructure and facilities utilization with reduced associated costs with agile, flexible and highly available services.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Identity and Access<\\\\\\/strong><\\\\\\/span><strong> -&nbsp;<\\\\\\/strong>Secure, identity-based access to systems. Applications and data both &ldquo;on-premise&rdquo; and &ldquo;in the Cloud&rdquo; from virtually any location or device with the use of a single identity.&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Hosted Infrastructure Services<\\\\\\/strong><\\\\\\/span><strong> -&nbsp;<\\\\\\/strong>We have a dedicated world-class hosting facility in our Bryanston and Pretoria premises from where we are hosting numerous private and commercial platforms. The hosting facilities are managed by our onsite, dedicated personnel and support logging is through our managed helpdesk via a service desk.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Data Centre<\\\\\\/strong><\\\\\\/span><strong> -&nbsp;<\\\\\\/strong>Dynamic Datacenter services and solutions to enable customers to implement and provide robust, scalable and adaptable &ldquo;Infrastructure as a Service&rdquo; services to customers and business, or both. All factors including Facilities, Hardware and Software Infrastructure, Virtualization, Connectivity, Storage, Networks, Security and Management are taken into consideration to provide a standardized, highly automated, agile, cost effective and complete infrastructure as a service solution.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><strong>Public, Private or Hybrid Cloud Services<\\\\\\/strong><\\\\\\/span><\\\\\\/p><ul style=\\\\\\"text-align: justify;\\\\\\"><li>Managed Hosting<\\\\\\/li><li>Application Managed Hosting.<ul><li>Remote Desktop Services.<\\\\\\/li><li>Windows 7\\\\\\/8\\\\\\/10.<\\\\\\/li><li>Office 2013\\\\\\/2016.<\\\\\\/li><li>Sage ERP.<\\\\\\/li><\\\\\\/ul><\\\\\\/li><li>Disaster Recovery Planning and Facilitation.<\\\\\\/li><\\\\\\/ul><p style=\\\\\\"text-align: justify;\\\\\\">Having access to the ECOTECH Converge Hosting facilities provides us with cost effective solutions for IT disaster recovery and business continuity. In partnership with the LCR providers, we develop and maintain a DRP for our clients and through virtualization and consolidation a cost effective alternative will be architected and tested.<\\\\\\/p><p style=\\\\\\"padding-left: 30px; text-align: justify;\\\\\\"><span style=\\\\\\"text-decoration: underline;\\\\\\"><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/public-private-or-hybrid\\\\\\">Read more about Public, Private or Hybrid Cloud options &gt;<\\\\\\/a><\\\\\\/span><\\\\\\/span><\\\\\\/p><p style=\\\\\\"text-align: left;\\\\\\">&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block; text-align: justify;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a><\\\\\\/div><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">&nbsp;<\\\\\\/p>"}}}}'),
(368, 0, 1479807415, 0, 0, 360, '', '', '', '', 0, 1, 0, '', 0, 0, 0, 29, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Platform as a Service (PaaS)<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">A Hosted SharePoint collaboration service platform which delivers a versatile technology that enables business employees to easily create and manage custom team and project-focused sites and portals to share content and collaborate effectively.<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\">Platform as a Service (PaaS) brings the benefits that SaaS brought for applications, but over to the software development world. PaaS can be defined as a computing platform that allows the creation of web applications quickly and easily and without the complexity of buying and maintaining the software and infrastructure underneath it.<\\\\\\/p><p>There are a few different takes on what constitutes PaaS but some basic characteristics include:<\\\\\\/p><ul><li style=\\\\\\"text-align: justify;\\\\\\">Services to develop, test, deploy, host and maintain applications in the same integrated development environment. All the varying services needed to fulfil the application development process.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Web based user interface creation tools help to create, modify, test and deploy different UI scenarios.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Multi-tenant architecture where multiple concurrent users utilize the same development application.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Built in scalability of deployed software including load balancing and failover.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Integration with web services and databases via common standards.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Support for development team collaboration &ndash; some PaaS solutions include project planning and communication tools.<\\\\\\/li><li style=\\\\\\"text-align: justify;\\\\\\">Tools to handle billing and subscription management.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><p style=\\\\\\"text-align: justify;\\\\\\"><strong>PaaS products:&nbsp;<\\\\\\/strong><\\\\\\/p><ul><li style=\\\\\\"text-align: justify;\\\\\\">SharePoint.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><strong><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">CALL US TODAY!<\\\\\\/a><\\\\\\/strong><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(369, 0, 1471515644, 0, 0, 325, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 30, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Consulting<\\\\\\/span><\\\\\\/h3><p>ECOTECH Converge provides Consulting and Professional Services and Solutions that consist of advisory, assessment, strategic, architectural and design planning, as well as implementation and management services to align business and IT.<\\\\\\/p><p><strong>Areas of specialisation:<\\\\\\/strong><strong>&nbsp;<\\\\\\/strong><\\\\\\/p><ul><li>Best practice &amp; solution architectures.<\\\\\\/li><li>Budget &amp; Services roadmap.<\\\\\\/li><li>Research and Development of customers&rsquo; IT roadmap.<\\\\\\/li><li>Microsoft Services.<\\\\\\/li><li>Roadmap and Software Asset Management.<\\\\\\/li><li>Application Services.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Talk to a consultant!&nbsp;<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(371, 0, 1470384126, 0, 0, 325, 'This is us', '', '', '', 0, 0, 0, '', 0, 0, 0, 31, NULL, NULL, NULL, NULL, 'this-is-us', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<p>Exceptional functional and technical expertise, coupled with extensive industry knowledge makes ECOTECH Converge the ideal choice in a consulting firm. Choose us to manage the implementation and operations of your ICT infrastructure requirements.<\\\\\\/p><p><img class=\\\\\\"img-responsive page-image\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=61&amp;width=1080\\\\\\" alt=\\\\\\"\\\\\\" width=\\\\\\"755\\\\\\" height=\\\\\\"702\\\\\\" \\\\\\/><\\\\\\/p><p>ECOTECH Converge provides superior consulting services for IT Infrastructure and services management. We work in the financial, mining, logistics, agriculture, hospitality, telecoms and multiple other industries, serving both the domestic and international sectors. Our headquarters is in Pretoria, South Africa.<\\\\\\/p><p>We pride ourselves on our proven track record, effectively administering multiple corporate ICT infrastructures. We have an acute understanding of each of our client&rsquo;s unique challenges, and are able to provide turnkey solutions for managing people, processes and technology.<\\\\\\/p><p>ECOTECH Converge has experience with large hosting facilities and datacenters. We successfully implement client infrastructures and datacenters across the globe, as well as having created our very own world-class infrastructures to manage our own business.<\\\\\\/p><p>In order to meet the individual needs of clients, ECOTECH Converge maintains a wide range of expertise:<\\\\\\/p><ul><li>We have established numerous strategic partnerships to assist us in delivering the highest quality and range of services.<\\\\\\/li><li>We excel in the Microsoft competency.<\\\\\\/li><li>We back this up with core understanding and delivery of managed hosting, broadband connectivity and cutting edge data solutions.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p>"}}}}'),
(370, 0, 1485939737, 0, 0, 363, '', '', '', '', 0, 0, 0, '', 0, 0, 0, 32, NULL, NULL, NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">Desktop Services<\\\\\\/span><\\\\\\/h3><p>Through the consulting expertise of ECOTECH Converge, we assist our clients in ensuring that the most cost effective strategy is developed for the entire lifecycle of the desktop and mobility devices. This includes services to maintain the software and hardware through its entire lifecycle, and further to ensure proper enforcement and IT governance through usage policies and monitoring capabilities.&nbsp;<\\\\\\/p><p><strong><img class=\\\\\\"img-responsive\\\\\\" style=\\\\\\"display: block; margin-left: auto; margin-right: auto;\\\\\\" src=\\\\\\"[basepath]\\\\\\/cms\\\\\\/modules\\\\\\/media\\\\\\/scripts\\\\\\/image\\\\\\/image.handler.php?media_files_id=128\\\\\\" alt=\\\\\\"\\\\\\" \\\\\\/><br \\\\\\/><\\\\\\/strong><\\\\\\/p><p><strong>Why us?<\\\\\\/strong><\\\\\\/p><p>Our capability with the Desktop Service is further enhanced by the Distribution division within ECOTECH Converge. This division has strategic partnerships with all major OEM brands, ensuring up to date technology offering and buying power. The ECOTECH Converge Distribution division was also appointed as Microsoft South Africa&rsquo;s first Associated Distributor, making it a very strategic partner for Microsoft software lifecycle management.<\\\\\\/p><p><strong>Desktop Service:<\\\\\\/strong><\\\\\\/p><ul><li>Windows 7\\\\\\/8\\\\\\/10.<\\\\\\/li><li>Office 2013\\\\\\/2016\\\\\\/365<\\\\\\/li><li>Anti-Virus<\\\\\\/li><li>Remote Management &amp; Monitoring.<\\\\\\/li><\\\\\\/ul><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">Call us today!<\\\\\\/a><\\\\\\/div><p>&nbsp;<\\\\\\/p>"}}}}'),
(372, 0, 1485951175, 0, 0, 372, 'Voice over IP (VOIP)', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'voice-over-ip-voip', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<h3><span class=\\\\\\"text-yellow\\\\\\" style=\\\\\\"color: #fbbf0f;\\\\\\">VOIP Services<\\\\\\/span><\\\\\\/h3><p style=\\\\\\"text-align: justify;\\\\\\">Voice over IP (VOIP) and Least Cost Routing solutions for streamlined communications over data networks enable businesses to lower costs for phone services, provides users with more features to boost productivity and provides the business with improved collaboration and simplified administration. Our Voice and Telephony consulting and professional services and solutions include &ldquo;In the Cloud&rdquo; and &ldquo;On Premise&rdquo; options.<\\\\\\/p><p>&nbsp;<\\\\\\/p><div class=\\\\\\"call-to-action\\\\\\" style=\\\\\\"background-color: #fbbf0f; color: #ffffff; padding: 10px; text-transform: uppercase; display: inline-block;\\\\\\"><a href=\\\\\\"..\\\\\\/content\\\\\\/contact-us\\\\\\">GET VOIP&nbsp;TODAY!<\\\\\\/a><\\\\\\/div>"}}}}'),
(373, 0, 1486027358, 0, 0, 364, 'Windows Server', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-server', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<div id=\\\\\\"dccn-1aef831d89\\\\\\">&nbsp;<\\\\\\/div><p><script type=\\\\\\"text\\\\\\/javascript\\\\\\">\\\\\\/\\\\\\/ <![CDATA[ var ccs_cc_args = ccs_cc_args || []; (function () { var o = ccs_cc_args; o.push([&#39;_SKey&#39;, &#39;b7c681fa&#39;]); o.push([&#39;_ZoneId&#39;, &#39;1aef831d89&#39;]); var sc = document.createElement(&#39;script&#39;); sc.type = &#39;text\\\\\\/javascript&#39;; sc.async = true; sc.src = (&#39;https:&#39; == document.location.protocol ? &#39;https:\\\\\\/\\\\\\/&#39; : &#39;http:\\\\\\/\\\\\\/&#39;) + &#39;cdn.cnetcontent.com\\\\\\/jsc\\\\\\/h.js&#39;; var n = document.getElementsByTagName(&#39;script&#39;)[0]; n.parentNode.insertBefore(sc, n); })(); \\\\\\/\\\\\\/ ]]><\\\\\\/script><\\\\\\/p>"}}}}'),
(374, 0, 1485952869, 0, 0, 365, 'Windows 10', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-10', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<div id=''dccn-emdedENUdistributorwindows10consumer''><\\/div>\\n<script type=''text\\/javascript''>\\nvar ccs_cc_args = ccs_cc_args || [];\\n(function () {\\n   var o = ccs_cc_args; o.push([''_SKey'', ''b7c681fa'']); o.push([''_ZoneId'', ''emdedENUdistributorwindows10consumer'']); \\n   var sc = document.createElement(''script''); sc.type = ''text\\/javascript''; sc.async = true;\\n   sc.src = (''https:'' == document.location.protocol ? ''https:\\/\\/'' : ''http:\\/\\/'') + ''cdn.cnetcontent.com\\/jsc\\/h.js'';\\n   var n = document.getElementsByTagName(''script'')[0]; n.parentNode.insertBefore(sc, n);\\n})();\\n<\\/script>"}}}}');
INSERT INTO `mod_page` (`page_id`, `media_files_id`, `page_timestamp`, `page_date`, `page_price`, `page_category_id`, `page_heading`, `page_short_description`, `page_description`, `page_link`, `page_link_video`, `page_archived`, `page_quantity`, `page_code`, `page_offset`, `page_featured`, `page_hidden_on_site`, `page_sort`, `page_colour`, `page_anchor`, `page_include`, `page_align`, `page_slug`, `page_author`, `page_special`, `page_in_the_news`, `page_latest_page`, `page_latest_article`, `page_latest_article_1`, `page_latest_article_2`, `page_latest_article_3`, `page_json`) VALUES
(375, 0, 1485952891, 0, 0, 367, 'Office 365', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'office-365', '', 0, 0, 0, 0, 0, 0, 0, '{"content":{"row-0":{"col1":{"type":"text","data":"<div id=''dccn-6e534c5d6f''><\\/div>\\n<script type=''text\\/javascript''>\\nvar ccs_cc_args = ccs_cc_args || [];\\n(function () {\\n   var o = ccs_cc_args; o.push([''_SKey'', ''b7c681fa'']); o.push([''_ZoneId'', ''6e534c5d6f'']); \\n   var sc = document.createElement(''script''); sc.type = ''text\\/javascript''; sc.async = true;\\n   sc.src = (''https:'' == document.location.protocol ? ''https:\\/\\/'' : ''http:\\/\\/'') + ''cdn.cnetcontent.com\\/jsc\\/h.js'';\\n   var n = document.getElementsByTagName(''script'')[0]; n.parentNode.insertBefore(sc, n);\\n})();\\n<\\/script>"}}}}'),
(376, 0, 1485952903, 0, 0, 368, 'Azure', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'azure', '', 0, 0, 0, 0, 0, 0, 0, '""'),
(377, 0, 1485952915, 0, 0, 369, 'MS SQL', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'ms-sql', '', 0, 0, 0, 0, 0, 0, 0, '""'),
(378, 0, 1485952924, 0, 0, 370, 'Hyper-V', '', '', '', 0, 0, 0, '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 'hyperv', '', 0, 0, 0, 0, 0, 0, 0, '""');

-- --------------------------------------------------------

--
-- Table structure for table `mod_page_category`
--

CREATE TABLE IF NOT EXISTS `mod_page_category` (
  `page_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_category_timestamp` int(11) NOT NULL,
  `page_category_date` int(11) NOT NULL,
  `page_category_price` double DEFAULT NULL,
  `page_category_category_id` int(11) DEFAULT NULL,
  `page_category_heading` varchar(400) NOT NULL,
  `page_category_short_description` text NOT NULL,
  `page_category_description` text NOT NULL,
  `page_category_link` varchar(400) NOT NULL,
  `page_category_link_video` int(1) NOT NULL,
  `page_category_archived` int(11) NOT NULL DEFAULT '0',
  `page_category_quantity` int(11) DEFAULT NULL,
  `page_category_code` varchar(100) DEFAULT NULL,
  `page_category_offset` int(11) DEFAULT NULL,
  `page_category_featured` int(11) DEFAULT '0',
  `page_category_hidden_on_site` int(11) DEFAULT '0',
  `page_category_sort` int(99) DEFAULT NULL,
  `page_category_colour` varchar(200) DEFAULT NULL,
  `page_category_anchor` varchar(400) DEFAULT NULL,
  `page_category_include` text,
  `page_category_align` varchar(100) DEFAULT NULL,
  `page_category_slug` varchar(400) DEFAULT NULL,
  `page_category_author` varchar(200) DEFAULT NULL,
  `page_category_special` int(11) DEFAULT '0',
  `page_category_in_the_news` int(11) DEFAULT '0',
  `page_category_latest_page` int(11) DEFAULT '0',
  `page_category_latest_article` int(11) DEFAULT '0',
  `page_category_latest_article_1` int(11) DEFAULT '0',
  `page_category_latest_article_2` int(11) DEFAULT '0',
  `page_category_latest_article_3` int(11) DEFAULT '0',
  `page_category_json` text,
  PRIMARY KEY (`page_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=373 ;

--
-- Dumping data for table `mod_page_category`
--

INSERT INTO `mod_page_category` (`page_category_id`, `media_files_id`, `page_category_timestamp`, `page_category_date`, `page_category_price`, `page_category_category_id`, `page_category_heading`, `page_category_short_description`, `page_category_description`, `page_category_link`, `page_category_link_video`, `page_category_archived`, `page_category_quantity`, `page_category_code`, `page_category_offset`, `page_category_featured`, `page_category_hidden_on_site`, `page_category_sort`, `page_category_colour`, `page_category_anchor`, `page_category_include`, `page_category_align`, `page_category_slug`, `page_category_author`, `page_category_special`, `page_category_in_the_news`, `page_category_latest_page`, `page_category_latest_article`, `page_category_latest_article_1`, `page_category_latest_article_2`, `page_category_latest_article_3`, `page_category_json`) VALUES
(317, 0, 1467901597, 0, NULL, 316, 'Operations & History', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 34, NULL, NULL, NULL, NULL, 'operations--history', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(316, 0, 1467190005, 0, NULL, 0, 'This is us', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 33, NULL, NULL, NULL, NULL, 'this-is-us', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(318, 0, 1467901709, 0, NULL, 316, 'Management', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 35, NULL, NULL, NULL, NULL, 'management', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(319, 0, 1467190641, 0, NULL, 316, 'Certifications & Awards', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 40, NULL, NULL, NULL, NULL, 'certifications--awards', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(320, 0, 1467190650, 0, NULL, 316, 'Partners', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 41, NULL, NULL, NULL, NULL, 'partners', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(321, 0, 1467190663, 0, NULL, 318, 'Simon Martyn', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 36, NULL, NULL, NULL, NULL, 'simon-martyn', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(322, 0, 1467190698, 0, NULL, 318, 'Zac Hooper', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 37, NULL, NULL, NULL, NULL, 'zac-hooper', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(323, 0, 1467190708, 0, NULL, 318, 'Alex Pohl', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 38, NULL, NULL, NULL, NULL, 'alex-pohl', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(324, 0, 1467190718, 0, NULL, 318, 'Thys Smith', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 39, NULL, NULL, NULL, NULL, 'thys-smith', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(325, 0, 1467190755, 0, NULL, 0, 'Home', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 'home', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(326, 0, 1467190766, 0, NULL, 0, 'Services', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 3, NULL, NULL, NULL, NULL, 'services', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(327, 0, 1467700140, 0, NULL, 326, 'Cloud', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 13, NULL, NULL, NULL, NULL, 'cloud', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(328, 0, 1467788750, 0, NULL, 327, 'Quick description', '<h1>Heading</h1>\r\n<p>content</p>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 14, NULL, NULL, NULL, NULL, 'quick-description', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(329, 0, 1467191248, 0, NULL, 328, '2nd level info - How it changes how you currently do things', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 15, NULL, NULL, NULL, NULL, '2nd-level-info--how-it-changes-how-you-currently-do-things', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(330, 0, 1467191262, 0, NULL, 328, '3rd level technical info', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 16, NULL, NULL, NULL, NULL, '3rd-level-technical-info', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(331, 0, 1467191284, 0, NULL, 327, 'Get this now', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 17, NULL, NULL, NULL, NULL, 'get-this-now', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(332, 0, 1467293653, 0, NULL, 326, 'Microsoft', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 18, NULL, NULL, NULL, NULL, 'microsoft', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(333, 38, 1485936341, 0, NULL, 326, 'Cloud', '<h4>The ECOTECH Cloud Platform and Hosting facilities meet the requirement to provide high levels of uptime to our clients, this is delivered through ECOTECH investments in secure, resilient and fault tolerant technology, coupled with full proactive management and monitoring, underpinned by highly skilled and experienced staff assure our clients cost effective and reliable ICT services.</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 5, NULL, NULL, NULL, NULL, 'cloud', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(334, 46, 1467926719, 0, NULL, 326, 'Voice & Telephony', '<h4>Voice Over IP (VOIP) and least cost routing options that have been designed to work simultaneously &ldquo;on premise&rdquo; as well as &ldquo;in the cloud&rdquo; as online services.</h4>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 28, NULL, NULL, NULL, NULL, 'voice--telephony', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(335, 45, 1467926732, 0, NULL, 326, 'Service Desk', '<h4>A dedicated service desk functions as ECOTECH central services center. It is also our single point of contact center (SPoC). From here we provide the full managed-services package, remote management and monitoring services to all our clients. We offer ad hoc, as well as permanent on-site based technicians and SLA support.</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 29, NULL, NULL, NULL, NULL, 'service-desk', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(336, 0, 1467803762, 0, NULL, 326, 'Service Desk', '<h4>A dedicated service desk functions as ECOTECH central services center. It is also our single point of contact center (SPoC). From here we provide the full managed-services package, remote management and monitoring services to all our clients. We offer ad hoc, as well as permanent on-site based technicians and SLA support.</h4>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 30, NULL, NULL, NULL, NULL, 'service-desk', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(337, 40, 1470313494, 0, NULL, 355, 'Consulting', '<h4>Our goal is to exceed the expectations of every client by offering outstanding customer service, increased flexibility, and greater value, thus optimizing solution functionality, and improving operational efficiency.</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 9, NULL, NULL, NULL, NULL, 'consulting', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(338, 42, 1485939018, 0, NULL, 326, 'Microsoft', '<h4>As a Microsoft certified partner we carry a number of Microsoft Gold competencies. Our Microsoft Windows Azure Pack Cloud hosting platform provides resilient and reliable utility-based computing services that have been developed with our clients&rsquo; explicit needs for reliability and security in mind.</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 6, NULL, NULL, NULL, NULL, 'microsoft', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(339, 41, 1467926767, 0, NULL, 326, 'Infrastructure', '<h4>ECOCORP Cloud Services addresses these requirements by providing &lsquo;anywhere access&rsquo; to your employees&rsquo; basic applications. ECOTECH &lsquo;Infrastructure as a Service&rsquo; allows companies to free up resources and allow IT to focus on more strategic objectives&hellip;</h4>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 31, NULL, NULL, NULL, NULL, 'infrastructure', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(340, 39, 1470312163, 0, NULL, 355, 'Connectivity', '<h4>ECOTECH Converge supply various turnkey connectivity solutions including, but not limited to, managed connectivity, broadband, V-Sat and internet service provider services...</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 8, NULL, NULL, NULL, NULL, 'connectivity', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(341, 37, 1470819556, 0, NULL, 326, 'Sales & Distribution', '<h4>ECOTECH Converge offers a wide range of information technology hardware and peripheral devices catering for most IT requirements...</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 32, NULL, NULL, NULL, NULL, 'sales--distribution', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(342, 0, 1467873710, 0, NULL, 325, 'Why us', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 2, NULL, NULL, NULL, NULL, 'why-us', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(343, 0, 1467873904, 0, NULL, 0, 'Why us', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 42, NULL, NULL, NULL, NULL, 'why-us', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(344, 0, 1467873982, 0, NULL, 343, 'Scalibility', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 45, NULL, NULL, NULL, NULL, 'scalibility', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(345, 0, 1467874011, 0, NULL, 343, 'Migration solutions', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 43, NULL, NULL, NULL, NULL, 'migration-solutions', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(346, 0, 1467874057, 0, NULL, 343, 'Service Level Agreement', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 44, NULL, NULL, NULL, NULL, 'service-level-agreement', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(347, 0, 1485941812, 0, NULL, 343, 'Sector & Industry list', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 46, NULL, NULL, NULL, NULL, 'sector--industry-list', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(348, 0, 1467902451, 0, NULL, 0, 'Contact Us', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 47, NULL, NULL, NULL, NULL, 'contact-us', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(349, 0, 1467958810, 0, NULL, 338, 'MS SQL', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 24, NULL, NULL, NULL, NULL, 'ms-sql', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(350, 0, 1467959635, 0, NULL, 338, 'Azure', '<p>Any developer or IT professional can be productive with Azure. The integrated tools, pre-built templates and managed services make it easier to build and manage enterprise, mobile, Web and Internet of Things (IoT) apps faster, using skills you already have and technologies you already know. Microsoft is also the only vendor positioned as a Leader across Gartner&rsquo;s Magic Quadrants for Cloud Infrastructure as a Service, Application Platform as a Service, and Cloud Storage Services for the second consecutive year.</p>\r\n<p><strong>Use an open and flexible cloud service platform</strong></p>\r\n<p>Azure supports the broadest selection of operating systems, programming languages, frameworks, tools, databases and devices. Run Linux containers with Docker integration; build apps with JavaScript, Python, .NET, PHP, Java and Node.js; build back-ends for iOS, Android and Windows devices. Azure cloud service supports the same technologies that millions of developers and IT professionals already rely on and trust.</p>\r\n<p><strong>Extend your existing IT</strong></p>\r\n<p>Some cloud providers make you choose between your data centre and the cloud. Not Azure, which easily integrates with your existing IT environment through the largest network of secure private connections, hybrid database and storage solutions, and data residency and encryption features &ndash; so your assets stay right where you need them. And with&nbsp;Azure Stack, you can bring the Azure model of application development and deployment to your data centre. Azure hybrid cloud solutions give you the best of both worlds: more IT options, less complexity and cost. That&rsquo;s why it&rsquo;s one of the best&nbsp;cloud computing services&nbsp;available.</p>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 26, NULL, NULL, NULL, NULL, 'azure', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(351, 0, 1467959646, 0, NULL, 338, 'Azure', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 27, NULL, NULL, NULL, NULL, 'azure', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(352, 0, 1467974920, 0, NULL, 338, 'Azure', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 25, NULL, NULL, NULL, NULL, 'azure', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(353, 0, 1470042376, 0, NULL, 0, 'Hosting', '<p>Hosting services... Find out more....</p>', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 'hosting', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(354, 134, 1485939004, 0, NULL, 326, 'Hosting', '<h4>We have a dedicated world-class hosting facility in our Bryanston, Isando and Pretoria premises from where we are hosting numerous private and commercial platforms. The hosting facilities are managed by our onsite, dedicated personnel and support logging is via our managed Service Desk.</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 4, NULL, NULL, NULL, NULL, 'hosting', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(356, 0, 1470130849, 0, NULL, 355, 'Connectivity Solutions', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 10, NULL, NULL, NULL, NULL, 'connectivity-solutions', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(355, 40, 1470315321, 0, NULL, 326, 'Managed Services', '<h4>ECOTECH Converge supplies various turnkey management solutions including, but not limited to Desktop Services, Connectivity Solutions &amp; Consulting.&nbsp;</h4>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 7, NULL, NULL, NULL, NULL, 'managed-services', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(357, 0, 1470131739, 0, NULL, 333, 'Public, Private or Hybrid', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 19, NULL, NULL, NULL, NULL, 'public-private-or-hybrid', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(358, 0, 1479803466, 0, NULL, 333, 'IaaS', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 20, NULL, NULL, NULL, NULL, 'iaas', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(359, 0, 1479803510, 0, NULL, 333, 'SaaS', '<ul>\r\n<li>Hosted Exchange.</li>\r\n<li>O365.</li>\r\n<li>Skype for Business.</li>\r\n</ul>', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 21, NULL, NULL, NULL, NULL, 'saas', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(360, 0, 1479803552, 0, NULL, 333, 'PaaS', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 22, NULL, NULL, NULL, NULL, 'paas', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(361, 0, 1479803568, 0, NULL, 333, 'DBaaS', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 23, NULL, NULL, NULL, NULL, 'dbaas', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(362, 0, 1470305091, 0, NULL, 355, 'Consulting', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, 11, NULL, NULL, NULL, NULL, 'consulting', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(363, 0, 1470305260, 0, NULL, 355, 'Desktop Services', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, 12, NULL, NULL, NULL, NULL, 'desktop-services', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(372, 0, 1485940266, 0, NULL, 355, 'Voice over IP (VOIP) ', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'voice-over-ip--voip-', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(364, 0, 1485952831, 0, NULL, 338, 'Windows Server', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-server', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(365, 0, 1485936817, 0, NULL, 338, 'Windows 10', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-10', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(366, 0, 1485936850, 0, NULL, 338, 'Windows 10', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'windows-10', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(367, 0, 1485936869, 0, NULL, 338, 'Office 365', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'office-365', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(368, 0, 1485936902, 0, NULL, 338, 'Azure', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'azure', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(369, 0, 1485936930, 0, NULL, 338, 'MS SQL', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'ms-sql', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(370, 0, 1485936946, 0, NULL, 338, 'Hyper-V', '', '', '', 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'hyperv', NULL, 0, 0, 0, 0, 0, 0, 0, NULL),
(371, 0, 1485937921, 0, NULL, 338, 'Hyper-V', '', '', '', 0, 1, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'hyperv', NULL, 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mod_page_document_link`
--

CREATE TABLE IF NOT EXISTS `mod_page_document_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_page_image_link`
--

CREATE TABLE IF NOT EXISTS `mod_page_image_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_service`
--

CREATE TABLE IF NOT EXISTS `mod_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_timestamp` int(11) NOT NULL,
  `service_date` int(11) NOT NULL,
  `service_price` double DEFAULT NULL,
  `service_category_id` int(11) DEFAULT NULL,
  `service_heading` varchar(400) NOT NULL,
  `service_short_description` text NOT NULL,
  `service_description` text NOT NULL,
  `service_link` varchar(400) NOT NULL,
  `service_link_video` int(1) NOT NULL,
  `service_archived` int(11) NOT NULL DEFAULT '0',
  `service_quantity` int(11) DEFAULT NULL,
  `service_code` varchar(100) DEFAULT NULL,
  `service_offset` int(11) DEFAULT NULL,
  `service_featured` int(11) DEFAULT '0',
  `service_hidden_on_site` int(11) DEFAULT '0',
  `service_sort` int(99) DEFAULT NULL,
  `service_colour` varchar(200) DEFAULT NULL,
  `service_anchor` varchar(400) DEFAULT NULL,
  `service_include` text,
  `service_align` varchar(100) DEFAULT NULL,
  `service_slug` varchar(400) DEFAULT NULL,
  `service_author` varchar(200) DEFAULT NULL,
  `service_special` int(11) DEFAULT '0',
  `service_in_the_news` int(11) DEFAULT '0',
  `service_latest_service` int(11) DEFAULT '0',
  `service_latest_article` int(11) DEFAULT '0',
  `service_latest_article_1` int(11) DEFAULT '0',
  `service_latest_article_2` int(11) DEFAULT '0',
  `service_latest_article_3` int(11) DEFAULT '0',
  `service_json` text,
  PRIMARY KEY (`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_service_category`
--

CREATE TABLE IF NOT EXISTS `mod_service_category` (
  `service_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_category_timestamp` int(11) NOT NULL,
  `service_category_date` int(11) NOT NULL,
  `service_category_price` double DEFAULT NULL,
  `service_category_category_id` int(11) DEFAULT NULL,
  `service_category_heading` varchar(400) NOT NULL,
  `service_category_short_description` text NOT NULL,
  `service_category_description` text NOT NULL,
  `service_category_link` varchar(400) NOT NULL,
  `service_category_link_video` int(1) NOT NULL,
  `service_category_archived` int(11) NOT NULL DEFAULT '0',
  `service_category_quantity` int(11) DEFAULT NULL,
  `service_category_code` varchar(100) DEFAULT NULL,
  `service_category_offset` int(11) DEFAULT NULL,
  `service_category_featured` int(11) DEFAULT '0',
  `service_category_hidden_on_site` int(11) DEFAULT '0',
  `service_category_sort` int(99) DEFAULT NULL,
  `service_category_colour` varchar(200) DEFAULT NULL,
  `service_category_anchor` varchar(400) DEFAULT NULL,
  `service_category_include` text,
  `service_category_align` varchar(100) DEFAULT NULL,
  `service_category_slug` varchar(400) DEFAULT NULL,
  `service_category_author` varchar(200) DEFAULT NULL,
  `service_category_special` int(11) DEFAULT '0',
  `service_category_in_the_news` int(11) DEFAULT '0',
  `service_category_latest_page` int(11) DEFAULT '0',
  `service_category_latest_article` int(11) DEFAULT '0',
  `service_category_latest_article_1` int(11) DEFAULT '0',
  `service_category_latest_article_2` int(11) DEFAULT '0',
  `service_category_latest_article_3` int(11) DEFAULT '0',
  `service_category_json` text,
  PRIMARY KEY (`service_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_service_document_link`
--

CREATE TABLE IF NOT EXISTS `mod_service_document_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_service_image_link`
--

CREATE TABLE IF NOT EXISTS `mod_service_image_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
