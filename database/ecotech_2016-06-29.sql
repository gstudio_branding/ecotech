# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.20)
# Database: ecotech
# Generation Time: 2016-06-29 14:54:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table access_level_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_admin`;

CREATE TABLE `access_level_admin` (
  `admin_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `admin_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`admin_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_admin` WRITE;
/*!40000 ALTER TABLE `access_level_admin` DISABLE KEYS */;

INSERT INTO `access_level_admin` (`admin_acc_id`, `admin_acc_level`, `admin_acc_admin_id`)
VALUES
	(9,'1',11);

/*!40000 ALTER TABLE `access_level_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_media`;

CREATE TABLE `access_level_media` (
  `media_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `media_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`media_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_media` WRITE;
/*!40000 ALTER TABLE `access_level_media` DISABLE KEYS */;

INSERT INTO `access_level_media` (`media_acc_id`, `media_acc_level`, `media_acc_admin_id`)
VALUES
	(7,'1',11);

/*!40000 ALTER TABLE `access_level_media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_page`;

CREATE TABLE `access_level_page` (
  `page_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `page_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`page_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_page` WRITE;
/*!40000 ALTER TABLE `access_level_page` DISABLE KEYS */;

INSERT INTO `access_level_page` (`page_acc_id`, `page_acc_level`, `page_acc_admin_id`)
VALUES
	(20,'1',11);

/*!40000 ALTER TABLE `access_level_page` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table access_level_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `access_level_service`;

CREATE TABLE `access_level_service` (
  `service_acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_acc_level` varchar(11) DEFAULT NULL COMMENT 'r=read; w=write; m=modify; f=full',
  `service_acc_admin_id` int(11) DEFAULT NULL COMMENT 'admin ID number',
  PRIMARY KEY (`service_acc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `access_level_service` WRITE;
/*!40000 ALTER TABLE `access_level_service` DISABLE KEYS */;

INSERT INTO `access_level_service` (`service_acc_id`, `service_acc_level`, `service_acc_admin_id`)
VALUES
	(20,'1',11);

/*!40000 ALTER TABLE `access_level_service` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_admin`;

CREATE TABLE `cms_admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(200) DEFAULT NULL COMMENT 'name of the user',
  `admin_surname` varchar(200) DEFAULT NULL COMMENT 'surname of the user',
  `admin_dob` int(11) DEFAULT NULL,
  `admin_gender` int(11) DEFAULT NULL COMMENT '1:male; 0:female',
  `admin_email` varchar(200) DEFAULT NULL COMMENT 'email address of the user',
  `admin_cell_private` varchar(12) DEFAULT NULL,
  `admin_cell_work` varchar(12) DEFAULT NULL,
  `admin_tel_private` varchar(12) DEFAULT NULL,
  `admin_tel_work` varchar(12) DEFAULT NULL,
  `admin_fax` varchar(12) DEFAULT NULL,
  `admin_address` varchar(200) DEFAULT NULL,
  `admin_password` varchar(200) DEFAULT NULL COMMENT 'password for the user',
  `admin_added_by_id` int(11) DEFAULT NULL COMMENT 'the id of the user that added the user',
  `admin_archived` int(11) DEFAULT '0',
  `admin_picture` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `cms_admin` WRITE;
/*!40000 ALTER TABLE `cms_admin` DISABLE KEYS */;

INSERT INTO `cms_admin` (`admin_id`, `admin_name`, `admin_surname`, `admin_dob`, `admin_gender`, `admin_email`, `admin_cell_private`, `admin_cell_work`, `admin_tel_private`, `admin_tel_work`, `admin_fax`, `admin_address`, `admin_password`, `admin_added_by_id`, `admin_archived`, `admin_picture`)
VALUES
	(11,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=','Qg42ym9OyRJVDrJyru5wx+SUgyiAUNtXRRnvYTJue6o=',NULL,NULL,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=',NULL,NULL,NULL,NULL,NULL,NULL,'LOxkVdsrWWklA4AuBBEgzU77mbfvOTbrJWveslbqRP0=',NULL,0,NULL);

/*!40000 ALTER TABLE `cms_admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_media_dimensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_dimensions`;

CREATE TABLE `cms_media_dimensions` (
  `media_dimensions_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `media_dimensions_x` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_x2` int(11) DEFAULT NULL COMMENT 'crop x pos',
  `media_dimensions_y2` int(11) DEFAULT NULL COMMENT 'crop y pos',
  `media_dimensions_aspect_ratio` varchar(11) DEFAULT NULL COMMENT 'aspect ratio eg 1x1',
  `media_dimensions_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_dimensions_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table cms_media_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_files`;

CREATE TABLE `cms_media_files` (
  `media_files_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `media_files_filename` varchar(400) DEFAULT NULL,
  `media_files_type` int(11) DEFAULT NULL,
  `media_files_title` varchar(200) DEFAULT NULL,
  `media_files_description` text,
  `media_files_timestamp` int(11) DEFAULT NULL,
  `media_files_original_name` text NOT NULL,
  PRIMARY KEY (`media_files_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table cms_media_folder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_folder`;

CREATE TABLE `cms_media_folder` (
  `media_folder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_name` varchar(200) DEFAULT NULL,
  `media_folder_link` int(11) DEFAULT '0',
  PRIMARY KEY (`media_folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table cms_media_folder_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_media_folder_link`;

CREATE TABLE `cms_media_folder_link` (
  `media_folder_link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media_folder_id` int(11) DEFAULT NULL,
  `media_files_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_folder_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page`;

CREATE TABLE `mod_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_timestamp` int(11) NOT NULL,
  `page_date` int(11) NOT NULL,
  `page_price` double DEFAULT NULL,
  `page_category_id` int(11) DEFAULT NULL,
  `page_heading` varchar(400) NOT NULL,
  `page_short_description` text NOT NULL,
  `page_description` text NOT NULL,
  `page_link` varchar(400) NOT NULL,
  `page_link_video` int(1) NOT NULL,
  `page_archived` int(11) NOT NULL DEFAULT '0',
  `page_quantity` int(11) DEFAULT NULL,
  `page_code` varchar(100) DEFAULT NULL,
  `page_offset` int(11) DEFAULT NULL,
  `page_featured` int(11) DEFAULT '0',
  `page_hidden_on_site` int(11) DEFAULT '0',
  `page_sort` int(99) DEFAULT NULL,
  `page_colour` varchar(200) DEFAULT NULL,
  `page_anchor` varchar(400) DEFAULT NULL,
  `page_include` text,
  `page_align` varchar(100) DEFAULT NULL,
  `page_slug` varchar(400) DEFAULT NULL,
  `page_author` varchar(200) DEFAULT NULL,
  `page_special` int(11) DEFAULT '0',
  `page_in_the_news` int(11) DEFAULT '0',
  `page_latest_page` int(11) DEFAULT '0',
  `page_latest_article` int(11) DEFAULT '0',
  `page_latest_article_1` int(11) DEFAULT '0',
  `page_latest_article_2` int(11) DEFAULT '0',
  `page_latest_article_3` int(11) DEFAULT '0',
  `page_json` text,
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `mod_page` WRITE;
/*!40000 ALTER TABLE `mod_page` DISABLE KEYS */;

INSERT INTO `mod_page` (`page_id`, `media_files_id`, `page_timestamp`, `page_date`, `page_price`, `page_category_id`, `page_heading`, `page_short_description`, `page_description`, `page_link`, `page_link_video`, `page_archived`, `page_quantity`, `page_code`, `page_offset`, `page_featured`, `page_hidden_on_site`, `page_sort`, `page_colour`, `page_anchor`, `page_include`, `page_align`, `page_slug`, `page_author`, `page_special`, `page_in_the_news`, `page_latest_page`, `page_latest_article`, `page_latest_article_1`, `page_latest_article_2`, `page_latest_article_3`, `page_json`)
VALUES
	(314,0,1467195986,0,0,324,'Management Bios','','','',0,0,0,'',0,0,0,NULL,NULL,NULL,NULL,NULL,'management-bios','',0,0,0,0,0,0,0,'{\"content\":{\"row-0\":{\"col1\":{\"type\":\"text\",\"data\":\"<p>Thys Smith Business Development Manager <br \\\\\\/>Highly experienced in Managing Operations, Solution Design and Solution <br \\\\\\/>Sales of Corporate &amp; Enterprise Solutions<\\\\\\/p>\"}},\"row-1\":{\"col1\":{\"type\":\"text\",\"data\":\"<p><strong>Background <\\\\\\/strong><\\\\\\/p><p>ipicius apernat emperferitae eum eic tem aut lab iuscips undunt ad quae ventendi doloresequae num et quiae ex elitiumquis simolup tianihit omnist eaquam quae consequia cum restrum et lam que ea consediciis quate num fuga. Itatquatecum fuga. Nes dolupta turistis dolor mo il in et excersp ersperit eum quidelest, sum vita pro magnimaxim quid qui dundae odisquis aut as ad quis rero tem et apedis mo moles ant pa isto eum que vendi consequam, conse simi, comnihi tatures errore nam int plaborro blam qui dolor sitatae nis eossim quibusd antu.<\\\\\\/p><p><strong>Functional Experience &amp; Industry Experience <\\\\\\/strong><\\\\\\/p><p>Rem quae derferate natus rem auta ditatur. Quis mos pa eos inistrum cupta cuptatur atemoss imaxima gnihici molorrore nem labores reprae nonsequame et dolorrovides estoren ditiur, quuntiis reperferate maximi, optur, ipsandi sit eum aut volum inienisse nonesequi to bea dolorit atecupta pro molupta seruptatiam net apit, audam res serum aut aborpos ium fugit et ra sae velenis vid eum earum es net laboratiissi te non earumet lacepro moditaestet volupiciis re, conet enihicia essequam, ut poris magnim resed molut et poreicturem ex eiur, soloresti odio. Et volupiste sequi inctate num fugias re am, volestium re volenecusam, cullupta dolesti volut faccus quae escidus autae rerum reptate vellecto quam quatet harisquis dollabo. Ita parum volenim rehentibus, quia aut veliquo et, alicitat.<\\\\\\/p><p><strong>Highest Qualification &amp; Industry <\\\\\\/strong><\\\\\\/p><p>Accreditation Rem quae derferate natus rem auta ditatur. Quis mos pa eos inistrum cupta cuptatur atemoss imaxima gnihici molorrore nem labores reprae nonsequame et dolorrovides estoren ditiur, quuntiis reperferate maximi, optur, ipsandi sit eum aut volum inienisse nonesequi to bea dolorit atecupta pro molupta seruptatiam net apit, audam res serum aut aborpos ium fugit et ra sae velenis vid eum earum es net laboratiissi te non earumet lacepro moditaestet volupiciis re, conet enihicia essequam, ut poris magnim resed molut et poreicturem ex eiur.<\\\\\\/p><p><strong>Project Experience <\\\\\\/strong><\\\\\\/p><p>ipicius apernat emperferitae eum eic tem aut lab iuscips undunt ad quae ventendi doloresequae num et quiae ex elitiumquis simolup tianihit omnist eaquam quae consequia cum restrum et lam que ea consediciis quate num fuga. Itatquatecum fuga. Nes dolupta turistis dolor mo il in et excersp ersperit eum quidelest, sum vita pro magnimaxim quid qui dundae odisquis aut as ad quis rero<\\\\\\/p>\"}}}}');

/*!40000 ALTER TABLE `mod_page` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mod_page_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_category`;

CREATE TABLE `mod_page_category` (
  `page_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `page_category_timestamp` int(11) NOT NULL,
  `page_category_date` int(11) NOT NULL,
  `page_category_price` double DEFAULT NULL,
  `page_category_category_id` int(11) DEFAULT NULL,
  `page_category_heading` varchar(400) NOT NULL,
  `page_category_short_description` text NOT NULL,
  `page_category_description` text NOT NULL,
  `page_category_link` varchar(400) NOT NULL,
  `page_category_link_video` int(1) NOT NULL,
  `page_category_archived` int(11) NOT NULL DEFAULT '0',
  `page_category_quantity` int(11) DEFAULT NULL,
  `page_category_code` varchar(100) DEFAULT NULL,
  `page_category_offset` int(11) DEFAULT NULL,
  `page_category_featured` int(11) DEFAULT '0',
  `page_category_hidden_on_site` int(11) DEFAULT '0',
  `page_category_sort` int(99) DEFAULT NULL,
  `page_category_colour` varchar(200) DEFAULT NULL,
  `page_category_anchor` varchar(400) DEFAULT NULL,
  `page_category_include` text,
  `page_category_align` varchar(100) DEFAULT NULL,
  `page_category_slug` varchar(400) DEFAULT NULL,
  `page_category_author` varchar(200) DEFAULT NULL,
  `page_category_special` int(11) DEFAULT '0',
  `page_category_in_the_news` int(11) DEFAULT '0',
  `page_category_latest_page` int(11) DEFAULT '0',
  `page_category_latest_article` int(11) DEFAULT '0',
  `page_category_latest_article_1` int(11) DEFAULT '0',
  `page_category_latest_article_2` int(11) DEFAULT '0',
  `page_category_latest_article_3` int(11) DEFAULT '0',
  `page_category_json` text,
  PRIMARY KEY (`page_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `mod_page_category` WRITE;
/*!40000 ALTER TABLE `mod_page_category` DISABLE KEYS */;

INSERT INTO `mod_page_category` (`page_category_id`, `media_files_id`, `page_category_timestamp`, `page_category_date`, `page_category_price`, `page_category_category_id`, `page_category_heading`, `page_category_short_description`, `page_category_description`, `page_category_link`, `page_category_link_video`, `page_category_archived`, `page_category_quantity`, `page_category_code`, `page_category_offset`, `page_category_featured`, `page_category_hidden_on_site`, `page_category_sort`, `page_category_colour`, `page_category_anchor`, `page_category_include`, `page_category_align`, `page_category_slug`, `page_category_author`, `page_category_special`, `page_category_in_the_news`, `page_category_latest_page`, `page_category_latest_article`, `page_category_latest_article_1`, `page_category_latest_article_2`, `page_category_latest_article_3`, `page_category_json`)
VALUES
	(317,0,1467190559,0,NULL,316,'History and Operations','','','',0,0,NULL,NULL,NULL,0,0,3,NULL,NULL,NULL,NULL,'history-and-operations',NULL,0,0,0,0,0,0,0,NULL),
	(316,0,1467190005,0,NULL,0,'This is us','','','',0,0,NULL,NULL,NULL,0,0,2,NULL,NULL,NULL,NULL,'this-is-us',NULL,0,0,0,0,0,0,0,NULL),
	(318,0,1467190619,0,NULL,316,'Management Bios','','','',0,0,NULL,NULL,NULL,0,0,4,NULL,NULL,NULL,NULL,'management-bios',NULL,0,0,0,0,0,0,0,NULL),
	(319,0,1467190641,0,NULL,316,'Certifications & Awards','','','',0,0,NULL,NULL,NULL,0,0,9,NULL,NULL,NULL,NULL,'certifications--awards',NULL,0,0,0,0,0,0,0,NULL),
	(320,0,1467190650,0,NULL,316,'Partners','','','',0,0,NULL,NULL,NULL,0,0,10,NULL,NULL,NULL,NULL,'partners',NULL,0,0,0,0,0,0,0,NULL),
	(321,0,1467190663,0,NULL,318,'Simon Martyn','','','',0,0,NULL,NULL,NULL,0,0,5,NULL,NULL,NULL,NULL,'simon-martyn',NULL,0,0,0,0,0,0,0,NULL),
	(322,0,1467190698,0,NULL,318,'Zac Hooper','','','',0,0,NULL,NULL,NULL,0,0,6,NULL,NULL,NULL,NULL,'zac-hooper',NULL,0,0,0,0,0,0,0,NULL),
	(323,0,1467190708,0,NULL,318,'Alex Pohl','','','',0,0,NULL,NULL,NULL,0,0,7,NULL,NULL,NULL,NULL,'alex-pohl',NULL,0,0,0,0,0,0,0,NULL),
	(324,0,1467190718,0,NULL,318,'Thys Smith','','','',0,0,NULL,NULL,NULL,0,0,8,NULL,NULL,NULL,NULL,'thys-smith',NULL,0,0,0,0,0,0,0,NULL),
	(325,0,1467190755,0,NULL,0,'Home','','','',0,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,'home',NULL,0,0,0,0,0,0,0,NULL),
	(326,0,1467190766,0,NULL,0,'Services','','','',0,0,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,'services',NULL,0,0,0,0,0,0,0,NULL),
	(327,0,1467191174,0,NULL,326,'Cloud','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'cloud',NULL,0,0,0,0,0,0,0,NULL),
	(328,0,1467191186,0,NULL,327,'Quick description','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'quick-description',NULL,0,0,0,0,0,0,0,NULL),
	(329,0,1467191248,0,NULL,328,'2nd level info - How it changes how you currently do things','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'2nd-level-info--how-it-changes-how-you-currently-do-things',NULL,0,0,0,0,0,0,0,NULL),
	(330,0,1467191262,0,NULL,328,'3rd level technical info','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'3rd-level-technical-info',NULL,0,0,0,0,0,0,0,NULL),
	(331,0,1467191284,0,NULL,327,'Get this now','','','',0,0,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,'get-this-now',NULL,0,0,0,0,0,0,0,NULL);

/*!40000 ALTER TABLE `mod_page_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mod_page_document_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_document_link`;

CREATE TABLE `mod_page_document_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_page_image_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_page_image_link`;

CREATE TABLE `mod_page_image_link` (
  `page_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `page_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`page_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service`;

CREATE TABLE `mod_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_timestamp` int(11) NOT NULL,
  `service_date` int(11) NOT NULL,
  `service_price` double DEFAULT NULL,
  `service_category_id` int(11) DEFAULT NULL,
  `service_heading` varchar(400) NOT NULL,
  `service_short_description` text NOT NULL,
  `service_description` text NOT NULL,
  `service_link` varchar(400) NOT NULL,
  `service_link_video` int(1) NOT NULL,
  `service_archived` int(11) NOT NULL DEFAULT '0',
  `service_quantity` int(11) DEFAULT NULL,
  `service_code` varchar(100) DEFAULT NULL,
  `service_offset` int(11) DEFAULT NULL,
  `service_featured` int(11) DEFAULT '0',
  `service_hidden_on_site` int(11) DEFAULT '0',
  `service_sort` int(99) DEFAULT NULL,
  `service_colour` varchar(200) DEFAULT NULL,
  `service_anchor` varchar(400) DEFAULT NULL,
  `service_include` text,
  `service_align` varchar(100) DEFAULT NULL,
  `service_slug` varchar(400) DEFAULT NULL,
  `service_author` varchar(200) DEFAULT NULL,
  `service_special` int(11) DEFAULT '0',
  `service_in_the_news` int(11) DEFAULT '0',
  `service_latest_service` int(11) DEFAULT '0',
  `service_latest_article` int(11) DEFAULT '0',
  `service_latest_article_1` int(11) DEFAULT '0',
  `service_latest_article_2` int(11) DEFAULT '0',
  `service_latest_article_3` int(11) DEFAULT '0',
  `service_json` text,
  PRIMARY KEY (`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_category`;

CREATE TABLE `mod_service_category` (
  `service_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_files_id` int(11) DEFAULT NULL,
  `service_category_timestamp` int(11) NOT NULL,
  `service_category_date` int(11) NOT NULL,
  `service_category_price` double DEFAULT NULL,
  `service_category_category_id` int(11) DEFAULT NULL,
  `service_category_heading` varchar(400) NOT NULL,
  `service_category_short_description` text NOT NULL,
  `service_category_description` text NOT NULL,
  `service_category_link` varchar(400) NOT NULL,
  `service_category_link_video` int(1) NOT NULL,
  `service_category_archived` int(11) NOT NULL DEFAULT '0',
  `service_category_quantity` int(11) DEFAULT NULL,
  `service_category_code` varchar(100) DEFAULT NULL,
  `service_category_offset` int(11) DEFAULT NULL,
  `service_category_featured` int(11) DEFAULT '0',
  `service_category_hidden_on_site` int(11) DEFAULT '0',
  `service_category_sort` int(99) DEFAULT NULL,
  `service_category_colour` varchar(200) DEFAULT NULL,
  `service_category_anchor` varchar(400) DEFAULT NULL,
  `service_category_include` text,
  `service_category_align` varchar(100) DEFAULT NULL,
  `service_category_slug` varchar(400) DEFAULT NULL,
  `service_category_author` varchar(200) DEFAULT NULL,
  `service_category_special` int(11) DEFAULT '0',
  `service_category_in_the_news` int(11) DEFAULT '0',
  `service_category_latest_page` int(11) DEFAULT '0',
  `service_category_latest_article` int(11) DEFAULT '0',
  `service_category_latest_article_1` int(11) DEFAULT '0',
  `service_category_latest_article_2` int(11) DEFAULT '0',
  `service_category_latest_article_3` int(11) DEFAULT '0',
  `service_category_json` text,
  PRIMARY KEY (`service_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_document_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_document_link`;

CREATE TABLE `mod_service_document_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table mod_service_image_link
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mod_service_image_link`;

CREATE TABLE `mod_service_image_link` (
  `service_media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `media_files_id` int(11) NOT NULL,
  `service_media_link_sort` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`service_media_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
